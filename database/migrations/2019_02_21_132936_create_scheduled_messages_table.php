<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',190);
            $table->text('text');
            $table->unsignedTinyInteger('gender')->default(0);
            $table->unsignedTinyInteger('type')->default(0);
            $table->enum('period',['daily','weekly','monthly','yearly'])->default('monthly');
            $table->unsignedTinyInteger('is_enabled')->default(1);
            $table->timestamp('last_update')->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_messages');
    }
}
