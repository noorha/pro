!function(a){"use strict";

var b=a("#hosturl").val();
    a(".add-to-favorite").on("click",function(c)
    {var d=a(this);if(c.preventDefault(),!d.hasClass("added"))
        {var e=d.find("i"),f=d.find("span"),
            g=a.ajax({url:b+"/teacher/favourite",
                method:"POST",
                data:{teacher_id:a(this).attr('data-id')},
                dataType:"json"
                ,beforeSend:function(a)
                {e.addClass("fa-spin")}});g.done(function(a)
                {e.removeClass("fa-spin"),
                    "success"==a.status?(d.addClass("added"),
                    e.removeClass("fa-star-o").addClass("fa-star"),f.removeClass("failed"),
                    f.html(a.message)):(f.addClass("failed"),f.html(a.message))}),
                    g.fail(function(a,b){alert("حدث خطأ: "+b)})}})
                    a(".remove-from-favorite").on("click",function(c){c.preventDefault();
                        var d=a(this),e=d.closest(".col-grid-post"),f=d.siblings(".loader"),
                        g=a.ajax({url:b+"/teacher/removefavourite",
                            type:"POST",
                            data:{teacher_id:d.attr("data-id")},
                            dataType:"json",
                            beforeSend:function(a){d.hide(),f.css("display","block")}});
                            g.done(function(b){if("success"==b.status){e.remove();
                                var c=a("#resultvalue").html(),d=c-1;a("#resultvalue").html(d)}
                                else f.hide(),alert(b.message)}),g.fail(function(a,b){alert("حدث خطأ: "+b)})})}(jQuery);

