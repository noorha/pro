var locale = window.lang;
var privacy_text = 'لم تتم الموافقة على سياسة الخصوصية وشروط الإستخدام';
if (locale === 'en'){
    privacy_text = 'Accept Privacy And Policy Term Please !'
}
var validator = jQuery("#form").validate({
    success: function (element) {
        $(element).each(function () {
            $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).remove();
        });
    },
    errorPlacement: function (error, element) {
        var elem = $(element);
        if (elem.hasClass(".selectpicker")) {
            error.appendTo(element.parent());
            element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
        } else {
            error.insertAfter(element);
            element.removeClass('has-success').addClass('has-error');
        }
        error.css('color', 'red');
    },
    submitHandler: function (f, e) {
        e.preventDefault();
        if (!$('#accept_privacy').is(":checked")) {
            customSweetAlert(
                'info',
                privacy_text,
                ''
            )
        } else {
            $('.upload-spinn').removeClass('hidden');
            var formData = new FormData($('#form')[0]);
            var url = $('#form').attr('action');
            var to = $('#form').attr('to');
            // if (objects !== undefined) {
            //     formData.append('times', objects);
            // }
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                type: 'json',
                success: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                afterSuccess(event);
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (jqXhr) {
                    $('.upload-spinn').addClass('hidden');
                    getErrors(jqXhr, '/'+locale+'/login');
                }
            });
        }
    }
});