const player = new Plyr('#player');
var element_position = $('#scroll-to').offset().top;
var screen_height = $(window).height();
var activation_offset = 0.5;//determines how far up the the page the element needs to be before triggering the function
var activation_point = element_position - (screen_height * activation_offset);
var max_scroll_height = $('body').height() - screen_height - 5;//-5 for a little bit of buffer

var is_count = false;
//Does something when user scrolls to it OR
//Does it when user has reached the bottom of the page and hasn't triggered the function yet
$(window).on('scroll', function () {
    var y_scroll_pos = window.pageYOffset;

    var element_in_view = y_scroll_pos > activation_point;
    var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;

    if ((element_in_view || has_reached_bottom_of_page) && !is_count) {
        is_count = true;
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    }
});

$('.sub_specialty_main').selectToAutocomplete({});
$('#specialty_main').selectToAutocomplete({});
$('#speciality').on('change', function (event) {
    event.preventDefault();
    var id = $('#speciality').val();
    updateItems(id);
});

function updateItems(id) {
    $('#loader').removeClass('hidden');
    $('#container').addClass('hidden');
    var url = '/' + window.lang + '/sub_speciality/' + id;
    $.ajax({
        type: 'GET',
        url: url
    }).done(function (response) {
        $('#container').removeClass('hidden');
        $('#loader').addClass('hidden');
        $('#items').html(response.item);
    });
}

$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
       autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    rtl: true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 4
        },
        1000: {
            items: 6
        }
    }
});

$('.owl-carousel3').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
      autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    rtl: true,
    navText : ["<i class='fa fa-chevron-left new_icon'></i>","<i class='fa fa-chevron-right new_icon'></i>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$('.owl-carousel2').owlCarousel({
    loop: true,
    margin: 10,
//    nav:true,
    dots: true,
      autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    rtl: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});



(function($) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');
    
    $.extend(Defaults.defaults, {
        searchInputPlaceholder: ''
    });
    
    var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');
    
    var _renderSearchDropdown = SearchDropdown.prototype.render;
    
    SearchDropdown.prototype.render = function(decorated) {
     
      // invoke parent method
      var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));
      
      this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));
      
      return $rendered;
    };
    
  })(window.jQuery);

$('.select2').select2({
    placeholder: window.lang === 'ar' ? "اختر الموضوع" : "Select Topic" ,
    searchInputPlaceholder:  window.lang === 'ar' ? "اختر الموضوع" : "Select Topic",
    allowClear: true,
    dir: window.lang === 'ar' ? "rtl" : "ltr",
    dropdownPosition: 'below',
    language: {
        "noResults": function () {
            return   window.lang === 'ar' ? "لا توجد نتائج" : "no Results"  ;
        }
    }
});
