//var iScrollPos = 0;
//  var $document = $(document);
//  $(window).scroll(function () {
//      var iCurScrollPos = $(this).scrollTop();
//      if ($document.scrollTop() >= 20) {
//        $('.header').removeClass('wow');
//        $('.header').removeClass('fadeIn');
//        $('.header').addClass('fixedsw');
//        if (iCurScrollPos > iScrollPos) {
//            $('.header').removeClass('fixed');
//        } else {
//            $('.header').addClass('fixed');
//            $('.header').addClass('fixedsw');
//        }
//      }else{
//        $('.header').removeClass('fixedsw');
//        $('.header').removeClass('fixed');
//      }
//      iScrollPos = iCurScrollPos;
//  });
$(window).scroll(function () {
    if ($(window).scrollTop() >= 100) {
        $('.new-sec-io').addClass('fixed-header');
    } else {
        $('.new-sec-io').removeClass('fixed-header');
    }
});
$(document).ready(function () {
    $('#logout').on('click', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'POST',
            success: function (response) {
                window.location = '/' + window.lang;
            }
        });
    });
});

$(document).ready(function () {
    $('.f_avila .checbosx input[type="checkbox"]').click(function () {
        // alert('asd');
        if ($(this).is(':checked')) {
            $(this).parent().parent().addClass('active');
        } else {
            $(this).parent().parent().removeClass('active');
        }
    });
});
$(document).ready(function () {
    $(document).on("click", ".remo_thoisw a", function () {
        // alert('asd');
        $(this).parent().parent().remove();
    });
});
// $(document).ready(function () {
//     $('.item_avail p').click(function () {
//         var item = "<div class='item_avail1f'><div class='inputsws bgw nopadrights'><input type='text' name='' class='datetimepickers' placeholder='الساعة'></div><div class='inputsws bgw nopadrights'><input type='text' name='' class='datetimepickers' placeholder='الساعة'></div></div>";
//         $(this).parent().find('.item_avail1').append(item);
//         $('.datetimepickers').datetimepicker({
//             // debug:true
//             format: 'LT'
//         });
//     });
// });
$(document).ready(function () {
    $('.item_taps>ul>li a').click(function () {
        var val = $(this).data('id');
        $('.item_taps>ul').find('a').removeClass('active');
        $('.items_taps').find('.tabs_itemsw').removeClass('active');
        $('.items_taps').find('#tsp_' + val).addClass('active');
        $(this).addClass('active');
    });
});
$(document).ready(function () {
    $('.trin_index>ul>li a').click(function () {
        var val = $(this).data('id');
        $('.trin_index>ul').find('a').removeClass('active');
        $('.list_in').find('.list1 ').removeClass('active');
        $('.list_in').find('#list_index_' + val).addClass('active');
        $(this).addClass('active');

    });
});
$(document).ready(function () {
    $('.tabos_modals>ul>li a').click(function () {
        var val = $(this).data('id');
        $('.tabos_modals>ul').find('a').removeClass('active');
        $('.div_mosw').find('.list1 ').removeClass('active');
        $('.div_mosw').find('.itemspw ').removeClass('active');
        $('.div_mosw').find('#itemswaw_0' + val).addClass('active');
        $(this).addClass('active');
    });
});
$(document).ready(function () {
    $('.tab_req>li a').click(function () {
        var val = $(this).data('id');
        $('.tab_req').find('a').removeClass('active');
        $('.f_rig_request1').find('.ite_tabe').removeClass('active');
        $('.f_rig_request1').find('#reque_0' + val).addClass('active');
        $(this).addClass('active');
    });
});

$(document).ready(function () {
    $('.flistrigs>ul>li a').click(function () {
        var val = $(this).data('id');
        $('.flistrigs>ul').find('a').removeClass('active');
        $('.list_tow_righst').find('.flistrigs2').removeClass('active');
        $('.list_tow_righst').find('#register2_' + val).addClass('active');
        $(this).addClass('active');
    });
});

$(document).ready(function () {
    $('.list_all_tabs_register>ul>li a').click(function () {
        var val = $(this).data('id');
        var vals = $('.list_all_tabs_register').data('id');
        var itsp = 100 / vals;
        var vao = val * itsp;
        if (!$(this).hasClass('afull')) {
            if (val <= vals) {
                $('.procssss').find('span span').css('width', vao + '%');
            } else {
                // alert('by');
            }
        }
        $('.list_all_tabs_register>ul').find('a.active').addClass('afull');
        $('.list_all_tabs_register>ul').find('a').removeClass('active');
        $('.se0').find('.hobsts').removeClass('active');
        $('.se0').find('#sregis_' + val).addClass('active');
        $(this).addClass('active');

    });
});

$(document).ready(function () {
    $('.inputswts button').click(function (event) {
        event.preventDefault();
        var val = $(this).data('id');
        var bl = $(this).data('bl');
        var $valid = $("#form").valid();
        if (!$valid) {
            validator.focusInvalid();
            return false;
        }
        if (val === 4) {
            var itemsCount = $('input.specialties:checked').length;
            if (itemsCount <= 0) {
                validator.focusInvalid();
                customSweetAlert('warning', '', window.lang === 'ar' ? 'الرجاء تحديد تخصص فرعي واحد على الأقل' : 'Select one Specialty at least');
                return false;
            }
        }
        if (val === 5) {
            var daysCount = $('input.days-select:checked').length;
            if (daysCount === 0) {
                validator.focusInvalid();
                customSweetAlert('warning', '', window.lang === 'ar' ? 'الرجاء تحديد يوم واحد على الأقل' : 'Select at least one day ');
                return false;
            }
        }
        if (val === 6) {
            var trainingCount = $('input.training-type-cls:checked').length;
            if (trainingCount === 0) {
                validator.focusInvalid();
                customSweetAlert('warning', '', window.lang === 'ar' ? 'الرجاء تحديد نوع التدريب' : 'Select Training Type');
                return false;
            }
        }
        var vals = $('.list_all_tabs_register').data('id');
        var itsp = 100 / vals;
        var vao = val * itsp;
        if (!$(this).hasClass('afull')) {
            if (val <= vals) {
                $('.procssss').find('span span').css('width', vao + '%');
            } else {
                if (bl !== 4) {
                    $('#form').submit();
                }
            }
        }

        if (val != 'last') {
            $('.list_all_tabs_register>ul').find('a.active').addClass('afull');
            $('.list_all_tabs_register>ul').find('a').removeClass('active');
            $('.list_all_tabs_register>ul').find('#reis_menu_' + val).addClass('active');

            $('.se0').find('.hobsts').removeClass('active');
            $('.se0').find('#sregis_' + val).addClass('active');
        }
    });
});

$(document).ready(function () {
    $('.availablil>ul>li>a').click(function () {
        var val = $(this).data('id');

        $('.availablil>ul').find('a').removeClass('active');
        $(this).addClass('active');

        $('#laskd').find('.tabosw_ds').removeClass('active');
        $('#laskd').find('#tab_dsh_' + val).addClass('active');
    });
});

$(document).ready(function () {
    $('.sumonsws .chexbox input').change(function () {
        if ($(this).attr("checked", "checked")) {
            var item = $(this).val();
            $('.sumonsws').find('.asdiow').removeClass('active');
            $('.sumonsws').find('#asdiow_' + item).addClass('active');
        } else {
            $('.sumonsws').find('.asdiow').removeClass('active');
        }
    });
});

$(document).ready(function () {
    $('.chx').change(function () {
        var item = $(this).val();
        var element = $('#asdiow_' + item);
        if ($(this).is(":checked")) {
            element.addClass('active');
        } else {
            element.removeClass('active');
        }
    });
});


$(document).ready(function () {
    $('#country_id').on('change', function (event) {
        $.ajax({
            url: '/' + window.lang + '/cities/' + $('#country_id').val(),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    $('#cities_select').html(response.item);
                    $('.selectpicker').selectpicker('render');

                }
            },
            error: function (jqXhr) {
                // $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/' + window.lang + '/login');
            }
        });
    });
});

$(document).ready(function () {
    var mailing = $('#mailing_form');
    mailing.validate({
        success: function (element) {
            $(element).each(function () {
                $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                $(this).remove();
            });
        },
        errorPlacement: function (error, element) {
            var elem = $(element);
            if (elem.hasClass("selectpicker")) {
                error.appendTo(element.parent());
                element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
            } else {
                error.insertAfter(element);
                element.removeClass('has-success').addClass('has-error');
            }
            error.css('color', 'red');
        },
        submitHandler: function (f, e) {
            e.preventDefault();
            $('.upload-spinn').removeClass('hidden');
            var formData = new FormData(mailing[0]);
            var url = mailing.attr('action');
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                type: 'json',
                success: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                mailing.find("input[type=text],input[type=email], textarea").val("");
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (jqXhr) {
                    $('.upload-spinn').addClass('hidden');
                    getErrors(jqXhr, '/' + window.lang + '/login');
                }
            });
        }
    });
});

$(document).ready(function () {
    $('.tiomso input').change(function () {
        var item = parseInt($(this).val());
        var sum = item * parseInt('5');
        $(this).parent().find('label').find('span').html(item * 5);
    });
});
$(document).ready(function () {
    $('.itemmasp').change(function () {
        var item = $(this).val();
        if (item == 0) {
            $('#exampleModal2').modal('show');
        }
    });
});
$(document).ready(function () {
    $('.file input').click(function () {
        $(this).parent().find('label').click();
    });
});


/*use*/

$(document).ready(function () {
    function readURLs(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.acsw').before("<li class='uploadded'><img src='" + e.target.result + "'><a href='" + e.target.result + "' data-rel='lightcase'><i class='icon-search'></i></a><a data-toggle='modal' data-target='#exampleModal'><i class='icon-cross-symbol'></i></a></li>");
                // $('.upload_multi_images ul').find('.acsw').removeClass('acsw');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#multi_upload").change(function () {
        $(this).parent().addClass('acsw');
        readURLs(this);
    });
});

$(document).ready(function () {
    $('.tabs_prof>ul>li a').click(function () {
        var val = $(this).data('id');
        $('.tabs_prof>ul').find('a').removeClass('active');
        $('.tabs_prof').find('.main_tab1').removeClass('active');
        $('.tabs_prof').find('#tabs' + val).addClass('active');
        $(this).addClass('active');
    });
});
$(document).ready(function () {
    $('.add_imagespkldwsw').click(function () {
        $('.tabs_prof').find('.main_tab1').removeClass('active');
        $('.add_imgaspkldw').addClass('active');
    });
    $('.col-sdlasw').click(function () {
        var val = $('.tabs_prof ul').find('.active').data('id');
        $('.main_tab1').removeClass('active');
        $('.tabs_prof').find('#tabs' + val).addClass('active');
        $(this).removeClass('thisopen');
    });
});
$(document).ready(function () {
    $('.studendtext_inprofe a').click(function () {
        var it = $(this).hasClass('thisopen');
        if (it) {
            var val = $('.tabs_prof ul').find('.active').data('id');
            $('.main_tab1').removeClass('active');
            $('.tabs_prof').find('#tabs' + val).addClass('active');
            $(this).removeClass('thisopen');
        } else {
            $('.main_tab1').removeClass('active');
            $('.contactusodw').addClass('active');
            $(this).addClass('thisopen');
        }
    });
    $('.thisopen').click(function () {
        $('.main_tab1').removeClass('active');
        $('.contactusodw').addClass('active');
        $(this).addClass('thisopen');
    });
    $('.contactusodw button').click(function () {
        var val = $('.tabs_prof ul').find('.active').data('id');
        $('.main_tab1').removeClass('active');
        $('.tabs_prof').find('#tabs' + val).addClass('active');
        $('.studendtext_inprofe a').removeClass('thisopen');
    });
});
if (window.lang === 'ar') {

    $(document).ready(function () {
        $(".btn-menu").click(function () {
            $(".menu-side").css({'left': '0'});
            $(".btn-menu-close").css({'left': '0px'});
            $(".btn-menu").css({'display': 'none'});
            $(".back-menu").css({'display': 'block'});
        });

        $(".btnmenu").click(function () {
            $(".body").addClass('active');
            $(".menu_sidebar").addClass('active');
            $(".body_bg").css({'display': 'block'});
        });

        $(".body_bg").click(function () {
            $(".menu_sidebar").removeClass('active');
            $(".body").removeClass('active');
            $(".colse").css({'display': 'none'});
            $(".body_bg").css({'display': 'none'});
            $(".btnmenu").css({'display': 'block'});
        });

        $(".btn-menu").click(function () {
            $(".menu-side").css({'left': '0'});
            $(".btn-menu-close").css({'left': '0px'});
            $(".btn-menu").css({'display': 'none'});
            $(".back-menu").css({'display': 'block'});
        });

        function closeMenu() {
            $(".menu-side").css({'left': '-350px'});
            $(".btn-menu-close").css({'left': '-10px'});
            $(".btn-menu").css({'display': 'block'});
            $(".back-menu").css({'display': 'none'});
        }

        $(".btn-menu-close").click(function () {
            closeMenu();
        });
        $(".back-menu").click(function () {
            closeMenu();
        });


    });
} else {
    $(document).ready(function () {
        $(".btn-menu").click(function () {
            $(".menu-side").css({'right': '0'});
            $(".btn-menu-close").css({'right': '0px'});
            $(".btn-menu").css({'display': 'none'});
            $(".back-menu").css({'display': 'block'});
        });

        $(".btnmenu").click(function () {
            $(".body").addClass('active');
            $(".menu_sidebar").addClass('active');
            $(".body_bg").css({'display': 'block'});
        });

        $(".body_bg").click(function () {
            $(".menu_sidebar").removeClass('active');
            $(".body").removeClass('active');
            $(".colse").css({'display': 'none'});
            $(".body_bg").css({'display': 'none'});
            $(".btnmenu").css({'display': 'block'});
        });

        $(".btn-menu").click(function () {
            $(".menu-side").css({'right': '0'});
            $(".btn-menu-close").css({'right': '0px'});
            $(".btn-menu").css({'display': 'none'});
            $(".back-menu").css({'display': 'block'});
        });

        function closeMenu() {
            $(".menu-side").css({'right': '-350px'});
            $(".btn-menu-close").css({'right': '-10px'});
            $(".btn-menu").css({'display': 'block'});
            $(".back-menu").css({'display': 'none'});
        }

        $(".btn-menu-close").click(function () {
            closeMenu();
        });
        $(".back-menu").click(function () {
            closeMenu();
        });


    });

}

$(document).ready(function () {
    $('#notify_btn').on('click', function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        $('#notify_btn').children('span').addClass('hidden');
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                if (!response.status) {
                    $('#notify_btn').children('span').removeClass('hidden');
                }
            },
            error: function () {
                $('#notify_btn').children('span').removeClass('hidden');
            }
        });
    });
});


$('.header-2 a.maiswpow.profsiw').click(function () {
    $('.header-2 .dropmenus').toggleClass('active');
    $('.notif-ui .dropdown').removeClass('open')
})

$('.header-2 .dropdown-toggle').click(function () {
    // $('.notif-ui .dropdown-menu').toggleClass('active')  
    $('.header-2 .dropmenus').removeClass('active')
});

$(document).on('click', '.closed-notify', function (e) {
    e.preventDefault();
    $(this).parent().fadeOut();
    $('#menu_side').removeClass('t-80');
    $('#new_sec_io').removeClass('mt-80');
    $('#header_section_owl').removeClass('mt-80');
});
