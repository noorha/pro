jQuery("#form").validate({
    rules: {
        password: {
            minlength: 6
        },
        password_confirmation: {
            minlength: 6,
            equalTo: "#password"
        }
    },
    success: function (element) {
        $(element).each(function () {
            $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
            $(this).remove();
        });
    },
    errorPlacement: function (error, element) {
        var elem = $(element);
        if (elem.hasClass("selectpicker")) {
            error.appendTo(element.parent());
            element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
        } else {
            error.insertAfter(element);
            element.removeClass('has-success').addClass('has-error');
        }
        error.css('color', 'red');
    },
    submitHandler: function (f, e) {
        e.preventDefault();
        $('.upload-spinn').removeClass('hidden');
        var formData = new FormData($('#form')[0]);
        var url = $('#form').attr('action');
        var to = $('#form').attr('to');
        $.ajax({
            url: url,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            type: 'json',
            success: function (response) {
                $('.upload-spinn').addClass('hidden');
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            afterSuccess(event);
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (jqXhr) {
                $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/' + window.lang + '/login');
            }
        });
    }
});