var request_id = 0;

$('#status').on('change', function (event) {
    event.preventDefault();
    updateItems();
});

function onClickModal(id) {
    $(document).on('click', '.' + id + '_btn', function (event) {
        request_id = $(this).parent().parent().data('id');
        $('#amount').val($(this).data('id'));
        $('#' + id).modal('show');
    });
}

onClickModal('accept_modal');

onClickModal('reject_modal');

function afterSuccess() {
    $('.modal').modal('hide');
    updateItems();
}

$(document).ready(function () {
    $(document).on('click', '.new-box-v', function () {
        if ($(this).parent().hasClass('active')) {
            $('.all_fstnotif').find('.stnotif').removeClass('active');
        } else {
            $('.all_fstnotif').find('.stnotif').removeClass('active');
            $(this).parent().addClass('active');
        }

//        $('html,body').animate({
//                scrollTop: $(this).parent().offset().top
//            },
//            'slow');
    });
});

$('#accept_btn').on('click', function (event) {
    $('.upload-spinn').removeClass('hidden');

    event.preventDefault();
    $.ajax({
        url: '/' + window.lang + '/student/training/approved/' + request_id,
        method: 'POST',
        processData: false,
        contentType: false,
        type: 'json',
        success: function (response) {
            $('.upload-spinn').addClass('hidden');
            if (response.status) {
                customSweetAlert(
                    'success',
                    response.message,
                    response.item,
                    function (event) {
                        afterSuccess(event);
                    }
                );
            } else {
                swal({
                    title: '<span class="error">' + response.message + '</span>',
                    type: 'error',
                    html: response.errors_object,
                    confirmButtonText: (window.lang === 'ar'? ' إدفع مباشرة ' : 'Pay ') + $('#amount').val() + ' $ ',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function () {
                    $('#paypal_form').submit();
                });

            }
        },
        error: function (jqXhr) {
            $('.upload-spinn').addClass('hidden');
            getErrors(jqXhr, '/' + window.lang + '/login');
        }
    });
});

var reject_form = $('#form');
$('#reject_btn').on('click', function (event) {
    event.preventDefault();
    reject_form.attr('action', '/' + window.lang + '/student/training/reject/' + request_id);
    reject_form.submit();
});
$(document).on('click', '.pagination a', function (e) {
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    updateItems(page);
});

function updateItems(page) {
    $('#loader').removeClass('hidden');
    $('#container').addClass('hidden');
    var url = searchURL(page);
    $.ajax({
        type: 'GET',
        url: url
    }).done(function (response) {
        $('#container').removeClass('hidden');
        $('#loader').addClass('hidden');
        $('#items').html(response.item);
    });
}

function searchURL(page) {
    var page_number = page || 1;
    return document.URL + '?page=' + page_number + '&status=' + $('#status').val();
}

var current_url = $('#confirm_form').attr('action');
newForm('confirm_form', 'confirm_modal');
$(document).on('click', '.confirm-btn-pg', function (event) {
    current_url = $(this).data('url');
});

function newForm(id, modal_id) {
    jQuery("#" + id).validate({
        success: function (element) {
            $(element).each(function () {
                $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
                $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
                $(this).remove();
            });
        },
        errorPlacement: function (error, element) {
            var elem = $(element);
            if (elem.hasClass("selectpicker")) {
                error.appendTo(element.parent());
                element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
            } else {
                error.insertAfter(element);
                element.removeClass('has-success').addClass('has-error');
            }
            error.css('color', 'red');
        },
        submitHandler: function (f, e) {
            e.preventDefault();
            $('.upload-spinn').removeClass('hidden');
            var formData = new FormData($('#' + id)[0]);
            var url = current_url;
            var to = $('#' + id).attr('to');
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                type: 'json',
                success: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    if (response.status) {
                        updateItems();
                        customSweetAlert(
                            'success',
                            response.message,
                            '',
                            function (event) {
                                $('#' + modal_id).modal('hide');
                                $('.modal-backdrop').hide();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (jqXhr) {
                    $('.upload-spinn').addClass('hidden');
                    getErrors(jqXhr, '/login');
                }
            });
        }
    });
}