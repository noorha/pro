window.discount_value = 0;
$('#hour_no').on('keyup', function (event) {
    updateTotal()
});

$('#type').on('change', function (event) {
    updateTotal();
});

$('#payment_method').on('change', function (event) {
    if ($(this).val() === 'website') {
        $('#coupon_main_cont').removeClass('hidden');
    } else {
        $('#coupon_main_cont').addClass('hidden');
    }
});
var discount_container = $('#discount_container');

$('#promo_code_btn').on('click', function (event) {
    var code = $('#promo_code').val();
    var url = $(this).data('url');
    $.ajax({
        url: url,
        method: 'GET',
        data: {code: code},
        type: 'json',
        success: function (response) {
            if (response.status) {
                window.discount_value = response.item.discount;
                updateTotal();
                discount_container.removeClass('hidden');
                $('#coupon_code').val(response.item.code);
                $('#coupon_status').html('<i style="margin-top: 10px;color: #3da23dbf;font-size: 22px;" class="fa fa-check"></i>');
                updateTotal();
            } else {
                window.discount_value = 0;
                $('#coupon_code').val('');
                discount_container.addClass('hidden');
                $('#coupon_status').html('<i style="margin-top: 10px;color: #fc0019;font-size: 22px;" class="fa fa-remove"></i>');
            }
        },
        error: function (response) {
            errorCustomSweet();
        }
    });
});
$('#coupon_btn').on('click', function (event) {
    event.preventDefault();
    $(this).parent('span').siblings('.inputsws').fadeIn();
});


function updateTotal() {
    var type = $('#type').val();
    var price = 0;
    if (type === '1') {
        price = online_price * ($('#hour_no').val());
    } else {
        price = interview_price * ($('#hour_no').val());
    }
    var value_after_discount = ((100 - window.discount_value) * price) / 100;
    $('#hour_no_total').text((window.lang === 'ar') ? 'السعر الإجمالي ' + price + '  ' + window.currency_txt : 'Total Price ' + price + window.currency_txt);
    $('#total').val(price);
    $('#value_after_discount').html(value_after_discount);
    console.log(window.discount_value.toFixed(2));
}

$('#submit_btn').click(function (event) {
    event.preventDefault();
    if (isUploaded) {
        $('#form').submit();
    } else {
        customSweetAlert('warning', (window.lang === 'ar') ? 'توجد ملفات قيد الرفع' : 'Please Wait Files Until Finish Uploading');
    }
});


var files = [];
var i = 0;

function addFile(file, res) {
    files [i] = res.id;
    i++;
    updateFiles();
}

function updateFiles() {
    $('#files').val(files);
}

// function removeFile(file) {
//     name = file.name;
//     if (files !== undefined && files.length > 0){
//         file.forEach(function (currentValue, index, arr) {
//             if (name === currentValue.display_name) {
//                 file.splice(index, 1);
//             }
//         });
//     }
// }

var isUploaded = true;

var element = $('#files');
var myDropzone;
Dropzone.options.imageUpload = {
    maxFilesizee: 20,
    acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf,.docx,.doc,.xls,.zip,.rar",
    previewsContainer: '#image-upload',
    uploadMultiple: false,
    parallelUploads: 100,
    addRemoveLinks: true,
    dictremoveFile: 'Remove',
    init: function () {
        myDropzone = this;
        var form = $("#image-upload");
        this.on("success", function (file, res) {
            addFile(file, res);
        });
        this.on("complete", function (res) {
            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                isUploaded = true;
            }

        });
        this.on("processing", function (res) {
            isUploaded = false;
        });
        this.on("removedfile", function (file) {
            removeFile(file);
            var name = file.previewElement.id;
            if (name === '') {
                name = file.name;
            }
            $.ajax({
                method: "POST",
                url: '/' + window.lang + '/image_delete',
                data: {id: name},
                dataType: 'json',
                success: function (data) {
                }
            });
        });
    }
};
(function ($) {

    $(function () {
        // $('#sub_specialty').selectToAutocomplete({'copy-attributes-to-text-field': true});
        $('#teacher').selectToAutocomplete({'copy-attributes-to-text-field': true});
    });
})(jQuery);
