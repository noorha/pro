(function ($) {
    $(function () {
        $('#sub_specialty').selectToAutocomplete({'copy-attributes-to-text-field': true});
    });
})(jQuery);
var loaded = false;

$(document).ready(function () {
    $('.datetimepicker').datetimepicker({
        // debug:true
    });
    $("#price_range").slider({id: "slider12c", min: 0, step: 5, max: window.max_range, range: true, value: [0, window.max_range]})
        .on('slideStop', function (event) {
            event.preventDefault();
            updateItems();
        });

    // $("#age_range").slider({id: "slider12c", min: 10, step: 5, max: 90, range: true, value: [10, 90]})
    //     .on('slideStop', function (event) {
    //         event.preventDefault();
    //         updateItems();
    //     });


    $('.rate').click(function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        location.href = '/' + window.lang + '/teacher/ratings/' + id
    })
});

$('#teacher').on('keyup', function (event) {
    let code = (event.keyCode ? event.keyCode : event.which);
    if (code === 13 || code === 32 || $('#teacher').val() === '') {
        updateItems();
    }
});

$('.on-change').on('change', function (event) {
    event.preventDefault();
    updateItems();
});

$('#specialities').on('change', function (event) {
    event.preventDefault();
    updateItems();
});
$('#sub_specialty').on('change', function (event) {
    event.preventDefault();
    let specialty_id = $('#sub_specialty').find(":selected").data('specialty');
    $('#specialities').val(specialty_id);
    $('.selectpicker').selectpicker('refresh');
    updateItems();
});


$('#country').on('change', function (event) {
    event.preventDefault();
    updateItems();
    if (($(this).val() !== undefined) && $(this).val() !== '') {
        $.ajax({
            url: '/' + window.lang + '/cities/' + $(this).val(),
            type: 'GET',
            success: function (response) {
                $('#city_div').html(response.item);
                $('.selectpicker').selectpicker();
            }
        });
    } else  {
        $('#city_id').val('0');
        $('#city_id').find('option:not(:first)').remove();
        $('.selectpicker').selectpicker('refresh');

    }

});

$(document).on('change', '#city_id', function (event) {
    event.preventDefault();
    updateItems();
});


$(document).on('click', '.pagination a', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 });
    var page = $(this).attr('href').split('page=')[1];
    updateItems(page);
});


$('.days').on('change', function (event) {
    event.preventDefault();
    updateItems();
});
$('.type').on('change', function (event) {
    event.preventDefault();
    updateItems();
});
$('.gender').on('change', function (event) {
    event.preventDefault();
    updateItems();
});

$('.search').on('click', function (event) {
    event.preventDefault();
    updateItems();
});

var form = $('#form');

function updateItems(page) {
    if (loaded) {
        $('#loader').removeClass('hidden');
        $('.cont-oi').addClass('hidden');
        var page_number = page || 1;
        $.ajax({
            url: form.attr('action') + '?page=' + page_number,
            type: form.attr('method'),
            data: form.serialize(),
            success: function (response) {
                $('.cont-oi').removeClass('hidden');
                $('#loader').addClass('hidden');
                $('#items').html(response.item);
            }
        });
    }

    return false;
}


jQuery(window).load(function () {
    let specialty_id = $('#sub_specialty').find(":selected").data('specialty');
    if (specialty_id !== undefined) {
        $('#specialities').val(specialty_id);
    }
    if (window.specialities !== undefined && window.specialities !== '') {
        $('#specialities').val(window.specialities);
    }
    $('.selectpicker').selectpicker('refresh');

    loaded = true;
});