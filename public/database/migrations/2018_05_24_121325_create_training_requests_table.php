<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('sub_id')->unsigned();
            $table->foreign('sub_id')->references('id')->on('sub_specialties')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('qualification_id')->unsigned();
            $table->foreign('qualification_id')->references('id')->on('academic_qualifications')->onUpdate('cascade')->onDelete('cascade');
            $table->enum('status',['pending','accepted','rejected']);
            $table->enum('type',[1,2]);
            $table->integer('hour_no');
            $table->float('total');
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_requests');
    }
}
