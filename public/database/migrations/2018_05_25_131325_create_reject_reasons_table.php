<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reject_reasons', function (Blueprint $table) {
            $table->integer('request_id')->unsigned();
            $table->foreign('request_id')->references('id')->on('training_requests')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('reason_id')->unsigned();
            $table->foreign('reason_id')->references('id')->on('reject_training_reasons')->onUpdate('cascade')->onDelete('cascade');
            $table->text('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reject_reasons');
    }
}
