<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnHoldCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('on_hold_cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status',['pending','progress','done']);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('withdraw_process_id')->unsigned();
            $table->foreign('withdraw_process_id')->references('id')->on('financial_processes')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('credit_process_id')->unsigned();
            $table->foreign('credit_process_id')->references('id')->on('financial_processes')->onUpdate('cascade')->onDelete('cascade');
            $table->double('value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('on_hold_cashes');
    }
}
