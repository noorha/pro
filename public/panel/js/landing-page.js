jQuery(document).ready(function () {
    onChange('about_photo');
    onChange('certification');
    onChange('slider_photo');
    onChange('info_section_photo');
    onChange('content_section_photo');

    function onChange(element) {
        $('#' + element + '_fileupload').change(function () {
            previewURL(this);
            if ($(this).val() !== '') {
                var progress = $('.jasny_progress');
                var percent = $('.jasny_percent');
                var formData = new FormData();
                formData.append('image', $(this)[0].files[0]);
                $.ajax({
                    url: '/ar/image/upload',
                    type: 'POST',
                    data: formData,
                    beforeSend: function (xhr) {
                        isPhotoUploaded = false;
                        progress.removeClass('hidden');
                        percent.css("width", '0%');
                        percent.attr('aria-valuenow', '0');
                        percent.html('0%');
                    },
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                percent.css("width", percentComplete + '%');
                                percent.attr('aria-valuenow', percentComplete);
                                percent.html(percentComplete + '%');
                                if (percentComplete === 100) {
                                    setTimeout(function () {
                                        progress.addClass('hidden');
                                    }, 500);
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    success: function (res) {
                        if (res.status) {
                            isPhotoUploaded = true;
                            if (res.file_name !== undefined && res.file_name !== '') {
                                $('#' + element).val(res.file_name);
                            }
                        } else {
                            swal(
                                'حدث خطأ أثناء رفع الصورة',
                                res.message,
                                'error'
                            )
                        }
                    },

                    error: function () {
                        swal(
                            'حدث خطأ أثناء رفع الصورة',
                            'رجاءً تأكد من وصولك بالإنترنت',
                            'error'
                        )
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    }

    function previewURL(input, element) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview_' + element).css("background", "url(" + e.target.result + ")" + " right top no-repeat");
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#submit_btn').on('click', function (event) {
        event.preventDefault();
        $('#form').submit();
    });
});

var isPhotoUploaded = true;

var line_index = 1;
$('.add_sec2').on('click', function (event) {
    event.preventDefault();
    $('.lines2').append(
        ' <div class="form-group">' +
        '      <div class="col-sm-12" style="padding-left: 1px;padding-right: 1px;">\n                   ' +
        '        <input type="text"  name="line[' + line_index + ']" placeholder=" سطر جديد " class="form-control" required/>' +
        '     </div> ' +
        '</div>');
    line_index++;
});

$('.add_sec1').on('click', function (event) {
    event.preventDefault();
    $('.lines1').append(
        ' <div class="form-group">' +
        '      <div class="col-sm-12" style="padding-left: 1px;padding-right: 1px;">\n                   ' +
        '        <input type="text"  name="adv_lines[' + line_index + ']" placeholder=" سطر جديد " class="form-control" required/>' +
        '     </div> ' +
        '</div>');
    line_index++;
});

var section_id;
$(document).on('click', '.delete', function (event) {
    var delete_url = $(this).data('url');
    event.preventDefault();
    var element = $(this);
    swal({
        title: '<span class="info">  هل أنت متأكد من الحذف  ؟</span>',
        type: 'info',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonText: 'موافق',
        cancelButtonText: 'إغلاق',
        confirmButtonColor: '#56ace0',
        width: '500px'
    }).then(function (value) {
        $.ajax({
            url: delete_url,
            method: 'delete',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                        }
                    );
                    element.parent().parent().parent().remove();
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (response) {
                $('.upload-spinn').addClass('hidden');
                errorCustomSweet();
            }
        });
    });
});
$(document).on('click', '.slider-modal-class', function (event) {
    var type = $(this).data('type');
    if (type == 'add') {
        clear('1');
        $('#slider_modal').modal('show');
    } else {
        $('#slider_modal').modal('show');
        $('.loader').removeClass('hidden');
        $('.modal_content').addClass('hidden');
        $.ajax({
            url: '/ar/admin/landing/section/data/' + $(this).data('id'),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    clear('1');
                    var section = response.item;
                    section_id = section.id;
                    var lines = JSON.parse(section.text);
                    $('#first_line_slider').remove();
                    for (var k in lines) {
                        $('.lines2').append(
                            ' <div class="form-group">' +
                            '      <div class="col-sm-12" style="padding-left: 1px;padding-right: 1px;">\n                   ' +
                            '        <input type="text"  name="line[' + k + ']" placeholder=" سطر جديد " class="form-control" value="' + lines[k] + '" required/>' +
                            '     </div> ' +
                            '</div>'
                        );
                    }
                    $('.modal_content').removeClass('hidden');
                    $('.loader').addClass('hidden');
                    $('#slider_photo').val(section.photo);
                    $('#slider_title').val(section.title);
                    $('#img_slider_photo').attr('src', '/image/200x200/' + section.photo);
                    $('.slider-thumbnail img').attr('src', '/image/200x200/' + section.photo);
                }
            },
            error: function (response) {
                // $('.upload-spinn').addClass('hidden');
                $('#slider_modal').modal('hide');
            }
        });
    }
    return false;
});
$(document).on('click', '.info_section_class', function (event) {
    var type = $(this).data('type');
    if (type == 'add') {
        clear('2');
        $('#info_section_modal').modal('show');
    } else {
        $('#info_section_modal').modal('show');
        $('.loader').removeClass('hidden');
        $('.modal_content').addClass('hidden');
        $.ajax({
            url: '/ar/admin/landing/section/data/' + $(this).data('id'),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    clear('2');
                    var section = response.item;
                    section_id = section.id;
                    $('.modal_content').removeClass('hidden');
                    $('.loader').addClass('hidden');
                    $('#info_section_photo').val(section.photo);
                    $('#info_section_title').val(section.title);
                    $('#info_section_text').val(section.text);
                    $('.info_section-thumbnail img').attr('src', '/image/200x200/' + section.photo);
                }
            },
            error: function (response) {
                // $('.upload-spinn').addClass('hidden');
                $('#info_section_modal').modal('hide');
            }
        });
    }
    return false;
});
$(document).on('click', '.content_section_class', function (event) {
    var type = $(this).data('type');
    if (type == 'add') {
        clear('3');
        $('#content_section_modal').modal('show');
    } else {
        $('#content_section_modal').modal('show');
        $('.loader').removeClass('hidden');
        $('.modal_content').addClass('hidden');
        $.ajax({
            url: '/ar/admin/landing/section/data/' + $(this).data('id'),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    clear('3');
                    var section = response.item;
                    section_id = section.id;
                    $('.modal_content').removeClass('hidden');
                    $('.loader').addClass('hidden');
                    $('#content_section_photo').val(section.photo);
                    $('#content_section_title').val(section.title);
                    $('#content_section_text').val(section.text);
                    $('.content_section-thumbnail img').attr('src', '/image/200x200/' + section.photo);
                }
            },
            error: function (response) {
                // $('.upload-spinn').addClass('hidden');
                $('#content_section_modal').modal('hide');
            }
        });
    }
    return false;
});
$(document).on('click', '.title-text_class', function (event) {
    var type = $(this).data('type');
    if (type == 'add') {
        clear('3');
        $('#why_section_modal').modal('show');
    } else {
        $('#why_section_modal').modal('show');
        $('.loader').removeClass('hidden');
        $('.modal_content').addClass('hidden');
        $.ajax({
            url: '/ar/admin/landing/section/data/' + $(this).data('id'),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    clear('3');
                    var section = response.item;
                    section_id = section.id;
                    $('.modal_content').removeClass('hidden');
                    $('.loader').addClass('hidden');
                    $('#why_title').val(section.title);
                    $('#why_text').val(section.text);
                }
            },
            error: function (response) {
                // $('.upload-spinn').addClass('hidden');
                $('#why_section_modal').modal('hide');
            }
        });
    }
    return false;
});
validate_form($('#slider_form'), 'slider_modal', '1');
validate_form($('#info_section_form'), 'info_section_modal', '2');
validate_form($('#content_section_form'), 'content_section_modal', '3');
validate_form($('#why_form'), 'why_section_modal', '4');

function clear(type) {
    section_id = '';
    switch (type) {
        case '1' :
            $('#slider_photo').val('');
            $('#slider_title').val('');
            $('#line_slider').val('');
            $('.slider-thumbnail img').attr('src', 'https://pancoastbenefits.com/wp-content/themes/pancoast_nsfs/img/default-staff.jpg');
            $('.lines2').empty();
            break;

        case '2' :
            $('#info_section_photo').val('');
            $('#info_section_title').val('');
            $('#info_section_text').val('');
            $('.info_section-thumbnail img').attr('src', 'https://pancoastbenefits.com/wp-content/themes/pancoast_nsfs/img/default-staff.jpg');
            break;

        case '3' :
            $('#content_section_photo').val('');
            $('#content_section_title').val('');
            $('#content_section_text').val('');
            $('.content_section-thumbnail img').attr('src', 'https://pancoastbenefits.com/wp-content/themes/pancoast_nsfs/img/default-staff.jpg');
            break;

        case '4' :
            $('#why_title').val('');
            $('#why_text').val('');
            break;
    }
}

function validate_form(form, modal, type) {
    form.validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler: function (f, e) {
            $('.upload-spinn').removeClass('hidden');
            var formData = new FormData(form[0]);

            if (section_id !== undefined && section_id !== '') {
                form_url = '/ar/admin/landing/section/edit/' + section_id;
            } else {
                form_url = '/ar/admin/landing/section/create'
            }
            if ($('#page_id').val() !== (undefined || '')) {
                formData.append('page_id', $('#page_id').val());
                submitForm(form_url, formData,
                    function (response, modal) {
                        customSweetAlert(
                            'success',
                            response.message,
                            '',
                            function () {
                                $('#type_' + type).html(response.item);
                                $('#' + modal).modal('hide');
                                clear(type);
                            }
                        );
                    }, function (response, modal) {
                        customSweetAlert(
                            'error',
                            response.message,
                            '',
                            function () {
                                $('#' + modal).modal('hide');
                            }
                        );
                    }, modal)
            } else {
                var form_data = new FormData($('#form')[0]);
                form_data.append('status', 'draft');
                submitForm('/ar/admin/landing/create-draft', form_data,
                    function (response, modal) {
                        $('#page_id').val(response.item.id);
                        formData.append('page_id', response.item.id);
                        submitForm(form_url, formData,
                            function (response, modal) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    '',
                                    function () {
                                        $('#type_' + type).html(response.item);
                                        $('#' + modal).modal('hide');
                                        clear(type);
                                    }
                                );
                            }, function (response, modal) {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    '',
                                    function () {
                                        $('#' + modal).modal('hide');
                                    }
                                );
                            }, modal)
                    }, function (response, modal) {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object,
                            function () {
                                $('#' + modal).modal('hide');

                            }
                        );
                    }, modal)
            }
        }
    });
}

function submitForm(url, formData, success_function, error_function, modal) {
    $.ajax({
        url: url,
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $('.upload-spinn').addClass('hidden');
            if (response.status) {
                success_function(response, modal);
            } else {
                error_function(response, modal);
            }
        },
        error: function (jqXhr) {
            $('.upload-spinn').addClass('hidden');
            getErrors(jqXhr, '/ar/admin/login');
            $('#' + modal).modal('hide');
        }
    })
}

// $('#accordion2 .panel-default').on('click', function (event) {
// });

$('#accordion2 .panel-heading').on('click', function (event) {
    $(this).siblings('.panel-collapse').collapse('toggle');
    var toggle = $(this).children('.panel-title').children('a').children('.toggle');
    if (toggle.hasClass('off')) {
        toggle.removeClass('btn btn-xs btn-default off').addClass(' btn btn-xs btn-success');
    } else {
        toggle.addClass('btn btn-xs btn-default off').removeClass(' btn btn-xs btn-success');
    }
    var input = toggle.children('input');
    input.prop('checked', function (i, value) {
        return !value;
    });
});

$(document).on('change', '.toggle', function (event) {
    event.preventDefault();
    var toggle = $(this).parent();
    if ($(this).hasClass('off')) {
        $(this).removeClass('btn btn-xs btn-default off').addClass(' btn btn-xs btn-success');
    } else {
        $(this).addClass('btn btn-xs btn-default off').removeClass(' btn btn-xs btn-success');
    }
    var input = $(this).children('input');
    input.prop('checked', function (i, value) {
        return !value;
    });
});