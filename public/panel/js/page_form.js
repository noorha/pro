var form = $('#form');
form.validate({
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    submitHandler: function (f, e) {
        update_editor();
        $('.upload-spinn').removeClass('hidden');
        var formData = new FormData(form[0]);
        var _method = form.attr('method');
        var url = form.attr('action');
        var redirectUrl = form.attr('to');
        $.ajax({
            url: url,
            method: _method,
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('.upload-spinn').addClass('hidden');
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            window.location = redirectUrl
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (jqXhr) {
                $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/ar/admin/login');
            }
        })
    }
});
