var form = $('#template_form');
var form_url = form.attr('action');
$(document).on('click', '.delete', function (event) {
    var delete_url = $(this).data('url');
    event.preventDefault();
    swal({
        title: '<span class="info">  هل أنت متأكد من الحذف  ؟</span>',
        type: 'info',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonText: 'موافق',
        cancelButtonText: 'إغلاق',
        confirmButtonColor: '#56ace0',
        width: '500px'
    }).then(function (value) {
        $.ajax({
            url: delete_url,
            method: 'delete',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            location.reload();
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (response) {
                $('.upload-spinn').addClass('hidden');
                errorCustomSweet();
            }
        });
    });
});
form.validate({
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    submitHandler: function (f, e) {
        $('.upload-spinn').removeClass('hidden');
        var formData = new FormData($('#template_form')[0]);
        console.log(formData);
        if (constant_id.val() !== undefined && constant_id.val() !== '') {
            form_url = '/ar/admin/template/main/edit/' + constant_id.val();
        } else {
            form_url = '/ar/admin/template/main/create'
        }
        $.ajax({
            url: form_url,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('.upload-spinn').addClass('hidden');
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            modal.modal('hide');
                            location.reload();
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (jqXhr) {
                $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/ar/admin/login');
            }
        })
    }
});
var loader = $('#loader');
var title_en = $('#title_en');
var title = $('#title');
var text = $('#text');
var text_en = $('#text_en');
var link = $('#link');
var constant_id = $('#id');
var modal_body = $('#modal_body');
var modal = $('#template_modal');
var preview_photo = $('#jasny_photo');
// var photo_form = $('#photo_form');
$(document).on('click', '.section', function (event) {
    constant_id.val($(this).data('id'));
    if (constant_id.val() !== undefined && constant_id.val() !== '') {
        modal_body.addClass('hidden');
        loader.removeClass('hidden');
        $.ajax({
            url: '/ar/admin/template/main/data/' + constant_id.val(),
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    loader.addClass('hidden');
                    modal_body.removeClass('hidden');
                    title.val(response.item.title);
                    title_en.val(response.item.title_en);
                    text.val(response.item.text);
                    link.val(response.item.link);
                    text_en.val(response.item.text_en);
                    constant_id.val(response.item.id);
                    preview_photo.attr('src', '/ar/image/' + response.item.photo);
                    $('#photo').val(response.item.photo);
                }
            },
            error: function (response) {
                // $('.upload-spinn').addClass('hidden');
                modal.hide();
            }
        });
    }
    event.preventDefault();
});

modal.on('hidden.bs.modal', function (e) {
    title.val('');
    title_en.val('');
    text.val('');
    text_en.val('');
    link.val('');
    constant_id.val('');
    preview_photo.attr('src', '');
    $('#photo').val('');
});
