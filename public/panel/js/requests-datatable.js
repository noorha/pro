jQuery(document).ready(function () {
    var tbl = $('#table1').DataTable({
        "columnDefs": [
            {"orderable": false, targets: '_all'}
        ],
        "bSort": false,
        "processing": true,
        "serverSide": true,
        "info": false,
        "ajax": {
            "url": url
        },
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'course_id', name: 'course_id'},
            {data: 'email', name: 'email'},
            {data: 'name', name: 'name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'address', name: 'address'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: '',
                className: 'hidden'
            }
        ],
        "bLengthChange": true,
        "bFilter": true,
        "pageLength": 10,
        language: {
            "sSearch": " ",
            "searchPlaceholder": "إبحث ",
            "sProcessing": " جارٍ التحميل ... ",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });

    $(document).on('click', '.delete', function (event) {
        var url = $(this).data('url');
        event.preventDefault();
        swal({
            title: '<span class="info">  هل أنت متأكد من حذف الدورة المحددة ؟</span>',
            type: 'info',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'موافق',
            cancelButtonText: 'إغلاق',
            confirmButtonColor: '#56ace0',
            width: '500px'
        }).then(function (value) {
            $.ajax({
                url: url,
                method: 'delete',
                type: 'json',
                success: function (response) {
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    errorCustomSweet();
                }
            });
        });
    });
});

