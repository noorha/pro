var questions_form = $('#questions_form');
var categories_form = $('#categories_form');
var form_url = questions_form.attr('action');
var modal = $('#questions_modal');
var cat_modal = $('#categories_modal');
var loader = $('.loader');
var body = $('.modal_content');
var question_id = $('#question_id');
var cat_id = $('#cat_id');
var question = $('#question');
var answer = $('#answer');
var question_en = $('#question_en');
var answer_en = $('#answer_en');
$('.submit-btn').on('click', function (e) {
    e.preventDefault();
    $(this).parents('form').submit();
});
questions_form.validate({
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    submitHandler: function (f, e) {
        $('.upload-spinn').removeClass('hidden');
        var formData = new FormData(questions_form[0]);
        if (question_id.val() !== undefined && question_id.val() !== '') {
            form_url = '/ar/admin/page/faqs/questions/edit/' + question_id.val();
        } else {
            form_url = '/ar/admin/page/faqs/questions/create'
        }
        $.ajax({
            url: form_url,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('.upload-spinn').addClass('hidden');
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        '',
                        function (resp) {
                            modal.modal('hide');
                            $('#type_' + $('#category_id').val()).html(response.item)
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (jqXhr) {
                $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/ar/admin/login');
            }
        })
    }
});
categories_form.validate({
    highlight: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    submitHandler: function (f, e) {
        $('.upload-spinn').removeClass('hidden');
        var formData = new FormData(categories_form[0]);
        if (cat_id.val() !== undefined && cat_id.val() !== '') {
            form_url = '/ar/admin/page/faqs/categories/edit/' + cat_id.val();
        } else {
            form_url = '/ar/admin/page/faqs/categories/create'
        }
        $.ajax({
            url: form_url,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                $('.upload-spinn').addClass('hidden');
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            cat_modal.modal('hide');
                            location.reload();
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (jqXhr) {
                $('.upload-spinn').addClass('hidden');
                getErrors(jqXhr, '/ar/admin/login');
            }
        })
    }
});
$(document).on('click', '.question-modal-class', function (event) {
    event.preventDefault();
    var type = $(this).data('type');
    var id = $(this).data('id');
    var current_id = $(this).data('question');
    question_id.val(current_id);
    $('#category_id').val(id);
    modal.modal('show');
    if (type === 'edit') {
        loader.removeClass('hidden');
        body.addClass('hidden');
        $.ajax({
            url: '/ar/admin/page/faqs/question/' + current_id,
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    loader.addClass('hidden');
                    body.removeClass('hidden');
                    question.val(response.item.question);
                    answer.val(response.item.answer);
                    question_en.val(response.item.question_en);
                    answer_en.val(response.item.answer_en);
                    question_id.val(response.item.id);
                }
            },
            error: function (response) {
                modal.modal('hide');
            }
        });
    }
});
$(document).on('click', '.categories-modal-class', function (event) {
    event.preventDefault();
    var type = $(this).data('type');
    var id = $(this).data('id');
    $('#categories_modal').modal('show');
    if (type === 'edit') {
        loader.removeClass('hidden');
        body.addClass('hidden');
        $.ajax({
            url: '/ar/admin/page/faqs/category/' + id,
            method: 'GET',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    loader.addClass('hidden');
                    body.removeClass('hidden');
                    $('#title').val(response.item.text);
                    $('#title_en').val(response.item.text_en);
                    $('#cat_id').val(response.item.id);
                }
            },
            error: function (response) {
                $('#categories_modal').modal('hide');
            }
        });
    }
});
cat_modal.on('hidden.bs.modal', function (e) {
    $('#title').val('');
    $('#title_en').val('');
    $('#cat_id').val('');
});
modal.on('hidden.bs.modal', function (e) {
    $('#category_id').val('');
    question.val('');
    answer.val('');
    question_en.val('');
    answer_en.val('');
    question_id.val('');
});

$(document).on('click', '.delete', function (event) {
    var delete_url = $(this).data('url');
    var type = $(this).data('type');
    var element = $(this);
    event.preventDefault();
    swal({
        title: '<span class="info">  هل أنت متأكد من الحذف  ؟</span>',
        type: 'info',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonText: 'موافق',
        cancelButtonText: 'إغلاق',
        confirmButtonColor: '#56ace0',
        width: '500px'
    }).then(function (value) {
        $.ajax({
            url: delete_url,
            method: 'delete',
            type: 'json',
            success: function (response) {
                if (response.status) {
                    customSweetAlert(
                        'success',
                        response.message,
                        response.item,
                        function (event) {
                            if (type === 'cat') {
                                location.reload();
                            } else {
                                element.parent().parent().parent().remove();
                            }
                        }
                    );
                } else {
                    customSweetAlert(
                        'error',
                        response.message,
                        response.errors_object
                    );
                }
            },
            error: function (response) {
                $('.upload-spinn').addClass('hidden');
                errorCustomSweet();
            }
        });
    });
});