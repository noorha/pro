jQuery(document).ready(function () {
    $(document).on('change', '.parent', function (event) {
        var _this = $(this);
        _this.parent('.ckbox-fs').siblings('.form-group').find('input').each(function () {
            this.checked = _this.is(':checked');
        });
    });
});
