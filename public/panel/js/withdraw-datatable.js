jQuery(document).ready(function () {
    var tbl = $('#table1').DataTable({
        "columnDefs": [
            {"orderable": false, targets: '_all'}
        ],
        "bSort": false,
        "processing": true,
        "serverSide": true,
        "info": false,
        "ajax": {
            "url": url
        },
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'user_id', name: 'user_id'},
            {data: 'email', name: 'email'},
            {data: 'balance', name: 'balance'},
            {data: 'value', name: 'value'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action'}
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: '',
                className: 'hidden'
            }
        ],
        "bLengthChange": true,
        "bFilter": true,
        "pageLength": 10,
        language: {
            "sSearch": " ",
            "searchPlaceholder": "إبحث ",
            "sProcessing": " جارٍ التحميل ... ",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });
    $(document).on('click', '.accept', function (event) {
        var id = $(this).data('id');
        event.preventDefault();
        swal({
            title: '<span class="info">  هل أنت متأكد من قبول طلب السحب ؟</span>',
            type: 'info',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'موافق',
            cancelButtonText: 'إغلاق',
            confirmButtonColor: '#56ace0',
            width: '500px'
        }).then(function (value) {
            var formData = new FormData();
            $.ajax({
                url: '/ar/admin/withdraw/accept/' + id,
                type: 'POST',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    errorCustomSweet();
                }
            });
        });
    });
    $(document).on('click', '.reject', function (event) {
        var id = $(this).data('id');
        event.preventDefault();
        swal({
            title: '<span class="info">  هل أنت متأكد من رفض طلب السحب  ؟</span>',
            type: 'info',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'موافق',
            cancelButtonText: 'إغلاق',
            confirmButtonColor: '#56ace0',
            width: '500px'
        }).then(function (value) {
            var formData = new FormData();
            $.ajax({
                url: '/ar/admin/withdraw/reject/' + id,
                type: 'POST',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    errorCustomSweet();
                }
            });
        });
    });
    $(document).on('click', '.delete', function (event) {
        var id = $(this).data('id');
        event.preventDefault();
        swal({
            title: '<span class="info">  هل أنت متأكد من حذف طلب السحب ؟</span>',
            type: 'info',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'موافق',
            cancelButtonText: 'إغلاق',
            confirmButtonColor: '#56ace0',
            width: '500px'
        }).then(function (value) {
            $.ajax({
                url: '/ar/admin/withdraw/delete/' + id,
                method: 'delete',
                type: 'json',
                success: function (response) {
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    errorCustomSweet();
                }
            });
        });
    });

    $('.data_range').on('change',function (event) {
        tbl.ajax.url(url + '?date=' + $(this).val()).load();
    });

    $(function() {
        $('.data_range').daterangepicker({

            timePicker: false,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(15, 'day'),
            locale:
                {
                    format: 'Y-MM-DD',
                    "separator": "   ",
                    "applyLabel": "بحث",
                    "cancelLabel": "إلغاء",
                    "fromLabel": "من",
                    "toLabel": "إلى",
                    "customRangeLabel": "مخصص",
                    "daysOfWeek": [
                        "الأحد",
                        "الإثنين",
                        "الثلاثاء",
                        "الأربعاء",
                        "الخميس",
                        "الجمعة",
                        "السبت"
                    ],
                    "monthNames": [
                        "يناير",
                        "فبراير",
                        "مارس",
                        "إبريل",
                        "مايو",
                        "يونيو",
                        "يوليو",
                        "أغسطس",
                        "سبتمبر",
                        "أكتوبر",
                        "نوفمبر",
                        "ديسمبر"
                    ],
                    "firstDay": 1
                }
        });
    });
});

