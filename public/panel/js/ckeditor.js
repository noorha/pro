var ckeditor = $('.ckeditor').ckeditor();
function update_editor() {
    if (ckeditor !== undefined) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
    }
}
