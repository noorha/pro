jQuery(document).ready(function () {
    var form = $('#form');
    var form_url = form.attr('action');
    form.validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        submitHandler: function (f, e) {
            $('.upload-spinn').removeClass('hidden');
            var formData = new FormData(form[0]);
            if (constant_id.val() !== undefined && constant_id.val() !== '') {
                form_url = '/ar/admin/partners/edit/' + constant_id.val();
            } else {
                form_url = '/ar/admin/partners/create'
            }
            $.ajax({
                url: form_url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                                partners_modal.modal('hide');
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (jqXhr) {
                    $('.upload-spinn').addClass('hidden');
                    getErrors(jqXhr, '/ar/admin/login');
                }
            })
        }
    });

    var tbl = $('#table1').DataTable({
        "columnDefs": [
            {"orderable": false, targets: '_all'}
        ],
        "bSort": false,
        "processing": true,
        "serverSide": true,
        "info": false,
        "ajax": {
            "url": url
        },
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'photo', name: 'photo'},
            {data: 'link', name: 'link'},
            {data: 'action', name: 'action'}
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: '',
                className: 'hidden'
            }
        ],
        "bLengthChange": true,
        "bFilter": true,
        "pageLength": 10,
        language: {
            "sSearch": " ",
            "searchPlaceholder": "إبحث ",
            "sProcessing": " جارٍ التحميل ... ",
            "sLengthMenu": "أظهر _MENU_ مدخلات",
            "sZeroRecords": "لم يعثر على أية سجلات",
            "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
            "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
            "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
            "sInfoPostFix": "",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "الأول",
                "sPrevious": "السابق",
                "sNext": "التالي",
                "sLast": "الأخير"
            }
        }
    });
    var id;
    var loader = $('#loader');
    var partners_modal = $('#partners_modal');
    var link = $('#link');
    var constant_id = $('#id');
    var modal_body = $('#modal_body');
    var preview_photo = $('.jasny_photo').children('img');
    $(document).on('click', '.edit', function (event) {
        constant_id.val($(this).data('id'));
        var url = $(this).data('edit');
        if (constant_id.val() !== undefined && constant_id.val() !== '') {
            modal_body.addClass('hidden');
            loader.removeClass('hidden');
            $.ajax({
                url: url,
                method: 'GET',
                type: 'json',
                success: function (response) {
                    if (response.status) {
                        loader.addClass('hidden');
                        modal_body.removeClass('hidden');
                        link.val(response.item.link);
                        constant_id.val(response.item.id);
                        preview_photo.attr('src','/ar/image/'+response.item.photo);
                        $('#photo').val(response.item.photo);
                    }
                },
                error: function (response) {
                    // $('.upload-spinn').addClass('hidden');
                    partners_modal.hide();
                }
            });
        }
        event.preventDefault();
    });
    partners_modal.on('hidden.bs.modal', function (e) {
        link.val('');
        constant_id.val('');
        $('#jasny_photo').attr('src','/ar/image/default.png');
        $('#photo').val('');
    });
    $(document).on('click', '.delete', function (event) {
        var delete_url = $(this).data('url');
        event.preventDefault();
        swal({
            title: '<span class="info">  هل أنت متأكد من الحذف  ؟</span>',
            type: 'info',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'موافق',
            cancelButtonText: 'إغلاق',
            confirmButtonColor: '#56ace0',
            width: '500px'
        }).then(function (value) {
            $.ajax({
                url: delete_url,
                method: 'delete',
                type: 'json',
                success: function (response) {
                    if (response.status) {
                        customSweetAlert(
                            'success',
                            response.message,
                            response.item,
                            function (event) {
                                tbl.ajax.reload();
                            }
                        );
                    } else {
                        customSweetAlert(
                            'error',
                            response.message,
                            response.errors_object
                        );
                    }
                },
                error: function (response) {
                    $('.upload-spinn').addClass('hidden');
                    errorCustomSweet();
                }
            });
        });
    });
});

