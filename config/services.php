<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'firebase' => [
        'api_key' => 'api key taken from firebase',
        'auth_domain' => 'auth domain taken from firebase',
        'database_url' => 'https://ustazk-ad90f.firebaseio.com/',
        'secret' => 'EJuRHfpIrpI8uTp8mLoL5Bxxxclv0xN05Bu5Gd5j',
        'storage_bucket' => 'storage bucket taken from firebase',
    ],
     'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID','1031806263877186'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET','cfb6f001221f50affb4f662f6ed49d53'),
        'redirect' => 'http://localhost:7000/auth/facebook/callback',//env('FACEBOOK_CALLBACK_URL'),
    ],
    'linkedin' => [
       'client_id' => env('LINKEDIN_CLIENT_ID','864t4ie60hx2l4'),
       'client_secret' => env('LINKEDIN_CLIENT_SECRET','a1wqQUAbPXOhIKEV'),
       'redirect' => 'http://localhost:7000/auth/linkedin/callback',//env('FACEBOOK_CALLBACK_URL'),
   ],

];


   
