<?php
return array(
    /** set your paypal credential **/
    'client_id' =>'Adljpv9y0UCHKJ96JVHY-yQdC0cDxFbo0TrCvNHf7e4z3c91_bz-SZQHsahv1gV9lK5_E6RyUa0pbpXm',
    'secret' => 'ECoGC5Jx1oDvGBhiFgK1CuQXt1WLz1JHt38xc2Y2F0HAMV7gKARorouEqk04bo3L_GrcgFfplluM60KT',
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'live',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 1000,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);