<?php


Route::post('/image-upload', ['uses' => 'API\MainController@upload_image']);
Route::post('/file-upload', ['uses' => 'API\MainController@upload_file']);
Route::post('login', 'API\Auth\LoginController@authenticate');
Route::post('/logout', 'API\ProfileController@logout');

Route::post('token/refresh', 'API\Auth\LoginController@refreshToken');
Route::post('register', 'API\Auth\RegisterController@register');
Route::post('code/send', 'API\Auth\RegisterController@reSendMobileCode');
Route::post('change-password/{id}', 'API\Auth\RegisterController@changePassword');
Route::post('contact', 'API\MainController@sendVisitorMessage');
Route::get('activate/{code}', 'API\Auth\RegisterController@activateAccount');

Route::get('page/{type}', 'API\MainController@getPageDetails');
Route::get('countries', 'API\MainController@countries');
Route::get('countries/with-cities', 'API\MainController@getCountriesWithCities');
Route::get('cities', 'API\MainController@cities');
Route::get('cities/{id}', 'API\MainController@getCitiesByCountry');
Route::get('days', 'API\MainController@getWeekDays');
Route::get('complete-text/{type}/{auth_type}', 'API\MainController@getCompleteProfileText');
Route::prefix('/specialities')->group(function () {
    Route::get('/', 'API\MainController@specialities');
    Route::get('/{id}', 'API\MainController@findSpecialty');
});
Route::get('sub-specialities', 'API\MainController@subSpecialities');
Route::get('test', 'API\MainController@test');

Route::get('/teachers/search', 'API\TrainingController@teacherSearch');
Route::get('/students/search', 'API\TrainingController@studentSearch');

Route::prefix('/user')->group(function () {
    Route::get('/specialities/{id}', 'API\ProfileController@getUserSpecs');
    Route::get('/{id}/sub-specialities/', 'API\ProfileController@getUserSubSpecialities');
});

Route::middleware('auth:api')->group(function () {
    Route::prefix('/profile')->group(function () {
        Route::get('/user', 'API\ProfileController@getCurrentUser');
        Route::get('/ratings/{id}', 'API\ProfileController@getTeacherRatings');
        Route::get('/user/specialities', 'API\ProfileController@getUserSpecialities');
        Route::get('/financial-processes', 'API\ProfileController@getMyFinancialProcesses');
        Route::post('/complete', 'API\ProfileController@completeProfile');
        Route::get('/notifications', 'API\ProfileController@getNotifications');
        Route::get('/notifications/un-read', 'API\ProfileController@getUnReadNotificationsCount');
        Route::get('/notifications/mark/read/{id}', 'API\ProfileController@makeNotificationOpen');
        Route::get('/notifications/mark/read', 'API\ProfileController@markNotificationsAsRead');
        Route::post('/change-password', 'API\ProfileController@changePassword');
        Route::get('/data/{id}', 'API\ProfileController@getProfileData');
        Route::post('/add-balance/', 'API\ProfileController@addCredit');
        Route::post('/withdraw-balance/', 'API\ProfileController@sendWithdrawRequest');
        Route::post('/cash/pay', 'API\ProfileController@payCashRequest');
    });


    Route::prefix('/request')->group(function () {

        Route::post('/accept/teacher/{trainingRequest}', ['uses' => 'API\TrainingController@acceptTrainingRequestByTeacher']);
        Route::post('/accept/student/{trainingRequest}', ['uses' => 'API\TrainingController@acceptTrainingRequestByStudent']);

        Route::post('/confirm/teacher/{trainingRequest}', ['uses' => 'API\TrainingController@confirmRequestByTeacher']);
        Route::post('/confirm/student/{trainingRequest}', ['uses' => 'API\TrainingController@confirmRequestByStudent']);

        Route::post('/reject/student/{trainingRequest}', ['uses' => 'API\TrainingController@rejectRequestByStudent']);
        Route::post('/reject/teacher/{trainingRequest}', ['uses' => 'API\TrainingController@rejectRequestByTeacher']);

        Route::post('/create/{teacherId}', 'API\TrainingController@createTrainingRequest');
        Route::get('/find/{id}', 'API\TrainingController@findTrainingRequest');
        Route::get('/all', 'API\TrainingController@getTrainingRequests');
    });

    Route::post('/rating/teacher/{id}', 'API\TrainingController@teacherRating');
    Route::prefix('/code')->group(function () {
        Route::post('/check', ['as' => 'api.chat.all', 'uses' => 'API\TrainingController@checkPromoCode']);
    });
    Route::prefix('/chat')->group(function () {
        Route::get('/all', ['as' => 'api.chat.all', 'uses' => 'API\ChatController@getAllConversations']);
        Route::get('/request/{trainingRequest}', ['as' => 'api.teacher.chat.request', 'uses' => 'API\ChatController@chat_request_index']);
        Route::get('/get/{type}/{id}', ['as' => 'api.teacher.chat', 'uses' => 'API\ChatController@chat_index']);
        Route::get('/messages/{chat}', ['as' => 'api.chat.messages', 'uses' => 'API\ChatController@get_chat_messages']);
        Route::post('/send/{type}/{chat}', ['as' => 'api.chat.send', 'uses' => 'API\ChatController@send_new_message']);
        Route::get('/whiteboard/{id}', ['as' => 'api.chat.whiteboard', 'uses' => 'API\ChatController@whiteboard_index']);
    });

});

