<?php

Route::get('/', function () {
    return redirect()->to('/' . get_current_locale());
});
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@show_login_form']);
\Longman\LaravelMultiLang\Facades\MultiLang::routeGroup(function ($router) {
    Route::post('/broadcasting/auth', ['uses' => 'BroadcastController@index']);

    Route::get('/coupon/check', ['as' => 'front.coupon.check', 'uses' => 'front\MainController@checkCoupon'])->middleware('auth');

//    Route::get('/test', ['as' => 'front.test', 'uses' => 'front\MainController@test']);
    Route::get('/test', ['as' => 'front.test', 'uses' => 'front\MainController@test']);
    Route::get('/schedule/message/{encryption}', ['as' => 'schedule.message', 'uses' => 'front\MainController@scheduledMessage']);


    Route::get('/', ['as' => 'main', 'uses' => 'front\MainController@index']);

    Route::get('/faq', ['as' => 'front.faq', 'uses' => 'front\MainController@faqIndex']);

//    Route::get('/page1', ['as' => 'faq', 'uses' => 'front\MainController@page1']);
//    Route::get('/page2', ['as' => 'faq', 'uses' => 'front\MainController@page2']);
//    Route::get('/page3', ['as' => 'faq', 'uses' => 'front\MainController@page3']);
//    Route::get('/page4', ['as' => 'faq', 'uses' => 'front\MainController@page4']);
    Route::get('/faq', ['as' => 'faq', 'uses' => 'front\MainController@faqIndex']);


    Route::prefix('/blog')->group(function () {
        Route::get('/all/{category?}', ['as' => 'blog.all', 'uses' => 'front\BlogController@index']);
        Route::get('/view/{id}/{name?}', ['as' => 'blog.view', 'uses' => 'front\BlogController@viewBlog']);
    });


//    Route::get('/test', ['as' => 'test', 'uses' => 'front\MainController@test']);

    Route::get('/currency/{currency}', ['as' => 'front.currency', 'uses' => 'front\MainController@setCurrentCurrency']);

    Route::prefix('/contact')->group(function () {
        Route::get('/', ['as' => 'contact', 'uses' => 'front\MainController@contact']);
        Route::post('/', ['as' => 'contact.msg', 'uses' => 'front\MainController@store_visitor_message']);
    });


    Route::group(['prefix' => 'ticket', 'as' => 'ticket.'], function () {
        Route::get('/all', ['as' => 'all', 'uses' => 'front\TicketController@index']);
        Route::get('/view/{id}', ['as' => 'view', 'uses' => 'front\TicketController@viewTicket']);

        Route::post('/replay/{id}', ['as' => 'replay.post', 'uses' => 'front\TicketController@replay']);

        Route::group(['prefix' => '/create', 'as' => 'create.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'front\TicketController@create']);
            Route::post('/', ['as' => 'post', 'uses' => 'front\TicketController@store']);
        });
    });

    Route::get('/css/update', ['as' => 'test', 'uses' => 'front\MainController@cssMin']);
    Route::get('/js/update/{type?}', ['as' => 'test', 'uses' => 'front\MainController@jsMin']);

    Route::post('/mailing-list/subscribe', ['as' => 'subscribe.mail', 'uses' => 'front\MainController@subscribe_mail']);

    Route::get('/sub_speciality/{speciality}', ['as' => 'speciality', 'uses' => 'front\MainController@get_sub_specialities']);

    Route::get('/cities/{country}', ['as' => 'cities', 'uses' => 'front\MainController@get_country_cities']);


    Route::get('/about', ['as' => 'about', 'uses' => 'front\MainController@about']);
    Route::get('/how', ['as' => 'work.how', 'uses' => 'front\MainController@page3']);
    Route::get('/privacy', ['as' => 'privacy', 'uses' => 'front\MainController@privacy']);
    Route::get('/policy', ['as' => 'policy', 'uses' => 'front\MainController@policy']);
    Route::get('/work', ['as' => 'work.us', 'uses' => 'front\MainController@work_with_us']);
    Route::get('/register/{type}', ['as' => 'register', 'uses' => 'Auth\RegisterController@show_registration_form']);
    Route::post('/register/{type}', ['as' => 'post.register', 'uses' => 'Auth\RegisterController@store']);
    Route::get('/confirm-email/{token}', 'Auth\RegisterController@activate_user_account');
    Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@show_login_form']);
    Route::post('/login', ['as' => 'post.login', 'uses' => 'Auth\LoginController@login']);
    Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::get('/auth/redirect/{provider}', 'Auth\LoginController@redirectToApi');
    Route::get('auth/{provider}/callback', 'Auth\LoginController@handleCallback');

    
    Route::prefix('/payment')->group(function () {
        Route::get('paywithpaypal', array('as' => 'addmoney.paywithpaypal', 'uses' => 'front\PaymentController@payWithPaypal'));
        Route::post('paypal', array('as' => 'addmoney.paypal', 'uses' => 'front\PaymentController@postPaymentWithpaypal'));
        Route::get('paypal', array('as' => 'payment.status', 'uses' => 'front\PaymentController@getPaymentStatus'));
    });

    Route::prefix('/image')->group(function () {
        Route::post('/upload', ['as' => 'upload.image', 'uses' => 'ImageController@upload_image']);
        Route::post('/delete', ['as' => 'delete.image', 'uses' => 'ImageController@delete_image']);
        Route::get('/{size}/{id}', ['as' => 'image', 'uses' => 'ImageHandler@getPublicImage']);
        Route::get('/limit/{size}/{id}', ['as' => 'image', 'uses' => 'ImageHandler@getImageResize']);
        Route::get('/{id}', ['as' => 'image', 'uses' => 'ImageHandler@getDefaultImage']);
    });

    Route::prefix('/file')->group(function () {
        Route::post('/upload', ['as' => 'file.upload', 'uses' => 'FileController@uploadFile']);
        Route::post('/delete', ['as' => 'file.delete', 'uses' => 'FileController@delete_file']);
        Route::get('/{id}', 'FileController@get_file');
    });

    Route::prefix('/course')->group(function () {
        Route::get('/all', ['as' => 'course.all', 'uses' => 'front\CourseController@index']);
        Route::get('/view/{id}/{name?}', ['as' => 'course.view', 'uses' => 'front\CourseController@view_course']);
        Route::post('/subscribe/{id}', ['as' => 'course.subscribe', 'uses' => 'front\CourseController@course_subscription']);
    });


    Route::prefix('/reset')->group(function () {
        Route::get('/', ['as' => 'forget', 'uses' => 'front\MainController@reset_index']);
        Route::get('/{code}', ['as' => 'forget.check', 'uses' => 'front\MainController@check_password_link']);
        Route::post('/', ['as' => 'reset.send', 'uses' => 'front\MainController@send_reset_email']);
        Route::post('/change-password/{id}', ['as' => 'password.reset', 'uses' => 'front\MainController@change_password']);
    });

    Route::prefix('/profile')->group(function () {
        Route::get('/dashboard', ['as' => 'profile.dashboard', 'uses' => 'front\ProfileController@dashboard_index'])->middleware('is_complete');
        Route::get('/notifications', ['as' => 'profile.notifications', 'uses' => 'front\ProfileController@notifications_index'])->middleware('is_complete');
        Route::get('/notifications/markAsRead', ['as' => 'notifications.read', 'uses' => 'front\ProfileController@mark_notifications_as_read']);
        Route::get('/notifications/count', ['as' => 'notifications.count', 'uses' => 'front\ProfileController@get_unread_notifications_count']);
        Route::get('/balance', ['as' => 'profile.balance', 'uses' => 'front\ProfileController@balance_index'])->middleware('is_complete');
        Route::post('/cash', ['as' => 'profile.cash', 'uses' => 'front\ProfileController@payCashRequest'])->middleware('is_complete');
        Route::get('/messages/{chat?}', ['as' => 'profile.messages', 'uses' => 'front\ProfileController@messages_index'])->middleware('is_complete');
        Route::post('/message/create', ['as' => 'profile.message.create', 'uses' => 'front\ProfileController@messageCreate'])->middleware('is_complete');
        Route::get('/messages-data/', ['as' => 'profile.messages.data', 'uses' => 'front\ProfileController@fetchVueData'])->middleware('is_complete');
        Route::get('/activate', ['as' => 'profile.activate', 'uses' => 'front\ProfileController@activate_index']);
        Route::post('/activate', ['as' => 'profile.activate', 'uses' => 'front\ProfileController@activate_account']);
        Route::get('/wait/activate', ['as' => 'profile.wait_activate', 'uses' => 'front\ProfileController@wait_activate_index']);
        Route::get('/complete', ['as' => 'profile.complete', 'uses' => 'front\ProfileController@complete_index']);
        Route::post('/complete', ['as' => 'profile.complete.post', 'uses' => 'front\ProfileController@complete_profile']);
        Route::post('/withdraw', ['as' => 'profile.withdraw.send', 'uses' => 'front\ProfileController@create_withdraw_request'])->middleware('is_complete');
        Route::get('/{id}/{name?}', ['as' => 'profile', 'uses' => 'front\ProfileController@profile_index']);
        Route::delete('/delete', ['as' => 'front.profile.delete', 'uses' => 'front\ProfileController@sendDeleteRequest']);
        Route::post('/cancel-delete', ['as' => 'front.profile.cancel-delete', 'uses' => 'front\ProfileController@cancelDeleteRequest']);
    });

    Route::prefix('/chat')->group(function () {
        Route::get('/request/{trainingRequest}', ['as' => 'teacher.chat.request', 'uses' => 'front\ChatController@chat_request_index']);
        Route::get('/view/{type}/{id}', ['as' => 'teacher.chat', 'uses' => 'front\ChatController@chat_index']);
        Route::get('/messages/{type}/{chat}', ['as' => 'chat.messages', 'uses' => 'front\ChatController@get_chat_messages']);
        Route::post('/send/{type}/{chat}', ['as' => 'chat.send', 'uses' => 'front\ChatController@send_new_message']);
        Route::get('/whiteboard/{id}', ['as' => 'chat.whiteboard', 'uses' => 'front\ChatController@whiteboard_index']);
    });


    Route::prefix('/teacher')->group(function () {
        Route::get('/ratings/{teacher}', ['as' => 'teacher.ratings', 'uses' => 'front\TeacherController@teacher_ratings_index']);

        Route::get('/search/{key?}/{value?}/{is_array?}', ['as' => 'teacher.search', 'uses' => 'front\TeacherController@teacher_search_index']);
        Route::post('/confirm/{trainingRequest}', ['as' => 'confirm.request.teacher', 'uses' => 'front\TeacherController@confirm_request'])->middleware(['teacher']);
        Route::post('/report/{trainingRequest}', ['as' => 'report.request.teacher', 'uses' => 'front\TeacherController@report_request'])->middleware(['teacher']);
        Route::post('/favourite/', ['as' => 'favourite.teacher', 'uses' => 'front\TeacherController@teacher_favourite']);
        Route::post('/removefavourite/', ['as' => 'removefavourite.teacher', 'uses' => 'front\TeacherController@teacher_removefavourite']);

        
        Route::prefix('/request')->group(function () {
            Route::get('/{id}', ['as' => 'teacher.request', 'uses' => 'front\TeacherController@training_request_index'])->middleware(['student']);
            Route::post('/{id}', ['as' => 'teacher.request.send', 'uses' => 'front\TeacherController@send_training_request'])->middleware(['student']);
        });

        Route::prefix('/training')->group(function () {
            Route::prefix('/requests')->group(function () {
                Route::get('/', ['as' => 'teacher.training.requests', 'uses' => 'front\TeacherController@training_requests'])->middleware(['teacher']);
                Route::post('/accept/{id}', ['as' => 'teacher.training.requests.accept', 'uses' => 'front\TeacherController@accept_teacher_training_request'])->middleware(['teacher']);
                Route::post('/reject/{id}', ['as' => 'teacher.training.requests.reject', 'uses' => 'front\TeacherController@reject_teacher_training_request'])->middleware(['teacher']);
            });
        });
    });


    Route::prefix('/student')->group(function () {
        Route::get('/search/{key?}/{value?}/{is_array?}', ['as' => 'student.search', 'uses' => 'front\StudentController@student_search_index']);
        Route::post('/rate/{teacher}', ['as' => 'rating', 'uses' => 'front\StudentController@rating_teacher'])->middleware(['student']);
        Route::post('/confirm/{trainingRequest}', ['as' => 'confirm.request.student', 'uses' => 'front\StudentController@confirm_request'])->middleware(['student']);
        Route::post('/report/{trainingRequest}', ['as' => 'report.request.student', 'uses' => 'front\StudentController@report_request'])->middleware(['student']);

        Route::prefix('/request')->group(function () {
            Route::get('/{id}', ['as' => 'student.request', 'uses' => 'front\StudentController@training_request_index'])->middleware(['student']);
            Route::post('/{id}', ['as' => 'student.request.send', 'uses' => 'front\StudentController@send_training_request'])->middleware(['student']);
        });

        Route::prefix('/training')->group(function () {
            Route::get('requests', ['as' => 'student.training.requests', 'uses' => 'front\StudentController@training_requests'])->middleware(['student']);
            Route::post('/approved/{trainingRequest}', ['as' => 'teacher.training.requests.approved', 'uses' => 'front\StudentController@student_approved_training_request'])->middleware(['student']);
            Route::post('/reject/{id}', ['as' => 'student.training.requests.reject', 'uses' => 'front\StudentController@reject_training_request'])->middleware(['student']);
        });
    });


    Route::prefix('/admin')->group(function () {
        Route::get('/', ['as' => 'panel', 'uses' => 'Auth\PanelLoginController@index']);
        Route::get('/login', ['as' => 'panel.login', 'uses' => 'Auth\PanelLoginController@showLoginForm']);
        Route::post('/login', ['as' => 'panel.login.post', 'uses' => 'Auth\PanelLoginController@login']);
        Route::get('/dashboard', ['as' => 'panel.dashboard', 'uses' => 'panel\HomeController@index']);
        Route::post('/logout', 'panel\HomeController@logout');


        Route::group(['prefix' => 'scheduled-messages', 'as' => 'panel.scheduled-messages.'], function () {
            Route::group(['prefix' => 'send', 'as' => 'send.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'panel\ScheduledMessageController@sendIndex']);
                Route::post('/', ['as' => 'post', 'uses' => 'panel\ScheduledMessageController@sendMail']);
            });
            Route::get('/search', ['as' => 'search', 'uses' => 'panel\ScheduledMessageController@userSearch']);

            Route::group(['prefix' => 'create', 'as' => 'create.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'panel\ScheduledMessageController@create']);
                Route::post('/', ['as' => 'store', 'uses' => 'panel\ScheduledMessageController@store']);
            });
            Route::group(['prefix' => 'edit', 'as' => 'edit.'], function () {
                Route::get('/{id}', ['as' => 'index', 'uses' => 'panel\ScheduledMessageController@edit']);
                Route::post('/{id}', ['as' => 'update', 'uses' => 'panel\ScheduledMessageController@update']);
            });
            Route::group(['prefix' => 'all', 'as' => 'all.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'panel\ScheduledMessageController@scheduleIndex']);
                Route::get('/data', ['as' => 'data', 'uses' => 'panel\ScheduledMessageController@getDataTable']);
            });
            Route::get('/change-status/{offer}', ['as' => 'change-status', 'uses' => 'panel\ScheduledMessageController@changeStatus']);

            Route::delete('/{id}', ['as' => 'delete', 'uses' => 'panel\ScheduledMessageController@delete']);
        });


        Route::prefix('/student')->group(function () {
            Route::prefix('/notifications')->group(function () {
                Route::get('/', ['as' => 'panel.student.notifications', 'uses' => 'panel\StudentController@notifications_index'])->middleware('permission:notify_student');;
                Route::post('/', ['as' => 'panel.student.notifications.send', 'uses' => 'panel\StudentController@send_notifications'])->middleware('permission:notify_student');;
            });
            Route::prefix('/create')->group(function () {
                Route::get('/', ['as' => 'panel.student.create', 'uses' => 'panel\StudentController@create'])->middleware('permission:add_student');;
                Route::post('/', ['as' => 'panel.student.create', 'uses' => 'panel\StudentController@store'])->middleware('permission:add_student');;
            });
            Route::prefix('/edit')->group(function () {
                Route::get('/{user}', ['as' => 'panel.student.edit', 'uses' => 'panel\StudentController@edit'])->middleware('permission:add_student');
                Route::put('/{user}', ['as' => 'panel.student.edit', 'uses' => 'panel\StudentController@update'])->middleware('permission:add_student');
            });
            Route::get('/all', ['as' => 'panel.student.all', 'uses' => 'panel\StudentController@index'])->middleware('permission:view_student');
            Route::get('/all/data', ['as' => 'panel.student.all.data', 'uses' => 'panel\StudentController@get_data_table'])->middleware('permission:view_student');
            Route::get('/status/{id}', ['as' => 'panel.student.status', 'uses' => 'panel\StudentController@change_status'])->middleware('permission:add_student');
            Route::patch('/activate/{id}', ['as' => 'panel.student.activate', 'uses' => 'panel\StudentController@activate'])->middleware('permission:add_student');
            Route::delete('/delete/{student}', ['as' => 'panel.student.delete', 'uses' => 'panel\StudentController@delete'])->middleware('permission:delete_student');
        });

        Route::prefix('/teacher')->group(function () {
            Route::prefix('/notifications')->group(function () {
                Route::get('/', ['as' => 'panel.teacher.notifications', 'uses' => 'panel\TeacherController@notifications_index'])->middleware('permission:notify_teacher');
                Route::post('/', ['as' => 'panel.teacher.notifications.send', 'uses' => 'panel\TeacherController@send_notifications'])->middleware('permission:notify_teacher');
            });
            Route::prefix('/create')->group(function () {
                Route::get('/', ['as' => 'panel.teacher.create', 'uses' => 'panel\TeacherController@create'])->middleware('permission:add_teacher');
                Route::post('/', ['as' => 'panel.teacher.create', 'uses' => 'panel\TeacherController@store'])->middleware('permission:add_teacher');
            });
            Route::prefix('/edit')->group(function () {
                Route::get('/{user}', ['as' => 'panel.teacher.edit', 'uses' => 'panel\TeacherController@edit'])->middleware('permission:add_teacher');
                Route::put('/{user}', ['as' => 'panel.teacher.edit', 'uses' => 'panel\TeacherController@update'])->middleware('permission:add_teacher');
            });
            Route::get('/all', ['as' => 'panel.teacher.all', 'uses' => 'panel\TeacherController@index'])->middleware('permission:view_teacher');
            Route::get('/show/{id}', ['as' => 'panel.teacher.show', 'uses' => 'panel\TeacherController@show'])->middleware('permission:view_teacher');
            Route::get('/all/data', ['as' => 'panel.teacher.all.data', 'uses' => 'panel\TeacherController@get_data_table'])->middleware('permission:view_teacher');
            Route::get('/status/{id}', ['as' => 'panel.teacher.status', 'uses' => 'panel\TeacherController@change_status'])->middleware('permission:add_teacher');
            Route::patch('/activate/{id}', ['as' => 'panel.teacher.activate', 'uses' => 'panel\TeacherController@activate'])->middleware('permission:add_teacher');
            Route::delete('/delete/{teacher}', ['as' => 'panel.teacher.delete', 'uses' => 'panel\TeacherController@delete'])->middleware('permission:delete_teacher');
        });

        Route::prefix('/page')->group(function () {
            Route::get('/edit/{type}', ['as' => 'panel.page.edit', 'uses' => 'panel\PageController@index'])->middleware('permission:page_settings');
            Route::put('/edit/{type}', ['as' => 'panel.page.edit', 'uses' => 'panel\PageController@update_page'])->middleware('permission:page_settings');
            Route::prefix('/faq')->group(function () {
                Route::get('/', ['as' => 'panel.page.faq.index', 'uses' => 'panel\PageController@faqIndex']);
                Route::prefix('/create')->group(function () {
                    Route::get('/', ['as' => 'panel.page.faq.create.index', 'uses' => 'panel\PageController@createQuestionIndex']);
                    Route::post('/', ['as' => 'panel.page.faq.create.post', 'uses' => 'panel\PageController@storeQuestion']);
                });
                Route::prefix('/edit')->group(function () {
                    Route::get('/{id}', ['as' => 'panel.page.faq.edit.index', 'uses' => 'panel\PageController@editQuestionIndex']);
                    Route::post('/{id}', ['as' => 'panel.page.faq.edit.post', 'uses' => 'panel\PageController@editQuestion']);
                });
                Route::get('/data/{id}', ['as' => 'panel.page.faq.item', 'uses' => 'panel\PageController@getQuestionData']);
                Route::get('/data', ['as' => 'panel.page.faq.data', 'uses' => 'panel\PageController@getQuestionDataTable']);
                Route::delete('/delete/{id}', ['as' => 'panel.page.faq.delete', 'uses' => 'panel\PageController@deleteQuestion']);
            });
        });
        Route::group(['prefix' => 'promo-code', 'as' => 'panel.promo-code.'], function () {
            Route::group(['prefix' => 'create', 'as' => 'create.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'panel\PromoCodeController@create']);
                Route::post('/', ['as' => 'store', 'uses' => 'panel\PromoCodeController@store']);
            });
            Route::group(['prefix' => 'edit', 'as' => 'edit.'], function () {
                Route::get('/{id}', ['as' => 'index', 'uses' => 'panel\PromoCodeController@edit']);
                Route::post('/{id}', ['as' => 'update', 'uses' => 'panel\PromoCodeController@update']);
            });
            Route::group(['prefix' => 'all', 'as' => 'all.'], function () {
                Route::get('/', ['as' => 'index', 'uses' => 'panel\PromoCodeController@index']);
                Route::get('/data', ['as' => 'data', 'uses' => 'panel\PromoCodeController@getDataTable']);
            });
            Route::get('/change-status/{category}', ['as' => 'change-status', 'uses' => 'panel\PromoCodeController@changeStatus']);

            Route::delete('/{id}', ['as' => 'delete', 'uses' => 'panel\PromoCodeController@delete']);
        });

        Route::prefix('/template')->group(function () {
            Route::prefix('/settings')->group(function () {
                Route::get('/', ['as' => 'panel.template.settings', 'uses' => 'panel\TemplateController@settings'])->middleware('permission:template_settings');
                Route::put('/', ['as' => 'panel.template.settings', 'uses' => 'panel\TemplateController@update_settings'])->middleware('permission:template_settings');
            });
            Route::prefix('/socials')->group(function () {
                Route::get('/', ['as' => 'panel.template.socials', 'uses' => 'panel\TemplateController@socials'])->middleware('permission:template_settings');
                Route::put('/', ['as' => 'panel.template.socials', 'uses' => 'panel\TemplateController@update_socials'])->middleware('permission:template_settings');
            });
            Route::prefix('/main')->group(function () {
                Route::get('/', ['as' => 'panel.template.main', 'uses' => 'panel\TemplateController@main'])->middleware('permission:template_settings');
                Route::get('/data/{id}', ['as' => 'panel.template.main.data', 'uses' => 'panel\TemplateController@get_section_data'])->middleware('permission:template_settings');
                Route::post('/create', ['as' => 'panel.template.main.create', 'uses' => 'panel\TemplateController@create_section'])->middleware('permission:template_settings');
                Route::post('/edit/{id}', ['as' => 'panel.template.main.edit', 'uses' => 'panel\TemplateController@edit_section'])->middleware('permission:template_settings');
                Route::delete('/delete/{id}', ['as' => 'panel.template.main.delete', 'uses' => 'panel\TemplateController@delete_section'])->middleware('permission:template_settings');
            });
        });

        Route::prefix('/inbox/')->group(function () {
            Route::get('/all', 'panel\InboxController@index')->middleware('permission:view_inbox');
            Route::get('/view/{id}', 'panel\InboxController@view_msg')->middleware('permission:view_inbox');
            Route::post('/replay-msg/{id}', 'panel\InboxController@replay_msg')->middleware('permission:replay_inbox');
            Route::delete('/delete/{id}', 'panel\InboxController@delete_msg')->middleware('permission:delete_inbox');
            Route::post('/delete', 'panel\InboxController@delete')->middleware('permission:delete_inbox');
        });

        Route::prefix('/ticket/')->group(function () {
            Route::get('/all', 'panel\TicketController@index')->middleware('permission:view_ticket');
            Route::get('/view/{id}', 'panel\TicketController@show')->middleware('permission:view_ticket');
            Route::post('/replay/{id}', 'panel\TicketController@replay')->middleware('permission:delete_ticket');
            Route::post('/end/{id}', 'panel\TicketController@end')->middleware('permission:delete_ticket');
            Route::delete('/delete/{id}', 'panel\TicketController@deleteTicket')->middleware('permission:delete_ticket');
            Route::post('/delete', 'panel\TicketController@delete')->middleware('permission:delete_ticket');
            Route::post('/end', 'panel\TicketController@endTickets')->middleware('permission:delete_ticket');
        });

        Route::prefix('/blog')->group(function () {

            Route::get('/create', ['as' => 'panel.blog', 'uses' => 'panel\BlogController@create']);
            Route::post('/create', ['as' => 'panel.blog.post', 'uses' => 'panel\BlogController@store']);

            Route::get('/{id}/edit', ['as' => 'panel.blog.edit', 'uses' => 'panel\BlogController@edit']);
            Route::post('/{id}/edit', ['as' => 'panel.blog.edit', 'uses' => 'panel\BlogController@update']);

            Route::get('/all', ['as' => 'panel.blog.all', 'uses' => 'panel\BlogController@index']);
            Route::get('/all/data', ['as' => 'panel.blog.all.data', 'uses' => 'panel\BlogController@get_data_table']);

            Route::delete('/{id}', ['as' => 'panel.blog.delete', 'uses' => 'panel\BlogController@delete']);

            Route::prefix('/categories')->group(function () {
                Route::get('/', ['as' => 'panel.blog.categories', 'uses' => 'panel\BlogController@categories_index']);
                Route::post('/create', ['as' => 'panel.blog.categories.create', 'uses' => 'panel\BlogController@store_category']);
                Route::post('/edit/{id}', ['as' => 'panel.blog.categories.edit', 'uses' => 'panel\BlogController@edit_category']);
                Route::get('/data/{id}', ['as' => 'panel.blog.categories.item', 'uses' => 'panel\BlogController@get_category_data']);
                Route::get('/data', ['as' => 'panel.blog.categories.data', 'uses' => 'panel\BlogController@categories_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.blog.categories.delete', 'uses' => 'panel\BlogController@delete_category']);
            });
        });

        Route::prefix('/training/')->group(function () {
            Route::get('/all', 'panel\TrainingController@index')->middleware('permission:view_training_request');
            Route::get('/all/data', ['as' => 'panel.training.data', 'uses' => 'panel\TrainingController@getDataTable'])->middleware('permission:view_training_request');
            Route::get('/view/{id}', 'panel\TrainingController@view_msg')->middleware('permission:view_training_request');
            Route::get('/show/{id}', ['as' => 'panel.training.show', 'uses' => 'panel\TrainingController@showRequestDetails'])->middleware('permission:view_training_request');
            Route::get('/conversation/view/{id}', ['as' => 'panel.training.conversation', 'uses' => 'panel\TrainingController@view_conversation'])->middleware('permission:view_conversation_training_request');
        });

        Route::prefix('/conversation/')->group(function () {
            Route::get('/all', 'panel\ConversationController@index')->middleware('permission:view_conversation');
            Route::get('/all/data', ['as' => 'panel.conversation.data', 'uses' => 'panel\ConversationController@getDataTable'])->middleware('permission:view_conversation');
            Route::get('/view/{id}', 'panel\ConversationController@view_msg')->middleware('permission:view_conversation');
            Route::get('/view/{id}', ['as' => 'panel.conversation.view', 'uses' => 'panel\ConversationController@view_conversation'])->middleware('permission:view_conversation');
        });

        Route::prefix('/partners/')->group(function () {
            Route::get('/', ['as' => 'panel.partners.index', 'uses' => 'panel\PartnersController@index'])->middleware('permission:view_partner');
            Route::post('/create', ['as' => 'panel.partners.create', 'uses' => 'panel\PartnersController@store'])->middleware('permission:add_partner');
            Route::post('/edit/{id}', ['as' => 'panel.partners.edit', 'uses' => 'panel\PartnersController@edit'])->middleware('permission:add_partner');
            Route::get('/data/{id}', ['as' => 'panel.partners.item', 'uses' => 'panel\PartnersController@get_item_data'])->middleware('permission:view_partner');
            Route::get('/data', ['as' => 'panel.partners.data', 'uses' => 'panel\PartnersController@all_data_table'])->middleware('permission:view_partner');
            Route::delete('/delete/{id}', ['as' => 'panel.partners.delete', 'uses' => 'panel\PartnersController@delete'])->middleware('permission:delete_partner');
        });

        Route::prefix('/mail-list')->group(function () {
            Route::get('/', ['as' => 'panel.mail.index', 'uses' => 'panel\MaillistController@index'])->middleware('permission:view_mailing_list');
            Route::get('/create', ['as' => 'panel.mail.create', 'uses' => 'panel\MaillistController@create'])->middleware('permission:send_mailing_list');
            Route::get('/status/{mailItem}', ['as' => 'panel.mail.status', 'uses' => 'panel\MaillistController@changeStatus'])->middleware('permission:active_mailing_list');
            Route::get('/data', ['as' => 'panel.mail.data', 'uses' => 'panel\MaillistController@getDataTable'])->middleware('permission:view_mailing_list');
            Route::delete('/delete/{mailItem}', ['as' => 'panel.mail.delete', 'uses' => 'panel\MaillistController@destroy'])->middleware('permission:delete_mailing_list');
            Route::post('/send', ['as' => 'panel.mail.send', 'uses' => 'panel\MaillistController@sendEmailsToList'])->middleware('permission:send_mailing_list');
        });

        Route::prefix('/constant')->group(function () {
            Route::prefix('/countries')->group(function () {
                Route::get('/', ['as' => 'panel.constant.countries.index', 'uses' => 'panel\ConstantController@countries_index']);
                Route::post('/create', ['as' => 'panel.constant.countries.create', 'uses' => 'panel\ConstantController@store_country']);
                Route::post('/edit/{id}', ['as' => 'panel.constant.countries.edit', 'uses' => 'panel\ConstantController@edit_country']);
                Route::get('/data/{id}', ['as' => 'panel.constant.countries.item', 'uses' => 'panel\ConstantController@get_country_data']);
                Route::get('/data', ['as' => 'panel.constant.countries.data', 'uses' => 'panel\ConstantController@countries_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.constant.countries.delete', 'uses' => 'panel\ConstantController@delete_country']);
            });

            Route::prefix('/cities')->group(function () {
                Route::get('/', ['as' => 'panel.constant.cities.index', 'uses' => 'panel\ConstantController@cities_index']);
                Route::post('/create', ['as' => 'panel.constant.cities.create', 'uses' => 'panel\ConstantController@store_city']);
                Route::post('/edit/{id}', ['as' => 'panel.constant.cities.edit', 'uses' => 'panel\ConstantController@edit_city']);
                Route::get('/data/{id}', ['as' => 'panel.constant.cities.item', 'uses' => 'panel\ConstantController@get_city_data']);
                Route::get('/data', ['as' => 'panel.constant.cities.data', 'uses' => 'panel\ConstantController@cities_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.constant.cities.delete', 'uses' => 'panel\ConstantController@delete_city']);
            });

            Route::prefix('/specialities')->group(function () {
                Route::get('/', ['as' => 'panel.constant.specialities.index', 'uses' => 'panel\ConstantController@specialities_index']);
                Route::post('/create', ['as' => 'panel.constant.specialities.create', 'uses' => 'panel\ConstantController@store_speciality']);
                Route::post('/edit/{id}', ['as' => 'panel.constant.specialities.edit', 'uses' => 'panel\ConstantController@edit_speciality']);
                Route::get('/data/{id}', ['as' => 'panel.constant.specialities.item', 'uses' => 'panel\ConstantController@get_speciality_data']);
                Route::get('/data', ['as' => 'panel.constant.specialities.data', 'uses' => 'panel\ConstantController@specialities_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.constant.specialities.delete', 'uses' => 'panel\ConstantController@delete_speciality']);
            });

            Route::prefix('/sub-specialities')->group(function () {
                Route::get('/', ['as' => 'panel.constant.sub-specialities.index', 'uses' => 'panel\ConstantController@sub_specialities_index']);
                Route::post('/create', ['as' => 'panel.constant.sub-specialities.create', 'uses' => 'panel\ConstantController@store_sub_speciality']);
                Route::get('/change-select/{id}', ['as' => 'panel.constant.sub-specialities.change-select', 'uses' => 'panel\ConstantController@change_select']);
                Route::post('/edit/{id}', ['as' => 'panel.constant.sub-specialities.edit', 'uses' => 'panel\ConstantController@edit_sub_speciality']);
                Route::get('/data/{id}', ['as' => 'panel.constant.sub-specialities.item', 'uses' => 'panel\ConstantController@get_sub_speciality_data']);
                Route::get('/data', ['as' => 'panel.constant.sub-specialities.data', 'uses' => 'panel\ConstantController@sub_specialities_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.constant.sub-specialities.delete', 'uses' => 'panel\ConstantController@delete_sub_speciality']);
            });

            Route::prefix('/categories')->group(function () {
                Route::get('/', ['as' => 'panel.constant.categories.index', 'uses' => 'panel\ConstantController@categories_index']);
                Route::post('/create', ['as' => 'panel.constant.categories.create', 'uses' => 'panel\ConstantController@store_category']);
                Route::post('/edit/{id}', ['as' => 'panel.constant.categories.edit', 'uses' => 'panel\ConstantController@edit_category']);
                Route::get('/data/{id}', ['as' => 'panel.constant.categories.item', 'uses' => 'panel\ConstantController@get_category_data']);
                Route::get('/data', ['as' => 'panel.constant.categories.data', 'uses' => 'panel\ConstantController@categories_data']);
                Route::delete('/delete/{id}', ['as' => 'panel.constant.categories.delete', 'uses' => 'panel\ConstantController@delete_category']);
            });

        });

        Route::prefix('/role')->group(function () {
            Route::get('/create', 'panel\RoleController@create')->middleware('permission:add_role');
            Route::post('/create', 'panel\RoleController@store')->middleware('permission:add_role');
            Route::get('/{id}/edit', 'panel\RoleController@edit')->middleware('permission:add_role');
            Route::post('/{id}/edit', 'panel\RoleController@store')->middleware('permission:add_role');
            Route::get('/all', 'panel\RoleController@index')->middleware('permission:view_role');
            Route::get('/all/data', 'panel\RoleController@roles_data_table')->middleware('permission:view_role');
            Route::delete('/{id}', 'panel\RoleController@delete')->middleware('permission:delete_role');
        });


        Route::prefix('/account')->group(function () {
            Route::get('/create', 'panel\AdminController@create')->middleware('permission:add_admin');
            Route::post('/create', 'panel\AdminController@store')->middleware('permission:add_admin');
            Route::get('/{id}/edit', 'panel\AdminController@edit')->middleware('permission:add_admin');
            Route::post('/edit/{id}', 'panel\AdminController@update')->middleware('permission:add_admin');
            Route::get('/all', 'panel\AdminController@index')->middleware('permission:view_admin');
            Route::get('/all/data', 'panel\AdminController@admins_data_table')->middleware('permission:view_admin');
            Route::delete('/{id}', 'panel\AdminController@delete')->middleware('permission:delete_admin');
        });

        Route::prefix('/payment/')->group(function () {
            Route::prefix('/credit/')->group(function () {
                Route::get('/', ['as' => 'panel.payment.credit', 'uses' => 'panel\PaymentController@credit_index'])->middleware('permission:view_credit_financial_process');
                Route::get('/data', ['as' => 'panel.payment.credit.data', 'uses' => 'panel\PaymentController@credit_data'])->middleware('permission:view_credit_financial_process');
            });

            Route::prefix('/withdraw/')->group(function () {
                Route::get('/', ['as' => 'panel.payment.withdraw', 'uses' => 'panel\PaymentController@withdraw_index'])->middleware('permission:view_withdraw_financial_process');
                Route::get('/data', ['as' => 'panel.payment.withdraw.data', 'uses' => 'panel\PaymentController@withdraw_data'])->middleware('permission:view_withdraw_financial_process');

                Route::post('/accept/{id}', ['as' => 'panel.payment.withdraw.accept', 'uses' => 'panel\PaymentController@accept_withdraw'])->middleware('permission:accept_withdraw_financial_process');
                Route::post('/reject/{id}', ['as' => 'panel.payment.withdraw.reject', 'uses' => 'panel\PaymentController@reject_withdraw'])->middleware('permission:accept_withdraw_financial_process');
                Route::delete('/delete/{id}', ['as' => 'panel.payment.withdraw.delete', 'uses' => 'panel\PaymentController@delete_withdraw'])->middleware('permission:accept_withdraw_financial_process');
            });
        });

        Route::prefix('/delete-request/')->group(function () {
            Route::get('/all', ['as' => 'panel.delete-request.all', 'uses' => 'panel\DeleteAccountController@index'])->middleware('permission:view_delete_request');
            Route::get('/all/data', ['as' => 'panel.delete-request.all.data', 'uses' => 'panel\DeleteAccountController@getDataTables'])->middleware('permission:view_delete_request');
            Route::post('/accept/{id}', ['as' => 'panel.delete-request.accept', 'uses' => 'panel\DeleteAccountController@accept'])->middleware('permission:accept_delete_request');
            Route::post('/reject/{id}', ['as' => 'panel.delete-request.reject', 'uses' => 'panel\DeleteAccountController@reject'])->middleware('permission:accept_delete_request');
        });

        Route::prefix('/report/')->group(function () {
            Route::get('/all', ['as' => 'panel.report.index', 'uses' => 'panel\ReportController@reportIndex'])->middleware('permission:view_report');
            Route::get('/all/data', ['as' => 'panel.report.data', 'uses' => 'panel\ReportController@reportDataTable'])->middleware('permission:view_report');
            Route::post('/accept/{id}', ['as' => 'panel.report.accept', 'uses' => 'panel\ReportController@accept'])->middleware('permission:view_report');
            Route::post('/reject/{id}', ['as' => 'panel.report.reject', 'uses' => 'panel\ReportController@reject'])->middleware('permission:view_report');
        });


        Route::prefix('/course/')->group(function () {
            Route::prefix('/create')->group(function () {
                Route::get('/', ['as' => 'panel.course.create', 'uses' => 'panel\CourseController@create'])->middleware('permission:add_course');
                Route::post('/', ['as' => 'panel.course.create', 'uses' => 'panel\CourseController@store'])->middleware('permission:add_course');
            });
            Route::prefix('/edit')->group(function () {
                Route::get('/{id}', ['as' => 'panel.course.edit', 'uses' => 'panel\CourseController@edit'])->middleware('permission:add_course');
                Route::put('/{id}', ['as' => 'panel.course.edit', 'uses' => 'panel\CourseController@update'])->middleware('permission:add_course');
            });
            Route::delete('/delete/{id}', ['as' => 'panel.course.delete', 'uses' => 'panel\CourseController@delete'])->middleware('permission:delete_course');
            Route::delete('/requests/delete/{id}', ['as' => 'panel.course.requests.delete', 'uses' => 'panel\CourseController@delete_join_request'])->middleware('permission:delete_course');

            Route::get('/all', ['as' => 'panel.course.all', 'uses' => 'panel\CourseController@index'])->middleware('permission:view_course');
            Route::get('/all/data', ['as' => 'panel.course.all.data', 'uses' => 'panel\CourseController@get_data_table'])->middleware('permission:view_course');

            Route::get('/requests', ['as' => 'panel.course.requests', 'uses' => 'panel\CourseController@requests_index'])->middleware('permission:view_course');
            Route::get('/requests/data', ['as' => 'panel.course.requests.data', 'uses' => 'panel\CourseController@requests_data_table'])->middleware('permission:view_course');

        });
    });
});
