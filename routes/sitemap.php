<?php
Route::get('/sitemap.xml', ['as' => 'index', 'uses' => 'SitemapController@index']);

Route::group(['prefix' => 'sitemap'], function () {
    Route::get('/trainers', ['as' => 'trainers', 'uses' => 'SitemapController@trainers']);
    Route::get('/courses', ['as' => 'courses', 'uses' => 'SitemapController@courses']);
    Route::get('/pages', ['as' => 'pages', 'uses' => 'SitemapController@pages']);
    Route::get('/blogs', ['as' => 'blogs', 'uses' => 'SitemapController@blogs']);
});