<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Pusher\Pusher;
use Pusher\PusherException;

class UserNotification extends Notification
{
    use Queueable;

    public $from_id;
    public $type;
    public $notification;
    public $notification_en;
    public $url;
    public $created_at;


    public function __construct($source, $type, $text, $text_en, $url)
    {
        $this->from_id = $source;
        $this->type = $type;
        $this->notification = $text;
        $this->notification_en = $text_en;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $options = array(
            'cluster' => 'us2',
            'encrypted' => true
        );
        $pusher = new Pusher(
            'd8e93d92ea33ee7289d4',
            '49e0a36d64966e782b44',
            '532742',
            $options
        );
        $data['from_id'] = $this->from_id;
        $data['type'] = $this->type;
        $data['notification'] = $this->notification;
        $data['notification_en'] = $this->notification_en;
        $data['url'] = $this->url;
//        $data['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
        $notifiable->sendNotification([
            'title' => ($notifiable->locale == 'ar') ? 'وصلك إشعار جديد' : 'You got a new notification.' ,
            'message' =>($notifiable->locale == 'ar') ? $this->notification : $this->notification_en,
            'type'=> $this->type,
            'from_id'=> $this->from_id
        ]);
        try {
            $pusher->trigger('notify_' . $notifiable->id, 'new-notification', $data);
        } catch (PusherException $e) {

        }
        return ['database'];
    }


    public function toMail($notifiable)
    {
        return (new MailMessage)->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */


    public function toArray($notifiable)
    {
        return [
            'from_id' => $this->from_id,
            'type' => $this->type,
            'text' => $this->notification,
            'text_en' => $this->notification_en,
            'url' => $this->url,
//            'created_at' => $this->created_at,
        ];
    }
}
