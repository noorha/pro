<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TrainingNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public  $title ;
    public  $text ;
    public  $btn_text ;
    public  $link ;
    public function __construct($title,$text,$btn_text,$link)
    {
        $this->title = $title;
        $this->text = $text;
        $this->btn_text = $btn_text;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->title;
        $text = $this->text;
        $btn_text = $this->btn_text;
        $link = $this->link;
        return (new MailMessage)->from('info@halapro.com', ' HalaPro')->subject($this->title)->view('mail.notification', compact(['link','title','text','btn_text']));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
