<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class ResetMail extends Notification
{
    use Queueable;

    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = $notifiable;
        $link = lang_url('reset/'.$user->link('password')->first()->link);
        return (new MailMessage)->from('info@halapro.com', ' HalaPro')->subject($notifiable->localeText('إستعادة كلمة المرور الخاصة بك ','Password Recovery Message'))->view('mail.reset', compact(['link','user']));
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
