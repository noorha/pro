<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ConfirmMail extends Notification
{
    use Queueable;


    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = $notifiable->mobileVerification('confirm')->link;
//        $link = url('/confirm-email/'.$this->user->link('confirm')->first()->link);
        $user = $notifiable;
        return (new MailMessage)->from('info@halapro.com', ' HalaPro')->subject($notifiable->isLocaleAr() ? 'تفعيل الحساب' : 'Activate account')->view('mail.confirm', compact(['link','user']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
