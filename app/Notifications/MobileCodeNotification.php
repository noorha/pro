<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MobileCodeNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $code ;
    public $type ;
    public function __construct($code , $type)
    {
        $this->code = $code;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $title = $this->type == 'confirm' ? $notifiable->localeText('رسالة تفعيل الحساب - HalaPro','Activate account - HalaPro') : $notifiable->localeText('بريد إعادة تعيين كلمة المرور HalaPro ','Password Reset - HalaPro') ;
        $code = $this->code;
        $user = $notifiable;
        return (new MailMessage)->from('info@halapro.com', ' HalaPro')->subject($title)->view('mail.mobile', compact(['code','user']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
