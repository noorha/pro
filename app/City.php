<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'name_en', 'country_id'
    ];


    protected $hidden = [
        'country_id',  'name', 'name_en',  'created_at', 'updated_at', 'deleted_at',
    ];

    protected $appends = ['text'];

    /**
     * @return string
     */
    public function getTextAttribute()
    {
        return get_text_locale($this,'name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return isset($this->country) ? get_text_locale($this->country,'name') : no_data();
    }


}
