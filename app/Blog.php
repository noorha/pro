<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Blog extends Model
{

    protected $fillable = [
        'admin_id', 'locale', 'title', 'text', 'image', 'link', 'views',
    ];

    public function categories()
    {
        return $this->belongsToMany(BlogCategory::class, 'category_blogs', 'blog_id', 'category_id', 'id', 'id');
    }

    public function categoryName()
    {
        $text = '';
        foreach ($this->categories as $i => $category) {
            $text .= (($i == 0) ? '' : ' , ') . locale_value($category->text_ar, $category->text_en);
        }
        return $text;
    }

    public function scopeLocale($q)
    {
        return $q->where('locale', get_current_locale());
    }

    public function incrementViews()
    {
        return $this->update(['views' => (intval($this->views) + 1)]);
    }

    public function scopeSearch($q, Request $request,$id)
    {
        if ($id && !empty($id)) {
            $category = $id;
            $q = $q->locale()->whereHas('categories', function ($query) use ($category) {
                $query->where('id', $category);
            })->orderBy('created_at', 'DESC');
        }
        if (isset($request->q) && !empty($request->q)) {
            $q = $q->where('title', 'LIKE', $request->q)->orWhere('text', 'LIKE', $request->q);
        }
        return $q;
    }

    public function scopeOrderByMostViews($q)
    {
        return $q->orderBy('views', 'DESC');
    }


    public function relatedPosts()
    {
        $categories = $this->categories()->pluck('id')->toArray();
        return self::locale()->whereHas('categories', function ($q) use ($categories) {
            $q->whereIn('id', $categories);
        })->orderBy('created_at', 'DESC');
    }
}
