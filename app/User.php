<?php

namespace App;

use App\Logic\Whiteboard\ScheduleClass;
use App\Notifications\MobileCodeNotification;
use App\Notifications\TrainingNotification;
use App\Notifications\UserNotification;
use App\Traits\MobileNotification;
use App\Traits\Sms;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Laravel\Passport\HasApiTokens;
use Mpociot\Firebase\SyncsWithFirebase;


class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Sms, MobileNotification, SyncsWithFirebase;
    protected $database;

    protected $firebase;

    public function __construct(array $attributes = [])
    {

        $serviceAccount = ServiceAccount::fromJsonFile(app_path('Firebase/halapronew-firebase-adminsdk-quqwv-b8a076f72f.json'));
        $firebase = (new Factory())->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://halatest.firebaseio.com')->create();
        $this->database = $firebase->getDatabase();
        parent::__construct($attributes);
    }

    protected $attributes = ['id' => 0];

    protected $fillable = [
        'type', 'status', 'first_name', 'last_name', 'email', 'gender', 'birth_date', 'mobile', 'description', 'email', 'password', 'locale', 'country_id', 'nationality_id', 'city_id', 'address', 'is_public', 'is_profile_completed', 'uri_link', 'photo', 'rating', 'is_mobile_verified', 'is_mobile_verified',
        'longitude', 'latitude', 'device_token', 'whiteboard_teacher_id','provider_id','provider'
    ];

    protected $touches = ['wallet'];

    protected $hidden = [
        'whiteboard_teacher_id', 'is_profile_completed', 'uri_link', 'birth_date', 'is_public', 'trainingType', 'status', 'password', 'remember_token', 'device_token', 'updated_at', 'deleted_at',
    ];

    protected $appends = [
        'pending_cash', 'name', 'is_online', 'is_interview', 'online_price', 'interview_price', 'photoUrl', 'profileUrl'
    ];


    public function syncWithFirebase()
    {
        $userFirebase = $this->database->getReference('/users/' . $this->id)->update([
            'id' => $this->id,
            'fullName' => $this->name,
            'avatarURL' => image_url($this->photo, '100x100'),
            'originalAvatarURL' => image_url($this->photo),
            'lastSeen' => (string)Carbon::now(),
            'isOnline' => 0,
            'type' => $this->type,
            'fcmToken' => $this->device_token,
            'createdAt' =>(float) parse_to_carbon($this->created_at)->getTimestamp(),
            'updatedAt' => (float) parse_to_carbon($this->updated_at)->getTimestamp(),
        ]);
        return $userFirebase;
    }

    public function makeOnline()
    {
        $userFirebase = $this->database->getReference('/users/' . $this->id)->set([
            'lastSeen' => (float) parse_to_carbon(Carbon::now())->getTimestamp(),
            'isOnline' => 1,
        ]);
        return $userFirebase;
    }

    public function makeOffline()
    {
        $userFirebase = $this->database->getReference('/users/' . $this->id)->set([
            'isOnline' => 0,
        ]);
        return $userFirebase;
    }

    public function getChat($type, $id)
    {
        return $this->createChat($type, $id);
    }


    public function getProfileUrlAttribute()
    {
        return lang_route('profile', [$this->id]);
    }

    public function getPendingCashAttribute()
    {
        return $this->walletPendingCash();
    }

    public static function create(array $data)
    {
        $instance = new self;
        $instance->fill($data);
        if(isset($data['password']))
        $instance->password = bcrypt($data['password']);
        $instance->save();
        $instance->syncWithFirebase();
        return $instance;
    }

    public function scopeScheduledMessages($q, Request $request)
    {
        $q = $q->active()->where(function ($query) use ($request) {
            if (isset($request->gender) && !empty($request->gender) && (int)$request->gender != 0) {
                $query->where('gender', $request->gender);
            }
            if (isset($request->account_type) && !empty($request->account_type) && (int)$request->account_type != 0) {
                $query->where('type', $request->account_type);
            }
        })->orWhere(function ($query) use ($request) {
            $query->whereIn('id', $request->users);
        });
        return $q;
    }

    public function isRegisteredInWhiteboard()
    {
        return isset($this->whiteboard_teacher_id);
    }


    public function getIsOnlineAttribute()
    {
        return $this->isWorkOnline() ? 1 : 0;
    }


    public function registerTeacherInWhiteboard()
    {
        $class = new ScheduleClass();
        if ($this->isTeacher()) {
            $response = $class->addTeacher($this);
            if (isset($response) && $response['status']) {
                $this->update(['whiteboard_teacher_id' => $response['teacher_id']]);
                return true;
            }
        }
        return false;
    }


    public function isProfileCompleted()
    {
        return (int)$this->is_profile_completed == 1;
    }

    public function getDefaultAvatar()
    {
        return $this->isMale() ? 'male-avatar.png' : 'female-avatar.png';
    }

    public function getIsInterviewAttribute()
    {
        return $this->isWorkInterview() ? 1 : 0;
    }

    public function getOnlinePriceAttribute()
    {
        return $this->isWorkOnline() ? (double)get_currency_value($this->onlinePrice()) : 0;
    }

    public function getPhotoUrlAttribute()
    {
        return image_url($this->getAvatar($this->photo));
    }


    public function getPhotoAttribute($photo)
    {
        return $this->getAvatar($photo);
    }

    public function getAvatar($photo)
    {
        return isset($photo) && !empty($photo) && $photo != 'default.png' ? $photo : $this->getDefaultAvatar();
    }

    public function getNameAttribute()
    {
        return $this->getUserName();
    }

    public function getInterviewPriceAttribute()
    {
        return $this->isWorkInterview() ? (double)get_currency_value($this->interviewPrice()) : 0;
    }

//    public function syncWithFirebase()
//    {
//        $userFirebase = $this->database->getReference('/users/' . $this->id)->update([
//            'id' => $this->id,
//            'fullName' => $this->name,
//            'avatarURL' => image_url($this->photo, '100x100'),
//            'originalAvatarURL' => image_url($this->photo),
//            'lastSeen' => (string)Carbon::now(),
//            'isOnline' => 0,
//            'type' => $this->type,
//            'createdAt' => (string)$this->created_at,
//            'updatedAt' => (string)$this->updated_at,
//        ]);
//        return $userFirebase;
//    }

//    public function makeOnline()
//    {
//        $userFirebase = $this->database->getReference('/users/' . $this->id)->set([
//            'lastSeen' => (string)Carbon::now(),
//            'isOnline' => 1,
//        ]);
//        return $userFirebase;
//    }
//
//    public function makeOffline()
//    {
//        $userFirebase = $this->database->getReference('/users/' . $this->id)->set([
//            'isOnline' => 0,
//        ]);
//        return $userFirebase;
//    }

    /**
     * @return mixed
     */
    public function nonPaidTrainingRequests()
    {
        return $this->teacherTrainingRequests()->nonPaidCash();
    }

    public function totalNonPaid()
    {
        return $this->nonPaidTrainingRequests()->sum('total');
    }

    public function totalNonPaidProfit()
    {
        return get_profit($this->totalNonPaid(), true);
    }


    public function canAcceptCashRequest()
    {
        return $this->nonPaidTrainingRequests()->count() < 3;
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['birth_date'])->age;
    }

    public function updateAge()
    {
        return $this->update(['age' => $this->getAgeAttribute()]);
    }

    public function getCountryCode()
    {
        return (isset($this->place)) ? $this->place->code : '';
    }

    public function place()
    {
        return $this->belongsTo('App\Country', 'nationality_id', 'id');

    }

    public function userinfo()
    {
        return $this->belongsTo('App\UserInfo', 'id', 'user_id');
    }

    public function userinfo_file()
    {
        return $this->belongsToMany(UserInfoFile::class, 'user_information_file', 'user_id', 'id')->withPivot('file');
    }

    public function hasUserFile($id)
    {
        return isset($this->userinfo_file()->find($id)->id);
    }

    public function getUserFile($id)
    {
        return $this->hasUserFile($id) ? $this->userinfo_file()->find($id)->pivot->file : '';
    }


    public function getMobileNo()
    {
        return $this->getCountryCode() . $this->mobile;

    }


    public function createWithdrawRequest(Request $request)
    {
        if ($this->walletAvailableCash() >= $request->value) {
            $process = $this->createWithdrawnProcess($request->value, 'website', 0, 1, false);
            $process->createWithdrawRequest($request);
            $this->notify(new UserNotification($this->id, 'user', 'تم تحويل طلب السحب الخاص بحسابك بقيمة (' . $request->value . ' ) دولار.'
                , __('تم تحويل طلب السحب الخاص بحسابك بقيمة') . ' ( ' . $request->value . '$ )', lang_route('profile.balance')));
            return true;
        }
        return false;
    }

    public function getFullLocation()
    {
        $country = (isset($this->country) && !empty($this->country->name)) ? get_text_locale($this->country, 'name') : '';
        $city = (isset($this->city) && !empty($this->city->name)) ? ' - ' . get_text_locale($this->city, 'name') : '';
        $address = (isset($this->address) && !empty($this->address)) ? ' - ' . $this->address : '';
        return $country . $city . $address;
    }

    public function getUserName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function getCountryName()
    {
        return isset($this->country) ? get_text_locale($this->country, 'name') : no_data();
    }

    public function getCityName()
    {
        return isset($this->city) ? get_text_locale($this->city, 'name') : no_data();
    }

    public function getCountryFlag()
    {
        return isset($this->country) ? image_url($this->country->icon, '20x20') : no_data();
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }

    public function getCountryCities()
    {
        if (isset($this->country) && isset($this->country->cities)) {
            return $this->country->cities;
        }
        return collect([]);
    }

    public function createLink($type)
    {
        if (isset($this->link($type)->first()->link)) {
            return $this->link($type);
        } else {
            Link::create([
                'user_id' => $this->id,
                'type' => $type,
                'email' => $this->email,
                'link' => mt_rand(100000, 999999),
                'is_mobile' => 0
            ]);
        }
    }

    public function link($type, $is_mobile = 0)
    {
        return $this->hasOne('App\Link', 'user_id', 'id')->where(['type' => $type, 'is_mobile' => $is_mobile]);
    }

    public function scopeStudents($query)
    {
        return $query->where('type', '1');
    }

    public function scopeTeachers($query)
    {
        return $query->where('type', '2');
    }

    public function scopeActive($query)
    {
        return $query->where(function ($q) {
            $q->where('status', 'active')->orWhere('is_mobile_verified', 1);
        });
    }

    public function scopeProfileCompleted($q)
    {
        return $q->where('is_profile_completed', 1);
    }

    public function isStudent()
    {
        return ($this->type == '1');
    }

    public function isTeacher()
    {
        return ($this->type == '2');
    }

    public function isMale()
    {
        return ($this->gender == '1');
    }

    public function isFemale()
    {
        return ($this->gender == '2');
    }

    public function removeActivationLink()
    {
        $link = $this->link('confirm');
        return (isset($link)) ? $link->delete() : false;
    }

    public function updateApiSpecialities($json)
    {
        $array = json_decode(isset($json) ? $json : '');
        if (isset($array) && count($array) > 0) {
            if (isset($this->userSubSpecialties)) {
                $this->userSubSpecialties()->delete();
            }
            foreach ($array as $item) {
                $this->createApiUserSpeciality($item);
            }
            return true;
        }
        return false;
    }


    public function updateSpecialities($array)
    {
        if (isset($array) && count($array) > 0) {
            if (isset($this->userSubSpecialties)) {
                $this->userSubSpecialties()->delete();
            }
            foreach ($array as $item) {
                $this->createUserSpeciality($item);
            }
            return true;
        }
        return false;
    }


    public function userArray($hasDays = false, $hasType = false)
    {
        $data = [
            'id' => (int)$this->id,
            'type' => (int)$this->type,
            'status' => (string)$this->status,
            'first_name' => (string)$this->first_name,
            'last_name' => (string)$this->last_name,
            'email' => (string)$this->email,
            'gender' => (int)$this->gender,
            'birth_date' => (string)$this->birth_date,
            'address' => (string)$this->address,
            'description' => (string)strip_tags($this->description),
            'country_id' => (int)$this->country_id,
            'nationality_id' => (int)$this->nationality_id,
            'city_id' => (int)$this->city_id,
            'rating' => (int)$this->rating,
            'mobile' => (string)$this->mobile,
            'profileUrl' => (string)$this->profileUrl,
            'photoUrl' => (string)image_url($this->photo),
            'photo' => (string)$this->photo,
            'is_public' => (int)$this->is_public,
            'longitude' => (int)$this->longitude,
            'latitude' => (int)$this->latitude,
            'created_at' => (string)$this->created_at,
            'country' => (isset($this->country)) ? $this->country : null,
            'nationality' => (isset($this->place)) ? $this->place : null,
            'city' => (isset($this->city)) ? $this->city : null,
            'is_profile_completed' => (int)$this->is_profile_completed,
            'is_mobile_verified' => (int)$this->is_mobile_verified,
            'total_cash' => (double)get_currency_value($this->walletTotalCash()),
            'available_cash' => (double)get_currency_value($this->walletAvailableCash()),
            'pending_cash' => (double)get_currency_value($this->pending_cash),
        ];
        if ($this->isTeacher()) {
            $data['nonPaid'] = get_currency_value($this->totalNonPaidProfit());
            $data['rating'] = $this->rating;
            $data['profileURL'] = lang_route('profile', [$this->id]);
            $data['ratings_count'] = $this->teacherRatings()->count();
            if ($hasDays) {
                $data['days'] = $this->days;
            }
            if ($hasType && isset($this->trainingType)) {
                $data['is_online'] = $this->trainingType->is_online;
                $data['is_interview'] = $this->trainingType->is_interview;
                $data['online_price'] = get_currency_value($this->trainingType->online_price);
                $data['interview_price'] = get_currency_value($this->trainingType->interview_price);
                $data['online_price_dolar'] = ($this->trainingType->online_price);
                $data['interview_price_dolar'] = ($this->trainingType->interview_price);
            }
        }
        return $data;
    }

    public function userSubSpecialties()
    {
        return $this->hasMany('App\UserSubSpecialty', 'user_id', 'id');
    }


    public function trainingType()
    {
        return $this->hasOne('App\TeacherTrainingType', 'user_id', 'id');
    }

    public function subSpecialties()
    {
        return $this->belongsToMany('App\SubSpecialty', 'user_sub_specialties', 'user_id', 'sub_id');
    }

    public function specialties()
    {
        return $this->belongsToMany('App\Speciality', 'user_sub_specialties', 'user_id', 'specialty_id');
    }


//    public function days()
//    {
//        return $this->hasMany('App\TrainingTime', 'user_id', 'id');
//    }

    public function trainingTimes()
    {
        return $this->hasMany('App\TrainingTime', 'user_id', 'id');
    }


    public function days()
    {
        return $this->belongsToMany(Day::class, 'training_times', 'user_id', 'day_id');
    }

    public function hasSpeciality($id)
    {
        $item = $this->specialties()->find($id);
        return isset($item);
    }

    public function hasSubSpeciality($id)
    {
        $item = $this->subSpecialties()->find($id);
        return isset($item);
    }


    public function createApiUserSpeciality($item)
    {
        if (isset($item->specialty_id) && isset($item->sub_id)) {
            return UserSubSpecialty::create([
                'user_id' => $this->id,
                'specialty_id' => $item->specialty_id,
                'sub_id' => $item->sub_id,
            ]);
        }
        return null;
    }

    public function updateApiDays($json)
    {
        $array = json_decode(isset($json) ? $json : '');
        return (isset($array) && count($array) > 0) ? $this->days()->sync($array) : false;
    }

    public function createUserSpeciality($json_string)
    {
        $object = json_decode($json_string);
        return UserSubSpecialty::create([
            'user_id' => $this->id,
            'specialty_id' => $object->specialty_id,
            'sub_id' => $object->sub_id,
        ]);
    }

    public function createDayTrainingTimes($day, $start_array, $end_array)
    {
        foreach ($start_array as $i => $item) {
            TrainingTime::create([
                'user_id' => $this->id,
                'day_id' => $day,
            ]);
        }
        return true;
    }

    public function updateTrainingTimes(Request $request)
    {
        $days = convert_array_map_int($request->days);
        if (isset($days) && count($days) > 0) {
            $this->days()->sync($days);
//            $this->trainingTimes()->delete();
//            foreach ($days as $i => $day) {
//                if ($request->has('start_' . $day) && $request->has('end_' . $day)) {
//                    $this->createDayTrainingTimes($day, $request->get('start_' . $day), $request->get('end_' . $day));
//                }
//            }
        }
        return true;
    }

    public function hasTrainingType()
    {
        return isset($this->trainingType);
    }


    public function updateOrCreateTrainingType($request)
    {
        return isset($request->training_type) ? TeacherTrainingType::updateOrCreate(['user_id' => $this->id], ['is_online' => in_array('1', $request->training_type), 'is_interview' => in_array('2', $request->training_type), 'online_price' => ($request->online_price), 'interview_price' => ($request->interview_price),]) : false;
    }

    public function updateApiTrainingInfo(Request $request)
    {
        $array = json_decode(isset($request->training_type) ? $request->training_type : '');
        return isset($array) && count($array) > 0 ? TeacherTrainingType::updateOrCreate(['user_id' => $this->id], ['is_online' => in_array(1, $array), 'is_interview' => in_array(2, $array), 'online_price' => $request->online_price, 'interview_price' => $request->interview_price,]) : false;

    }

    public function isMyProfile()
    {
        return (is_auth() && ($this->id == auth()->user()->id));
    }

    public function isWorkOnline()
    {
        return ($this->hasTrainingType() && $this->trainingType->is_online == 1);
    }

    public function isWorkInterview()
    {
        return ($this->hasTrainingType() && $this->trainingType->is_interview == 1);
    }

    public function interviewPrice()
    {
        return ($this->hasTrainingType()) ? $this->trainingType->interview_price : '';
    }

    public function onlinePrice()
    {
        return ($this->hasTrainingType()) ? $this->trainingType->online_price : '';
    }

    public function availableDays()
    {
        return $this->trainingTimes->map(function ($item) {
            return $item->day;
        })->unique();
    }

    public function hasDay($day)
    {
        $item = $this->trainingTimesByDay($day)->first();
        return isset($item);
    }

    public function trainingTimesByDay($day)
    {
        return $this->trainingTimes()->where('day_id', $day);
    }

    public function scopeSearchByDayIds($query, $array)
    {
        return $query->whereHas('days', function ($relation_query) use ($array) {
            $relation_query->whereIn('id', $array);
        });
    }

    public function scopeSearchBySpecialities($query, $spec)
    {
        return $query->whereHas('specialties', function ($relation_query) use ($spec) {
            $relation_query->where('specialty_id', (int)$spec);
        });
    }

    public function scopeSearchByTrainingType($query, $array)
    {
        $online = 0;
        $interview = 0;
        foreach ($array as $type) {
            if ($type == 1) {
                $online = 1;
            } else {
                $interview = 1;
            }
        }
        $query->whereHas('trainingType', function ($relation_query) use ($online, $interview) {
            $relation_query->where(function ($q) use ($online, $interview) {
                $q->orWhere('is_online', $online)->orWhere('is_interview', $interview);
            });
        });
        return $query;
    }

    public function scopeSearchByTrainingPrice($query, $array)
    {
        $array = array_map(function ($value) {
            return round(convert_to_USD($value), 1);
        }, $array);
        $query->whereHas('trainingType', function ($relation_query) use ($array) {
            $relation_query->whereBetween('online_price', $array)->orWhereBetween('interview_price', $array);
        });
        return $query;
    }


    public function searchByMobile(Request $request)
    {
        return $this->where(['email' => $request->email,])->first();
//        return $this->where(['country_id' => $request->country_id, 'mobile' => $request->mobile])->first();
    }

    public function scopeSearchTeachers($query, Request $request)
    {
        if ($request->has('sub_specialty') && !empty($request->sub_specialty)) {
            $value = $request->sub_specialty;
            session()->put(['sub_specialty' => $request->sub_specialty]);
            $query->whereHas('subSpecialties', function ($relation_query) use ($value) {
                $relation_query->where('sub_id', $value);
            });
        }
        if (isset($request->membership) && !empty($request->membership)) {
            $intArray = convert_array_map_int($request->membership);
            $query->where(function ($q) use ($intArray) {
                if (in_array(1, $intArray)) {
                    $q->whereBetween('rating', [4.6, 6]);
                }
                if (in_array(2, $intArray)) {
                    $q->orWhereBetween('rating', [3.5, 4.5]);
                }
                if (in_array(3, $intArray)) {
                    $q->orWhereBetween('rating', [0, 3.4]);
                }
            });
        }

        if ($request->has('subSpecialty') && !empty($request->subSpecialty)) {
            $value = $request->subSpecialty;
            session()->put(['subSpecialty' => $value]);
            $query->whereHas('subSpecialties', function ($relation_query) use ($value) {
                $relation_query->where('sub_id', $value);
            });
        }

        if ($request->has('country') && !empty($request->country)) {
            $query->where('country_id', $request->country);
        }

        if ($request->has('city_id') && !empty($request->city_id)) {
            $query->where('city_id', $request->city_id);
        }
//        if ($request->has('age_range') && !empty($request->age_range)) {
//            $query->select()->whereBetween(DB::raw('floor(datediff(curdate(),birth_date)/365)'), array_map_int($request->age_range));
//        }
        if ($request->has('price_range') && !empty($request->price_range)) {
            $query->searchByTrainingPrice(array_map_int($request->price_range));
        }

        if ($request->has('specialities') && !empty($request->specialities)) {
            $query->searchBySpecialities($request->specialities);
        }

        if ($request->has('days') && !empty($request->days)) {
            $days = is_array($request->days) ? convert_array_map_int($request->days) : json_decode($request->days);
            $query->searchByDayIds($days);
        }

        if ($request->has('type') && !empty($request->type)) {
            $types = is_array($request->type) ? convert_array_map_int($request->type) : json_decode($request->type);
            $query->searchByTrainingType($types);
        }

        if ($request->has('teacher') && !empty($request->teacher)) {
            $query->where(DB::raw('concat(first_name," ",last_name)'), 'LIKE', "%$request->teacher%");
        }

        if ($request->has('gender') && !empty($request->gender)) {
            $genders = is_array($request->gender) ? convert_array_map_int($request->gender) : json_decode($request->gender);
            $query->whereIn('gender', $genders);
        }
        if ($request->has('rating') && !empty($request->rating)) {
            $query = $query->where('rating', '>=', $request->rating);
        }

        $query = ($request->order_by == 'common') ? $query->withCount('teacherTrainingRequests')->orderBy('teacher_training_requests_count', 'DESC') : $query->orderBy('created_at', 'DESC');
        return $query->where('is_profile_completed', 1);
    }


    public function scopeFilter($query, Request $request)
    {

        if (isset($request->gender) && !empty($request->gender)) {
            $query->where('gender', $request->gender);
        }

        if (isset($request->country_id) && !empty($request->country_id)) {
            $query->where('country_id', $request->country_id);
        }

        if (isset($request->text) && !empty($request->text)) {
            $query->where(DB::raw('concat(first_name," ",last_name)'), 'LIKE', "%$request->text%");
        }

    }

    public function isPublic()
    {
        return ($this->is_public == 1);
    }

    public function createTrainingRequest(Request $request)
    {
        if (isset($request->coupon_code) && !empty($request->coupon_code) && $request->payment_method == 'website') {
            $coupon = PromoCode::query()->findByCode($request->coupon_code)->first();
            $coupon = isset($coupon) && $coupon->isValid() ? $coupon : null;
        }
        $total = (double)(((int)$request->type == 1) ? ((int)$request->hour_no) * $this->onlinePrice() : ((int)$request->hour_no) * $this->interviewPrice());
        return TrainingRequest::create([
            'student_id' => $request->student_id,
            'teacher_id' => $this->id,
            'sub_id' => $request->sub_id,
            'qualification_id' => $request->qualification_id,
            'type' => $request->type,
            'hour_no' => $request->hour_no,
            'payment_method' => $request->payment_method,
            'total' => $total,
            'note' => $request->note,
            'contact' => $request->contact,
            'promo_code_id' => @$coupon->id,
            'discount_value' => isset($coupon) ? (($total * $coupon->discount) / 100) : 0,
        ]);
    }

    public function createWithdrawProcess($value)
    {
        $this->updateWalletWithdrawn($value);
        return FinancialProcess::create([
            'user_id' => $this->id,
            'type' => 2,
            'credit_type' => 'website',
            'value' => $value,
        ]);
    }


    public function hasCredit($value)
    {
        return ($this->walletAvailableCash() >= (double)$value);
    }

    public function studentTrainingRequests()
    {
        return $this->hasMany('App\TrainingRequest', 'student_id', 'id')->orderBy('created_at', 'DESC');
    }

    public function studentSubSpeciality()
    {
        return $this->hasManyThrough(
            'App\SubSpecialty',
            'App\TrainingRequest',
            'student_id',
            'id',
            'id',
            'sub_id'
        )->with('speciality')->orderBy('specialty_id');
    }

    public function studentSpeciality()
    {
        return $this->studentSubSpeciality->map(function ($item) {
            return $item->speciality;
        })->unique();
    }

    public function hasStudentSubSpeciality($id)
    {
        $item = $this->studentSpeciality()->find($id);
        return isset($item);
    }

    public function teacherTrainingRequests()
    {
        return $this->hasMany('App\TrainingRequest', 'teacher_id', 'id')->orderBy('created_at', 'DESC');
    }


    public function studentChats()
    {
        return $this->hasMany('App\Chat', 'student_id', 'id')->orderBy('updated_at', 'DESC');
    }


    public function teacherChats()
    {
        return $this->hasMany('App\Chat', 'teacher_id', 'id')->orderBy('updated_at', 'DESC');
    }

    public function chats()
    {
        if ($this->isStudent()) {
            return $this->studentChats();
        }
        return $this->teacherChats();
    }

//    public function getChat($type, $id)
//    {
//        if (($this->type) == ($type)) {
//            $item = $this->chats()->request('0')->where(($type == '1') ? 'teacher_id' : 'student_id', $id)->first();
//            return isset($item) ? $item : $this->createChat($type, $id);
//        }
//        return null;
//    }

    public function getRequestChat($type, $id)
    {
        if (($this->type) == ($type)) {
            $item = $this->chats()->request('1')->where(($type == '1') ? 'teacher_id' : 'student_id', $id)->first();
            if (isset($item)) {
                return $item;
            }
            $chat = $this->createChat($type, $id);
            if (auth()->user()->isStudent()) {
                $this->teacher->notify(new TrainingNotification(locale_value('لديك رسالة جديدة داخل موقع هلا برو', __('لديك رسالة جديدة داخل موقع هلا برو')), null, locale_value('الوصول للرسالة', __('الوصول للرسالة')), lang_route('teacher.chat', ['type' => '2', 'id' => $chat->student_id])));
            }
            return $chat;
        }
        return null;
    }


    public function pendingRequestsCount()
    {
        if ($this->isTeacher()) {
            return $this->teacherTrainingRequests()->status('pending_teacher')->count();
        }
        return $this->studentTrainingRequests()->status('pending_student')->count();
    }

    public function trainingRequests()
    {
        return ($this->isTeacher()) ? $this->teacherTrainingRequests() : $this->studentTrainingRequests();
    }

    public function unReadCount()
    {
        return $this->chats()->request('0')->whereHas('unReadMessages', function ($q) {
            $q->whereNull('read_at');
        })->count();
    }

    public function createChat($id, $message = '')
    {
        $studentID = ($this->isStudent()) ? $this->id : intval($id);
        $teacherID = ($this->isTeacher()) ? $this->id : intval($id);
        $chat = $this->database->getReference('/messagingChats/' . $studentID . '_' . $teacherID)->update([
            'studentID' => $studentID,
            'teacherID' => $teacherID,
            'createdAt' => (float) parse_to_carbon($this->created_at)->getTimestamp(),
        ]);
        $this->database->getReference('/users/' . $studentID . '/messagingChats/' . $studentID . '_' . $teacherID)->update([
            'studentID' => $studentID,
            'teacherID' => $teacherID,
        ]);
        $this->database->getReference('/users/' . $teacherID . '/messagingChats/' . $studentID . '_' . $teacherID)->update([
            'studentID' => $studentID,
            'teacherID' => $teacherID,
        ]);
        if ($message){
            $this->database->getReference('/messages/' . $studentID . '_' . $teacherID)->push([
                'content' => $message,
                'createdAt' => (float) parse_to_carbon($this->created_at)->getTimestamp(),
                'seenAt' => '',
                'senderID' => $this->id
            ]);
        }
        return $chat->getValue();
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'user_id', 'id');
    }

    public function studentRates()
    {
        return $this->hasMany('App\TeacherRating', 'student_id', 'id');
    }


    public function teacherRatings()
    {
        return $this->hasMany('App\TeacherRating', 'teacher_id', 'id')->orderBy('created_at', 'DESC');
    }


    public function pendingCredit()
    {
        return $this->hasMany('App\OnHoldCash', 'user_id', 'id')->where('status', 'pending');
    }


    public function hasAuthRating()
    {
        $auth_rate = $this->authRating();
        return isset($auth_rate);
    }

    public function authRating()
    {
        return $this->teacherRatings()->where('student_id', auth()->user()->id)->first();
    }


    public function updateTeacherRating()
    {
        $avg = round((float)$this->teacherRatings()->avg('value'), 1);
        return $this->update(['rating' => $avg]);
    }


    public function TeacherRatingsCount()
    {
        return $this->teacherRatings()->count();
    }


    public function financialProcesses()
    {
        return $this->hasMany('App\FinancialProcess', 'user_id', 'id')->orderBy('updated_at', 'DESC')->where('is_confirmed', 1);
    }

    public function localeText($ar, $en = null)
    {
        return $this->isLocaleAr() ? $ar : isset($en) ? $en : __($ar);
    }

    public function isLocaleAr()
    {
        return $this->locale == 'ar';
    }

    public function creditProcesses()
    {
        return $this->financialProcesses()->credit();
    }

    public function withdrawProcesses()
    {
        return $this->financialProcesses()->withdraw();
    }

    public function createPayment($type, $credit_type, $value)
    {
        return FinancialProcess::create([
            'user_id' => $this->id,
            'type' => $type,
            'credit_type' => $credit_type,
            'value' => $value,
        ]);
    }


    public function createWithdrawnProcess($value, $type = 'website', $is_confirmed = 1, $is_withdraw = 0, $decrement = true)
    {
        $process = FinancialProcess::create([
            'user_id' => $this->id,
            'type' => 2,
            'credit_type' => $type,
            'value' => $value,
            'is_confirmed' => $is_confirmed,
            'is_withdraw' => $is_withdraw,
        ]);
        if ($decrement) {
            return (isset($process) && $this->decrementWalletAvailable($process->value)) ? $process : null;
        }
        return $process;
    }


    public function createCreditProcess($value, $is_refund = 0)
    {
        $process = FinancialProcess::create([
            'user_id' => $this->id,
            'type' => 1,
            'credit_type' => 'website',
            'value' => $value,
            'is_refund' => $is_refund,
        ]);
        if ($is_refund == 1) {
            return (isset($process) && $this->incrementWalletAvailable($process->value)) ? $process : null;
        }
        return (isset($process) && $this->incrementWalletOnHold($process->value)) ? $process : null;
    }


    public function wallet()
    {
        return $this->hasOne('App\Wallet', 'user_id', 'id');
    }

    public function hasWallet()
    {
        return (isset($this->wallet));
    }

    public function incrementWalletAvailable($value)
    {
        return ($this->hasWallet()) ? $this->wallet()->update(['available' => ($this->walletAvailableCash() + $value)]) : isset($this->createWallet()->id);
    }

    public function decrementWalletAvailable($value)
    {
        return ($this->hasWallet()) ? $this->wallet()->update(['available' => ($this->walletAvailableCash() - $value)]) : isset($this->createWallet()->id);
    }

    public function incrementWalletOnHold($value)
    {
        return ($this->hasWallet()) ? $this->wallet()->update(['pending' => ($this->walletPendingCash() + $value)]) : isset($this->createWallet()->id);
    }

    public function decrementWalletOnHold($value)
    {
        return ($this->hasWallet()) ? $this->wallet()->update(['pending' => ($this->walletPendingCash() - $value)]) : isset($this->createWallet()->id);
    }

    public function walletAvailableCash()
    {
        return ($this->hasWallet()) ? $this->wallet->available : $this->createWallet()->available;
    }

    public function walletPendingCash()
    {
        return ($this->hasWallet()) ? $this->wallet->pending : $this->createWallet()->pending;
    }

    public function walletTotalCash()
    {
        return ($this->hasWallet()) ? $this->wallet->total() : $this->createWallet()->total();
    }

    public function updateWalletFromLog()
    {
        return ($this->hasWallet()) ? $this->wallet()->update(['available' => $this->cashableBalance() - $this->pendingBalance(), 'pending' => $this->pendingBalance()]) : isset($this->createWallet()->id);
    }


    public function createWallet()
    {
        $wallet = Wallet::create([
            'user_id' => $this->id,
            'available' => $this->cashableBalance() - $this->pendingBalance(),
            'pending' => $this->pendingBalance(),
        ]);
        $this->load('wallet');
        return $wallet;
    }


    public function cashableBalance()
    {
        return ($this->creditSum() - $this->withdrawSum());
    }


    public function creditSum()
    {
        return $this->creditProcesses()->sum('value');
    }

    public function withdrawSum()
    {
        return $this->withdrawProcesses()->sum('value');
    }

    public function pendingBalance()
    {
        return $this->pendingCredit()->sum('value');
    }

    public function notifyBand()
    {
        if ($this->status == 'suspended') {
            $this->notify(new UserNotification($this->id, 'manager', 'تم إيقاف حسابك لوجود إنتهاك لسياسة الموقع', __('تم إيقاف حسابك لوجود إنتهاك لسياسة الموقع'), '#'));
        } else {
            $this->notify(new UserNotification($this->id, 'manager', 'تم إعادة تفعيل حسابك من قبل إدارة الموقع', __('تم إعادة تفعيل حسابك من قبل إدارة الموقع'), '#'));
        }
        return true;
    }


    public function lastDay()
    {
        return $this->whereRaw('Date(created_at) = CURDATE()')->get()->count();
    }

    public function lastMonth()
    {
        return $this->where(\Illuminate\Support\Facades\DB::raw('MONTH(created_at)'), '=', date('n'))->get()->count();
    }


    public function incrementCount()
    {
        $constant = new Constant();
        $item = $constant->key(($this->isStudent()) ? 'students' : 'teachers');
        if (isset($item) && $item->update(['value' => ((int)$item->value + 1)])) {
            return true;
        }
        return false;
    }

    public function decrementCount()
    {
        $constant = new Constant();
        $item = $constant->key(($this->isStudent()) ? 'students' : 'teachers');
        if (isset($item) && $item->update(['value' => ((int)$item->value - 1)])) {
            return true;
        }
        return false;
    }


    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($instance) {
            $links = Link::where('user_id', $instance->id)->delete();
        });
        static::updating(function ($instance) {
            $instance->syncWithFirebase();
        });
//        static::updating(function ($instance) {
//            $instance->syncWithFirebase();
//        });
//        static::deleting(function ($instance) {
//            $links = Link::where('user_id', $instance->id)->delete();
//            $instance->syncWithFirebase();
//        });
    }


    public function createMobileCode($type = 'confirm')
    {
        $code = ($this->hasMobileVerification($type)) ? $this->mobileVerification($type) :
            Link::create([
                'user_id' => $this->id,
                'type' => $type,
                'email' => $this->email,
                'link' => mt_rand(100000, 999999),
                'is_mobile' => 1
            ]);
        $this->sendCode($code);
        return $code;
    }

    public function hasMobileVerification($type = 'confirm')
    {
        return (isset($this->mobileVerification($type)->id));
    }

    public function mobileVerification($type = 'confirm')
    {
        return Link::mobileVerification($type)->where('user_id', $this->id)->first();
    }

    public function sendCode($code)
    {
        $this->notify(new MobileCodeNotification($code->link, $code->type));
        return $this->send(['to' => $this->getCountryCode() . $this->mobile, 'message' => 'Your code is : ' . $code->link]);
    }


    public function scopeFindByEmail($q, $email)
    {
        return $q->where('email', $email)->first();
    }

    public function isNotActivated()
    {
        return $this->status == 'inactive';
    }

    public function isWaitActivated()
    {
        return $this->status == 'wait_active';
    }

    public function createDeleteRequest()
    {
        return DeleteAccountRequest::create([
            'user_id' => auth()->user()->id,
        ]);
    }

    public function deleteRequest()
    {
        return $this->hasOne(DeleteAccountRequest::class, 'user_id', 'id');
    }

    public function hasDeleteRequest()
    {
        return isset($this->deleteRequest);
    }

    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'user_contacts', 'user_id', 'contact_id', 'id', 'id')->withPivot('link');
    }

    public function hasContact($id)
    {
        return isset($this->contacts()->find($id)->id);
    }

    public function getContact($id)
    {
        return $this->hasContact($id) ? $this->contacts()->find($id)->pivot->link : '';
    }

    public function updateOrCreateTrainingInfo($request)
    {
        return isset($request->qualified_title) ? UserInfo::updateOrCreate(['user_id' => $this->id], ['qualified_title' =>  ($request->qualified_title), 'experience' => ($request->experience), 'video_link' => ($request->video_link),]) : false;
    }

    public function user_favourite($teacher_id)
    {
        $user_favourite = Favourite::where(['student_id'=>$this->id ,'teacher_id'=>$teacher_id])->get()->count();

        if($user_favourite > 0){
           return true;
        }else{
           return false;
         }  
    }

}
