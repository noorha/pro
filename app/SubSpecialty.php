<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubSpecialty extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'specialty_id', 'name', 'name_en', 'is_selected', 'icon'
    ];


    protected $hidden = [
        'pivot', 'icon', 'specialty_id', 'name', 'name_en', 'is_selected', 'created_at', 'updated_at', 'deleted_at',
    ];
    protected $appends = ['text', 'is_checked'];


    /**
     * @return string
     */

    public function getIsCheckedAttribute()
    {

        return $this->isChecked();

    }

    public function getTextAttribute()
    {
        return get_text_locale($this, 'name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function speciality()
    {
        return $this->belongsTo('App\Speciality', 'specialty_id', 'id');
    }

    public function scopeSelected($q)
    {
        return $q->where('is_selected', 1);
    }

    /**
     * @return string
     */
    public function getSpecialtyName()
    {
        return isset($this->speciality) ? $this->speciality->name : no_data();
    }

    public function isChecked()
    {

        return auth('api')->check() && isset($this->authSubSpec()->id);
    }

    public function authSubSpec()
    {
        $user_id = auth('api')->check() ? auth('api')->user()->id : 0;
        return UserSubSpecialty::where(['user_id' => $user_id, 'sub_id' => $this->id])->first();
    }
}
