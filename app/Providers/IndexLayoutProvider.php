<?php

namespace App\Providers;

use App\Ticket;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class IndexLayoutProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    #
    public function boot()
    {
        View::composer('panel.layout.index', function ($view) {
            $view->with(['pendingTickets'=> Ticket::where('status',0)->count()]);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
