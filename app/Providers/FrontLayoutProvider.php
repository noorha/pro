<?php

namespace App\Providers;

use App\Constant;
use App\Logic\MobileDetect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class FrontLayoutProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
//    public function boot()
//    {
//        View::composer('front.layout.index', function ($view) {
//            $view->with(['setting'=>new Constant(),'cities'=>\App\City::orderBy('created_at','DESC')->take(15)->get()]);
//        });
//    }
    public function boot()
    {
        View::composer('front.layout.index', function ($view) {
            $view->with(['mobileDetect'=> new MobileDetect(),'setting'=>new Constant(),'cities'=>\App\City::orderBy('created_at','DESC')->take(15)->get()]);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
