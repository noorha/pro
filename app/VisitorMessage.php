<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class VisitorMessage extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'name', 'email', 'subject', 'text', 'read_at'
    ];


    public function markAsRead()
    {
        return $this->update(['read_at' => Carbon::now()]);
    }

    public function scopeUnReadMessages($query)
    {
        return $query->whereNull('read_at');
    }

    public function getArrayCount()
    {
        $array ['all'] = $this->get()->count();
        $array ['day'] = $this->whereRaw('Date(created_at) = CURDATE()')->get()->count();
        $array ['month'] = $this->where(\Illuminate\Support\Facades\DB::raw('MONTH(created_at)'), '=', date('n'))->get()->count();
        return $array;
    }

    public function replays()
    {
        return $this->hasMany(VisitorMessageReplay::class, 'message_id', 'id');
    }

    public function replay(Request $request)
    {
        return VisitorMessageReplay::create([
            'admin_id' => auth('admin')->user()->id,
            'message_id' => $this->id,
            'text' => $request->text
        ]);
    }

    public function hasReplay()
    {
        return ($this->replays()->count() > 0);
    }


}
