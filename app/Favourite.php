<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
//    use SoftDeletes;

public $table = "favourites";    
protected $fillable = [
        'teacher_id', 'student_id'
    ];


    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }

}
