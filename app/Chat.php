<?php

namespace App;

use Carbon\Carbon;
use Firebase\FirebaseLib;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Mpociot\Firebase\SyncsWithFirebase;

class Chat extends Model
{
    use SoftDeletes;
    use SyncsWithFirebase;

    protected $fillable = [
        'is_request', 'request_id', 'student_id', 'teacher_id', 'text', 'updated_at'
    ];

    protected $hidden = [
        'is_request', 'request_id', 'deleted_at'
    ];

    public function scopeRequest($q, $is_request)
    {
        return $q->where('is_request', $is_request);
    }


    public function saveToFirebase($mode)
    {
        if (is_null($this->firebaseClient)) {
            $this->firebaseClient = new FirebaseLib(config('services.firebase.database_url'), config('services.firebase.secret'));
        }
        $path ='chats/'. $this->getKey() ;

        if ($mode === 'set' && $fresh = $this->fresh()) {
            $this->firebaseClient->set($path, $fresh->toArray());
        } elseif ($mode === 'update' && $fresh = $this->fresh()) {
            $this->firebaseClient->update($path, $fresh->toArray());
        } elseif ($mode === 'delete') {
            $this->firebaseClient->delete($path);
        }
    }

    public function isRequest()
    {
        return ($this->is_request == '1');
    }

    public function messages()
    {
        return $this->hasMany('App\ChatMessage', 'chat_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id', 'id');
    }

    public function getTeacherName()
    {
        return (isset($this->teacher)) ? $this->teacher->getUserName() : no_data();
    }

    public function getStudentName()
    {
        return (isset($this->student)) ? $this->student->getUserName() : no_data();
    }

    public function unReadMessages()
    {
        return $this->messages()->whereNull('read_at');
    }

    public function unReadMessagesCount()
    {
        return $this->unReadMessages()->count();
    }

    public function hasUnReadMessages()
    {
        return ($this->unReadMessagesCount() > 0);
    }

    public function markAsRead()
    {
        $this->unReadMessages()->update(['read_at' => Carbon::now()]);
    }

    public function createMessage(Request $request, $type)
    {
        $this->update(['updated_at' => Carbon::now()]);
        return ChatMessage::create([
            'chat_id' => $this->id,
            'type' => ($type == '1') ? 'student' : 'teacher',
            'is_file' => $request->is_file,
            'content' => $request->message,
        ]);
    }


    public function getTeacherPhoto()
    {
        return (isset($this->teacher)) ? $this->teacher->photo : 'default.png';
    }

    public function getStudentPhoto()
    {
        return (isset($this->student)) ? $this->student->photo : 'default.png';
    }
}
