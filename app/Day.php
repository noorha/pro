<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public $timestamps = false;

    protected $hidden = ['name','name_en','pivot'];
    protected $appends = ['text'];

    /**
     * @return string
     */
    public function getTextAttribute()
    {
        return get_text_locale($this, 'name');
    }

}
