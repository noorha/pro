<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Whiteboard extends Model
{

    protected $fillable = [
        'request_id',  'board_id', 'teacher_link', 'student_link', 'recording_link',
    ];


    public function hasAttendee(){
        return isset($this->student_link);
    }
}
