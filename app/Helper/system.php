<?php


function get_current_locale()
{
    return \Illuminate\Support\Facades\App::getLocale();
}

function date_localized($timestamp)
{
    $date = \Carbon\Carbon::createFromTimeStamp(strtotime($timestamp));
    return $date->formatLocalized('%d %B %Y');
}

function get_all_contacts()
{
    return \App\Contact::all();
}

function is_auth()
{
    return (isset(auth()->user()->id)) ? true : false;
}

function get_pending_delete_count()
{
    return \App\DeleteAccountRequest::status('pending')->count();
}

function get_all_partners()
{
    return \App\Partner::orderBy('created_at', 'DESC')->get();
}

function get_all_countries()
{
    return \App\Country::all()->sortBy(locale_value('name', 'name_en'));
}

function get_all_course_categories()
{
    return \App\CourseCategory::orderBy('created_at', 'DESC')->get();
}


function get_main($type)
{
    return \App\PageSection::where('type', $type)->get();
}

function get_footer_countries($type, $is_arab = true)
{
    $array = ['السعودية', 'المملكة العربية السعودية', 'الامارات العربية المتحدة', 'الإمارات', 'الأردن', 'الاردن', 'الكويت'];
    if ($type) {
        return \App\Country::enabled()->whereIn('name', $array)->take(4)->get();
    }
    return $is_arab ? \App\Country::enabled()->arab()->whereNotIn('name', $array)->get() : \App\Country::enabled()->nonArab()->whereNotIn('name', $array)->get();
}

function get_all_academic_qualifications()
{
    return \App\AcademicQualification::all();
}

function get_all_reject_training_request_reasons()
{
    return \App\RejectTrainingReason::all();
}

function get_all_report_teacher_reasons($type = null)
{
    $reasons = new \App\TeacherReportReason();
    return isset($type) ? $reasons->type($type)->get() : $reasons->get();
}

function get_all_specialities($num = null)
{
    return isset($num) ? \App\Speciality::orderBy('created_at', 'DESC')->take($num)->get() : \App\Speciality::orderBy('created_at', 'DESC')->get();
}

function get_all_sub_specialities()
{
    return get_current_locale() == 'ar' ? \App\SubSpecialty::orderBy('name')->get() : \App\SubSpecialty::orderBy('name_en')->get();
}

function get_all_days()
{
    return \App\Day::all();
}


function get_selected_sub_specs($count = 18)
{
    return \App\SubSpecialty::selected()->take($count)->orderBy('updated_at', 'DESC')->get();
}

function ajax_render_view($view, $data)
{
    try {
        return view($view, $data)->render();
    } catch (\Throwable $e) {
    }
    return [''];
}

function get_constant_value($key)
{
    $constant = new \App\Constant();
    return $constant->valueOf($key);
}


function get_currency_value($value)
{
    switch (get_current_currency()) {
        case "AED" :
            return truncate_number($value * (float)get_constant_value('USDAED'), 2);
        case "SAR" :
            return truncate_number($value * (float)get_constant_value('USDSAR'), 2);
        default :
            return truncate_number($value, 2);
    }
}

//function get_currency_value($value)
//{
//    switch (get_current_currency()) {
//        case "AED" :
//            return round((float)$value * (float)get_constant_value('USDAED'),2);
//        case "SAR" :
//            return round((float)$value * (float)get_constant_value('USDSAR'),2);
//        default :
//            return (double)round((float)$value,2);
//    }
//}

function truncate_number($number, $precision = 2)
{
    // Zero causes issues, and no need to truncate
    if (0 == (int)$number) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor($number * $precision) / $precision * $negative;
}

function get_currency_icon()
{
    switch (get_current_currency()) {
        case "AED" :
            return 'fa fa-usd';
        case "SAR" :
            return 'fa fa-usd';
        default :
            return 'fa fa-usd';
    }
}

function image_url($img, $size = '')
{
    return (!empty($size)) ? lang_url('image/' . $size . '/' . $img) : lang_url('image/' . $img);
}


function file_url($file)
{
    return lang_url('file/' . $file);
}

function get_date_from_timestamp($timestamp)
{
    return format_timestamp_date($timestamp, 'Y-m-d');
}

function get_time_from_timestamp($timestamp)
{
    return format_timestamp_date($timestamp, 'H:i');
}

function format_timestamp_date($timestamp, $format)
{
    return (isset($timestamp) && isset($format)) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timestamp)->format($format) : '';
}

function format_12_to_24($input)
{
    return date("H:i", strtotime($input));
}

function format_24_to_12($input)
{
    return date("g:i a", strtotime($input));
}


function get_social_icon($social)
{
    switch ($social) {
        case 'facebook' :
            return 'icon-facebook';
        case 'twitter' :
            return 'icon-twitter';
        case 'linkedin' :
            return 'icon-linkedin-logo';
        case 'google' :
            return 'icon-google-plus';
        case 'instagram' :
            return 'icon-instagram-symbol';
        case 'youtube' :
            return 'icon-youtube-logo';
    }
    return '';
}

function get_social_icon_footer($social)
{
    switch ($social) {
        case 'facebook' :
            return 'fa fa-facebook-official';
        case 'twitter' :
            return 'fa fa-twitter';
        case 'linkedin' :
            return 'fa fa-linkedin-square';
        case 'google' :
            return 'icon-google-plus';
        case 'instagram' :
            return 'fa fa-instagram';
        case 'youtube' :
            return 'fa fa-youtube';
    }
    return '';
}


function main_count()
{
    $constant = new \App\Constant();
    $array['teachers'] = $constant->valueOf('teachers');
    $array['students'] = $constant->valueOf('students');
    $array['courses'] = $constant->valueOf('courses');
    $array['requests'] = $constant->valueOf('requests');
    return $array;
}

function get_socials()
{
    return \App\SocialMedia::all();
}


function get_all_categories()
{
    return \App\CourseCategory::all();
}

function string_limit($str, $char_num)
{
    return str_limit(strip_tags($str), $limit = $char_num, $end = ' ...');
}

function is_free($value)
{
    return (isset($value) && $value > 0) ? $value . '$' : 'مجاناً';
}

function array_map_int($input)
{
    if (isset($input) && is_string($input)) {
        return array_map('intval', explode(',', $input));
    }
    return [];
}

function convert_array_map_int($input)
{
    if (isset($input) && is_array($input)) {
        return array_map('intval', $input);
    }
    return [];
}

function get_request_status($request)
{
    switch ($request->status) {
        case 'pending_teacher' :
            return __("بإنتظار موافقة المدرس");
        case 'pending_student' :
            return __('بإنتظار تأكيد الطالب');
        case 'progress' :
            return (isset($request->is_confirmed) && (int)$request->is_confirmed == 0) ? __('قيد التنفيذ') : __('بإنتظار تأكيد التسليم');
        case 'completed' :
            return __('مكتملة');
        case 'canceled' :
            return __('ملغية');
        case 'rejected' :
            return __('مرفوضة');
        default:
            return '';
    }
}

function get_most_teacher_request()
{
    $teachers = new \App\User();
    return $teachers->active()->teachers()->orderBy('rating', 'DESC')->take(8)->get();
}

function get_all_teachers()
{
    $teachers = new \App\User();
    return $teachers->active()->teachers()->orderBy('rating', 'DESC')->get();
}


function limit_text($text, $limit)
{
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function is_type_checked($array, $input)
{
    if (isset($array) && isset($input)) {
        foreach ($array as $item) {
            if ($input == $item) {
                return 'checked';
            }
        }
    }
    return '';
}

function is_speciality_selected($inputs, $speciality)
{
    $cond1 = (isset($inputs['specialities']) && (int)$inputs['specialities'] == $speciality->id);
    $cond2 = (isset($inputs['sub_specialty']) && isset($speciality->subSpecialties()->find($inputs['sub_specialty'])->id));
    if ($cond1 || $cond2) {
        return ' selected ';
    }
    return '';
}


function get_financial_process_status($process)
{
    if ($process->isCredit()) {
        switch ($process->credit_type) {
            case 'paypal':
                return locale_value('ايداع رصيد عن طريق باي بال', __('ايداع رصيد عن طريق باي بال'));
            case 'visa':
                return locale_value('ايداع رصيد عن طريق فيزا', __('ايداع رصيد عن طريق فيزا'));
            case 'website':
                if ($process->isRefund()) {
                    return locale_value('إعادة إرجاع رسوم طلب تدريب', __('إعادة إرجاع رسوم طلب تدريب'));
                }
                return locale_value('الدخل من إكمال طلب تدريب', __('الدخل من إكمال طلب تدريب'));
            case 'cash':
                return locale_value('الربح من إكمال طلب تدريب ( كاش )', __('الربح من إكمال طلب تدريب ( كاش )'));

        }
    } else {
        switch ($process->credit_type) {
            case 'paypal':
                return locale_value('سحب رصيد متوفر عن طريق باي بال', __('سحب رصيد متوفر عن طريق باي بال'));
            case 'visa':
                return locale_value('سحب رصيد عن طريق فيزا', __('سحب رصيد عن طريق فيزا'));
            case 'website':
                return $process->isWithdrawRequest() ?
                    locale_value('سحب رصيد متوفر عن طريق باي بال', __('سحب رصيد متوفر عن طريق باي بال'))
                    : locale_value('رسوم مقابل طلب تدريب', __('رسوم مقابل طلب تدريب'));
            case 'cash':
                return locale_value('رسوم مقابل طلب تدريب ( كاش )', __('رسوم مقابل طلب تدريب ( كاش )'));
        }
    }
    return '';
}


function generateCouponCode($length = 5)
{
    $chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
    $ret = '';
    for ($i = 0; $i < $length; ++$i) {
        $random = str_shuffle($chars);
        $ret .= $random[0];
    }
    return $ret;
}

function parse_to_carbon($date)
{
    \Carbon\Carbon::setLocale('ar');
    return new \Carbon\Carbon($date);
}


function no_data()
{
    return 'لا يوجد بيانات';
}

function get_training_percent()
{
    $constant = new \App\Constant();
    $percent = $constant->valueOf('percent');
    return isset($percent) ? (double)$percent : 20;
}

function get_profit($price, $flip = null)
{
    $percent = get_training_percent();
    if (isset($flip)) {
        $percent = (isset($percent) && $percent >= 0 && $percent <= 100) ? $percent : 20;
    } else {
        $percent = (isset($percent) && $percent >= 0 && $percent <= 100) ? (100 - $percent) : 80;
    }
    return round(($percent * $price) / 100, 2);
}

function get_unread_notifications_count()
{
    return (isset(auth()->user()->unreadNotifications) && auth()->user()->unreadNotifications->count() > 0) ? auth()->user()->unreadNotifications->count() : 0;
}


function admin_url($uri)
{
    return lang_url('admin/' . $uri);
}

function get_current_currency()
{
    return session()->has('currency') ? session()->get('currency') : 'USD';
}

function get_currency_URL($currency)
{
    return lang_url('currency/' . $currency);
}

function get_current_currency_value()
{

}

function get_currency_text()
{
    if (get_current_locale() == 'ar') {
        switch (session()->get('currency')) {
            case 'USD';
                return '$';
            case 'AED';
                return 'د.إ';
            case 'SAR';
                return 'ر.س';
            default :
                return '$';
        }
    }
    return get_current_currency();
}

function convert_to_USD($value)
{
    if (is_numeric((double)$value)) {
        switch (session()->get('currency')) {
            case 'USD';
                return round((double)$value, 2);
            case 'AED';
                return round((double)$value / (double)get_constant_value('USDAED'), 2);
            case 'SAR';
                return round((double)$value / (double)get_constant_value('USDSAR'), 2);
            default :
                return round((double)$value, 2);
        }
    }
    return 0;
}


function diff_for_humans($timestamp)
{
    \Carbon\Carbon::setLocale(get_current_locale());
    return (is_string($timestamp)) ? \Carbon\Carbon::createFromTimestampUTC(strtotime($timestamp))->diffForHumans() : $timestamp->diffForHumans();
}

function is_nav_active($array)
{
    foreach ($array as $item) {
        if (strpos(url()->current(), $item) !== false) {
            return 'display: block';
        }
    }
    return '';
}

function is_active($uri)
{
    if (admin_url($uri) == url()->current()) {
        return 'active';
    }
    return '';
}


function is_element_active($uri, $uri2 = null)
{
    return (isset($uri2)) ? ((preg_match($uri, url()->current()) || (preg_match($uri2, url()->current())) ? 'active nav-active' : '')) : ((preg_match($uri, url()->current())) ? 'active nav-active' : '');
}


function get_unread_message_count()
{
    $messages = new \App\VisitorMessage();
    return $messages->unReadMessages()->count();
}

function is_menu_active($uri)
{
    if (preg_match($uri, url()->current())) {
        return 'active';
    }
    return '';
}

function get_locale_changer_URL($locale)
{
    $uri = request()->segments();
    $uri[0] = $locale;
    return url(implode($uri, '/'));
}

function get_text_locale($obj, $text)
{
    if (isset($obj)) {
        $val = (get_current_locale() == 'ar') ? $text : ($text . '_en');
        return $obj->$val;
    }
    return no_data();
}


function locale_value($value_ar, $value_en)
{
    return get_current_locale() == 'ar' ? $value_ar : $value_en;
}

function get_training_request_data()
{
    $trainingRequests = new \App\TrainingRequest;
    return $trainingRequests->getArrayCount();
}

function get_visitor_data()
{
    $items = new \App\VisitorMessage;
    return $items->getArrayCount();
}

function get_teachers_data()
{
    $items = \App\User::teachers();
    $all = $items;
    $day = $items;
    $month = $items;
    $array['all'] = $all->count();
    $array['day'] = $day->whereRaw('Date(created_at) = CURDATE()')->get()->count();
    $array['month'] = $month->where(\Illuminate\Support\Facades\DB::raw('MONTH(created_at)'), '=', date('n'))->get()->count();
    return $array;
}

function get_students_data()
{
    $items = \App\User::students();
    $all = $items;
    $day = $items;
    $month = $items;
    $array['all'] = $all->count();
    $array['day'] = $day->whereRaw('Date(created_at) = CURDATE()')->get()->count();
    $array['month'] = $month->where(\Illuminate\Support\Facades\DB::raw('MONTH(created_at)'), '=', date('n'))->get()->count();
    return $array;
}

function YoutubeID($url)
{
    if (strlen($url) > 11) {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
            return $match[1];
        } else
            return false;
    }
    return $url;
}

function get_video_embed($link)
{
    $error_msg = '';
    $full_link = '';
    $thumbnail_url = null;
    $code_pattern = '<div><div style="width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;"><iframe src="{URL}" frameborder="0" allowfullscreen style="width: 100%; height: 100%; position: absolute;"></iframe></div></div>';
    $url_parts = parse_url(strtolower($link));
    if (array_key_exists('host', $url_parts)) {
        if (isset($url_parts['query'])) {
            parse_url(parse_str(parse_url($link, PHP_URL_QUERY), $query_parts));
            if (isset($query_parts['v']) && @file_get_contents('https://www.youtube.com/oembed?format=json&url=http://www.youtube.com/watch?v=' . $query_parts['v'])) {
                $full_link = 'https://www.youtube.com/embed/' . $query_parts['v'] . '?wmode=transparent&rel=0&autohide=1&showinfo=0&enablejsapi=1';
                $code = str_replace('{URL}', $full_link, $code_pattern);
                $thumbnail_url = '"https://img.youtube.com/vi/' . $query_parts['v'] . '/mqdefault.jpg';
            } else {
                $error_msg = trans('phrases.invalid_video_link');
            }
        } else {
            $error_msg = trans('phrases.invalid_video_link');
        }
    } else {
        $error_msg = "عذرا الرابط خطأ";
    }

    return $full_link;
}
