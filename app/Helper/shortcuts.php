<?php

function view_front()
{
    return 'front.';
}

function view_front_layout()
{
    return 'front.layout.';
}

function view_front_main()
{
    return 'front.main.';

}

function view_front_page()
{
    return view_front_main() . 'page.';
}

function view_front_profile()
{
    return view_front() . 'profile.';
}

function view_front_teacher()
{
    return view_front() . 'teacher.';
}

function view_front_student()
{
    return view_front() . 'student.';
}

function view_front_modal()
{
    return view_front() . 'modal.';
}


function default_photo()
{
    return 'default.png';
}


function view_panel()
{
    return 'panel.';
}


function view_panel_dashboard()
{
    return  view_panel().'dashboard';
}
function view_panel_page()
{
    return  view_panel().'page.';
}