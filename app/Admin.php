<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Admin extends Authenticatable
{
    use EntrustUserTrait;


    protected $guard = 'admin';


    protected $fillable = [
        'status','name', 'email', 'password', 'photo'
    ];

    protected $hidden = [
        'password',
    ];

    public function getRoleId()
    {
        $role = $this->role();
        return (isset($role)) ? $role->id : 0;
    }

    public function getRoleName()
    {
        $role = $this->role();
        return (isset($role)) ? $role->name : no_data();
    }

    public function role()
    {
        return $this->roles()->first();
    }

    public function hasAdminRole()
    {
        $role = $this->role();
        return (isset($role));
    }

    public function updateAdmin(Request $request)
    {
        return $this->update([
            'name' => $request->name,
            'email' => $request->email,
            'photo' => $request->photo,
            'password' => (isset($request->password)) ? bcrypt($request->password) : $this->password
        ]);
    }

}
