<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherReportReason extends Model
{


    public function scopeType($q, $type)
    {
        return $q->where('type',$type);
    }
}
