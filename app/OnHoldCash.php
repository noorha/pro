<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnHoldCash extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'status', 'user_id', 'withdraw_process_id', 'credit_process_id', 'value',
    ];

    public function scopeStatus($q, $status)
    {
        return $q->where('status', $status);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
