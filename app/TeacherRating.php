<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeacherRating extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'student_id', 'teacher_id', 'request_id', 'text', 'value'
    ];

    protected $hidden = [
        'updated_at','deleted_at'
    ];

    public function getStudentName()
    {
        return (isset($this->student)) ? $this->student->getUserName() : locale_value('لا يوجد إسم' , 'No Name');
    }

    public function getTeacherName()
    {
        return (isset($this->teacher)) ? $this->teacher->getUserName() :  locale_value('لا يوجد إسم' , 'No Name');
    }

    public function getStudentPhoto()
    {
        return (isset($this->student)) ? $this->student->photo : 'male-avatar.png';
    }

    public function getTeacherPhoto()
    {
        return (isset($this->teacher)) ? $this->teacher->photo : 'male-avatar.png';
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id', 'id');
    }
}
