<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Ticket extends Model
{
    use SoftDeletes;
    protected $fillable = ['status', 'user_id', 'subject', 'text', 'file', 'updated_at'];



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public function hasUser()
    {
        return isset($this->user);
    }

    public function userName()
    {
        return $this->hasUser() ? $this->user->name : 'لا يوجد إسم';
    }

    public function userAvatar($size = null)
    {
        return $this->hasUser() ? image_url($this->user->photo, $size) : image_url('default.png', $size);

    }

    public function canReplay()
    {
        return $this->status == 0;
    }

    public function statusText()
    {
        return __('lang.' . ($this->status != 0 ? 'solved' : 'unsolved'));
    }

    public function replays()
    {
        return $this->hasMany(TicketReplay::class, 'ticket_id', 'id')->orderBy('created_at', 'DESC');
    }

    public function lastReplay()
    {
        return $this->replays()->latest()->first();
    }

    public function lastReplayName()
    {
        return isset($this->lastReplay()->id) ?  $this->lastReplay()->name() : '';
    }


    public function lastReplayAvatar($size= null)
    {
        return isset($this->lastReplay()->id) ?  $this->lastReplay()->avatar($size) : '';
    }

    public function addReplay(Request $request)
    {
        $this->update(['updated_at' => Carbon::now()]);
        return TicketReplay::create([
            'ticket_id' => $this->id,
            'admin_id' => $request->admin_id,
            'text' => $request->text,
        ]);

    }
}
