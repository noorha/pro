<?php

namespace App;

use App\Logic\Whiteboard\ScheduleClass;
use App\Notifications\TrainingNotification;
use App\Notifications\UserNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class TrainingRequest extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'contact', 'student_id', 'teacher_id', 'sub_id', 'qualification_id', 'status', 'type', 'payment_method', 'hour_no', 'total', 'note', 'process_id', 'is_confirmed', 'is_paid', 'promo_code_id', 'discount_value',
    ];

    protected $hidden = [
        'student', 'teacher', 'sub_id', 'process_id', 'qualification_id', 'updated_at', 'deleted_at',
    ];

    protected $appends = ['currencyTotal', 'teacher_data', 'student_data', 'is_teacher_rated', 'total_with_discount', 'currencyTotalDiscount'];

    protected $attributes = [
        'payment_method' => 'website',
    ];


    protected $database;

    public function __construct(array $attributes = [])
    {
        $serviceAccount = ServiceAccount::fromJsonFile(app_path('Firebase/halapronew-firebase-adminsdk-quqwv-b8a076f72f.json'));
        $firebase = (new Factory())->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://halapronew.firebaseio.com')->create();
        $this->database = $firebase->getDatabase();
        parent::__construct($attributes);
    }


    public function getCurrencyTotalDiscountAttribute()
    {
        return  get_currency_value($this->total_with_discount);
    }

    public function getTotalWithDiscountAttribute()
    {
        return $this->total - $this->discount_value;
    }

    public function getCurrencyTotalAttribute()
    {
        return get_currency_value($this->total);
    }

    public function getTeacherDataAttribute()
    {
        return $this->hasTeacher() ? ['id' => $this->teacher->id, 'name' => $this->getTeacherName(), 'photo' => $this->getTeacherPhoto(), 'photoUrl' => image_url($this->getTeacherPhoto())] : [];
    }


    public function getStudentDataAttribute()
    {
        return $this->hasStudent() ? ['id' => $this->student->id, 'name' => $this->getStudentName(), 'photo' => $this->getStudentPhoto(), 'photoUrl' => image_url($this->getStudentPhoto())] : [];
    }

    public function getIsTeacherRatedAttribute()
    {
        return $this->hasRating() ? 1 : 0;
    }

    public function days()
    {
        return $this->belongsToMany('App\Day', 'request_days', 'request_id', 'day_id');
    }

    public function isAccepted()
    {
        return ($this->status != 'pending');
    }

    public function isCash()
    {
        return ($this->payment_method == 'cash');
    }

    public function statusText()
    {
        switch ($this->status) {
            case 'pending_teacher' :
                return 'بإنتظار موافقة المدرس';
            case 'pending_student' :
                return 'بإنتظار تأكيد الطالب';
            case 'progress' :
                return '';
            case 'canceled' :
                return '';
            case 'rejected' :
                return '';
            case 'completed' :
                return '';
        }
    }

    public function scopeStatus($query, $status)
    {
        return (isset($status) && !empty($status)) ? $query->where('status', $status) : $query;
    }

    public function getStudentName()
    {
        return (isset($this->student)) ? $this->student->getUserName() : '';
    }

    public function getTeacherName()
    {
        return (isset($this->teacher)) ? $this->teacher->getUserName() : '';
    }

    public function scopeNonPaidCash($q)
    {
        return $q->status('completed')->cash()->where(['is_paid' => 0]);
    }

    public function scopeCash($q)
    {
        return $q->where('payment_method', 'cash');
    }

    public function scopePaidCash($q)
    {
        return $q->status('completed')->cash()->where(['is_paid' => 1]);
    }

    public function getStudentPhoto()
    {
        return (isset($this->student)) ? $this->student->photo : 'default.png';
    }

    public function getTeacherPhoto()
    {
        return (isset($this->teacher)) ? $this->teacher->photo : 'default.png';
    }


    public function student()
    {
        return $this->belongsTo('App\User', 'student_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id', 'id');
    }

    public function hasStudent()
    {
        return (isset($this->student));
    }

    public function hasTeacher()
    {
        return (isset($this->teacher));
    }


    public function hasDay($day)
    {
        $item = $this->days()->where('id', $day)->first();
        return isset($item);
    }


    public function getPaymentMethodText()
    {
        if (get_current_locale() == 'ar') {
            return (isset($this->payment_method) && $this->payment_method == 'cash') ? 'كاش' : 'باي بال';
        }
        return (isset($this->payment_method) && $this->payment_method == 'cash') ? 'Cash' : 'PayPal';
    }

    public function subSpecialty()
    {
        return $this->belongsTo('App\SubSpecialty', 'sub_id', 'id');
    }

    public function getSubSpecialtyName()
    {
        return (isset($this->subSpecialty)) ? get_text_locale($this->subSpecialty, 'name') : no_data();
    }

    public function academicQualification()
    {
        return $this->belongsTo('App\AcademicQualification', 'qualification_id', 'id');
    }

    public function getQualificationName()
    {
        return (isset($this->academicQualification)) ? get_text_locale($this->academicQualification, 'name') : no_data();
    }


    public function getType()
    {
        if (get_current_locale() == 'ar') {
            return ($this->type == '1') ? 'أونلاين' : 'مقابلة';
        }
        return ($this->type == '1') ? 'Online' : 'Interview';
    }

    public function files()
    {
        return $this->belongsToMany('App\File', 'file_categories', 'owner_id', 'file_id')->where('owner_model', 'App\TrainingRequest');
    }

    public function rejectReason()
    {
        return $this->hasMany('App\RejectTrainingReason', 'request_id', 'id');
    }


    public function storeFiles($files)
    {
        if (isset($files) && count($files) > 0) {
            $pivotData = array_fill(0, count($files), ['owner_model' => 'App\TrainingRequest']);
            $syncData = array_combine($files, $pivotData);
            $this->files()->sync($syncData);
        }
    }


    public function reject(Request $request)
    {
        $reject = RejectTrainingReason::create(['request_id' => $this->id, 'note' => $request->note]);
        return (($this->status == 'pending_teacher') || ($this->status == 'pending_student')) ? $this->update(['status' => 'rejected']) : false;
    }


    public function teacherAccept()
    {
        return ($this->status == 'pending_teacher') ? $this->update(['status' => 'pending_student']) : false;
    }

    public function studentApprove()
    {
        if (!$this->hasChat()) {
            $this->createChat();
        }
    }


    public function createCreditProcessToTeacher()
    {
        if ($this->hasTeacher()) {
            $teacher = $this->teacher;
            $teacher->createCreditProcess(get_profit($this->total));
            $teacher->notify(new UserNotification($this->id, 'training', 'تم إيداع مبلغ ( ' . get_profit($this->total) . ' دولار ) في رصيدك مقابل تدريب ', __('تم إيداع مبلغ') . ' ( ' . get_profit($this->total) . ' $ ) ' . __('في رصيدك مقابل تدريب'), lang_route('profile.balance')));
//            $teacher->notify(new UserNotification($this->id, 'training', 'تم إشتراك الطالب ( ' . $this->getStudentName() . '  ) في التدريب.', _('تم إشتراك الطالب').' ( ' . $this->getStudentName() . ' ) '.__('في التدريب'), lang_route('teacher.training.requests')));
            return true;
        }
        return false;
    }

    public function createWithdrawProcessFromStudent()
    {
        if ($this->hasStudent()) {
            $student = $this->student;
            $student->createWithdrawnProcess($this->total_with_discount);
            $student->notify(new UserNotification($this->id, 'training', 'تم سحب مبلغ ( ' . $this->total_with_discount . ' دولار ) مقابل طلب تدريبك.', __('تم سحب مبلغ') . ' (' . $this->total_with_discount . ' $) ' . __('مقابل طلب تدريبك'), lang_route('profile.balance')));
            return true;
        }
        return false;
    }


    public function createRefundProcessToStudent()
    {
        if ($this->hasStudent()) {
            $student = $this->student;
            $student->createCreditProcess($this->total, 1);
            $student->notify(new UserNotification($this->id, 'training', __('lang.cancel_training_student_notify') . ' ' . $this->total . ' $', __('lang.cancel_training_student_notify') . ' ' . $this->total . ' $', lang_route('profile.balance')));
            return true;
        }
        return false;
    }
//    public function updateTeacherCashStatus()
//    {
//        $item = $this->creditOnHoldCash()->first();
//        dd($item);
//        return isset($item) ? $item->update(['status'=>'progress']) : false;
//    }

    public function canTeacherAccept()
    {
        return $this->hasTeacher() && $this->teacher->canAcceptCashRequest();
    }

    public function createOnHoldCash()
    {
        $item = OnHoldCash::create([
            'status' => 'pending',
            'user_id' => $this->teacher_id,
//            'withdraw_process_id' => $student_process->id,
//            'credit_process_id' => $teacher_process->id,
            'value' => $this->total,
        ]);
        $this->incrementCount();
        return isset($item);
    }

    public function cancel()
    {
        return $this->update(['status' => 'canceled']);
    }

    public function chat()
    {
        return $this->hasOne('App\Chat', 'request_id', 'id')->where('is_request', '1');
    }
    public function getChat()
    {
//        if ($this->hasChat()) {
//            return $this->chat;
//        }
        $chat = $this->createChat();
//        if (auth()->user()->isStudent()) {
//            $teacher = $this->teacher;
//            $teacher->notify(new TrainingNotification($teacher->localeText('لقد تم فتح محادثة جديدة معك لطلب تدريب')
//                , null, $teacher->localeText('الوصول للمحادثة'), lang_route('teacher.chat.request', [$this->id])));
//        } else {
//            $student = $this->student;
//            $student->notify(new TrainingNotification($student->localeText('لقد تم فتح محادثة جديدة معك لطلب تدريب')
//                , null, $student->localeText('الوصول للمحادثة'), lang_route('teacher.chat.request', [$this->id])));
//        }

        return $chat;
    }



    public function hasChat()
    {
        return (isset($this->chat));
    }


    public function notifyTeacherConfirm($auth_api = 'web')
    {
        if ($this->hasStudent()) {
            $this->student->notify(new UserNotification($this->id, 'training', 'تم تسليم طلب التدريب الخاص بك من قبل المدرب  ( ' . $this->getTeacherName() . ' ( ', __('تم تسليم طلب التدريب الخاص بك من قبل المدرب') . ' ( ' . $this->getTeacherName() . ' ).', lang_route((auth($auth_api)->user()->isStudent() ? 'teacher' : 'student') . '.training.requests')));
        }
        return true;
    }

    public function notifyStudentConfirm($auth_api = 'web')
    {
        if ($this->hasTeacher()) {
            $this->teacher->notify(new UserNotification($this->id, 'training', 'تم تأكيد التسليم لطلب التدريب من قبل الطالب  ( ' . $this->getStudentName() . ' ( ', __('تم تأكيد التسليم لطلب التدريب من قبل الطالب') . ' ( ' . $this->getStudentName() . ' ).', lang_route((auth($auth_api)->user()->isStudent() ? 'teacher' : 'student') . '.training.requests')));
            $student = $this->student;
            $student->notify(new TrainingNotification($student->localeText('اكتمال طلب التدريب', 'Training Request Completed')
                , $student->localeText('تم اكمال طلب التدريب/ التدريس الخاص بك.', 'Your training request has been successfully completed.'), null, null));

        }
        return true;
    }

    public function rating()
    {
        return $this->hasOne(TeacherRating::class, 'request_id', 'id');
    }

    public function hasRating()
    {
        return isset($this->rating);
    }


    public function createRating(Request $request)
    {
        return TeacherRating::create([
            'student_id' => $this->student_id,
            'teacher_id' => $this->teacher_id,
            'request_id' => $this->id,
            'text' => isset($request->text) ? $request->text : "",
            'value' => $request->value
        ]);
    }


    public function createChat()
    {
        $chat = $this->database->getReference('/trainingChats/' . $this->id)->update([
            'studentID' => $this->student_id,
            'teacherID' => $this->teacher_id,
            'createdAt' => (float) parse_to_carbon($this->created_at)->getTimestamp(),
            'updatedAt' => (float) parse_to_carbon($this->updated_at)->getTimestamp(),

        ]);
        $this->database->getReference('/users/' . $this->student_id . '/trainingChats/' . $this->id)->update([
            'studentID' => $this->student_id,
            'teacherID' => $this->teacher_id,
        ]);
        $this->database->getReference('/users/' . $this->teacher_id . '/trainingChats/' . $this->id)->update([
            'studentID' => $this->student_id,
            'teacherID' => $this->teacher_id,
        ]);
        return $chat->getValue();
    }

    public function confirmFromTeacher($auth_api = 'web')
    {
        return (auth($auth_api)->user()->isTeacher()) ? $this->update(['is_confirmed' => '1']) : false;
    }

    public function isConfirmByTeacher()
    {
        return ($this->is_confirmed == '1');
    }


    public function reportReason()
    {
        return $this->hasMany('App\TeacherReportReason', 'request_id', 'id');
    }

    public function report(Request $request)
    {
        $report = ReportReason::create([
            'type' => auth()->user()->type,
            'request_id' => $this->id,
            'note' => $request->note
        ]);
        $text_ar = '
        <p>مرحباً،</p>
        <p>نود أعلامكم بأنه تم ارسال طلبكم بالابلاغ عن المشكلة، سيقوم فريق هلا برو بالتواصل معكم لحل مشكلتك خلال 24 ساعة.</p>
        <p>شكرأ جزيلاً 😊 
    فريق هلا برو.          
        </p>';
        $text_en = '
        <p>Hello،</p>
        <p>This is to confirm receiving your problem report.</p>
        <p>HalaPro team will contact you to resolve your problem within 24 hours.</p>
        <p>
         Thank You  😊
         HalaPro Team
         </p>';
        $student = $this->student;
        $student->notify(new TrainingNotification($student->localeText('رسالة تأكيد ابلاغ عن مشكلة', 'Report a problem confirmation ')
            , $student->localeText($text_ar, $text_en), null, null));
        return isset($report);
    }

    public function notifyStudentAccept()
    {
        if ($this->hasStudent()) {
            $this->student->notify(new UserNotification($this->id, 'training', 'تم قبول طلب تدريبك من خلال ' . $this->getTeacherName() . ' , يرجى تأكيد الإستلام ودفع قيمة التدريب للمتابعة.  ', __('تم قبول طلب تدريبك من خلال') . ' ' . $this->getTeacherName() . ' ' . __('يرجى تأكيد الإستلام ودفع قيمة التدريب للمتابعة'), lang_route('student.training.requests')));
            return true;
        }
        return false;
    }

    public function notifyTeacherForAccept()
    {
        if ($this->hasTeacher()) {
            $this->teacher->notify(new UserNotification($this->id, 'training', 'تم  تأكيد قبول طلب التدريب من الطالب ' . $this->getStudentName() . '.  ', __('تم تأكيد قبول طلب التدريب من الطالب') . ' ' . $this->getStudentName(), lang_route('teacher.training.requests')));
            return true;
        }
        return false;
    }

    public function payStudentCash()
    {
        if ($this->hasStudent()) {
            $process = FinancialProcess::create([
                'user_id' => $this->student->id,
                'type' => 2,
                'credit_type' => 'cash',
                'value' => $this->total,
            ]);
        }

    }

    public function notifyStudentReject()
    {
        if (isset($this->student)) {
            $this->student->notify(new UserNotification($this->id, 'training', 'تم رفض طلب تدريبك من خلال ' . $this->getTeacherName(),
                __('تم رفض طلب تدريبك من خلال') . ' ' . $this->getTeacherName(), lang_route('student.training.requests')));
            $student = $this->student;
            $student->notify(new TrainingNotification($student->localeText('رفض طلب التدريب', 'Reject the training request'),
                $student->localeText('نعتذر، تم رفض طلب التدريب/ التدريس المرسل.
يرجى زيارة هلا برو لطلب مدرب/ مدرس اخر.                

شكرأ جزيلاً 😊                 
فريق هلا برو                  ',
                    'It is with our deep apologies to inform you that your training / tutoring request was rejected.
                 Please visit HalaPro to search and select another trainer / tutor.

                 Thank You  😊
                 HalaPro Team'), null, null));
            return true;
        }
        return false;
    }

    public function notifyTeacherReject()
    {
        if (isset($this->teacher)) {
            $this->teacher->notify(new UserNotification($this->id, 'training', '  تم رفض تأكيد الإستلام من خلال الطالب : ' . $this->getStudentName(),
                __('تم رفض تأكيد الإستلام من خلال الطالب') . ' ' . $this->getStudentName(), lang_route('student.training.requests')));
            return true;
        }
        return false;
    }


    public function getArrayCount()
    {
        $array ['all'] = $this->count();
        $array ['pending'] = $this->where('status', 'LIKE', '%pending%')->get()->count();
        $array ['completed'] = $this->where('status', 'LIKE', '%completed%')->get()->count();
        $array ['progress'] = $this->where('status', 'LIKE', '%progress%')->get()->count();
        return $array;
    }

    public function incrementCount()
    {
        $constant = new Constant();
        $item = $constant->key('requests');
        if (isset($item) && $item->update(['value' => ((int)$item->value + 1)])) {
            return true;
        }
        return false;
    }

    public function decrementCount()
    {
        $constant = new Constant();
        $item = $constant->key('requests');
        if (isset($item) && $item->update(['value' => ((int)$item->value - 1)])) {
            return true;
        }
        return false;
    }

    public function whiteboard()
    {
        return $this->hasOne('App\Whiteboard', 'request_id', 'id');
    }

    public function hasWhiteboard()
    {
        return isset($this->whiteboard);
    }


    public function sendCreateBoardRequest()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://awwapp.com/api/v2/admin/boards/create");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type' => 'application/x-www-form-urlencoded',
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=00e0a959-df6f-4fba-b2c3-55a73200bfa6&domain=https://ustazk.com/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function createWhiteboard()
    {
        $response = json_decode($this->sendCreateBoardRequest(), true);
        if (isset($response['success']) && $response['success']) {
            $board = collect($response['board']);
            return Whiteboard::create([
                'request_id' => $this->id,
                'status' => 1,
                'board_id' => $board['_id'],
                'board_link' => $board['boardLink'],
                'subscription' => $board['subscription'],
            ]);
        }
        return null;
    }

    public function makeWhiteboardInActive()
    {
        if ($this->hasWhiteboard()) {
            $this->whiteboard->update(['status', 0]);
        }
        return true;
    }

    public function getClassRoom()
    {
        $class = new ScheduleClass();
        if (is_auth() && auth()->user()->id == $this->teacher_id) {
            if ($this->hasTeacher() && $this->hasStudent()) {
                if (!$this->hasWhiteboard()) {
                    $data['email'] = $this->teacher->email;
                    $data['start_time'] = Carbon::now()->subHour(5)->addMinute(1)->format('Y-m-d H:i:s');
                    $data['title'] = '  حصة دراسية في موضوع ' . $this->getSubSpecialtyName() . '.';
                    if (!$this->teacher->isRegisteredInWhiteboard()) {
                        $this->teacher->registerTeacherInWhiteboard();
                    }
                    $response = $class->create($data);
                    if (isset($response) && $response['status']) {
                        $whiteboard = Whiteboard::create(['request_id' => $this->id, 'board_id' => $response['board_id'], 'teacher_link' => $response['teacher_link'], 'recording_link' => $response['recording_link']]);
                        return isset($whiteboard) ? $whiteboard : null;
                    }
//                    else {
//                        dd('error',$response);
//                    }
                }
                return $this->whiteboard;
            }
        } else if (auth()->user()->id == $this->student_id && $this->hasWhiteboard()) {
            $whiteboard = $this->whiteboard;
            if (!$this->hasAttendWhiteboard()) {
                $add_attendee = $class->AddAttendee(['class_id' => $whiteboard->board_id, 'name' => $this->student->getUserName()]);
                if (isset($add_attendee['attendee']['attendee_url'])) {
                    $whiteboard->update(['student_link' => $add_attendee['attendee']['attendee_url']]);
                }
            }
            return $whiteboard;
        }
        return null;
    }

    public function hasAttendWhiteboard()
    {
        return $this->hasWhiteboard() && isset($this->whiteboard->student_link);
    }

    public function studentAccept($from_api = 'web')
    {
        if ($this->isCash()) {
            return $this->canTeacherAccept() && $this->update(['status' => 'progress']) && $this->notifyTeacherForAccept();
        }
        return auth($from_api)->user()->hasCredit($this->total_with_discount) && $this->update(['status' => 'progress']) && $this->createWithdrawProcessFromStudent() && $this->notifyTeacherForAccept();
    }

    public function studentConfirm($auth_api = 'web')
    {
        if ($this->isCash()) {
            $this->makeCashFinancialProcesse(1);
            $this->makeCashFinancialProcesse(2);
            return ($this->status == 'progress') && $this->isConfirmByTeacher() && $this->update(['status' => 'completed']) && $this->makeWhiteboardInActive() && $this->notifyStudentConfirm($auth_api);
        }
        return ($this->status == 'progress') && $this->isConfirmByTeacher() && $this->update(['status' => 'completed']) && $this->createCreditProcessToTeacher() && $this->createOnHoldCash() && $this->makeWhiteboardInActive() && $this->notifyStudentConfirm($auth_api);
    }

    public function makeCashFinancialProcesse($type)
    {
        return FinancialProcess::create([
            'user_id' => ($type == 1) ? $this->teacher_id : $this->student_id,
            'type' => $type,
            'credit_type' => 'cash',
            'value' => $this->total,
        ]);
    }

    public function communication()
    {
        return $this->belongsTo(Contact::class,'contact','id');
    }

    public function hasCommunication()
    {
        return isset($this->communication);
    }

    public function communicationLink()
    {
        return isset( $this->teacher->contacts()->find($this->contact)->pivot->link) ?  $this->teacher->contacts()->find($this->contact)->pivot->link : '';
    }
    public function communicationName()
    {
        return isset( $this->teacher->contacts()->find($this->contact)->name) ?  $this->teacher->contacts()->find($this->contact)->name : '';
    }



}
