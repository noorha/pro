<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageSection extends Model
{

    use SoftDeletes;


    protected $fillable = [
        'type', 'title', 'text', 'title_en', 'text_en', 'photo', 'link'
    ];


}
