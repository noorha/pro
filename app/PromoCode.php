<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $fillable = self::FILLABLE;
    const FILLABLE = ['start_time', 'end_time', 'discount', 'code', 'max_student_no', 'is_enabled'];
    protected $appends = ['student_no', 'discount_value'];

    public function getStudentNoAttribute()
    {
        return $this->trainingRequests()->count();
    }

    public function getDiscountValueAttribute()
    {
        return round($this->trainingRequests()->sum('discount_value'), 2);
    }

    public function isValid()
    {
        return  $this->student_no <= $this->max_student_no;
    }

    public function trainingRequests()
    {
        return $this->hasMany(TrainingRequest::class, 'promo_code_id', 'id');
    }


    public static function create(array $data)
    {
        $instance = new self;
        $instance->fill($data);
        $instance->code = isset($instance->code) ? $instance->code : generateCouponCode();
        $instance->save();
        return $instance;
    }

    public function isActive()
    {
        $now = Carbon::today();
        return ($now->greaterThanOrEqualTo($this->start_time) && $now->lessThanOrEqualTo($this->end_time) && $this->is_enabled == 1);
    }

    public function scopeFindByCode($q, $code)
    {
        return $q->where(['code' => $code, 'is_enabled' => 1])->where(function ($query) {
            $query->whereDate('start_time', '<=', Carbon::today())->whereDate('end_time', '>=', Carbon::today());
        });
    }
}
