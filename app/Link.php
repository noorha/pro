<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    protected $fillable = [
        'user_id', 'type', 'email', 'link', 'is_mobile',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function search($link)
    {
        return $this->where('link', $link)->first();
    }


    public function scopeFindByMobileCode($q,$code)
    {
        return $q->where(['type'=>'confirm','link'=>$code,'is_mobile'=>1])->whereDate('created_at', Carbon::today())->first();
    }

    public function scopeMobileVerification($q,$type){
        return $q->where(['type'=>$type,'is_mobile'=>1])->whereDate('created_at', Carbon::today());
    }
}
