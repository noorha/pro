<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Link;
use App\Notifications\ConfirmMail;
use App\Notifications\UserNotification;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;



    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function show_registration_form($type)
    {
        return view(view_front_main() . 'register', ['type' => $type]);
    }

    public function store(RegisterRequest $request)
    {
        event(new Registered($user = $this->create($request->all())));

        $text = '<div>
                    <p>'.__('تم إرسال بريد إلكتروني  لتفعيل الحساب الخاص بك').'</p>  
                 </div>';
        return $this->registered($request, $user) ? $this->response_api(true, '', $text) : $this->response_api(false, __('فشلت عملية التسجيل'));
    }


    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    protected function registered(Request $request, $user)
    {
        $link = $user->createMobileCode('confirm');
        if (isset($link)) {
            Auth::login($user);
//            $user->notify(new ConfirmMail($user));
            $user->notify(new UserNotification(0,'all','آهلاً بك في موقع هلا برو',__('آهلاً بك في موقع هلا برو'),'#'));
            $user->incrementCount();
            return true;
        }
        return false;
    }



    public function activate_user_account($token, Link $link)
    {
        $link = $link->search($token);
        if (isset($link->user)) {
            $user = $link->user;
            switch ($user->status) {
                case 'active' :
                    session()->flash('alert', ['type' => 'info', 'message' => __('تم تفعيل الحساب الخاص بك مسبقاً')]);
                    break;
                case 'inactive' :
                        $user_chk =  ($user->type == 2) ? $user->update(['status' => 'wait_active']) : $user->update(['status' => 'active']);
                           if ($user_chk && $user->removeActivationLink()) {
                                Auth::login($user);
                                $user->notify(new UserNotification(0,'all','قم بإستكمال الملف الشخصي الخاص بك',__('قم بإستكمال الملف الشخصي الخاص بك'),'#'));
                                session()->flash('alert', ['type' => 'success', 'message' => __('تم تفعيل الحساب الخاص بك بنجاح')]);
                                return redirect()->route(get_current_locale().'.profile.complete');
                            } else {
                                session()->flash('alert', ['type' => 'error', 'message' => __('حدث خطأ غير متوقع أثناء عملية التفعيل')]);
                            }
                     
                    break;
                case 'suspended' :
                    session()->flash('alert', ['type' => 'warning', 'message' => __('الحساب الخاص بك موقوف حالياً .. الرجاء التواصل مع إدارة الموقع')]);
                    break;
            }
        }
        return redirect()->route(get_current_locale().'.login');
    }




    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'type' => $data['type'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'nationality_id' => $data['nationality_id'],
            'password' => $data['password'],
        ]);
    }
}
