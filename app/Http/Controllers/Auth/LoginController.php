<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Socialite;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show_login_form()
    {
        return view(view_front_main().'login');
    }


    public function login(Request $request)
    {
        $this->request_validation($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            session()->flash('alert', ['type' => 'error', 'message' => __('لقد تجاوزت الحد المسموح لمحاولات تسجيل الدخول')]);
            return redirect()->route(get_current_locale().'.login');
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    public function request_validation(Request $request)
    {
        $validation = Validator::make($request->all(), $this->rules());
        if ($validation->fails()) {
            $response = $this->get_validation_errors(false, 'حدثت الأخطاء التالية ', $validation->errors(), true);
            session()->flash('alert', ['type' => 'error', 'message' => $response['message'], 'errors_object' => $response['errors_object']]);
            return redirect()->to(get_current_locale().'.login');
        }
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|string|email|max:190',
            'password' => 'required|string|min:6',
        ];
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        auth()->user()->syncWithFirebase();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended('/'.get_current_locale().'/'.$this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        switch ($user->status) {
            case 'active':
                return '';
            case 'inactive' :
                return '';
            case  'suspended':
                $text_msg = __('حسابك موقوف حالياً .. عليك مراجعة الإدارة');
                break;
        }
        $this->guard()->logout();
        session()->flash('alert', ['type' => 'warning', 'message' => $text_msg]);
        return redirect()->route(get_current_locale().'.login');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        session()->flash('alert', ['type' => 'error', 'message' => __('تأكد من إدخال بريد إلكتروني وكلمة المرور صحيحة')]);
        return redirect()->route(get_current_locale().'.login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route(get_current_locale().'.main');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }


    public function redirectToApi($type)
    {
        return Socialite::driver($type)->redirect();
    }

    public function handleCallback(User $model,$provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $create['first_name'] = $user->getName();
        $create['email'] = $user->getEmail();
        $create['image'] = $user->getAvatar();
        $create['provider_id'] = $user->getId();
        $create['provider'] = $provider;
        $create['type'] = 1;
        $create['status'] = 'active';
        if (!isset($create['email'])) {
            session()->flash('alert', ['type' => 'error', 'message' => 'البريد الإلكتروني غير موجود']);
            return redirect()->to('/login');
        }

        $exist_model = $model::where('email',$user->getEmail())->first();
        if (count($exist_model) > 0) {
            $user_auth = $model::where('email',$user->getEmail())->first();
            if (isset($user_auth) && isset($user_auth->user)) {
                Auth::login($user_auth->user);
                return redirect()->to('/course/all');
            }

            Auth::login($exist_model);
          //  session()->flash('alert', ['type' => 'error', 'message' => 'البريد الإلكتروني موجود مسبقاً']);
            return redirect()->to('/');
        }
        $item =$model->create($create);
      //  $item->createSocialAuth($create);
//        if (isset($create['image'])) {
//            if ($type == 'facebook') {
//                $item->saveFacebookAvatar($user->getAvatar());
//            } else {
//                $item->saveTwitterAvatar($user->getAvatar());
//            }
//        }
        Auth::login($item);
        return redirect()->to('/');
    }

    private function save_facebook_avatar($path)
    {
        $path = str_replace('normal', 'large', $path);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $a = curl_exec($ch);
        if (preg_match('#Location: (.*)#', $a, $r)) {
            $l = trim($r[1]);
            $orig = pathinfo($l, PATHINFO_EXTENSION);
            $extension = substr($orig, 0, strpos($orig, '?'));
            $filename = 'image_' . time() . mt_rand() . ".{$extension}";
            $newName = storage_path('app/uploads/images/' . $filename);
            dd($l);
            Image::make($l)->save($newName);
            return $filename;
        } else {
            return $path;
        }
    }

    private function save_twitter_avatar($path)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        $filename = 'image_' . time() . mt_rand() . ".{$extension}";
        $newName = storage_path('app/uploads/images/' . $filename);
        Image::make($path)->save($newName);
        return $filename;
    }


}
