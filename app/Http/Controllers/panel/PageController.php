<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Page;
use App\Question;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Page $page, $type)
    {
        $data['page'] = $page->type($type);
        return (isset($data['page'])) ? view(view_panel_page() . 'edit', $data) : redirect()->route('ar.panel.dashboard');
    }

    public function update_page($type, Request $request, Page $page)
    {
        return ($page->firstOrNew(['type' => $type])->update(['title' => $request->title, 'title_en' => $request->title_en, 'text' => $request->text, 'text_en' => $request->text_en,]))
            ? $this->response_api(true, 'تمت عملية تعديل الصفحة بنجاح') : $this->response_api(false, 'فشلت عملية التعديل');
    }


    public function faqIndex()
    {
        return view(view_panel() . 'page.faq');
    }

    public function getQuestionDataTable(Question $items)
    {
        return DataTables::of($items->orderBy('created_at', 'DESC'))->addColumn('action', function ($item) {
            return '<div class="row">
                    <button title="تعديل" style="margin-right: 10px" data-url="' . lang_route('panel.page.faq.edit.index',[$item->id]) . '"  class="btn btn-sm btn-success btn-href" >
                    <i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل
                    </button>
                    <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('page/faq/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['icon', 'action'])->make(true);
    }


    public function getQuestionData($id)
    {
        $item = Question::find($id);
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function storeQuestion(Request $request)
    {
        $item = Question::create($request->only(Question::FILLABLE));
        return (isset($item)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function editQuestion(Request $request, $id)
    {
        $item = Question::updateOrCreate(['id' => $id], $request->only(Question::FILLABLE));
        return (isset($item)) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function deleteQuestion($id)
    {
        $item = Question::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function createQuestionIndex()
    {
        return view(view_panel() . 'page.faq-question');
    }

    public function editQuestionIndex($id)
    {
        $data['item'] = Question::find($id);
        return isset($data['item']) ? view(view_panel() . 'page.faq-question',$data) : redirect()->route(get_current_locale() . '.panel.dashboard');
    }
}
