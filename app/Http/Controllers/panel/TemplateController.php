<?php

namespace App\Http\Controllers\panel;

use App\Constant;
use App\Http\Controllers\Controller;
use App\PageSection;
use App\SocialMedia;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function settings()
    {
        return view(view_panel() . 'template.settings');
    }

    public function update_settings(Request $request, Constant $constant)
    {
        $inputs = $request->except('_token');
        foreach ($inputs as $i => $input) {
            $constant->firstOrNew(['key' => $i])->update(['value' => (isset($input)) ? $input : '']);
        }
        return response()->json(['status' => true, 'message' => 'تم تعديل إعدادات الموقع بنجاح ']);
    }


    public function socials()
    {
        return view(view_panel() . 'template.social');
    }


    public function update_socials(Request $request, SocialMedia $socials)
    {
        $inputs = $request->except(['_token', '_method']);
        foreach ($inputs as $i => $input) {
            $social = $socials->where('key', $i)->first();
            if (!isset($social)) {
                return $this->response_api(false, 'حدث خطأ غير متوقع');
            }
            $social->updateItem($input);
        }
        return $this->response_api(true, 'تم تعديل مواقع التواصل الإجتماعي');
    }


    public function main()
    {
        return view(view_panel() . 'template.main');
    }


    public function get_section_data($id)
    {
        $item = PageSection::find($id);
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function create_section(Request $request)
    {
        $item = PageSection::create($request->all());
        return (isset($item)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_section($id, Request $request)
    {
        $item =  PageSection::firstOrCreate(['id'=>$id],$request->all()) ;
        return (isset($item) && $item->update($request->all())) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function delete_section($id)
    {
        $item = PageSection::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }
}
