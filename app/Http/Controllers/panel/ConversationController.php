<?php

namespace App\Http\Controllers\panel;

use App\Traits\FireBase;
use App\Http\Controllers\Controller;
use App\User;
use Yajra\DataTables\DataTables;

class ConversationController extends Controller
{
    use FireBase;
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        return view('panel.chat.all-old');
    }

    public function getDataTable()
    {
        $items = $this->getMessagesChats();
        if ($items) {
            $items = $items->getValue();
        }else{
            $items = [];
        }
        if ($items == null) $items = [];
        $i = 0;
        $arr = [];
//        dd($items);
        foreach ($items as $key => $item){
            $i++;
            $res = $item;
            $res['id'] = $i;
            $res['key'] = $key;
            $res['teacher'] = User::find($item['teacherID']);
            $res['student'] = User::find($item['studentID']);
            if ($res['teacher'] && $res['student']) $arr[] = $res;
        }
//        $items = $items->request('0')->orderBy('created_at', 'DESC');
        return DataTables::of($arr)->editColumn('teacher_id', function ($item) {
            return '<a href="' . lang_route('panel.teacher.edit', [$item['teacher']->id]) . '">' . $item['teacher']->getUserName() . '</a>';
        })->editColumn('student_id', function ($item) {
            return '<a href="' . lang_route('panel.student.edit', [$item['student']->id]) . '">' . $item['student']->getUserName() . '</a>';
        })->editColumn('created_at', function ($item) {
            return ($item['createdAt'])? date("Y-m-d g:i a",intval($item['createdAt']/1000)):'';
        })->editColumn('status', function ($item) {
//            switch ($item->status) {
//                case 'pending_teacher':
//                    return '<a  disabled="disabled" class="btn btn-warning  btn-sm ">بإنتظار موافقة المدرس</a>';
//                case 'pending_student':
//                    return '<a  disabled="disabled" class="btn btn-warning  btn-sm ">بإنتظار موافقة الطالب</a>';
//                case 'progress':
//                    return '<a  disabled="disabled" class="btn btn-info  btn-sm ">قيد التنفيذ</a>';
//                case 'completed':
//                    return '<a  disabled="disabled" class="btn btn-success  btn-sm ">مكتملة</a>';
//                case 'canceled':
//                    return '<a  disabled="disabled" class="btn btn-danger  btn-sm ">ملغية</a>';
//                case 'rejected':
//                    return '<a  disabled="disabled" class="btn btn-danger  btn-sm ">مرفوضة</a>';
//            }
//            return get_date_from_timestamp($item->created_at) . '   ' . format_24_to_12($item->created_at);
            return '';
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a title="عرض المحادثة" target="_blank" href="' . lang_route('panel.conversation.view', [$item['key']]) . '" style="margin-right: 10px" class="btn btn-sm btn-success " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>عرض المحادثة</a>
                    </div>';
        })->rawColumns(['status', 'teacher_id', 'student_id', 'action'])->make(true);
    }

    public function view_conversation($id)
    {
//        $data['chat'] = \App\Chat::find($id);
//        return isset( $data['chat']) ? view('panel.chat.view',['chat'=> $data['chat'],'messages'=> $data['chat']->messages()->orderBy('created_at','DESC')->paginate(20)]) : redirect()->route(get_current_locale().'.panel.dashboard');
        $chat = $this->getMessagesChatByID($id)->getValue();
        $teacher = new User();
        $student = new User();
        if ($chat) {
            $teacher = User::find($chat['teacherID']);
            $student = User::find($chat['studentID']);
        }
//        if ($chat==null) redirect()->route(get_current_locale().'.panel.dashboard');
//        dd($chat);
        return view('panel.chat.view',compact('id', 'teacher', 'student'));
    }


}
