<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\ReportReason;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function reportIndex()
    {
        return view('panel.report.all');
    }

    public function reportDataTable(ReportReason $items)
    {
        $courses = $items->orderBy('created_at', 'DESC');
        return DataTables::of($courses)->editColumn('user_id', function ($item) {
            return $item->getUserName();
        })->addColumn('teacher', function ($item) {
            if ($item->hasRequest()) {
                return '<a href="' . lang_route('panel.teacher.edit', [$item->request->teacher->id]) . '">' . $item->request->getTeacherName() . '</a>';
            }
            return '';
        })->addColumn('student', function ($item) {
            if ($item->hasRequest()) {
                return '<a href="' . lang_route('panel.student.edit', [$item->request->student->id]) . '">' . $item->request->getStudentName() . '</a>';
            }
            return '';
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            switch ($item->status) {
                case 'pending':
                    return
                   '<div class="row">
                        <a title="عرض المحادثة" target="_blank" href="' . lang_route('panel.conversation.view', [isset($item->request->chat->id) ? $item->request->chat->id : 0]) . '" style="margin-right: 10px" class="btn btn-sm btn-primary " ><i style="margin-left: 5px" class="fa fa-comment"></i>عرض المحادثة</a>
                        <a title="قبول" data-toggle="accept" data-url="' . lang_route('panel.report.accept', [$item->id]) . '" href="' . lang_route('panel.teacher.edit', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success  accept" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>قبول</a>
                        <a  data-toggle="reject" title="" style="margin-right: 10px"  data-url="' . lang_route('panel.report.reject', [$item->id]) . '" class="btn btn-sm btn-warning reject"><i class="glyphicon glyphicon-ban-circle"></i> رفض</a>
                    </div>';
                default:
                    return
                        '<div class="row">
                        <a title="عرض المحادثة" target="_blank" href="' . lang_route('panel.conversation.view', [isset($item->request->chat->id) ? $item->request->chat->id : 0]) . '" style="margin-right: 10px" class="btn btn-sm btn-primary " ><i style="margin-left: 5px" class="fa fa-comment"></i>عرض المحادثة</a>
                    </div>';
            }
        })->rawColumns(['teacher', 'student', 'action'])->make(true);
    }

    public function accept($id)
    {
        $report = ReportReason::find($id);
        if (isset($report) && $report->hasRequest()) {
            $trainingRequest = $report->request;
            $trainingRequest->cancel();
            $report->update(['status' => 'accepted']);
            $report->notifyForAccept();
            if (!$trainingRequest->isCash()) {
                $trainingRequest->createRefundProcessToStudent();
            }
            return $this->response_api(true, 'تم إلغاء طلب التدريب بنجاح');
        }
        return $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }


    public function reject($id, Request $request)
    {

        $report = ReportReason::find($id);
        if (isset($report) && $report->hasRequest()) {
            $trainingRequest = $report->request;
            $report->update(['status' => 'rejected']);
            $report->notifyForReject($request);
            return $this->response_api(true, 'تم رفض الشكوى بنجاح');
        }
        return $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }
}
