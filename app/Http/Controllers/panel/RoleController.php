<?php

namespace App\Http\Controllers\panel;

use App\Http\Requests\PermissionsRequest;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('panel.role.all');
    }

    public function create()
    {
        $data['role'] = new Role();
        return view('panel.role.add',$data);
    }

    public function store(PermissionsRequest $request, Role $role)
    {
        $role = $role->updateOrCreate(['id'=>$request->id],$request->all());
        $role->perms()->sync([]);
        foreach ($request->permissions as $item) {
            $permission = Permission::firstOrCreate(['name' => $item]);
            if (!isset($permission)) {
                return $this->response_api(false, 'حدث خطأ أثناء المعالجة');
            }
            $role->attachPermission($permission);
        }
        return $this->response_api(true, 'تمت عملية حفظ الصلاحيات بنجاح');
    }

    public function roles_data_table()
    {
        $roles = Role::orderBy('created_at', 'DESC');
        return DataTables::of($roles)->editColumn('name', function ($role) {
            return '<a href="' . admin_url('role/' . $role->id . '/edit') . '">' . $role->name . '</a>';
        })->addColumn('action', function ($item) {
            return '<div class="row">
                       <a title="تعديل" style="margin-right: 10px"  href="' . admin_url('role/' . $item->id . '/edit') . '" class="btn btn-sm btn-primary edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                       <a  data-toggle="delete" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('role/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                    </div>';
        })->rawColumns(['name', 'action'])->make(true);
    }

    public function edit($id)
    {
        $data['role'] = Role::find($id);
        return isset($data['role']) ? view('panel.role.add', $data) : redirect()->route(get_current_locale().'.panel.dashboard');
    }

    public function delete($id)
    {
        $role = Role::find($id);
        $role->perms()->sync([]);
        return (isset($role) && $role->delete()) ? $this->response_api(true, 'تم حذف المجموعة بنجاح') : $this->response_api(false, 'حدث خطأ أثناء عملية الحذف');
    }
}
