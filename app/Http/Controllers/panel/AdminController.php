<?php

namespace App\Http\Controllers\panel;

use App\Admin;
use App\Http\Requests\AdminRequest;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        return view('panel.account.all');
    }

    public function create()
    {
        $data['roles'] = Role::all();
        return view('panel.account.add',$data);
    }

    public function store(AdminRequest $request)
    {
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'photo' => $request->photo
        ]);
        if (isset($admin)) {
            $admin->attachRole($request->role);
            return $this->response_api(true, 'تم إضافة حساب الإدارة بنجاح');
        }
        return $this->response_api(false, 'فشلت عملية الإضافة');
    }

    public function edit($id)
    {
        $data['admin'] = Admin::find($id);
        $data['roles'] = Role::all();
        return (isset($data['admin'])) ? view('panel.account.edit',$data) : redirect()->route(get_current_locale().'.panel.dashboard');
    }

    public function update($id, AdminRequest $request)
    {
        $admin = Admin::find($id);
        if (isset($admin)){
            $admin->roles()->sync([$request->role]);
            return ($admin->updateAdmin($request)) ? $this->response_api(true, 'تم تعديل حساب الإدارة بنجاح'):  $this->response_api(false, 'حدث خطأ غير متوقع');
        }
        return $this->response_api(false, 'فشلت عملية التعديل');
        }

    public function admins_data_table()
    {
        $admins = Admin::orderBy('created_at', 'DESC');
        return DataTables::of($admins)->editColumn('name', function ($item) {
            return '<a href="' . admin_url('account/' . $item->id . '/edit') . '">' . $item->name . '</a>';
        })->addColumn('role', function ($item) {
            return $item->getRoleName();
        })->addColumn('action', function ($item) {
            return '<div class="row">
                       <a title="تعديل" style="margin-right: 10px"  href="' . admin_url('account/' . $item->id . '/edit') . '" class="btn btn-sm btn-primary edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                       <a  data-toggle="delete" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('account/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                    </div>';
        })->rawColumns(['name', 'action'])->make(true);
    }

    public function delete($id){
        $admin = Admin::find($id);
        return (isset($admin) && $admin->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

}
