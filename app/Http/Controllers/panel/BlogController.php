<?php

namespace App\Http\Controllers\panel;

use App\Blog;
use App\BlogCategory;
use App\Http\Requests\BlogRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('panel.blog.all');
    }

    public function create()
    {
        $data['categories'] = BlogCategory::orderBy('created_at','DESC')->get();
        return view('panel.blog.add',$data);
    }

    public function edit($id)
    {
        $data['blog'] = Blog::find($id);
        $data['categories'] = BlogCategory::orderBy('created_at','DESC')->get();
        return (isset(  $data['blog'])) ? view('panel.blog.edit',$data) : redirect()->route(get_current_locale().'.panel.dashboard');
    }

    public function store(BlogRequest $request)
    {
        $item = Blog::create($request->all());
        $item->categories()->sync(convert_array_map_int($request->categories));
        return (isset($item)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function update($id,BlogRequest $request)
    {
        $item = Blog::updateOrCreate(['id'=>$id],$request->all());
        $item->categories()->sync(convert_array_map_int($request->categories));
        return (isset($item)) ? $this->response_api(true, 'تم تعديل التدوينة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function get_data_table(Blog $items)
    {
        $items = $items->orderBy('created_at', 'DESC')->get();
        try {
            return DataTables::of($items)->editColumn('locale', function ($item) {
                return $item->locale == 'ar' ? 'العربية' : 'الإنجليزية';
            })->editColumn('category_id', function ($item) {
                return $item->categoryName();
            })->editColumn('created_at', function ($item) {
                return get_date_from_timestamp($item->created_at);
            })->addColumn('action', function ($item) {
                return '<div class="row">
                        <a title="تعديل" style="margin-right: 10px" href="'.admin_url('blog/'.$item->id.'/edit/') .'"   class="btn btn-sm btn-primary " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                        <a  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' .lang_route('panel.blog.delete',['id'=>$item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                        </div>';
            })->rawColumns(['action'])->make(true);
        } catch (\Exception $e) {
            return $this->response_api(false, 'falied');
        }
    }

    public function categories_index()
    {
        return view('panel.blog.categories');
    }


    public function categories_data(BlogCategory $items)
    {
        $items = $items->orderBy('created_at', 'DESC')->get();
        try {
            return DataTables::of($items)->addColumn('action', function ($item) {
                return '<div class="row">
                        <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-primary edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('blog/categories/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                        </div>';
            })->rawColumns(['icon_class', 'action'])->make(true);
        } catch (\Exception $e) {
            return $this->response_api(false, 'falied');
        }
    }

    public function get_category_data($id)
    {
        $item = BlogCategory::find($id);
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_category(Request $request)
    {
        $item = BlogCategory::create($request->all());
        return (isset($item)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_category(Request $request, $id)
    {
        $item = BlogCategory::find($id);
        return (isset($item) && $item->update($request->all())) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_category($id)
    {
        $item = BlogCategory::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete($id){
        $item = Blog::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');

    }
}
