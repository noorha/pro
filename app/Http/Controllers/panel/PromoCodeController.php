<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\PromoCodeRequest;
use App\PromoCode;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PromoCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        return view('panel.promo-code.all');
    }


    public function create()
    {
        return view('panel.promo-code.create');
    }

    public function edit($id)
    {
        $data['item'] = PromoCode::find($id);
//        dd($data['item']->isActive());
        return isset($data['item'])?  view('panel.promo-code.create',$data) : redirect()->route(get_current_locale().'.panel.dashboard');
    }


    public function getDataTable(PromoCode $items)
    {
        $items = $items->orderBy('created_at', 'DESC');
        return DataTables::of($items)->editColumn('link', function ($item) {
            return '<a target="_blank" href="'.$item->link.' ">'.$item->link.'</a>';
        })->editColumn('start_time', function ($item) {
            return get_date_from_timestamp($item->start_time);
        })->editColumn('end_time', function ($item) {
            return get_date_from_timestamp($item->end_time);
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a title="تعديل" href="' . lang_route('panel.promo-code.edit.index', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.promo-code.delete', ['id'=>$item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['link','photo','action'])->make(true);
    }

    public function store(PromoCodeRequest $request)
    {
        $couponCode = PromoCode::updateOrCreate(['code'=>$request->code],$request->only(PromoCode::FILLABLE));
        return (isset($couponCode)) ? $this->response_api(true, 'تم الحفظ بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function update($id, PromoCodeRequest $request)
    {
        $couponCode = PromoCode::updateOrCreate(['id'=>$id,'code'=>$request->code],$request->only(PromoCode::FILLABLE));
        return (isset($couponCode)) ? $this->response_api(true, 'تم الحفظ بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete($id)
    {
        $couponCode = PromoCode::find($id);
        return (isset($couponCode) && $couponCode->delete()) ? $this->response_api(true, 'تم الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }
}
