<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\ReportReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {

        return view(view_panel() . 'home');
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect('/' . get_current_locale() . '/admin/login');
    }


}
