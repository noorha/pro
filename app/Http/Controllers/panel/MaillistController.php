<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Mail\ReplayMail;
use App\MailItem;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class MaillistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view(view_panel() . 'mail-list.view-mails');
    }

    public function create()
    {
        return view(view_panel() . 'mail-list.send-mail');
    }


    public function getDataTable()
    {
        $items = MailItem::orderBy('created_at', 'DESC');
        return DataTables::of($items)->editColumn('status', function ($item) {
            return ($item->status == 'active') ? '<a class="btn btn-success  btn-sm change-status" data-id="' . $item->id . '">فعّال</a>' : '<a class="btn btn-danger  btn-sm change-status" data-id="' . $item->id . '">موقوف</a> ';
        })->addColumn('action', function ($item) {
            return
                '<div class="row">
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.mail.delete',[ $item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                 </div>';
        })->rawColumns(['status', 'action'])->make(true);
    }

    public function changeStatus($id)
    {
        $mail = MailItem::find($id);
        if (isset($mail)) {
            $mail->status = ($mail->status == 'active') ? 'suspended' : 'active';
        }
        return response()->json(['status' => $mail->save()]);
    }


    public function sendEmailsToList(Request $request)
    {
        $list = MailItem::where('status', 'active')->get();
        $rules = [
            'title' => 'required',
            'msg' => 'required'
        ];
        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'message' => 'الرجاء التأكد من الحقول المدخلة']);
        }
        foreach ($list as $item) {
            Mail::to($item->email, 'Grow Me')->send(new  ReplayMail($request->title, $request->msg, $item->email));
            if ((count(Mail::failures()) > 0)) {
                return response()->json(['status' => false, 'message' => 'حدث خطأ أثناء الإرسال .. ']);
            }
        }
        if ($list->count() > 0) {
            return response()->json(['status' => true, 'message' => 'تمت العملية بنجاح ']);
        }
        return response()->json(['status' => false, 'message' => 'لا يوجد مشتركين في القائمة البريدية.. ']);
    }

    public function destroy($id)
    {
        $mail = MailItem::find($id);
        if ($mail->delete()) {
            return $this->response_api(true, 'تمت عملية الحذف بنجاح');
        }
        return $this->response_api(false, 'فشلت عملية الحذف');
    }
}
