<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\ScheduledMessageRequest;
use App\Mail\ReplayMail;
use App\ScheduledMessage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Yajra\DataTables\DataTables;

class ScheduledMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function sendIndex()
    {
        $data['users'] = User::active()->get();
        return view('panel.scheduled-messages.send-mail',$data);
    }

    public function userSearch()
    {
        return response()->json(User::paginate(9));
    }

    public function sendMail(Request $request)
    {
        $users = User::query()->scheduledMessages($request)->get();
        $rules = [
            'title' => 'required',
            'text' => 'required'
        ];
        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'message' => 'الرجاء التأكد من الحقول المدخلة']);
        }
        foreach ($users as $item) {
            Mail::to($item->email, 'HalaPro')->send(new  ReplayMail($request->title, $request->text, $item->email));
            if ((count(Mail::failures()) > 0)) {
                return response()->json(['status' => false, 'message' => 'حدث خطأ أثناء الإرسال .. ']);
            }
        }
        if ($users->count() > 0) {
            return response()->json(['status' => true, 'message' => 'تمت العملية بنجاح ']);
        }
        return response()->json(['status' => false, 'message' => 'لا يوجد مشتركين في القائمة البريدية.. ']);

    }

    public function scheduleIndex()
    {
        return view('panel.scheduled-messages.all');
    }

    public function create()
    {
        $data['item'] = new ScheduledMessage;
        return view('panel.scheduled-messages.create', $data);
    }

    public function edit($id)
    {
        $data['item'] = ScheduledMessage::find($id);
        return isset($data['item']) ? view('panel.scheduled-messages.create', $data) : redirect()->route(get_current_locale() . '.panel.dashboard');
    }

    public function store(ScheduledMessageRequest $request)
    {
        $schedule = ScheduledMessage::create($request->only(ScheduledMessage::FILLABLE));
        return (isset($schedule)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function getDataTable()
    {
        $items = ScheduledMessage::orderBy('created_at', 'DESC');
        return DataTables::of($items)->editColumn('is_enabled', function ($item) {
            return ($item->is_enabled == 1) ? '<a class="btn btn-success  btn-sm change-status" data-id="' . $item->id . '">فعّال</a>' : '<a class="btn btn-danger  btn-sm change-status" data-id="' . $item->id . '">موقوف</a> ';
        })->editColumn('type', function ($item) {
            switch ($item->type) {
                case 0 :
                    return 'الكل';
                case 1 :
                    return 'الطلاب فقط';
                case 2 :
                    return 'المدربين فقط';
            }
        })->editColumn('gender', function ($item) {
            switch ($item->gender) {
                case 0 :
                    return 'الكل';
                case 1 :
                    return 'الذكور فقط';
                case 2 :
                    return 'الإناث فقط';
            }
        })->editColumn('period', function ($item) {
            switch ($item->period) {
                case 'daily' :
                    return 'يومياً';
                case 'weekly' :
                    return ' أسبوعياً';
                case 'monthly' :
                    return 'شهرياً ';
                case 'yearly' :
                    return 'سنوياً ';
            }
        })->addColumn('action', function ($item) {
            return '<div class="row">
                       <a title="تعديل" style="margin-right: 10px"  href="' . lang_route('panel.scheduled-messages.edit.index', [$item->id]) . '" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                       <a  data-toggle="delete" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('role/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                    </div>';
        })->rawColumns(['is_enabled', 'action'])->make(true);
    }

    public function update($id, ScheduledMessageRequest $request)
    {
        $schedule = ScheduledMessage::updateOrCreate(['id'=>$id],$request->only(ScheduledMessage::FILLABLE));
        return (isset($schedule)) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }
}
