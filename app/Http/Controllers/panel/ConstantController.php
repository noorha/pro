<?php

namespace App\Http\Controllers\panel;

use App\City;
use App\Country;
use App\CourseCategory;
use App\Http\Controllers\Controller;
use App\Speciality;
use App\SubSpecialty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ConstantController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin','permission:constant_settings']);
    }

    public function countries_index()
    {
        return view(view_panel() . 'constant.countries');
    }

    public function countries_data(Country $countries)
    {
        $countries = $countries->orderBy('created_at', 'DESC');
        return DataTables::of($countries)->addColumn('icon', function ($item) {
            return '<img src="' . image_url($item->icon, '30x30') . '">';
        })->addColumn('name', function ($item) {
            return $item->name;
        })->addColumn('name_en', function ($item) {
            return $item->name_en;
        })->addColumn('action', function ($item) {
            return '<div class="row">
                    <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                    <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('constant/countries/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['icon', 'action'])->make(true);
    }


    public function get_country_data($id)
    {
        $item = Country::find($id)->makeVisible(['name', 'name_en', 'icon']);;
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_country(Request $request)
    {
        $country = Country::create(['name' => $request->name, 'name_en' => $request->name_en, 'icon' => $request->icon, 'code' => $request->code]);
        return (isset($country)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_country(Request $request, $id)
    {
        $country = Country::find($id);
        return (isset($country) && $country->update(['name' => $request->name, 'name_en' => $request->name_en, 'icon' => $request->icon, 'code' => $request->code])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_country($id)
    {
        $country = Country::find($id);
        return (isset($country) && $country->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function cities_index()
    {
        return view(view_panel() . 'constant.cities');
    }

    public function cities_data(City $cities)
    {
        $cities = $cities->orderBy('created_at', 'DESC')->get();
        return DataTables::of($cities)->editColumn('country_id', function ($item) {
            return $item->getCountryName();
        })->addColumn('name', function ($item) {
            return $item->name;
        })->addColumn('name_en', function ($item) {
            return $item->name_en;
        })->addColumn('action', function ($item) {
            return '<div class="row">
                    <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                    <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('constant/cities/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['action'])->make(true);
    }


    public function get_city_data($id)
    {
        $item = City::find($id)->makeVisible(['name', 'name_en', 'country_id']);;
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_city(Request $request)
    {
        $city = City::create(['country_id' => $request->country_id, 'name_en' => $request->name_en, 'name' => $request->name]);
        return (isset($city)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_city(Request $request, $id)
    {
        $city = City::find($id);
        return (isset($city) && $city->update(['country_id' => $request->country_id, 'name_en' => $request->name_en, 'name' => $request->name])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_city($id)
    {
        $city = City::find($id);
        return (isset($city) && $city->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function specialities_index()
    {
        return view(view_panel() . 'constant.specialities');
    }

    public function specialities_data(Speciality $specialities)
    {
        $specialities = $specialities->orderBy('created_at', 'DESC')->get();
        try {
            return DataTables::of($specialities)->addColumn('icon', function ($item) {
                return '<img style="max-width:100px;max-height:100px" src="' . image_url($item->icon) . '">';
            })->addColumn('name', function ($item) {
                return $item->name;
            })->addColumn('name_en', function ($item) {
                return $item->name_en;
            })->addColumn('action', function ($item) {
                return '<div class="row">
                            <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                            <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('constant/specialities/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                        </div>';
            })->rawColumns(['icon', 'action'])->make(true);
        } catch (\Exception $e) {
            return $this->response_api(false, 'failed');
        }
    }


    public function get_speciality_data($id)
    {
        $item = Speciality::find($id)->makeVisible(['name', 'name_en', 'icon']);;
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_speciality(Request $request)
    {
        $city = Speciality::create(['name' => $request->name, 'name_en' => $request->name_en, 'icon' => $request->icon]);
        return (isset($city)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_speciality(Request $request, $id)
    {
        $speciality = Speciality::find($id);
        return (isset($speciality) && $speciality->update(['name' => $request->name, 'name_en' => $request->name_en, 'icon' => $request->icon])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_speciality($id)
    {
        $speciality = Speciality::find($id);
        return (isset($speciality) && $speciality->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function sub_specialities_index()
    {
        return view(view_panel() . 'constant.sub_specialities');
    }

    public function sub_specialities_data(SubSpecialty $specialities)
    {
        $specialities = $specialities->orderBy('created_at', 'DESC')->get();
        return DataTables::of($specialities)->editColumn('specialty_id', function ($item) {
            return $item->getSpecialtyName();
        })->editColumn('is_selected', function ($item) {
            return '<input type="checkbox" class="select-checkbox-new" data-url="' . lang_route('panel.constant.sub-specialities.change-select', ['id' => $item->id]) . '" name="is_selected[' . $item->id . ']" ' . (($item->is_selected == 1) ? 'checked' : '') . '>';
        })->addColumn('name', function ($item) {
            return $item->name;
        })->addColumn('name_en', function ($item) {
            return $item->name_en;
        })->addColumn('icon', function ($item) {
            return '<img style="max-width:100px;max-height:100px" src="' . image_url($item->icon) . '">';
        })->addColumn('action', function ($item) {
            return '<div class="row">
                             
                        <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . admin_url('constant/sub-specialities/delete/' . $item->id) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['is_selected', 'icon', 'action'])->make(true);
    }


    public function get_sub_speciality_data($id)
    {
        $item = SubSpecialty::find($id)->makeVisible(['name', 'name_en', 'specialty_id']);
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_sub_speciality(Request $request)
    {
        $city = SubSpecialty::create(['name' => $request->name, 'name_en' => $request->name_en, 'icon' => $request->icon, 'specialty_id' => $request->specialty_id]);
        return (isset($city)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_sub_speciality(Request $request, $id)
    {
        $speciality = SubSpecialty::find($id);
        return (isset($speciality) && $speciality->update(['specialty_id' => $request->specialty_id, 'name_en' => $request->name_en, 'name' => $request->name, 'icon' => $request->icon])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_sub_speciality($id)
    {
        $speciality = SubSpecialty::find($id);
        return (isset($speciality) && $speciality->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }


    public function categories_index()
    {
        return view(view_panel() . 'constant.categories');
    }

    public function categories_data(CourseCategory $categories)
    {
        $categories = $categories->orderBy('created_at', 'DESC')->get();
        try {
            return DataTables::of($categories)->addColumn('text', function ($item) {
                return $item->text;
            })->addColumn('text_en', function ($item) {
                return $item->text_en;
            })->addColumn('icon', function ($item) {
                return '<img src="' . image_url($item->icon, '30x30') . '">';
            })->addColumn('action', function ($item) {
                return '<div class="row">
                        <button title="تعديل" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.constant.categories.delete', ['id' => $item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                        </div>';
            })->rawColumns(['icon', 'action'])->make(true);
        } catch (\Exception $e) {
            return $this->response_api(false, 'failed');
        }
    }

    public function change_select($id)
    {
        $subSpecialty =  SubSpecialty::find($id);
        if (isset($subSpecialty)){
            $subSpecialty->update(['is_selected' => (($subSpecialty->is_selected == 1) ? 0 : 1),'updated_at'=>Carbon::now()]);
            return $this->response_api(true,'success');
        }
        return $this->response_api(false,'failed');
    }


    public function get_category_data($id)
    {
        $item = CourseCategory::find($id)->makeVisible(['text', 'text_en']);;
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store_category(Request $request)
    {
        $country = CourseCategory::create(['text' => $request->name, 'text_en' => $request->name_en, 'icon' => $request->icon]);
        return (isset($country)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit_category(Request $request, $id)
    {
        $country = CourseCategory::find($id);
        return (isset($country) && $country->update(['text' => $request->name, 'text_en' => $request->name_en, 'icon' => $request->icon])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete_category($id)
    {
        $country = CourseCategory::find($id);
        return (isset($country) && $country->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

}
