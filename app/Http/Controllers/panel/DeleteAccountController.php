<?php

namespace App\Http\Controllers\panel;

use App\DeleteAccountRequest;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DeleteAccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getDataTables(Request $request, DeleteAccountRequest $deleteAccountRequests)
    {
        $deleteAccountRequests = $deleteAccountRequests->status($request->status)->withTrashed()->orderBy('created_at', 'DESC');
        return DataTables::of($deleteAccountRequests)->editColumn('user_id', function ($item) {
            return isset($item->user) ? '<a target="_blank" href="' . lang_route('profile', [$item->user->id]) . '">' . $item->user->getUserName() . '</a>' : '';
        })->editColumn('status', function ($item) {
            switch ($item->status) {
                case 'pending' :
                    return '<a disabled class="btn btn-warning  btn-sm"> قيد الإنتظار </a>';
                case 'accepted' :
                    return '<a disabled  class="btn btn-success  btn-sm">مقبول </a>';
                case 'rejected' :
                    return '<a disabled class="btn btn-danger  btn-sm">مرفوض</a>';
                default:
                    return $item->status;
            }
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            switch ($item->status) {
                case 'pending':
                    return
                        '<div class="row">
                        <a  data-toggle="reject" title="" style="margin-right: 10px"  data-url="' . lang_route('panel.delete-request.reject', [$item->id]) . '"      class="btn btn-sm btn-warning reject"><i class="glyphicon glyphicon-ban-circle"></i> رفض</a>
                        <a title="قبول" data-toggle="accept" data-url="' . lang_route('panel.delete-request.accept', [$item->id]) . '" href="' . lang_route('panel.teacher.edit', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success  accept" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>قبول</a>
                    </div>';

                default:
                    return 'لا يوجد عمليات';
            }
        })->rawColumns(['user_id', 'status', 'action'])->make(true);
    }

    public function index()
    {
        return view(view_panel() . 'delete.all');
    }

    public function accept($id)
    {
        $request = DeleteAccountRequest::withTrashed()->find($id);
        return (isset($request) && $request->deleteUser()) ? $this->response_api(true, 'تم قبول الطلب بنجاح') : $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }


    public function reject($id, Request $request1)
    {
        $request = DeleteAccountRequest::find($id);
        return (isset($request) && $request->reject($request1)) ? $this->response_api(true, 'تم رفض الطلب بنجاح') : $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }
}
