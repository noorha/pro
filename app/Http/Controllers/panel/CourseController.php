<?php

namespace App\Http\Controllers\panel;

use App\Course;
use App\CourseSubscription;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use Yajra\DataTables\DataTables;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        return view(view_panel() . 'course.all');
    }

    public function create()
    {
        return view(view_panel() . 'course.create');
    }


    public function store(CourseRequest $request)
    {
        $course = Course::create($request->all());
        return (isset($course) && $course->incrementCount()) ? $this->response_api(true, 'تم إضافة الدورة بنجاح')
            : $this->response_api(false, 'حدث خطأ غير متوقع أثناء المعالجة');
    }


    public function edit($id)
    {
        $course = Course::find($id);
        return (isset($course)) ? view(view_panel() . 'course.edit', ['course' => $course]) : redirect()->route('ar.panel.dashboard');
    }


    public function update($id, CourseRequest $request)
    {
        $course = Course::find($id);
        return (isset($course) && $course->update($request->all())) ? $this->response_api(true, 'تم تعديل الدورة بنجاح')
            : $this->response_api(false, 'حدث خطأ غير متوقع أثناء المعالجة');
    }

    public function get_data_table(Course $courses)
    {
        $courses = $courses->publishedCourses()->orderBy('created_at', 'DESC');
        return DataTables::of($courses)->editColumn('title', function ($item) {
            return '<a href="' . lang_route('panel.course.edit', [$item->id]) . '">' . $item->title . '</a>';
        })->editColumn('is_published', function ($item) {
            return ($item->is_published == '1') ? '<a disabled class="btn btn-success  btn-sm">منشورة </a>'
                : '<a disabled class="btn btn-warning  btn-sm">غير منشورة   </a>';

        })->editColumn('category_id', function ($item) {
            return $item->categoryName();
        })->editColumn('country_id', function ($item) {
            return $item->countryName();
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a title="تعديل" href="' . lang_route('panel.course.edit',[ $item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                        <a  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.course.delete',[ $item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                    </div>';
        })->rawColumns(['title', 'is_published', 'action'])->make(true);
    }

    public function delete($id)
    {
        $course = Course::find($id);
        return (isset($course) && $course->delete() && $course->decrementCount()) ? $this->response_api(true, 'تم حذف الدورة بنجاح')
            : $this->response_api(false, 'حدث خطأ غير متوقع أثناء المعالجة');
    }

    public function requests_index()
    {
        return view(view_panel() . 'course.join-requests');
    }

    public function requests_data_table(CourseSubscription $subscriptions)
    {
        $subscriptions = $subscriptions->orderBy('created_at', 'DESC')->get();
        return DataTables::of($subscriptions)->editColumn('course_id', function ($item) {
            return isset($item->course) ? '<a href="' . lang_route('panel.course.edit',[ $item->course->id]) . '">' . $item->course->title . '</a>' : no_data();
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.course.requests.delete', [$item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                    </div>';
        })->rawColumns(['course_id', 'action'])->make(true);
    }


    public function delete_join_request($id)
    {
        $item = CourseSubscription::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تم حذف طلب الإنضمام بنجاح')
            : $this->response_api(false, 'حدث خطأ غير متوقع أثناء المعالجة');
    }
}
