<?php

namespace App\Http\Controllers\panel;

use App\FinancialProcess;
use App\Http\Controllers\Controller;
use App\WithdrawRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function credit_index()
    {
        return view(view_panel().'payment.credit');
    }


    public function credit_data(FinancialProcess $financialProcesses)
    {
        $financialProcesses = $financialProcesses->payCredit()->orderBy('created_at', 'DESC')->get();
        return DataTables::of($financialProcesses)->editColumn('user_id', function ($item) {
            return  $item->getName();
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->rawColumns(['name', 'status', 'action'])->make(true);
    }

    public function withdraw_data(Request $request,WithdrawRequest $withdrawRequests)
    {
        $withdrawRequests = $withdrawRequests->status($request->status)->orderBy('created_at', 'DESC');
        return DataTables::of($withdrawRequests)->editColumn('user_id', function ($item) {
            return (isset($item->user)) ? $item->user->getUserName() : '';
        })->editColumn('status', function ($item) {
            switch ($item->status) {
                case 'pending' :
                    return '<a disabled class="btn btn-warning  btn-sm"> قيد الإنتظار </a>';
                case 'accepted' :
                    return '<a disabled  class="btn btn-success  btn-sm">مقبول </a>';
                case 'rejected' :
                    return '<a disabled class="btn btn-danger  btn-sm">مرفوض</a>';
                default:
                    return $item->status;
            }
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            switch ($item->status) {
                case 'pending':
                    return
                   '<div class="row">
                        <a  data-toggle="reject" title="" style="margin-right: 10px"  data-url="' . lang_route('panel.payment.withdraw.reject', [$item->id]) . '"      class="btn btn-sm btn-warning reject"><i class="glyphicon glyphicon-ban-circle"></i> رفض</a>
                        <a title="قبول" data-toggle="accept" data-url="' .  lang_route('panel.payment.withdraw.accept', [$item->id])  . '" href="' . lang_route('panel.teacher.edit', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success  accept" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>قبول</a>
                    </div>';
                default:
                    return 'لا يوجد عمليات';
            }
        })->rawColumns(['user_id', 'status', 'action'])->make(true);
    }

    public function withdraw_index()
    {
        return view(view_panel().'payment.withdraw');
    }

    public function accept_withdraw($id)
    {
        $request = WithdrawRequest::find($id);
        return (isset($request) && $request->accept()) ? $this->response_api(true, 'تم قبول الطلب بنجاح') : $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }

    public function reject_withdraw($id)
    {
        $request = WithdrawRequest::find($id);
        return (isset($request) && $request->reject()) ? $this->response_api(true, 'تم رفض الطلب بنجاح') : $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }

    public function delete_withdraw($id)
    {
        $request = WithdrawRequest::find($id);
        return (isset($request) && $request->delete()) ? $this->response_api(true, 'تم حذف الطلب بنجاح') : $this->response_api(false, 'فشلت العملية');

    }

}
