<?php

namespace App\Http\Controllers\panel;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class PartnersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        return view(view_panel().'partner.all');
    }


    public function all_data_table(Partner $items)
    {
        $items = $items->orderBy('created_at', 'DESC');
        return DataTables::of($items)->editColumn('link', function ($item) {
            return '<a target="_blank" href="'.$item->link.' ">'.$item->link.'</a>';
        })->editColumn('photo', function ($item) {
            return '<img style="max-height:200px;max-width:200px" src="'.image_url($item->photo).'">';
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <button title="تعديل" data-edit="'.lang_route('panel.partners.item',[$item->id]).'" style="margin-right: 10px" data-id="' . $item->id . '" data-toggle="modal" data-target=".bs-example-modal-photo" class="btn btn-sm btn-success edit" ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</button>
                        <button  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.partners.delete', ['id'=>$item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</button>
                    </div>';
        })->rawColumns(['link','photo','action'])->make(true);
    }

    public function get_item_data($id)
    {
        $item = Partner::find($id);
        return (isset($item)) ? $this->response_api(true, 'success', $item) : $this->response_api(false, 'success');
    }

    public function store(Request $request)
    {
        $item = Partner::create(['link' => $request->link, 'photo' => $request->photo]);
        return (isset($item)) ? $this->response_api(true, 'تمت الإضافة بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function edit(Request $request, $id)
    {
        $item = Partner::find($id);
        return (isset($item) && $item->update(['link' => $request->link, 'photo' => $request->photo])) ? $this->response_api(true, 'تمت عملية التعديل بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

    public function delete($id)
    {
        $item = Partner::find($id);
        return (isset($item) && $item->delete()) ? $this->response_api(true, 'تمت عملية الحذف بنجاح') : $this->response_api(false, 'حدث خطأ غير متوقع');
    }

}
