<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Notifications\PanelConfirmMail;
use App\Notifications\UserNotification;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Yajra\DataTables\DataTables;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view(view_panel() . 'user.all', ['type' => 'student']);
    }

    public function create()
    {
        return view(view_panel() . 'user.add', ['type' => 'student']);
    }

    public function edit(User $user)
    {
        return isset($user) ? view(view_panel() . 'user.edit', ['type' => 'student', 'user' => $user]) : redirect()->route('ar.panel.dashboard');
    }

    public function activate($id)
    {
        $user = User::find($id);
        if (isset($user) && $user->status == 'inactive') {
            $user->removeActivationLink();
            $user->update(['status' => 'active']);
            return $this->response_api(true, '');
        }
        return $this->response_api(false, '');

    }

    public function get_data_table(User $students,Request $request)
    {
        $students = $students->students()->filter($request)->orderBy('created_at', 'DESC');
        return DataTables::of($students)->editColumn('name', function ($item) {
            return '<a href="' . lang_route('panel.student.edit', [$item->id]) . '">' . $item->getUserName() . '</a>';
        })->editColumn('status', function ($item) {
            switch ($item->status) {
                case 'inactive' :
                    return '<a  data-url="' . lang_route('panel.student.activate', [$item->id]) . '" class="btn btn-info  btn-sm activate-account">غير مفعّل </a>';
                case 'active' :
                    return '<a disabled  class="btn btn-success  btn-sm">مفعّل </a>';
                case 'suspended' :
                    return '<a disabled class="btn btn-danger  btn-sm">موقوف</a>';
                default:
                    return $item->status;
            }
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at);
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a  data-toggle="reject" title="حذف" style="margin-right: 10px;background-color: #FA2A00"  data-url="' . lang_route('panel.student.delete', [$item->id]) . '"   class="btn btn-sm btn-danger delete"><i class="glyphicon glyphicon-remove"></i> حذف</a>
                        <a  data-toggle="reject" title="حظر /إلغاء حظر" style="margin-right: 10px"  data-url="' . lang_route('panel.student.status', [$item->id]) . '"   class="btn btn-sm btn-warning status"><i class="glyphicon glyphicon-ban-circle"></i> حظر/إلغاء</a>
                        <a title="تعديل" href="' . lang_route('panel.student.edit', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل</a>
                    </div>';
        })->rawColumns(['name', 'status', 'action'])->make(true);
    }


    public function store(RegisterRequest $request, User $user)
    {
        $user = $user->create($request->all());

        return $this->registered($request, $user) ? $this->response_api(true, 'تمت عملية التسجيل بنجاح') : $this->response_api(false, ' فشلت عملية التسجيل ');

    }

    public function update(RegisterRequest $request, $id)
    {
        $user = User::find($id);
        return (isset($user) && $user->update(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'nationality_id' => $request->nationality_id, 'email' => $request->email, 'photo' => $request->photo, 'mobile' => $request->mobile, 'password' => (!(isset($request->password) && !empty($request->password))) ? $user->password : bcrypt($request->password)])) ?
            $this->response_api(true, 'تم تعديل الطالب بنجاح') : $this->response_api(false, 'حدث خطأ أثناء المعالجة');
    }


    public function rules($id)
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users,email,' . $id . ',id,deleted_at,NULL',
            'country_id' => 'required',
        ];
    }

    public function change_status($id)
    {
        $user = User::find($id);
        return (isset($user) && $user->update(['status' => (($user->status == 'suspended') ? 'active' : 'suspended')]) && $user->notifyBand()) ? $this->response_api(true, 'تمت العملية بنجاح') : $this->response_api(false, 'فشلت العملية');
    }

    public function delete($id)
    {
        $student = User::find($id);
        return (isset($student) && $student->decrementCount() && $student->delete()) ? $this->response_api(true, 'تمت العملية بنجاح') : $this->response_api(false, 'فشلت العملية');

    }

    public function registered(Request $request, User $user)
    {
        $link = $user->createLink('confirm');
        if (isset($link)) {
            $user->notify(new PanelConfirmMail($request->password));
            $user->incrementCount();
            return true;
        }
        return false;
    }

    public function notifications_index(User $items)
    {
        $items = $items->students()->get();
        return view(view_panel() . 'notification.send', ['type' => 'student', 'items' => $items]);
    }


    public function send_notifications(Request $request, User $users)
    {

        if ($request->has('select_all')) {
            $users = new User();
            $users = ($request->type == 'student') ?   $users->students()->get() :  $users->teachers()->get() ;
            foreach ($users as $user) {
                $user->notify(new UserNotification(0, 'manager', $request->text, $request->text, '#'));
            }
            return $this->response_api(true, 'تم إرسال الإشعارات بنجاح');
        }
        if ($request->has('users') && !empty($request->users)) {
            $array = explode(',', $request->users);
            $users = $users->whereIn('first_name', 'like', $array)->get();
            if (isset($users)) {
                foreach ($users as $user) {
                    $user->notify(new UserNotification(0, 'manager', $request->text, $request->text, '#'));
                }
                return $this->response_api(true, 'تم إرسال الإشعارات بنجاح');
            }
        }
        return $this->response_api(false, 'الرجاء تحديد مستخدمين');
    }
}
