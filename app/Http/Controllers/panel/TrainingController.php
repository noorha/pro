<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\TrainingRequest;
use App\Traits\FireBase;
use App\User;
use Yajra\DataTables\DataTables;

class TrainingController extends Controller
{
    use FireBase;
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        return view('panel.training.all');
    }

    public function getDataTable(\App\TrainingRequest $items)
    {
        $requests = $items->orderBy('created_at', 'DESC');
        return DataTables::of($requests)->editColumn('teacher_id', function ($item) {
            return '<a target="_blank" href="' . lang_route('panel.teacher.show', [$item->teacher->id]) . '">' . $item->getTeacherName() . '</a>';
        })->editColumn('student_id', function ($item) {
            return '<a target="_blank" href="' . lang_route('panel.student.edit', [$item->student->id]) . '">' . $item->getStudentName() . '</a>';
        })->editColumn('created_at', function ($item) {
            return get_date_from_timestamp($item->created_at) . '   ' . format_24_to_12($item->created_at);
        })->editColumn('status', function ($item) {
            switch ($item->status) {
                case 'pending_teacher':
                    return '<a  disabled="disabled" class="btn btn-warning  btn-sm ">بإنتظار موافقة المدرس</a>';
                case 'pending_student':
                    return '<a  disabled="disabled" class="btn btn-warning  btn-sm ">بإنتظار موافقة الطالب</a>';
                case 'progress':
                    return '<a  disabled="disabled" class="btn btn-info  btn-sm ">قيد التنفيذ</a>';
                case 'completed':
                    return '<a  disabled="disabled" class="btn btn-success  btn-sm ">مكتملة</a>';
                case 'canceled':
                    return '<a  disabled="disabled" class="btn btn-danger  btn-sm ">ملغية</a>';
                case 'rejected':
                    return '<a  disabled="disabled" class="btn btn-danger  btn-sm ">مرفوضة</a>';
            }
            return get_date_from_timestamp($item->created_at) . '   ' . format_24_to_12($item->created_at);
        })->addColumn('action', function ($item) {
            return '<div class="row">
                        <a title="عرض الطلب" target="_blank" href="' . lang_route('panel.training.show', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-primary " ><i style="margin-left: 5px" class="fa fa-search"></i>عرض الطلب</a>
                        <a title="عرض المحادثة" target="_blank" href="' . lang_route('panel.training.conversation', [$item->id]) . '" style="margin-right: 10px" class="btn btn-sm btn-success " ><i style="margin-left: 5px" class="fa fa-check-square-o"></i>عرض المحادثة</a>
                    </div>';
        })->rawColumns(['status', 'teacher_id', 'student_id', 'action'])->make(true);
    }

    public function showRequestDetails($id)
    {
        $trainingRequest = TrainingRequest::find($id);
        return isset($trainingRequest) ?  view('panel.training.view',['item'=>$trainingRequest]) : redirect()->route(get_current_locale().'panel.dashboard');
    }

    public function view_conversation($id)
    {
//        $data['trainingRequest'] = TrainingRequest::find($id);
//        return isset($data['trainingRequest']) ? view('panel.chat.view', ['chat' => $data['trainingRequest']->chat, 'messages' => isset($data['trainingRequest']->chat) ? $data['trainingRequest']->chat->messages()->orderBy('created_at', 'DESC')->paginate(20) : null]) : redirect()->route(get_current_locale() . '.panel.dashboard');
        $chat = $this->getTrainingChatByID($id)->getValue();
        $teacher = new User();
        $student = new User();
        if ($chat) {
            $teacher = User::find($chat['teacherID']);
            $student = User::find($chat['studentID']);
        }
//        if ($chat==null) redirect()->route(get_current_locale().'.panel.dashboard');
//        dd($chat);
        return view('panel.chat.view',compact('id', 'teacher', 'student'));
    }
}
