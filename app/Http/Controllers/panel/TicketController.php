<?php

namespace App\Http\Controllers\panel;

use App\Http\Controllers\Controller;
use App\Notifications\UserNotification;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Ticket $tickets)
    {
        $data['tickets'] = $tickets->orderBy('updated_at', 'DESC')->paginate(10);
        return view('panel.ticket.all', $data);
    }

    public function show($id)
    {
        $data['ticket'] = Ticket::find($id);
        return (isset($data['ticket'])) ? view('panel.ticket.view', $data) : redirect()->route('ar.panel.dashboard');
    }

    public function replay($id, Request $request)
    {
        $ticket = Ticket::find($id);
        if (isset($ticket) && $ticket->canReplay()) {
            $request->request->add(['admin_id' => auth('admin')->user()->id]);
            $ticket->addReplay($request);
//            dd($ticket->user);
            $ticket->user->notify(new UserNotification( auth('admin')->user()->id,'ticket','هناك رد جديد على التذكرة الخاصة بك',__('هناك رد جديد على التذكرة الخاصة بك'),'/'.get_current_locale().'/ticket/view/'.$ticket->id));
            return response()->json(['status' => true, 'message' => 'تم إرسال ردك بنجاح ..', 'item' => '']);
        }
        return response()->json(['status' => false, 'message' => 'حدث خطأ أثناء الإرسال .. ']);
    }

    public function end($id)
    {
        $ticket = Ticket::find($id);
        return (isset($ticket) && $ticket->update(['status'=>1])) ? $this->response_api(true, 'تم إنهاء التذكرة بنجاح') : $this->response_api(false, 'فشلت العملية ');
    }

    public function endTickets(Request $request, Ticket $messages)
    {

        if (!(isset($request->tickets) && count($request->tickets) > 0)) {
            return $this->response_api(false, 'الرجاء تحديد تذكرة واحدة على الأقل');
        }
        $array = array_map('intval', $request->tickets);
        $messages = $messages->whereIn('id', $array);
        $messages->update(['status'=>1]);
        return (isset($messages)) ? $this->response_api(true, 'تمت العملية  بنجاح') : $this->response_api(false, 'فشلت عملية الحذف');
    }



    public function delete(Request $request, Ticket $messages)
    {
        if (!(isset($request->tickets) && count($request->tickets) > 0)) {
            return $this->response_api(false, 'الرجاء تحديد تذكرة واحدة على الأقل');
        }
        $array = array_map('intval', $request->tickets);
        $messages = $messages->whereIn('id', $array);
        return (isset($messages) && $messages->delete()) ? $this->response_api(true, 'تمت العملية  بنجاح') : $this->response_api(false, 'فشلت عملية الحذف');
    }


    public function deleteTicket($id)
    {
        $msg = Ticket::find($id);
        return (isset($msg) && $msg->delete()) ? $this->response_api(true, 'تم حذف التذكرة بنجاح') : $this->response_api(false, 'فشلت عملية الحذف');
    }
}
