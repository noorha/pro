<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Course;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SitemapController extends Controller
{
    public function index()
    {

        // create sitemap
        $sitemap = App::make("sitemap");

        // set cache
//        $sitemap->setCache('laravel.sitemap-index', 3600);

        // add sitemaps (loc, lastmod (optional))
        $blogs = Blog::query()->latest()->first();
        $teacher = User::query()->teachers()->orderBy('updated_at', 'DESC')->first();
        $course = Course::query()->first();
        $sitemap->addSitemap(URL::to(get_current_locale() . '/sitemap/trainers'), isset($teacher) ? $teacher->updated_at : Carbon::now());
        $sitemap->addSitemap(URL::to(get_current_locale() . '/sitemap/courses'), isset($course) ? $course->updated_at : Carbon::now());
        $sitemap->addSitemap(URL::to(get_current_locale() . '/sitemap/pages'), Carbon::now());
        $sitemap->addSitemap(URL::to(get_current_locale() . '/sitemap/blogs'),isset($blogs) ? $blogs->updated_at : Carbon::now());

        // show sitemap
        return $sitemap->render('sitemapindex');

    }

    public function blogs()
    {

        $sitemap_posts = App::make("sitemap");
        // set cache
//        $sitemap_posts->setCache('laravel.sitemap-posts',3600);

        // add items
        $courses = Blog::query()->latest()->get();

        foreach ($courses as $course) {
            $sitemap_posts->add(lang_route('blog.view', [$course->id]) . '/' . str_replace(' ', '-', $course->title), $course->updated_at, 0.7, 'weekly');
        }

        // show sitemap
        return $sitemap_posts->render('xml');
    }

    public function trainers()
    {
        // create sitemap
        $sitemap_posts = App::make("sitemap");

        // set cache
//        $sitemap_posts->setCache('laravel.sitemap-posts',3600);

        // add items
        $teachers = User::query()->teachers()->latest()->get();

        foreach ($teachers as $teacher) {
            $sitemap_posts->add(lang_route('profile', ['id' => $teacher->id]) . '/' . str_replace(' ', '-', $teacher->name), $teacher->updated_at, 0.9, 'weekly');
        }

        // show sitemap
        return $sitemap_posts->render('xml');

    }

    public function courses()
    {

        $sitemap_posts = App::make("sitemap");

        // set cache
//        $sitemap_posts->setCache('laravel.sitemap-posts',3600);

        // add items
        $courses = Course::query()->latest()->get();

        foreach ($courses as $course) {
            $sitemap_posts->add(lang_route('course.view', [$course->id]) . '/' . str_replace(' ', '-', $course->title), $course->updated_at, 0.7, 'weekly');
        }

        // show sitemap
        return $sitemap_posts->render('xml');
    }

    public function pages()
    {

        $sitemap_posts = App::make("sitemap");

        // set cache
//        $sitemap_posts->setCache('laravel.sitemap-posts',3600);

        // add items


        $sitemap_posts->add(lang_route('main'), Carbon::now(), 1, 'monthly');
        $sitemap_posts->add(lang_route('about'), Carbon::now(), 1, 'monthly');
        $sitemap_posts->add(lang_route('work.how'), Carbon::now(), 0.8, 'monthly');
        $sitemap_posts->add(lang_route('privacy'), Carbon::now(), 0.9, 'monthly');
        $sitemap_posts->add(lang_route('policy'), Carbon::now(), 0.9, 'monthly');
        $sitemap_posts->add(lang_route('work.us'), Carbon::now(), 0.9, 'monthly');
        $sitemap_posts->add(lang_route('teacher.search'), Carbon::now(), 0.9, 'monthly');


        // show sitemap
        return $sitemap_posts->render('xml');
    }

}
