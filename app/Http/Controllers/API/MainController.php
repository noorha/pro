<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Country;
use App\Day;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\VisitorMessageRequest;
use App\Logic\FileRepository;
use App\Logic\ImageRepository;
use App\Page;
use App\Speciality;
use App\SubSpecialty;
use App\VisitorMessage;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;

class MainController extends Controller
{

    public function upload_image()
    {
        $photo = Input::all();
        $image = new ImageRepository();
        $response = $image->upload($photo, 'image');
        return $response;
    }


    public function upload_file()
    {
        $photo = Input::all();
        $file = new FileRepository();
        $response = $file->upload($photo, 'file');
        return $response;
    }


    public function getWeekDays()
    {
        $days = Day::orderBy('id', 'ASC')->get();
        return (isset($days)) ? JsonResponse::create(['status' => true, 'data' => $days], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }

    public function countries()
    {
        $countries = Country::all()->sortBy(locale_value('name', 'name_en'))->values();
        return (isset($countries)) ? JsonResponse::create(['status' => true, 'data' => $countries], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }


    public function cities()
    {
        $cities = City::orderBy('name', 'DESC')->get();
        return (isset($cities)) ? JsonResponse::create(['status' => true, 'data' => $cities], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }

    public function getCitiesByCountry($id)
    {
        $country = Country::find($id);
        return (isset($country->cities)) ? JsonResponse::create(['status' => true, 'data' => $country->cities], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }


    public function specialities()
    {
        $specialities = Speciality::with('subSpecialties')->orderBy('name', 'DESC')->get();
        return (isset($specialities)) ? JsonResponse::create(['status' => true, 'data' => $specialities], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }

    public function sendVisitorMessage(VisitorMessageRequest $request)
    {
        $msg = VisitorMessage::create($request->all());
        return (isset($msg)) ? JsonResponse::create(['status' => true, 'message' => trans('messages.message_sent_successfully')], 200) : JsonResponse::create(['status' => false, 'error' => 'unknown_error']);
    }

    public function getPageDetails($type, Page $page)
    {
        $page = $page->type($type);
        return (isset($page)) ? JsonResponse::create(['status' => true, 'data' => $page], 200) : JsonResponse::create(['status' => true, 'error' => 'page_not_found'], 404);
    }

    public function subSpecialities()
    {
        $subSpecialities = SubSpecialty::with('speciality')->orderBy('name', 'DESC')->get();
        return (isset($subSpecialities)) ? JsonResponse::create(['status' => true, 'data' => $subSpecialities], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }


    public function findSpecialty($id)
    {
        $specialities = Speciality::with('subSpecialties')->find($id);
        return (isset($specialities)) ? JsonResponse::create(['status' => true, 'data' => $specialities], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }

    public function getCompleteProfileText($type, $auth_type)
    {
        return JsonResponse::create(['status' => true, 'text' => strip_tags(get_constant_value(get_current_locale() == 'ar' ? $auth_type . '_' . $type : $auth_type . '_' . $type . '_en'))]);
    }

    public function getCountriesWithCities()
    {
        $countries = Country::with('cities')->get()->sortBy(locale_value('name', 'name_en'))->values();
        return (isset($countries)) ? JsonResponse::create(['status' => true, 'data' => $countries], 200) : JsonResponse::create(['status' => false, 'error' => 'Server Error'], 404);
    }


}
