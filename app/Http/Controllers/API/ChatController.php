<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\Events\MessageSentEvent;
use App\Http\Controllers\Controller;
use App\Notifications\TrainingNotification;
use App\TrainingRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Pusher\Pusher;
use Pusher\PusherException;

class ChatController extends Controller
{

    public function chat_index($type, $id)
    {
        $chat = auth('api')->user()->getChat($type, $id);
        if (isset($chat)) {
            $chat->markAsRead();
            $data['status'] = true;
            $data['data']['id'] = $chat->id;
//            $data['data']['teacher_id'] =$chat->teacher_id;
//            $data['data']['student_id'] =$chat->student_id;
            $data['data']['created_at'] = (string)$chat->created_at;
            $data['data']['created_at_diff'] = diff_for_humans($chat->created_at);
            $data['data']['teacher'] = [
                'id' => $chat->teacher_id,
                'name' => $chat->getTeacherName(),
                'photo' => $chat->getTeacherPhoto(),
                'photoUrl' => image_url($chat->getTeacherPhoto())
            ];
            $data['data']['student'] = [
                'id' => $chat->student_id,
                'name' => $chat->getStudentName(),
                'photo' => $chat->getStudentPhoto(),
                'photoUrl' => image_url($chat->getStudentPhoto())
            ];
            return JsonResponse::create($data, 200);
        }
        return $this->response_api(false, 'failed');
    }

    public function chat_request_index($id)
    {
        $trainingRequest = TrainingRequest::find($id);
        if (isset($trainingRequest) && $trainingRequest->isAccepted()) {
            $chat = $trainingRequest->getChat();
            if (isset($chat)) {
                $chat->markAsRead();
                $data['status'] = true;
                $data['data']['id'] = $chat->id;
//                $data['data']['teacher_id'] =$chat->teacher_id;
//                $data['data']['student_id'] =$chat->student_id;
                $data['data']['created_at'] = (string)$chat->created_at;
                $data['data']['created_at_diff'] = diff_for_humans($chat->created_at);
                $data['data']['teacher'] = [
                    'id' => $chat->teacher_id,
                    'name' => $chat->getTeacherName(),
                    'photo' => $chat->getTeacherPhoto(),
                    'photoUrl' => image_url($chat->getTeacherPhoto())
                ];
                $data['data']['student'] = [
                    'id' => $chat->student_id,
                    'name' => $chat->getStudentName(),
                    'photo' => $chat->getStudentPhoto(),
                    'photoUrl' => image_url($chat->getStudentPhoto())
                ];
                return JsonResponse::create($data, 200);
            }
        }
        return $this->response_api(false, 'failed');

    }

    public function get_chat_messages(Chat $chat, Request $request)
    {
        $skip = (isset($request->skip)) ? $request->skip : 0;
        $data['status'] = true;
        $data['data'] = $chat->messages()->with('files')->latest()->skip($skip)->take(8)->get()->values();
        $data['count'] = $chat->messages()->count();
        return (isset($chat->messages) && $chat->messages()->count() > 0) ?
            JsonResponse::create($data, 200)
            : $this->response_api(false, 'failed');
    }

    public function send_new_message($type, $id, Request $request)
    {
        $chat = Chat::find($id);
        if (isset($chat)) {
            $msg = $chat->createMessage($request, $type);
            $notifiable = auth('api')->user()->isStudent() ? $chat->teacher : $chat->student;
            $notifiable->sendNotification([
                'title' => $notifiable->locale == 'ar' ?  'هناك رسالة جديدة' : 'New Message' ,
                'message' =>$msg->content,
                'type' => $chat->isRequest() ? 'chat_request' : 'chat',
                'from_id' => $chat->isRequest() ? $chat->request_id : auth('api')->user()->id
            ]);
            $options = array(
                'cluster' => 'us2',
                'encrypted' => true
            );
            $pusher = new Pusher(
                'd8e93d92ea33ee7289d4',
                '49e0a36d64966e782b44',
                '532742',
                $options
            );
            $data['msg_counter'] = $notifiable->unReadCount();
            $data['conversation_id'] = $chat->id;
            try {
                $pusher->trigger('notify_' . $notifiable->id, 'new_message', $data);
            } catch (PusherException $e) {

            }
//            if (isset($request->files_array) && count($request->files_array) > 0) {
//                $files_id = collect($request->files_array)->pluck('id');
//                $msg->storefiles($files_id);
//            }
//            if ($chat->messages()->count() == 1) {
////                ($chat->isRequest()) ? $chat->teacher->notify(new TrainingNotification('وصلتك رسالة جديدة من ' . $chat->student->getUserName() . ' بخصوص طلب تدريبي جديد. ', null, 'الوصول للمحادثة', lang_route('teacher.chat.request', [$chat->id])))
////                    : $chat->teacher->notify(new TrainingNotification('وصلتك رسالة جديدة من ' . $chat->student->getUserName(), null, 'الوصول للمحادثة', lang_route('teacher.chat', ['2', $chat->student_id])));
////            }
            broadcast(new MessageSentEvent(auth('api')->user(), $msg->with('files')->find($msg->id)))->toOthers();
            return (isset($msg)) ? $this->response_api(true, 'success', $msg) : $this->response_api(false, 'failed');
        }
        return $this->response_api(false, 'failed');
    }

    public function getAllConversations()
    {
        $array = auth('api')->user()->chats()->request('0')->with(['student', 'teacher'])->paginate(6)->toArray();
        $data['status'] = true;
        $data['from'] = $array['from'];
        $data['last_page'] = $array['last_page'];
        $data['total'] = $array['total'];
        $data['to'] = $array['to'];
        $data['per_page'] = $array['per_page'];
        $data['data'] = $array['data'];


        return JsonResponse::create($data, 200);
    }
}
