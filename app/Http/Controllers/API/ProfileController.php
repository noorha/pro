<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ChangePassword;
use App\Http\Requests\Api\WithdrawRequest;
use App\Notification;
use App\Notifications\UserNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    public function completeProfile(Request $request)
    {
        $user = auth('api')->user();
        $inputs = $request->except('password');
        if (isset($user)) {
            $user->update($inputs);
            $user->updateApiSpecialities($request->sub_specialties);
            if ($user->isTeacher()) {
                $user->updateApiDays($request->days);
                $user->updateApiTrainingInfo($request);
            }
            if ($request->has('password') && !empty($request->password))
            {
                $user->update(['password' => bcrypt($request->password)]);
            }
            return JsonResponse::create(['status' => true, 'message' => trans('messages.success')], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => __('الرجاء تسجيل الدخول للمتابعة')], 401);
    }

    public function getProfileData($id)
    {
        $user = User::find($id);
        return (isset($user)) ? JsonResponse::create(['status' => true, 'data' => $user->userArray(true, true)], 200) :
            JsonResponse::create(['status' => false, 'message' => 'user_not_found'], 401);
    }


    public function getMyFinancialProcesses()
    {

        $user = auth('api')->user();
        if (isset($user)) {
            $items = $user->financialProcesses()->where('value', '>', 0)->paginate(6)->toArray();
            $items['total_cash'] = get_currency_value($user->walletTotalCash());
            $items['available_cash'] = get_currency_value($user->walletAvailableCash());
            $items['pending_cash'] = get_currency_value($user->pending_cash);
            $items['nonPaid'] = get_currency_value($user->totalNonPaidProfit());

            $items['status'] = true;
            return JsonResponse::create($items, 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'server_error'], 500);
    }

    public function addCredit(Request $request)
    {
        $user = auth('api')->user();
        if (isset($request->value)) {
            $user->createPayment(1, 'paypal', $request->value);
            $user->incrementWalletAvailable($request->value);
            $user->notify(new UserNotification(auth()->user()->id, 'user', 'تم شحن الحساب الخاص بك  بقيمة (' . round((double)$request->value, 2) . ' ) دولار.'
                , __('تم شحن الحساب الخاص بك  بقيمة') . ' ' . ' ( ' . round((double)$request->value, 2). ' ) $', route(get_current_locale() . '.profile.balance')));
            return JsonResponse::create(['status' => true, 'message' => __('تمت العملية بنجاح'),'availableCash' =>get_currency_value($user->walletAvailableCash() + $request->value) ], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'server_error'], 500);
    }

    public function getNotifications()
    {
        $user = auth('api')->user();
        $items = $user->notifications()->select(['data', 'id', 'read_at', 'open_at', 'created_at'])->paginate(10);
        $pagination = $items->toArray();
        $data['status'] = true;
        $data['from'] = $pagination['from'];
        $data['last_page'] = $pagination['last_page'];
        $data['total'] = $pagination['total'];
        $data['to'] = $pagination['to'];
        $data['per_page'] = $pagination['per_page'];
        $result = $items->getCollection()->transform(function ($instance) {
            $result = $instance->data;
            $result['id'] = (string)$instance->id;
            $result['created_at'] = (string)$instance->created_at;
            $result['read_at'] = (string)$instance->read_at;
            $result['open_at'] = (string)$instance->open_at;
            return $result;
        });
        $data['data'] = $result->toArray();
        return JsonResponse::create($data, 200);
    }


    public function makeNotificationOpen($id)
    {
        $notification = Notification::where('id', $id)->first();
        if (isset($notification)) {
            $notification->open_at = Carbon::now();
            $notification->save();
            return $this->response_api(true, '');
        }
        return $this->response_api(false, '');
    }

    public function getUnReadNotificationsCount()
    {
        return JsonResponse::create(['status' => true, 'count' => get_unread_notifications_count(), 'messages_count' => auth('api')->user()->unReadCount()]);
    }

    public function markNotificationsAsRead()
    {
        auth('api')->user()->notifications->markAsRead();
        return JsonResponse::create(['status' => true, 'count' => get_unread_notifications_count()]);
    }

    public function changePassword(ChangePassword $request)
    {
        $user = auth('api')->user();
        $status = $user->update(['password' => bcrypt($request->password)]);
        return $status ? JsonResponse::create(['status' => true, 'message' => __('تم تغيير كلمة المرور بنجاح')], 200) :
            JsonResponse::create(['status' => false, 'message' => 'server error'], 500);
    }

    public function logout(Request $request)
    {
        if (auth('api')->check()) {
            auth('api')->user()->token()->revoke();
            auth('api')->user()->update(['device_token' => '']);
            return JsonResponse::create(['status' => true, 'message' => __('تم تسجيل الخروج بنجاح')], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'unauthorized'], 401);

    }

    public function getCurrentUser()
    {
        if (auth('api')->check()) {
            return JsonResponse::create(['status' => true, 'user' => auth('api')->user()->userArray()], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'unauthorized'], 401);
    }

    public function getUserSpecialities()
    {
        if (auth('api')->check()) {
            return JsonResponse::create(['status' => true, 'data' => auth('api')->user()->specialties()->get()], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'unauthorized'], 401);
    }

    public function getUserSpecs($id)
    {
        $user = User::find($id);
        if (isset($user)) {
            $specialtiesJson = $user->specialties()->with(['userSubSpecialties' => function ($q) use ($id) {
                $q->wherePivot('user_id', $id);
            }])->get()->unique('id');
            return JsonResponse::create(['status' => true, 'data' => $specialtiesJson->values()], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => 'المستخدم غير موجود'], 401);
    }

    public function sendWithdrawRequest(WithdrawRequest $request)
    {
        return auth('api')->user()->createWithdrawRequest($request) ? JsonResponse::create(['status' => true, 'message' => __('تم إرسال طلبك بنجاح')], 200) : JsonResponse::create(['status' => false, 'message' => __('لا يوجد لديك رصيد كافي')], 401);
    }

    public function getTeacherRatings($id)
    {
        $user = User::find($id);
        if (isset($user)) {
            $array1 = $user->teacherRatings()->with(array('student' => function ($query) {
                $query->select('id', 'first_name', 'last_name', 'description', 'photo', 'gender');
            }))->paginate(9);
            $array = $array1->toArray();
            $data['status'] = true;
            $data['from'] = $array['from'];
            $data['last_page'] = $array['last_page'];
            $data['total'] = $array['total'];
            $data['to'] = $array['to'];
            $data['per_page'] = $array['per_page'];
            $data['data'] = $array['data'];
            return ($user->isTeacher()) ? JsonResponse::create($data, 200) : JsonResponse::create(['status' => false, 'message' => __('الرجاء تبديل الحساب لمدرب')], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => __('المستخدم المطلوب غير موجود')], 200);
    }

    public function getUserSubSpecialities($id)
    {
        $user = User::find($id);
        if (isset($user)) {
            $specialtiesJson = $user->subSpecialties()->get()->unique('id');
            return JsonResponse::create(['status' => true, 'data' => $specialtiesJson->values()], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => __('المستخدم غير موجود')], 401);
    }

    public function payCashRequest()
    {
        $user = auth('api')->user();
        if (isset($user)) {
            $cash = $user->totalNonPaidProfit();
            $available = $user->walletAvailableCash();
            if ($available >= $cash) {
                if ($cash == 0) {
                    return $this->response_api(false, __('لا يوجد عليك مستحقات'));
                }
                $process = $user->createWithdrawnProcess($cash, 'cash');
//                foreach ($user->nonPaidTrainingRequests as $nonPaidRequest){
//                    $nonPaidRequest->payStudentCash();
//                    $nonPaidRequest->update(['is_paid' => 1]);
//                }
                $user->nonPaidTrainingRequests()->update(['is_paid' => 1]);
                $user->decrementWalletAvailable($cash);
                return $this->response_api(true, __('تمت العملية بنجاح'));
            }
            return $this->response_api(false, __('لا يوجد لديك رصيد كافي'), _('الرجاء الدفع مباشرة عن طريق paypal'));
        }
        return $this->response_api(false, __('حدث خطأ غير متوقع'));
    }

}
