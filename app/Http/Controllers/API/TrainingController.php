<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TrainingRequest;
use App\Notifications\TrainingNotification;
use App\Notifications\UserNotification;
use App\PromoCode;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TrainingController extends Controller
{


    public function teacherSearch(Request $request, User $teachers)
    {
        $data = $teachers->active()->teachers()->searchTeachers($request)->orderBy('created_at','DESC')->with(['city','country'])->paginate(6)->toArray();
        $data['status'] = true;
        return JsonResponse::create($data, 200);
    }

    public function studentSearch(Request $request, User $teachers)
    {
        $paginator = $teachers->active()->students()->searchTeachers($request)->orderBy('created_at','DESC')->with(['city','country'])->paginate(6);
        $data = $paginator->makeHidden(['rating','online_price','interview_price','is_mobile_verified','pending_cash','is_online','is_interview']);
        $paginator->data = $data;
        return JsonResponse::create(array_merge($paginator->toArray(),['status'=>true]), 200);
    }

    public function findTrainingRequest($id)
    {
        $request = auth('api')->user()->trainingRequests()->with('days', 'files')->find($id);
        if (isset($request)) {
            return JsonResponse::create(['status' => true, 'data' => $request], 200);
        }
        return JsonResponse::create(['status' => false, 'data' => $request], 400);
    }

    public function getTrainingRequests(Request $request)
    {
        $requests = auth('api')->user()->trainingRequests()->status($request->status)->with('files')->paginate(6)->toArray();
        $data['status'] = true;
        $data['from'] = $requests['from'];
        $data['last_page'] = $requests['last_page'];
        $data['total'] = $requests['total'];
        $data['to'] = $requests['to'];
        $data['per_page'] = $requests['per_page'];
        $data['data'] = $requests['data'];
        return JsonResponse::create($data, 200);
    }

    public function createTrainingRequest($id, TrainingRequest $request)
    {
        $teacher = auth('api')->user()->teachers()->find($id);
        if (isset($teacher)) {
            $training_request = $teacher->createTrainingRequest($request);
            if (isset($request->days) && count(json_decode($request->days))) {
                $training_request->days()->sync(json_decode($request->days));
            }
            if (isset($request->files_array) && count(json_decode($request->files_array))) {
                $training_request->storefiles(json_decode($request->files_array));
            }
            $training_request->student->notify(new UserNotification($training_request->id, 'training', 'تمت عملية طلب التدريب بنجاح', __('تمت عملية طلب التدريب بنجاح'), lang_route('student.training.requests')));
            $training_request->teacher->notify(new UserNotification($training_request->id, 'training', 'هناك طلب تدريب جديد من الطالب' .' '. $training_request->getStudentName() . '.', __('هناك طلب تدريب جديد من الطالب').' '. $training_request->getStudentName() . '.', lang_route('teacher.training.requests')));
            $training_request->teacher->notify(new TrainingNotification(locale_value('وصلك طلب تدريب جديد داخل هلا برو', 'You have received a new training request in HalaPro'), null, locale_value('الوصول للطلب', 'Access request '), lang_route('teacher.training.requests')));
            return JsonResponse::create(['status' => true, 'message' => __('تم إرسال طلبك بنجاح')], 200);
        }
        return JsonResponse::create(['status' => false, 'message' => __('حدث خطأ أثناء المعالجة'), 'error' => 'teacher_not_found'], 500);
    }

    public function teacherRating($id, Request $request)
    {
        $student = auth('api')->user();
        $trainingRequest = \App\TrainingRequest::find($id);
        if (isset($trainingRequest) && $student->isStudent() && !$trainingRequest->hasRating())
        {
            if(!isset($request->value)){
                return JsonResponse::create(['status'=>false,'message'=>trans('messages.value_not_found')],401);
            }
            $trainingRequest->createRating($request);
            $trainingRequest->teacher->updateTeacherRating();
            $trainingRequest->teacher->notify(new UserNotification($student->id,'rating','تم تقييمك من الطالب'.' '.$student->name.'.',__('تم تقييمك من الطالب').' '.$student->name.'.',lang_route('teacher.ratings',[$trainingRequest->teacher])));
            return $this->response_api(true, __('تم تقييم المدرب بنجاح'));
        }
        return $this->response_api(false, __('لا يمكنك تقييم هذا المدرب'));
    }

    public function checkPromoCode(Request $request)
    {
        $data = [];
        $coupon = PromoCode::query()->findByCode($request->code)->select(['id', 'max_student_no','discount', 'code'])->first();
        if (isset($coupon)&& $coupon->isValid()){
            $teacher = User::find($request->teacher_id);
            if (isset($teacher) && $teacher->isTeacher()){
                $hourNo = isset($request->hour_no) ? (int) $request->hour_no : 1 ;
                if ($teacher->is_online){
                    $data['online_discount'] = get_currency_value(($coupon->discount  * $hourNo *  $teacher->online_price) / 100) ;
                    $data['total_online_with_discount'] = get_currency_value(((100-$coupon->discount)  * $hourNo *  $teacher->online_price) / 100) ;
                }
                if ($teacher->is_online){
                    $data['interview_discount'] = get_currency_value(($coupon->discount  * $hourNo *  $teacher->interview_price) / 100) ;
                    $data['total_interview_with_discount'] = get_currency_value(((100-$coupon->discount)  * $hourNo *  $teacher->interview_price) / 100) ;
                }
            }
            return  JsonResponse::create(['status'=>true,'data'=>array_merge($coupon->makeHidden(['id', 'student_no', 'max_student_no', 'discount_value'])->toArray(),$data)]);
        }
        return JsonResponse::create(['status'=>false,'message'=>__('lang.coupon_not_found')]);
    }

    public function acceptTrainingRequestByTeacher($id)
    {
        $trainingRequest = \App\TrainingRequest::find($id);
        if (!$trainingRequest->isCash()){
            return (isset($trainingRequest) && $trainingRequest->teacherAccept() && $trainingRequest->notifyStudentAccept() ) ?
                $this->response_api(true, __('تم قبول الطلب بنجاح'),__('بإنتظار تأكيد القبول من الطالب')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }else {
            return $trainingRequest->canTeacherAccept() && $trainingRequest->teacherAccept()  && $trainingRequest->notifyStudentAccept() ?  $this->response_api(true, __('تم تأكيد القبول بنجاح')) :  $this->response_api(false, __('لا يمكنك تأكيد القبول لأنك تجاوزت عدد طلبات الكاش المسموح بها'));
        }
    }

    public function confirmRequestByStudent(\App\TrainingRequest $trainingRequest)
    {
        if (auth('api')->check() && auth('api')->user()->isStudent()) {
            return (isset($trainingRequest) && $trainingRequest->studentConfirm('api')) ? $this->response_api(true, __('تم تأكيد الإستلام بنجاح')) :
                $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('يجب أن يكون حسابك حساب طالب'));
    }

    public function acceptTrainingRequestByStudent(\App\TrainingRequest $trainingRequest)
    {
        if (isset($trainingRequest)) {
            if ($trainingRequest->studentAccept('api')) {
                return $this->response_api(true, __('تم تأكيد القبول بنجاح'));
            }
            return $trainingRequest->isCash() ? $this->response_api(false, __('لا يمكنك تأكيد القبول لأن المدرب تجاوز عدد طلبات الكاش المسموح بها'),'','cash') :
                $this->response_api(false, __('لا يوجد لديك رصيد كافي'), __('الرجاء شحن رصيدك لتتمكن من تأكيد القبول'),'website');
        }
        return $this->response_api(false, __('يجب أن يكون حسابك حساب طالب'));
    }

    public function rejectRequestByTeacher($id, Request $request)
    {
        if (auth('api')->check() && auth('api')->user()->isTeacher()) {
            $training_request = \App\TrainingRequest::find($id);
            return (isset($training_request) && $training_request->reject($request) && $training_request->notifyStudentReject()) ?
                $this->response_api(true, __('تم رفض الطلب بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('يجب أن يكون حسابك حساب مدرب'));
    }

    public function rejectRequestByStudent($id, Request $request)
    {
        $training_request = \App\TrainingRequest::find($id);
        return (isset($training_request) && $training_request->reject($request) && $training_request->notifyTeacherReject()) ?
            $this->response_api(true, __('تم رفض الطلب بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }


    public function confirmRequestByTeacher(\App\TrainingRequest $trainingRequest)
    {
        if (auth('api')->check() && auth('api')->user()->isTeacher()) {
            return (isset($trainingRequest) && $trainingRequest->confirmFromTeacher('api') && $trainingRequest->notifyTeacherConfirm('api')) ?
                $this->response_api(true, __('تم تأكيد الإستلام بنجاح'), __('بإنتظار تأكيد الإستلام من طرف الطالب'))
                : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('يجب أن يكون حسابك حساب مدرب'));
    }

}
