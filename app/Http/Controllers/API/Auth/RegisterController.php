<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\ChangePassword;
use App\Http\Requests\API\RegisterUser;
use App\Http\Requests\API\VerificationCode;
use App\Link;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:api');
    }


    public function register(RegisterUser $request, User $user)
    {
        $user = $user->create($request->all());
        return $this->registered($request, $user) ?: JsonResponse::create(['status' => false, 'message' => trans('messages.server_error')], 500);
    }


    public function registered(Request $request, User $user)
    {
       $code = $user->createMobileCode();
        $user->incrementCount();
        $accessToken = $user->createToken('accessToken')->accessToken;
        return JsonResponse::create([
            'status' => true,
            'accessToken' => $accessToken,
            'code' => $code->link,
            'message' => trans('messages.register_success'),
            'user' =>  $user->userArray()
        ], 200);
    }


    public function reSendMobileCode(VerificationCode $request,User $user)
    {
        $user = ($request->type == 'password') ? $user->searchByMobile($request) : $user->find($request->user_id);
        if (isset($user) && isset($request->type)){
            $code = $user->createMobileCode($request->type);
            return JsonResponse::create([
                'status' => true,
                'code' => $code->link,
                'user' => $user->userArray(),
            ], 200);
        }
        return JsonResponse::create([
            'status' => false,
            'message' => trans('messages.user_not_found')
        ], 200);
    }

    public function changePassword($code,ChangePassword $request)
    {

        $link = Link::mobileVerification('password')->where('link',$code)->first();
        if (isset($link) && isset($link->user) ){
          $link->user->update(['password'=>bcrypt($request->password)]);
          $link->delete();
            return JsonResponse::create([
                'status' => true,
                'message' => trans('messages.password_success')
            ], 200);
        }
        return JsonResponse::create([
            'status' => false,
            'message' => trans('messages.invalid_code')
        ], 200);
    }


    public function activateAccount($code)
    {
        $code = Link::findByMobileCode($code);
        if (isset($code->id) && isset($code->user)) {
            if ($code->user->status != 'inactive') {
                return JsonResponse::create([
                    'status' => false,
                    'message' => trans('messages.already_mobile_active'),
                    'user' => $code->user->userArray(),
                ], 200);
            }
            $code->user->update(['status' => 'active']);
            $code->delete();
            return JsonResponse::create([
                'status' => true,
                'message' =>  trans('messages.activation_success'),
                'user' => $code->user->userArray(),
            ], 200);
        }
        return JsonResponse::create([
            'status' => false,
            'message' => trans('messages.invalid_code'),
        ], 500);
    }


}
