<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }


    protected function authenticate(LoginRequest $request)
    {
        $user = User::findByEmail($request->username);

        if (isset($user->status) &&  Hash::check($request->password,$user->password) && $user->status == 'inactive') {
            return JsonResponse::create(['status' => false, 'error' => 'inactive_account', 'message' => trans('messages.inactive_account'),'user' => $user->userArray()], 401);
        }
        if (isset($user->status) && $user->status == 'suspended') {
            return JsonResponse::create(['status' => false, 'error' => 'suspended_account',  'message' =>   trans('messages.suspended_account')], 401);
        }
        if (isset($user->type) && ((int)$user->type == (int) $request->type) && Auth::guard()->attempt(['email' => $request->username, 'password' => $request->password]) ) {
            $user->update(['device_token'=>$request->device_token,'locale'=>($request->has('locale') ? $request->get('locale'): 'en') ]);
            return JsonResponse::create(['status' => true , 'accessToken' => $user->createToken('accessToken')->accessToken ,'user' => $user->userArray()],200);
        }
        return JsonResponse::create(['status' => false, 'error' => 'unauthorized', 'message' =>  trans('messages.invalid_auth') ], 401);
    }


    protected function refreshToken(Request $request)
    {
        $request->request->add([
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
        ]);
        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );
        return Route::dispatch($proxy);
    }


}
