<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\TrainingRequest;
use App\Link;
use App\Notification;
use App\Notifications\UserNotification;
use App\User;
use Illuminate\Http\Request;
use App\Logic\FileRepository;

class ProfileController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth')->except(['profile_index']);
    }


    public function complete_index()
    {
        return (auth()->user()->isStudent()) ? view(view_front_profile() . 'student.complete') : view(view_front_profile() . 'teacher.complete');
    }

    public function activate_index()
    {
        return view('front.profile.activate', ['user' => auth()->user()]);
    }

    public function wait_activate_index()
    {
        return view('front.profile.wait_activate', ['user' => auth()->user()]);
    }
    
    public function activate_account(Request $request)
    {
        $user = auth()->user();
        $code = Link::findByMobileCode($request->code);
        if (isset($user) && isset($code->id)) {
            switch ($user->status) {
                case 'active' :
                    session()->flash('alert', ['type' => 'info', 'message' => __('تم تفعيل الحساب الخاص بك مسبقاً')]);
                    break;
                case 'inactive' :
                    $user_chk =  ($user->type == 2) ? $user->update(['status' => 'wait_active']) : $user->update(['status' => 'active']);
                    if ($user_chk && $code->delete()) {
                        $user->notify(new UserNotification(0, 'all', 'قم بإستكمال الملف الشخصي الخاص بك', __('قم بإستكمال الملف الشخصي الخاص بك'), '#'));
                        session()->flash('alert', ['type' => 'success', 'message' => __('تم تفعيل الحساب الخاص بك بنجاح')]);
                        return redirect()->route(get_current_locale() . '.profile.complete');
                    } else {
                        session()->flash('alert', ['type' => 'error', 'message' => __('حدث خطأ غير متوقع أثناء عملية التفعيل')]);
                    }
                      
                    break;
                case 'suspended' :
                    session()->flash('alert', ['type' => 'warning', 'message' => __('الحساب الخاص بك موقوف حالياً .. الرجاء التواصل مع إدارة الموقع')]);
                    break;
            }

        }
        session()->flash('alert', ['type' => 'warning', 'message' => __('كود التفعيل خاطئ')]);
        return redirect()->route(get_current_locale() . '.profile.activate');
    }

    public function dashboard_index()
    {
        return (auth()->user()->isStudent()) ? view(view_front_profile() . 'student.dashboard') : view(view_front_profile() . 'teacher.dashboard');
    }

    public function balance_index()
    {
        return (auth()->user()->isStudent()) ? view(view_front_profile() . 'student.balance') : view(view_front_profile() . 'teacher.balance');
    }

    public function profile_index($id)
    {
        $data['user'] = User::find($id);
        if (isset($data['user'])) {
            return ($data['user']->isStudent()) ? view(view_front_profile() . 'student.profile', $data) : view(view_front_profile() . 'teacher.profile', $data);
        }
        session()->flash('alert', ['type' => 'warning', 'message' => __('لقد إتبعت رابط خاطئ')]);
        return redirect()->route(get_current_locale() . '.main');
    }

    public function messages_index($id = null)
    {

        $data = [];
//        if (isset($id)) {
//            $data['chat'] = auth()->user()->createChat($id);
//            $data['chat']['hash'] = isset($data['chat']['studentID']) ? (@$data['chat']['studentID'] . '_' . @$data['chat']['teacherID']) : '';
//        }
        return view(view_front_profile() . 'messages', $data);
    }

    public function messageCreate(Request $request){
        auth()->user()->createChat($request->contact_id, $request->message);
        return response()->json(['status' => true]);
    }

    public function fetchVueData()
    {
        $data['user'] = auth()->user();
        return response()->json(['status' => true, 'data' => $data]);
    }


    public function complete_profile(TrainingRequest $request)
    {

        $user = auth()->user();
        $inputs = $request->except(['password', 'type']);
        $inputs['is_profile_completed'] = 1;
        if (isset($user)) {
            $user->update($inputs);
            $user->updateSpecialities($request->sub_specialties);
            if ($user->isTeacher() && $request->has('sub_specialties')) {
                $user->updateTrainingTimes($request);
                $user->updateOrCreateTrainingType($request);
                $user->updateOrCreateTrainingInfo($request);
                if($request->hasfile('training_file'))
                {
                    $user->userinfo_file()->sync([]);
                    $tr_array = [];
                    foreach($request->file('training_file') as $tr_key=>$file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->move(storage_path().'/app/uploads/images/', $name);  
                        $data = $name;  
                        $tr_array[$tr_key] = ['file'=>$data];
                    }
                    $user->userinfo_file()->sync($tr_array);
                }
               
            }
            $user->contacts()->sync([]);
            if (isset($request->contacts)){
                $contacts = array_filter($request->contacts);
            }
            if (isset($contacts) && count($contacts) > 0) {
                $array = [];
                foreach ($contacts as $key=>$contact){
                    $array[$key] = ['link'=>$contact];
                }
                $user->contacts()->sync($array);
            }
            if ($request->has('password') && !empty($request->password)) {
                $user->update(['password' => bcrypt($request->password)]);
            }
            $user->touch();
            session()->put(['is_profile_completed' => $user->is_profile_completed]);
            return $this->response_api(true, __('تمت عملية حفظ البيانات بنجاح'));
        }
        return $this->response_api(false, __('الرجاء تسجيل الدخول للمتابعة'));
    }


    public function notifications_index()
    {
        $data['notifications'] = Notification::where('notifiable_id', auth()->user()->id)->orderBy('created_at', 'DESC')->paginate(9);
        auth()->user()->notifications->markAsRead();
        return view(view_front_profile() . 'notifications', $data);
    }

    public function get_unread_notifications_count()
    {
        return $this->response_api(true, 'true',
            [
                'pending_requests_count' => auth()->user()->pendingRequestsCount(),
                'msg_counter' => auth()->user()->unReadCount(),
                'count' => ajax_render_view(view_front_layout() . 'notifications-btn', ['un_read' => get_unread_notifications_count()]),
                'body' => ajax_render_view(view_front_layout() . 'notifications', [])]);
    }

    public function mark_notifications_as_read()
    {
        auth()->user()->notifications->markAsRead();
        return $this->response_api(true, 'true');
    }


    public function create_withdraw_request(Request $request)
    {
        return (auth()->user()->createWithdrawRequest($request)) ? $this->response_api(true, __('تم إرسال طلبك بنجاح'), __('سيتم الرد عليك من إدارة الموقع خلال 24 ساعة')) :
            $this->response_api(false, __('لا يوجد لديك رصيد كافي'));
    }

    public function payCashRequest()
    {
        $user = auth()->user();
        if (isset($user)) {
            $cash = $user->totalNonPaidProfit();
            $available = $user->walletAvailableCash();
            if ($available >= $cash) {
                $process = $user->createWithdrawnProcess($cash, 'cash');
//                foreach ($user->nonPaidTrainingRequests as $nonPaidRequest) {
//                    $nonPaidRequest->payStudentCash();
//                    $nonPaidRequest->update(['is_paid' => 1]);
//                }
                $user->nonPaidTrainingRequests()->update(['is_paid' => 1]);
                $user->decrementWalletAvailable($cash);
                return $this->response_api(true, __('تمت العملية بنجاح'));
            }
            return $this->response_api(false, __('لا يوجد لديك رصيد كافي'), _('الرجاء الدفع مباشرة عن طريق paypal'));
        }
        return $this->response_api(false, __('حدث خطأ غير متوقع'));
    }

    public function sendDeleteRequest()
    {
        $user = auth()->user();
        if ($user->hasDeleteRequest()) {
            return $this->response_api(false, __('lang.delete_request_already_exist'));
        }
        $deleteRequest = $user->createDeleteRequest();
        return isset($deleteRequest) ? $this->response_api(true, __('lang.delete_request_sent'), __('lang.process_delete_request')) : $this->response_api(false, __('حدث خطأ غير متوقع'));
    }

    public function cancelDeleteRequest()
    {
        $user = auth()->user();
        if ($user->hasDeleteRequest()) {
            $user->deleteRequest->forceDelete();
            return $this->response_api(true, __('lang.delete_request_canceled_successfully'));
        }
        return $this->response_api(false, __('حدث خطأ غير متوقع'));
    }
}
