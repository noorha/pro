<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\TrainingRequest;
use App\User;
use App\Favourite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TeacherController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'is_complete'])->except(['teacher_ratings_index','teacher_search_index']);
    }


    public function confirm_request(TrainingRequest $trainingRequest)
    {
        return (isset($trainingRequest) && $trainingRequest->confirmFromTeacher() && $trainingRequest->notifyTeacherConfirm()) ? $this->response_api(true, __('تم تأكيد الإستلام بنجاح'), ajax_render_view('front.chat.side-btns', ['training_request' => $trainingRequest, 'chat' => $trainingRequest->getChat(), 'teacher' => $trainingRequest->teacher]))
            : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }

    public function teacher_search_index(User $user, Request $request, $key = null, $value = null,$is_array = null)
    {
        if (isset($key) && isset($value)) {
            if (isset($is_array)){
                $request->request->add([$key => array_map_int($value)]);
            }else{
                $request->request->add([$key => $value]);
            }
        }
        $data['teachers'] = $user->active()->teachers()->searchTeachers($request)->paginate(8);
        if ($request->ajax()) {
            return $this->response_api(true, 'success', ajax_render_view(view_front_teacher() . 'paginate', $data));
        }
        $data['inputs'] = $request->all();
        return view(view_front_teacher() . 'search', $data);
    }

    public function training_request_index($id, User $user)
    {
        $data['teacher'] = $user->teachers()->find($id);
        return isset($data['teacher']) ? view(view_front_teacher() . 'request.create', $data) : redirect()->route(get_current_locale() . '.main');
    }


    public function training_requests(Request $request)
    {
        $data['requests'] = auth()->user()->teacherTrainingRequests()->status($request->status)->paginate(5);
        if ($request->ajax()) {
            return $this->response_api(true, 'success', ajax_render_view(view_front_teacher() . 'request.paginate', $data));
        }
        return view(view_front_teacher() . 'request.all', $data);
    }

    public function accept_teacher_training_request($id)
    {
        $trainingRequest = TrainingRequest::find($id);
        if (!$trainingRequest->isCash()) {
            return (isset($trainingRequest) && $trainingRequest->teacherAccept() && $trainingRequest->notifyStudentAccept()) ?
                $this->response_api(true, __('تم قبول الطلب بنجاح'), __('بإنتظار تأكيد القبول من الطالب')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        } else {
            return $trainingRequest->canTeacherAccept() && $trainingRequest->teacherAccept() && $trainingRequest->notifyStudentAccept() ? $this->response_api(true, __('تم تأكيد القبول بنجاح')) : $this->response_api(false, '', __('لا يمكنك تأكيد القبول لأنك تجاوزت عدد طلبات الكاش المسموح بها'));
        }
    }


    public function reject_teacher_training_request($id, Request $request)
    {
        $training_request = TrainingRequest::find($id);
        return (isset($training_request) && $training_request->reject($request) && $training_request->notifyStudentReject()) ?
            $this->response_api(true, __('تم رفض الطلب بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }

    public function report_request(TrainingRequest $trainingRequest, Request $request)
    {
        return (isset($trainingRequest) && $trainingRequest->report($request)) ?
            $this->response_api(true, __('تم التبليغ بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));

    }

    public function teacher_ratings_index(User $teacher)
    {
        if (isset($teacher)) {
            $data['teacher'] = $teacher;
            $data['ratings'] = $teacher->teacherRatings()->paginate(6);
            return view(view_front_teacher() . 'ratings', $data);
        }
        return redirect()->route(get_current_locale() . '.main');
    }


    public function teacher_favourite(Request $request){
        
        try{
            $teacher_id =  $request->teacher_id;
            $student_id =  Auth::user()->id;
        
            Favourite::create([
                'teacher_id' =>$teacher_id,
                'student_id'=>$student_id,
            ]);

          $status = 'success';
          $message = "المفضلة";
           
        } catch (Exception $e) {
            $status = 'error';
            $message = " حدث خطأ";
        }
         $reslt = array("status" => $status, "message" => $message);
    echo json_encode($reslt);
    }

    public function teacher_removefavourite(Request $request)
    {
        try{
            $teacher_id =  $request->teacher_id;
            $student_id =  Auth::user()->id;
        
            Favourite::where([
                'teacher_id' =>$teacher_id,
                'student_id'=>$student_id,
            ])->delete();

          $status = 'success';
          $message = "المفضلة";
           
        } catch (Exception $e) {
            $status = 'error';
            $message = " حدث خطأ";
        }
         $reslt = array("status" => $status, "message" => $message);
    echo json_encode($reslt);

    }
}
