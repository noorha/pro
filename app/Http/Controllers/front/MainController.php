<?php

namespace App\Http\Controllers\front;

use App\Country;
use App\CourseCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitorMessageRequest;
use App\Link;
use App\Mail\ReplayMail;
use App\MailItem;
use App\Notifications\ResetMail;
use App\Page;
use App\PromoCode;
use App\Question;
use App\ScheduledMessage;
use App\Speciality;
use App\SubSpecialty;
use App\User;
use App\VisitorMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use MatthiasMullie\Minify as Minify;


class MainController extends Controller
{

    public function __construct()
    {
        $this->middleware('is_complete')->except('get_country_cities');
    }

    public function index()
    {
        $data['courses'] = CourseCategory::orderBy('text', 'asc')->get();
        $data['most_teachers'] = User::active()->teachers()->profileCompleted()->inRandomOrder()->take(20)->get();
        return view(view_front_main() . 'main', $data);
    }

    public function checkCoupon(Request $request)
    {
        $coupon = PromoCode::query()->findByCode($request->code)->select(['id', 'discount', 'code'])->first();
        return isset($coupon) ? $this->response_api(true, '', $coupon->makeHidden(['student_no', 'discount_value'])) : $this->response_api(false, '');
    }

    public function scheduledMessage($encryption)
    {
        if (Hash::check('!@!%^&&', $encryption)) {
            $items = ScheduledMessage::enabled()->get();
            foreach ($items as $item) {
                if ($item->isActive()) {
                    $users = $item->getUsers();
                    foreach ($users as $user) {
                        Mail::to($user->email, 'HalaPro')->send(new  ReplayMail($item->title, $item->text, $user->email));
                        if ((count(Mail::failures()) > 0)) {
                            return response()->json(['status' => false, 'message' => 'حدث خطأ أثناء الإرسال .. ']);
                        }
                    }
                }
            }
            return $this->response_api(true, '');
        }
        return $this->response_api(false, '');
    }

    public function contact(Page $page)
    {
        $data['page'] = $page->type('contact');
        return (isset($data['page'])) ? view(view_front_page() . 'contact', $data) : redirect()->route(get_current_locale() . '.main');
    }

    public function about(Page $page)
    {
        $data['page'] = $page->type('about');
        return (isset($data['page'])) ? view(view_front_page() . 'about', $data) : redirect()->route(get_current_locale() . '.main');
    }

    public function privacy(Page $page)
    {
        $data['page'] = $page->type('privacy');
        return (isset($data['page'])) ? view(view_front_page() . 'privacy', $data) : redirect()->route(get_current_locale() . '.main');
    }

    public function policy(Page $page)
    {
        $data['page'] = $page->type('policy');
        return (isset($data['page'])) ? view(view_front_page() . 'policy', $data) : redirect()->route(get_current_locale() . '.main');
    }


    public function how_it_work(Page $page)
    {
        $data['page'] = $page->type('how');
        return (isset($data['page'])) ? view(view_front_page() . 'how-work', $data) : redirect()->route(get_current_locale() . '.main');
    }

    public function work_with_us(Page $page)
    {
        $data['page'] = $page->type('work');
        return (isset($data['page'])) ? view(view_front_page() . 'work-us', $data) : redirect()->route(get_current_locale() . '.main');
    }

    public function reset_index()
    {
        return view('front.reset.forget');
    }


    public function send_reset_email(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (isset($user)) {
            $link = $user->createLink('password');
            $user->notify(new ResetMail());
            return $this->response_api(true, __('تم إرسال بريد إعادة تعيين كلمة المرور'));
        }
        return $this->response_api(false, __('البريد المدخل غير موجود لدينا'));
    }


    public function check_password_link($code, Link $link)
    {
        $link = $link->search($code);
        if (isset($link->user)) {
            $data['user'] = $link->user;
            $data['link'] = $code;
            return view('front.reset.reset', $data);
        }
        session()->flash('alert', ['type' => 'error', 'message' => __('كود إستعادة كلمة المرور غير صحيح')]);
        return redirect()->route(get_current_locale() . '.login');
    }

    public function change_password($id, Request $request)
    {
        $user = User::find($id);
        if (isset($user->link('password')->first()->link)) {
            $link = $user->link('password')->first();
            return ($user->update(['password' => bcrypt($request->password)]) && $link->delete()) ? $this->response_api(true, __('تم تغيير كلمة المرور الخاصة بك بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }


    public function store_visitor_message(VisitorMessageRequest $request)
    {

        $data['secret'] = env('CAPTCHA_SECRET_KEY');
        $data['response'] = $request['g-recaptcha-response'];
        $data['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $data['secret'] . "&response=" . $data['response'] . "&remoteip=" . $data['remoteip']), true);
        if ($response['success']) {
            $msg = VisitorMessage::create([
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'text' => $request->text
            ]);
            return $this->response_api(true, __('تم إرسال رسالتك للإدارة بنجاح'), __('سيتم الرد بأقرب وقت ممكن .. شكراً لتواصلك'));
        } else {
            return $this->response_api(false, 'تحقق من ال recaptcha');
        }
    }

    public function get_sub_specialities($id, SubSpecialty $subSpecialty)
    {
        if ($id == 'all') {
            $subSpecialty = $subSpecialty->orderBy('updated_at', 'DESC')->selected()->take(12)->get();
            return $this->response_api(true, 'success', ajax_render_view(view_front() . 'layout.sub-speciality', ['subSpecialties' => $subSpecialty]));
        }
        $speciality = Speciality::find($id);
        if (isset($speciality) && isset($speciality->subSpecialties)) {
            return $this->response_api(true, 'success', ajax_render_view(view_front() . 'layout.sub-speciality', ['subSpecialties' => $speciality->subSpecialties()->take(12)->get()]));
        }
        return $this->response_api(false, 'failed');
    }

    public function cssMin()
    {

        $en = new Minify\CSS();
        $array = [
            '/front/css/bootstrap.min.css', '/front/css/animate.css',
            '/front/css/bootstrap-select.css', '/front/css/select2.min.css', '/front/css/bootstrap-datetimepicker.min.css',
            '/front/css/font-style.css', '/front/css/font-awesome.min.css',
            '/front/css/style.css', '/front/css/style_en.css'
            , '/front/css/file-preview.css',
            '/front/css/owl.carousel.css', '/front/css/owl.theme.css',
            '/front/css/jquery-ui.css', '/front/css/plyr.css',
        ];


        foreach ($array as $item) {
            $css = file_get_contents(asset($item), false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))));;
            $en->add($css);
        }

        $array_ar = [
            '/front/css/bootstrap.min.css', '/front/css/animate.css',
            '/front/css/bootstrap-select.css', '/front/css/select2.min.css', '/front/css/bootstrap-datetimepicker.min.css',
            '/front/css/font-style.css', '/front/css/font-awesome.min.css',
            '/front/css/bootstrap-rtl.css', '/front/css/style.css',
            'https://fonts.googleapis.com/css?family=Cairo', '/front/css/file-preview.css',
            '/front/css/owl.carousel.css', '/front/css/owl.theme.css',
            '/front/css/jquery-ui.css', '/front/css/plyr.css',
        ];

        $ar = new Minify\CSS();

        foreach ($array_ar as $item) {
            $css = file_get_contents(asset($item), false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))));;
            $ar->add($css);
        }
// save minified file to disk
        $minifiedPath1 = ('front\css\en.min.css');
        $minifiedPath2 = ('front\css\ar.min.css');
        $en->minify($minifiedPath1);
        $ar->minify($minifiedPath2);
        echo $en->minify();
        echo $ar->minify();

        return $this->response_api(true, '');
    }

    public function jsMin($type)
    {
        $en = new Minify\JS();
        $file_name = 'script';
        switch (@$type) {
            case 'home' :
                $array = [
                    '/front/js/plyr.min.js', '/front/js/plyr.polyfilled.min.js',
                    '/front/js/particles.min.js', '/front/js/app.js',
                    '/front/js/jquery-ui.min.js', '/front/js/bootstrap-slider.min.js',
                    '/front/js/jquery.select-to-autocomplete.js', '/front/js/main.js',
                ];
                $file_name = 'home';
                break;

            default :
                $array = [
                    '/front/js/bootstrap.min.js', '/front/js/bootstrap-select.min.js',
                    '/front/js/moment.js', '/front/js/bootstrap-datetimepicker.min.js',
                    '/front/js/jquery.validate.min.js', '/front/js/validate-ar.js',
                    '/front/js/select2.min.js', '/front/js/errors.js', 'front/js/owl.carousel.min.js',
                    '/front/js/function.js', '/front/js/wow.min.js',
                    '/front/js/sweetalert2.all.min.js', '/front/js/custom.sweet.js',
                ];
                break;

        }
        foreach ($array as $item) {
            $css = file_get_contents(asset($item), false, stream_context_create(array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false))));;
            $en->add($css);
        }
        $minifiedPath1 = "front\js\\" . $file_name . ".min.js";
        $en->minify($minifiedPath1);
        echo $en->minify();

        return $this->response_api(true, '');
    }


    public function subscribe_mail(Request $request, MailItem $mailItem)
    {
        if ($request->has('email')) {
            $mail = MailItem::where('email', $request->email)->first();
            if (isset($mail)) {
                return $this->response_api(false, __('تم الإشتراك مسبقا لبريدك الإلكتروني'));
            }
            $mailItem = $mailItem->create(['email' => $request->email]);
            return isset($mailItem) ? $this->response_api(true, __('تم الإشتراك بنجاح'))
                : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('الرجاء إدخال بريد إلكتروني'));
    }

    public function get_country_cities(Country $country)
    {
        if (isset($country->cities)) {
            return $this->response_api(true, 'success', ajax_render_view(view_front_layout() . 'cities-select', ['items' => $country->cities, 'user' => auth()->user()]));
        }
        return $this->response_api(false, 'failed', ajax_render_view(view_front_layout() . 'cities-select', []));
    }


    public function faqIndex()
    {
        $data['items'] = Question::orderBy('created_at', 'DESC')->get();
        return view('front.main.page.faq', $data);
    }

    public function page1()
    {
        return view(view_front_page() . 'page1');
    }

    public function page2()
    {
        return view(view_front_page() . 'page2');
    }

    public function page3()
    {
        return view(view_front_page() . 'page3');
    }

    public function page4()
    {
        return view(view_front_page() . 'page4');
    }

    public function page5()
    {
        return view(view_front_page() . 'page5');
    }

    public function messagingIndex()
    {
        return view('front.chat.messaging');
    }

    public function test(Request $request)
    {
        $user = User::find(1393);
        $old = $user->email;
        if (isset($user)) {

            if (!empty($request->email)){
//                dd($request->email, $user);
                $user->email = $request->email;
//                $user->save();
            }

            $link = $user->createLink('password');
            $user->notify(new ResetMail());
            $user->email = $old;
            $user->save();
            return dd(__('تم إرسال بريد إعادة تعيين كلمة المرور'));
        }
    }
//    public function test1(Constant $constant)
//    {
////        $user = auth()->user();
////        dd($user->registerTeacherInWhiteboard());
////
////        $users = User::all();
////        foreach ($users as $user){
////            $user->update(['status'=>'active']);
////        }
////        $user = User::where('email','wesamjibreen@gmail.com')->first();
////        $user->notify(new TrainingNotification('وصلك طلب تدريب جديد داخل منصة هلا برو',null, 'الوصول للطلب','#'));
////
////          return view('mail.confirm',['user'=>User::first()]);
//
////         $user = User::find(126);
//////         $user->sendNotification(['title' => 'Hello', 'message' => 'Hello']);
////
////         $user->notify(new UserNotification(3,'training','هناك طلب جديد','هناك طلب جديد','profile/'));
////
//
//
//        $currencies = json_decode(file_get_contents('http://www.apilayer.net/api/live?access_key=93e8d9bef793b02334f1741030c88b56&currencies=AED,SAR&format=1'), true);
//        if ($currencies['success']) {
//            foreach ($currencies['quotes'] as $i => $amount) {
//                $constant->updateOrCreate(['key' => $i], ['value' => (isset($amount)) ? $amount : 1]);
//            }
//        }
//        return $this->response_api(true, 'success');
////        $user = User::find(6);
////
////        if (isset($user)){
////            $res = $user->send(['to'=>'+970567666216','message'=> 'كود التفعيل الخاص بك 257821 .']);
////            return $this->response_api(true,'success',$res);
////        }
////        return $this->response_api(false,'false');
//
//    }

    public function setCurrentCurrency($currency)
    {
        if ($currency === 'SAR' || $currency === 'USD' || $currency === 'AED') {
            session()->put(['currency' => $currency]);
        } else {
            session()->put(['currency' => 'USD']);
        }
        return redirect()->back();
    }
}
