<?php

namespace App\Http\Controllers\front;

use App\Blog;
use App\BlogCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function index(Request $request, $id = null)
    {
        $data['items'] = Blog::locale()->search($request, $id)->orderBy('created_at', 'DESC')->paginate(6);
        $data['categories'] = BlogCategory::orderBy(locale_value('text_ar', 'text_en'))->get();
        $data['mostViews'] = Blog::locale()->orderBy('views', 'ASC')->take(6)->get();
        $data['q'] = $request->q;
        return view('front.blog.all', $data);
    }

    public function viewBlog($id)
    {
        $data['blog'] = Blog::locale()->find($id);
        $data['relatedBlogs'] = Blog::locale()->find($id);
        return (isset($data['blog']) && $data['blog']->incrementViews()) ? view('front.blog.view', $data) : redirect()->route(get_current_locale() . '.main');
    }


}
