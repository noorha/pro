<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Requests\TicketRequest;
use App\Logic\FileRepository;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','is_complete']);
    }

    public function index()
    {
        $data['tickets'] = auth()->user()->tickets()->orderBy('updated_at', 'DESC')->get();
        return view('front.ticket.all', $data);
    }

    public function create()
    {
        return view('front.ticket.create');
    }

    public function ticketType()
    {
        return view('front.ticket.type');
    }

    public function viewTicket($id)
    {
        $data['ticket'] = Ticket::find($id);
        return isset($data['ticket']) ? view('front.ticket.view', $data) : redirect()->back();
    }

    public function replay($id, Request $request)
    {
        $ticket = Ticket::find($id);
        if (isset($ticket) && $ticket->canReplay()) {
            $ticket->addReplay($request);
            return response()->json(['status' => true, 'message' => __('lang.ticket_sent_successfully') , 'item' => '']);
        }
        return response()->json(['status' => false, 'message' => 'حدث خطأ أثناء الإرسال .. ']);
    }

    public function store(TicketRequest $request)
    {

        if ($request->hasFile('file')) {
            $rep = new FileRepository();
            $file = $rep->upload($request->all(), 'file');
            if (isset($file->getData()->status) && $file->getData()->status) {
                $request->request->add(['file_path' => $file->getData()->file_name]);
            }
        }
        $ticket = Ticket::create(array_merge([
            'user_id' => auth()->user()->id,
            'status' => 0,
            'file' => $request->file_path,
        ], $request->only(['text', 'subject'])));
        return $this->response_api(true, __('lang.ticket_sent_successfully'));
    }
}
