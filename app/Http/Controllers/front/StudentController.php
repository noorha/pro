<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Notifications\TrainingNotification;
use App\Notifications\UserNotification;
use App\TrainingRequest;
use App\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','is_complete']);
    }

    public function student_search_index(User $user, Request $request, $key = null, $value = null,$is_array = null)
    {
        if (isset($key) && isset($value)) {
            if (isset($is_array)){
                $request->request->add([$key => array_map_int($value)]);
            }else{
                $request->request->add([$key => $value]);
            }
        }
        $data['students'] = $user->active()->students()->searchTeachers($request)->paginate(8);
        if ($request->ajax()) {
            return $this->response_api(true, 'success', ajax_render_view(view_front_student() . 'paginate', $data));
        }
        $data['inputs'] = $request->all();
        return view(view_front_student() . 'search', $data);
    }


    public function training_request_index($id, User $user, Request $request)
    {
        $data['teacher'] = $user->teachers()->find($id);
        $data['sub_specialty_input'] = $request->sub_specialty;
        return isset($data['teacher']) ? view(view_front_student() . 'request.create', $data) : redirect()->route(get_current_locale() . '.main');
    }


    public function training_requests(Request $request)
    {
        $data['requests'] = auth()->user()->studentTrainingRequests()->status($request->status)->paginate(5);
        if ($request->ajax()) {
            return $this->response_api(true, 'success', ajax_render_view(view_front_student() . 'request.paginate', $data));
        }
        return view(view_front_student() . 'request.all', $data);
    }

    public function send_training_request($id, Request $request, User $user)
    {
        $teacher = $user->teachers()->find($id);
        if (isset($teacher)) {
            $training_request = $teacher->createTrainingRequest($request);
            $training_request->days()->sync($request->days);
            $training_request->storefiles(array_map_int($request->files_array));
            $training_request->student->notify(new UserNotification($training_request->id, 'training', 'تمت عملية طلب التدريب بنجاح', __('تمت عملية طلب التدريب بنجاح'), lang_route('student.training.requests')));
            $training_request->teacher->notify(new UserNotification($training_request->id, 'training', 'هناك طلب تدريب جديد من الطالب' .' '. $training_request->getStudentName() . '.', __('هناك طلب تدريب جديد من الطالب').' '. $training_request->getStudentName() . '.', lang_route('teacher.training.requests')));
            $training_request->teacher->notify(new TrainingNotification(locale_value('وصلك طلب تدريب جديد داخل هلا برو', 'You have received a new training request in HalaPro'), null, locale_value('الوصول للطلب', 'Access request '), lang_route('teacher.training.requests')));
            return isset($training_request) ? $this->response_api(true, __('تم إرسال طلبك بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
        }
        return $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }

    public function rating_teacher($id, Request $request)
    {
        $student = auth()->user();
        $trainingRequest = TrainingRequest::find($id);
        if (isset($trainingRequest) && $student->isStudent() && !$trainingRequest->hasRating()) {
            $trainingRequest->createRating($request);
            $trainingRequest->teacher->updateTeacherRating();
            $trainingRequest->teacher->notify(new UserNotification(auth()->user()->id,'rating','تم تقييمك من الطالب'.' '.auth()->user()->name.'.',__('تم تقييمك من الطالب').' '.auth()->user()->name.'.',lang_route('teacher.ratings',[$trainingRequest->teacher])));
            return $this->response_api(true, __('تم تقييم المدرب بنجاح'));
        }
        return $this->response_api(false, __('لا يمكنك تقييم هذا المدرب'));
    }

    public function confirm_request(TrainingRequest $trainingRequest)
    {
        return (isset($trainingRequest) && $trainingRequest->studentConfirm() ) ? $this->response_api(true, __('تم تأكيد الإستلام بنجاح'),
            ajax_render_view('front.chat.side-btns', ['training_request' => $trainingRequest, 'chat' => $trainingRequest->getChat()
                , 'teacher' => $trainingRequest->teacher])) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }

    public function report_request(TrainingRequest $trainingRequest, Request $request)
    {
        return (isset($trainingRequest) && $trainingRequest->report($request)) ?
            $this->response_api(true, __('تم التبليغ بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }


    public function student_approved_training_request(TrainingRequest $trainingRequest)
    {
        if (isset($trainingRequest)) {
            if ($trainingRequest->studentAccept()) {
                return $this->response_api(true, __('تم تأكيد القبول بنجاح'));
            }
            return $trainingRequest->isCash() ? $this->response_api(false, __('لا يمكنك تأكيد القبول لأن المدرب تجاوز عدد طلبات الكاش المسموح بها'),'','cash') :
                $this->response_api(false, __('لا يوجد لديك رصيد كافي'), __('الرجاء شحن رصيدك لتتمكن من تأكيد القبول'),'website');
        }
        return $this->response_api(false, __('لا تملك صلاحيات للقيام بهذه العملية'));
    }


    public function reject_training_request($id, Request $request)
    {
        $training_request = TrainingRequest::find($id);
        return (isset($training_request) && $training_request->reject($request) && $training_request->notifyTeacherReject()) ?
            $this->response_api(true, __('تم رفض الطلب بنجاح')) : $this->response_api(false, __('حدث خطأ أثناء المعالجة'));
    }

}
