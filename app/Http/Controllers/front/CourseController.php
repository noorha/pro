<?php

namespace App\Http\Controllers\front;

use App\Course;
use App\CourseSubscription;
use App\Http\Controllers\Controller;
use App\Http\Requests\JoinCourseRequest;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('is_complete');
    }


    public function index(Course $courses, Request $request)
    {
        $data['items'] = $courses->publishedCourses()->search($request)->orderBy('created_at', 'DESC')->paginate(9);
        if ($request->ajax()) {
            return $this->response_api(true, 'success', ajax_render_view(view_front() . 'course.paginate', $data));
        }
        return view(view_front() . 'course.all', $data);
    }


    public function view_course($id)
    {
        $data['item'] = Course::find($id);
        return (isset($data['item']) && $data['item']->is_published == '1') ? view(view_front() . 'course.view', $data) : redirect()->route(get_current_locale().'.main');
    }


    public function course_subscription(JoinCourseRequest $request)
    {
        $join_request = CourseSubscription::create([
            'course_id' => $request->course_id,
            'email' => $request->email,
            'name' => $request->name,
            'mobile' => $request->mobile,
            'address' => $request->address,
        ]);
        return (isset($join_request)) ? $this->response_api(true, __('تم إرسال طلب الإنضمام بنجاح')) : $this->response_api(true, __('حدث خطأ أثناء المعالجة'));
    }
}
