<?php

namespace App\Http\Controllers\front;

use App\Notifications\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Validator;
use URL;
use Session;
use Redirect;
use \Illuminate\Support\Facades\Input;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
class PaymentController extends Controller
{
    private $_api_context;
    public function __construct()
    {
        $this->middleware(['is_complete','auth']);
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithPaypal()
    {
        return view('paywithpaypal');
    }

    public function postPaymentWithpaypal(Request $request)
    {

        session()->put('type',$request->has('type') ? $request->type : 'paypal');
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName(__('إضافة رصيد'.' Ustazak'))->setCurrency('USD')->setQuantity(1)->setPrice($request->get('value')); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($request->get('value'));
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route(get_current_locale().'.payment.status')) /** Specify return URL **/
        ->setCancelUrl(URL::route(get_current_locale().'.payment.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                session()->flash('alert', ['type' => 'error', 'message' => __('تم فقد الإتصال بالخادم')]);
                return redirect()->route(get_current_locale().'.profile.balance');
            } else {
                session()->flash('alert', ['type' => 'error', 'message' => __('حدث خطأ أثناء المعالجة')]);
                return redirect()->route(get_current_locale().'.profile.balance');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        Session::put('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }
        session()->flash('alert', ['type' => 'error', 'message' => __('حدث خطأ أثناء المعالجة')]);
        return redirect()->route(get_current_locale().'.profile.balance');
    }

    public function getPaymentStatus()
    {
        $payment_id = Session::get('paypal_payment_id');
        Session::forget('paypal_payment_id');
        $payer_id =Input::get('PayerID');
        if ( !isset($payer_id) && empty($payer_id) || empty(Input::get('token'))) {
            session()->flash('alert', ['type' => 'error', 'message' => __('فشلت عملية الدفع')]);
            return redirect()->route(get_current_locale().'.profile.balance');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            $price = (isset($result->transactions[0]->amount->total))? $result->transactions[0]->amount->total : 0;
            auth()->user()->createPayment(1,'paypal',$price);
            auth()->user()->incrementWalletAvailable($price);

            auth()->user()->notify(new UserNotification( auth()->user()->id, 'user', 'تم شحن الحساب الخاص بك  بقيمة (' .round((double)$price, 2). ' ) دولار.'
                ,__('تم شحن الحساب الخاص بك  بقيمة').' ( ' .round((double)$price, 2). ' ) $',route(get_current_locale().'.profile.balance')));
            if(session()->has('type') && session()->get('type') == 'training'){
                session()->forget('type');
                session()->flash('alert', ['type' => 'success', 'message' =>__( 'تمت عملية الدفع بنجاح')]);
                return redirect()->route(get_current_locale().'.student.training.requests');
            }
            if(session()->has('type') && session()->get('type') == 'cash'){
                session()->forget('type');
                auth()->user()->createPayment(1,'cash',$price);
                auth()->user()->nonPaidTrainingRequests()->update(['is_paid'=>1]);
                auth()->user()->decrementWalletAvailable($price);
                session()->flash('alert', ['type' => 'success', 'message' => __('تم تسديد العمولة المستحقة بنجاح')]);
                return redirect()->route(get_current_locale().'.profile.balance');
            }
            session()->flash('alert', ['type' => 'success', 'message' => __( 'تمت عملية الدفع بنجاح')]);
            return redirect()->route(get_current_locale().'.profile.balance');
        }
        session()->flash('alert', ['type' => 'error', 'message' =>  __('فشلت عملية الدفع')]);
        return redirect()->route(get_current_locale().'.profile.balance');
    }
}
