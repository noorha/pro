<?php

namespace App\Http\Controllers\front;

use App\Chat;
use App\Events\MessageSentEvent;
use App\Http\Controllers\Controller;
use App\Notifications\TrainingNotification;
use App\TrainingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Pusher\Pusher;
use Pusher\PusherException;

class ChatController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','is_complete']);
    }

//    public function chat_index($type, $id)
//    {
//        $data['chat'] = auth()->user()->getChat($type, $id);
//        if (isset($data['chat'])) {
//            $vue['chat'] = $data['chat']->toArray();
//            $vue['teacher'] = ['id' => $data['chat']->teacher_id, 'name' => $data['chat']->getTeacherName(), 'photo' => image_url($data['chat']->getTeacherPhoto(), '90x90')];
//            $vue['student'] = ['id' => $data['chat']->student_id, 'name' => $data['chat']->getStudentName(), 'photo' => image_url($data['chat']->getStudentPhoto(), '90x90')];
//            $vue['auth_type'] = auth()->user()->type == '1' ? 'student' : 'teacher';
//            $vue['auth_id'] = auth()->user()->id;
//            $vue['token'] = csrf_token();
//            $vue['fetch_url'] = lang_route('chat.messages', [$type, $data['chat']->id]);
//            $vue['send_url'] = lang_route('chat.send', [$type, $data['chat']->id]);
//            $vue['locale'] = get_current_locale();
//            $data['vue'] = $vue;
//            $data['chat']->markAsRead();
//            return view(view_front() . 'chat.chat', $data);
//        }
//        return redirect()->route(get_current_locale() . '.main');
//    }

    public function chat_request_index(TrainingRequest $trainingRequest)
    {
        if (isset($trainingRequest) && $trainingRequest->isAccepted()) {
            $data['chat'] = $trainingRequest->getChat();
            if (isset($data['chat'])) {
                $data['training_request'] = $trainingRequest;
                $vue['chat'] = array_merge($data['chat'],['id'=>$trainingRequest->id]);
                $vue['teacher'] = ['id' => $trainingRequest->teacher_id, 'name' => $trainingRequest->getTeacherName(), 'photo' => image_url($trainingRequest->getTeacherPhoto(), '90x90')];
                $vue['student'] = ['id' => $trainingRequest->student_id, 'name' => $trainingRequest->getStudentName(), 'photo' => image_url($trainingRequest->getStudentPhoto(), '90x90')];
                $vue['auth_type'] = auth()->user()->type != '1' ? 'student' : 'teacher';
                $vue['id'] = auth()->user()->id;
//                $vue['token'] = csrf_token();
//                $vue['fetch_url'] = lang_route('chat.messages', [auth()->user()->type, $data['chat']->id]);
//                $vue['send_url'] = lang_route('chat.send', [auth()->user()->type, $data['chat']->id]);
                $vue['locale'] = get_current_locale();
                $data['vue'] = $vue;
//                $data['chat']->markAsRead();
                return view(view_front() . 'chat.request-chat', $data);
            }
        }
        return redirect()->route('main');

    }
    public function get_chat_messages($type, Chat $chat, Request $request)
    {
        $data['chat'] = $chat->messages()->with('files')->latest()->skip($request->skip)->take(5)->get();
        $data['count'] = $chat->messages()->count();
        return (isset($chat->messages) && $chat->messages()->count() > 0) ? $this->response_api(true, 'success', $data) : $this->response_api(false, 'failed');
    }

    public function send_new_message($type, Chat $chat, Request $request)
    {
        if (isset($chat)) {
            $msg = $chat->createMessage($request, $type);
            $notifiable = auth()->user()->isStudent() ? $chat->teacher : $chat->student;
            $notifiable->sendNotification([
                'title' => $notifiable->locale == 'ar' ? 'هناك رسالة جديدة' : 'New Message',
                'message' => $notifiable->locale == 'ar' ? 'وصلتك رسالة جديدة إضغط للوصول للمحادثة' : 'New Message',
                'type' => $chat->isRequest() ? 'chat_request' : 'chat',
                'from_id' => $chat->isRequest() ? $chat->request_id : auth()->user()->id
            ]);

            $options = array(
                'cluster' => 'us2',
                'encrypted' => true
            );
            $pusher = new Pusher(
                'd8e93d92ea33ee7289d4',
                '49e0a36d64966e782b44',
                '532742',
                $options
            );
            $data['from_id'] = auth()->user()->id;
            $data['msg_counter'] = $notifiable->unReadCount();
            $data['conversation_id'] = $chat->id;

            try {
                $pusher->trigger('notify_' . $notifiable->id, 'new_message', $data);
            } catch (PusherException $e) {

            }


            if (isset($request->files_array) && count($request->files_array) > 0) {
                $files_id = collect($request->files_array)->pluck('id');
                $msg->storefiles($files_id);
            }
            if ($chat->messages()->count() == 1) {
                ($chat->isRequest()) ? $chat->teacher->notify(new TrainingNotification('وصلتك رسالة جديدة من ' . $chat->student->getUserName() . ' بخصوص طلب تدريبي جديد. ', null, 'الوصول للمحادثة', lang_route('teacher.chat.request', [$chat->id])))
                    : $chat->teacher->notify(new TrainingNotification('وصلتك رسالة جديدة من ' . $chat->student->getUserName(), null, 'الوصول للمحادثة', lang_route('teacher.chat', ['2', $chat->student_id])));
            }
            broadcast(new MessageSentEvent(auth()->user(), $msg->with('files')->find($msg->id)))->toOthers();
            return (isset($msg)) ? $this->response_api(true, 'success', $msg) : $this->response_api(false, 'failed');
        }
        return $this->response_api(false, 'failed');
    }

    public function whiteboard_index($id)
    {
        $trainingRequest = TrainingRequest::find($id);
        if (isset($trainingRequest->teacher)) {
            $whiteboard = $trainingRequest->getClassRoom();
            if (isset($whiteboard)) {
                if (auth()->user()->isStudent()) {
                    session()->flash('alert', ['type' => 'error', 'message' => __('لا يوجد غرف متاحة حالياً')]);
                    return $whiteboard->hasAttendee() ? Redirect::to($whiteboard->student_link) : redirect()->to(get_current_locale() . '/chat/request/' . $trainingRequest->id);
                } else {
                    return Redirect::to($whiteboard->teacher_link);
                }
            }
        }
        session()->flash('alert', ['type' => 'error', 'message' => __('لا يوجد غرف متاحة حالياً')]);
        return redirect()->to(get_current_locale() . '/');
    }
}
