<?php

namespace App\Http\Controllers;

use App\Notifications\PanelConfirmMail;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function get_validation_errors($type, $message_text, $errors, $is_array = null)
    {
        $error = [];
        $data['errors'] = [];
        foreach ($errors->getMessages() as $k => $message) {
            $error['key'] = $k;
            $error['message'] = $message[0];
            $data['errors'] [] = $error;
        }
        $view = ajax_render_view('layout.errors', $data);
        if (isset($is_array)) {
            return ['message' => $message_text, 'errors_object' => $view];
        }
        return $this->response_api(false, $message_text, ($type) ? $view : $data['errors']);

    }

    function response_api($status, $message, $items = null , $type = null)
    {
        $response = ['status' => $status, 'message' => $message];
        if ($status && isset($items)) {
            $response['item'] = $items;
        } else {
            $response['errors_object'] = $items;
        }
        if (isset($type)){
            $response['type'] = $type;
        }
        return response()->json($response);
    }


}
