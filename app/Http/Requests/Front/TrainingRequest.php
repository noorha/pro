<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class TrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->has('type')) {
            $data = [];
            $online = $this->request->get('online_price') ;
            $interview = $this->request->get('interview_price') ;
            if (isset($online)) {
                $data['online_price'] = 'required|numeric|min:0|not_in:0';
            }
            if (isset($interview)) {
                $data['interview_price'] = 'required|numeric|min:0|not_in:0';
            }
            return $data;
        }
        return [];
    }
}
