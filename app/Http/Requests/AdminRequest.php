<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ($this->request->has('admin_id'))?
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:255|unique:admins,email,'.$this->request->get('admin_id').',id,deleted_at,NULL',
                'role' => 'required',
                'photo' => 'required',
            ]:
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:admins,email,NULL,id,deleted_at,NULL',
                'role' => 'required',
                'photo' => 'required',
                'password' => 'required|string|min:6',
            ];
    }
}
