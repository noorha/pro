<?php

namespace App\Http\Requests\API;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class VerificationCode extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ($this->type == 'password') ? [
            'type' => 'required|in:password,confirm',
            'email' => 'required|email',
//            'mobile' => 'required|numeric',
        ] : [
            'user_id' => 'required|numeric',
            'type' => 'required|in:password,confirm',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['status' => false, 'errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
