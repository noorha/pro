<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JoinCourseRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'course_id' => 'required|numeric',
            'name' => 'required|string|max:190',
            'email' => 'required|string|email|max:190',
            'mobile' => 'required|numeric',
            'address' => 'required|string|max:190',
        ];
    }
}
