<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;

class PromoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('id') !== null && !empty($this->request->get('id'))) {

            return [
                'code' => 'required|string|max:255|unique:promo_codes,code,' . $this->request->get('id') . ',id,deleted_at,NULL',
                'discount' => 'required|numeric',
                'start_time' => 'required',
                'end_time' => 'required',
                'max_student_no' => 'required|numeric',
            ];
        }
        return [
            'code' => 'required|string|max:255|unique:promo_codes,code,NULL,id,deleted_at,NULL',
            'discount' => 'required|numeric',
            'start_time' => 'required',
            'end_time' => 'required',
            'max_student_no' => 'required|numeric',
        ];
    }
}
