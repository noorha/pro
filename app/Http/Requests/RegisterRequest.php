<?php

namespace App\Http\Requests;

use App\Rules\validEmail;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->has('id')) {
            return [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|max:255|unique:users,email,' . $this->request->get('id') . ',id,deleted_at,NULL',
                'nationality_id' => 'required',
                'mobile' => 'required',
            ];
        }
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' =>  ['required','string','email','max:255','unique:users'
//                ,new validEmail(new \GuzzleHttp\Client()),
            ],
            'password' => 'required|string|min:6|confirmed',
            'nationality_id' => 'required|numeric',
            'mobile' => 'required',
        ];
    }



}
