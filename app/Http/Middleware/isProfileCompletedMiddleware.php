<?php

namespace App\Http\Middleware;

use Closure;

class isProfileCompletedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check() && auth()->user()->isNotActivated()) {
            session()->flash('alert', ['type' => 'info', 'message' => __('lang.you_must_activate_your_account')]);
            return redirect()->route(get_current_locale() . '.profile.activate');
        }

        if (auth()->check() && auth()->user()->isWaitActivated()) {
            session()->flash('alert', ['type' => 'info', 'message' => __('lang.you_must_activate_your_account')]);
            return redirect()->route(get_current_locale() . '.profile.wait_activate');
        }

        if (auth()->check()) {
            if (!session()->has('is_profile_completed')) {
                session()->put(['is_profile_completed'=>auth()->user()->is_profile_completed]);
            }
            if ((int)session()->get('is_profile_completed') == 1) {
                return $next($request);
            }
            session()->flash('alert', ['type' => 'info', 'message' => __('يجب عليك إستكمال الملف الشخصي')]);
            return redirect()->route(get_current_locale() . '.profile.complete');
        }
        return $next($request);
    }
}
