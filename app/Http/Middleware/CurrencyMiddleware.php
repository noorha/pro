<?php

namespace App\Http\Middleware;

use Closure;

class CurrencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!session()->has('currency')) {
            session()->put(['currency'=>'USD']);
            return $next($request);
        }
        session()->put(['currency'=>session()->has('currency')]);
        return $next($request);
    }
}
