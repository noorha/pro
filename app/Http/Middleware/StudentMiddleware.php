<?php

namespace App\Http\Middleware;

use Closure;

class StudentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_auth()){
            if (auth()->user()->isStudent()){
                return $next($request);
            }else {
                if ($request->ajax()){
                    return response()->json(['status'=>false,'message'=>__('الصفحة المطلوبة خاصة بالطلاب')]);
                }
                session()->flash('alert', ['type' => 'warning', 'message' => __('الصفحة المطلوبة خاصة بالطلاب')]);
                return redirect()->route(get_current_locale().'.main');
            }
        }
        if ($request->ajax()){
            return response()->json(['status'=>false,'message'=>__('الرجاء تسجيل الدخول للمتابعة')]);
        }
        session()->flash('alert', ['type' => 'warning', 'message' =>__('الرجاء تسجيل الدخول للمتابعة')]);
        return redirect()->route(get_current_locale().'.login') ;
    }
}
