<?php

namespace App\Http\Middleware\Api;

use Closure;

class CurrencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currency = $request->header('Currency');
        if (isset($currency)){
            session()->put(['currency'=>$currency]);
            return $next($request);
        }
        session()->put(['currency'=>'USD']);
        return $next($request);
    }
}
