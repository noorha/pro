<?php

namespace App\Http\Middleware;

use Closure;

class isAccountActivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check() && auth()->user()->isNotActivated()) {
            session()->flash('alert', ['type' => 'info', 'message' => __('يجب عليك تفعيل حسابك')]);
            return redirect()->route(get_current_locale() . '.profile.activate');
        }
        return $next($request);
    }
}
