<?php

namespace App\Http\Middleware;

use Closure;

class TeacherMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_auth()) {
            if (auth()->user()->isTeacher()) {
                return $next($request);
            } else {
                if ($request->ajax()) {
                    return response()->json(['status' => false, 'message' => __('الصفحة المطلوبة خاصة بالمدرسين')]);
                }
                session()->flash('alert', ['type' => 'warning', 'message' => __('الصفحة المطلوبة خاصة بالمدرسين')]);
                return redirect()->route(get_current_locale().'.main');
            }
        }
        if ($request->ajax()) {
            return response()->json(['status' => false, 'message' => __('الرجاء تسجيل الدخول للمتابعة')]);
        }
        session()->flash('alert', ['type' => 'warning', 'message' => __('الرجاء تسجيل الدخول للمتابعة')]);
        return redirect()->route(get_current_locale().'.login');
    }
}
