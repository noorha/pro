<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseCategory extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'text' , 'text_en' , 'icon'
    ];

    protected $hidden = [
        'text', 'text_en', 'created_at', 'updated_at', 'deleted_at',
    ];

    protected $appends = ['name'];

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return get_text_locale( $this , 'text');
    }


}
