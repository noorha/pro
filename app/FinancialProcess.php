<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class FinancialProcess extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'type', 'credit_type', 'value', 'is_confirmed', 'is_withdraw', 'is_refund',
    ];

    protected $hidden = [
        'is_confirmed', 'is_withdraw', 'updated_at', 'deleted_at',
    ];

    protected $appends = [
        'description', 'currencyValue'
    ];

    public function isWithdrawRequest()
    {
        return $this->is_withdraw == 1;
    }

    public function getDescriptionAttribute()
    {
        return get_financial_process_status($this);
    }

    public function isRefund()
    {
        return $this->is_refund == 1 ;
    }

    public function scopeCredit($query)
    {
        return $query->where('type', 1)->where('credit_type', '!=', 'cash')->where('is_confirmed', 1);
    }


    public function scopeWithdraw($query)
    {
        return $query->where('type', 2)->where('credit_type', '!=', 'cash')->where('is_confirmed', 1);
    }


    public function getCurrencyValueAttribute()
    {
        return ($this->value > 0) ? get_currency_value($this->value) : 0;
    }

    public function getName()
    {
        return (isset($this->user)) ? $this->user->getUserName() : no_data();
    }

    public function getID()
    {
        return (isset($this->user)) ? $this->user->id : 0;
    }


    public function scopePayCredit()
    {
        return $this->credit()->where('credit_type', 'paypal');
    }


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function isCredit()
    {
        return ($this->type == 1) ? true : false;
    }

    public function isWithdraw()
    {
        return ($this->type == 2) ? true : false;
    }

    public function createWithdrawRequest(Request $request)
    {
        return WithdrawRequest::create([
            'process_id' => $this->id,
            'user_id' => $this->user_id,
            'status' => 'pending',
            'email' => $request->email,
            'value' => $request->value,
        ]);
    }


}
