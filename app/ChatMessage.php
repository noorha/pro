<?php

namespace App;

use Firebase\FirebaseLib;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mpociot\Firebase\SyncsWithFirebase;

class ChatMessage extends Model
{
    use SoftDeletes;
    use SyncsWithFirebase;

    protected $fillable = [
        'chat_id', 'type', 'is_file', 'content', 'read_at'
    ];

    protected $hidden = [
        'chat_id', 'updated_at', 'deleted_at'
    ];


    public function saveToFirebase($mode)
    {
        if (is_null($this->firebaseClient)) {
            $this->firebaseClient = new FirebaseLib(config('services.firebase.database_url'), config('services.firebase.secret'));
        }
        $path ='chats/'. $this->chat_id . '/messages/' . $this->getKey();

        if ($mode === 'set' && $fresh = $this->fresh()) {
            $this->firebaseClient->set($path, $fresh->toArray());
        } elseif ($mode === 'update' && $fresh = $this->fresh()) {
            $this->firebaseClient->update($path, $fresh->toArray());
        } elseif ($mode === 'delete') {
            $this->firebaseClient->delete($path);
        }
    }

    public function user()
    {
        if ($this->type == 'teacher') {
            return $this->chat->teacher();
        }
        return $this->chat->student();
    }

    public function hasUser()
    {
        return isset($this->user);
    }

    public function getUserName()
    {
        return $this->hasUser() ? $this->user->getUserName() : 'لا يوجد إسم';
    }

    public function getUserAvatar()
    {
        return $this->hasUser() ? $this->user->photo : 'default.png';
    }

    public function chat()
    {
        return $this->belongsTo('App\Chat', 'chat_id', 'id');
    }


    public function files()
    {
        return $this->belongsToMany('App\File', 'file_categories', 'owner_id', 'file_id')->where('owner_model', 'App\ChatMessage');
    }

    public function storeFiles($files)
    {
        if (isset($files) && count($files) > 0) {
            $pivotData = array_fill(0, count($files), ['owner_model' => 'App\ChatMessage']);
            $syncData = array_combine($files->toArray(), $pivotData);
            $this->files()->sync($syncData);
        }
    }


}
