<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $timestamps = false;

    protected $appends = ['name'];

    public function getNameAttribute()
    {
        return locale_value($this->name_ar, $this->name_en);
    }
}
