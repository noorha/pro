<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as Guzzle;

class validEmail implements Rule
{
    protected $client;
    protected $message = 'Sorry, invalid email address.';

    public function __construct(Guzzle $client)
    {
        $this->client = $client;
    }

    public function passes($attribute, $value)
    {
        $response = $this->getMailgunResponse($value);
        return (isset($response->is_valid) && $response->is_valid) ;
    }


    protected function getMailgunResponse($address)
    {
        $request = $this->client->request('GET', 'https://api.mailgun.net/v3/address/validate', [
            'query' => [
                'api_key' => env('MAILGUN_VALIDATION_KEY'),
                'address' => $address
            ]
        ]);
       return json_decode($request->getBody());
    }

    public function message()
    {
        return __('الرجاء إدخال بريد إلكتروني صحيح');
    }

}
