<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseSubscription extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'course_id', 'email', 'name', 'mobile', 'address'
    ];


    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }


    public function courseTitle()
    {
        return isset($this->course) ? $this->course->title : no_data();
    }
}
