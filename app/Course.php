<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Course extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'is_published', 'category_id', 'country_id', 'title', 'description', 'price', 'photo', 'center_name', 'start_date'
    ];


    public function category()
    {
        return $this->belongsTo('App\CourseCategory', 'category_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function countryName()
    {
        return (isset($this->country)) ? get_text_locale($this->country,'name') : no_data();
    }

    public function categoryName()
    {
        return (isset($this->category)) ?  get_text_locale($this->category,'text') : no_data();
    }


    public function scopePublishedCourses($query)
    {
        return $query->where('is_published', 'like', '1');
    }

    public function scopeSearch($query, Request $request)
    {
        if (isset($request->price) && !empty($request->price)) {
            $query = ($request->price == 'free') ? $query->where('price', 0) : $query->where('price', '!=', 0);
        }
        if (isset($request->category) && !empty($request->category && $request->category != 'all')) {
            $query = $query->where('category_id', $request->category);
        }
        return $query;
    }

    public function sections()
    {
        return $this->hasMany('App\CourseSection', 'course_id', 'id');
    }

    public function incrementCount()
    {
        $constant = new Constant();
        $item =  $constant->key('courses');
        if (isset($item)&& $item->update(['value'=>((int) $item->value +1)])) {
            return true ;
        }
        $item = Constant::create(['key'=>'courses','value'=>Course::all()->count()]);
        return isset($item) ;
    }

    public function decrementCount()
    {
        $constant = new Constant();
        $item =  $constant->key('courses');
        if (isset($item)&& $item->update(['value'=>((int) $item->value -1)])) {
            return true ;
        }
        $item = Constant::create(['key'=>'courses','value'=>Course::all()->count()]);
        return isset($item) ;
    }


}
