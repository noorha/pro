<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitorMessageReplay extends Model
{
    use SoftDeletes;
    protected $fillable=['admin_id', 'message_id', 'text',];
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }

    public function message()
    {
        return $this->belongsTo(VisitorMessage::class, 'message_id', 'id');
    }
}
