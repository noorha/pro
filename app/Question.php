<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const FILLABLE = ['question_ar', 'question_en', 'replay_ar', 'replay_en'];
    protected $fillable = self::FILLABLE;

    protected $appends = ['question', 'replay'];

    public function getQuestionAttribute()
    {
        return locale_value($this->question_ar,$this->question_en);
    }
    public function getReplayAttribute()
    {
        return locale_value($this->replay_ar,$this->replay_en);
    }
}
