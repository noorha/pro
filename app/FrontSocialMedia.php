<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontSocialMedia extends Model
{
    protected $fillable = [
        'social_id', 'link'
    ];

}
