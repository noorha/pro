<?php

namespace App;

use App\Http\Requests\PermissionsRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = [
        'name','display_name','description'
    ];
    public function findOrCreate(Request $request){
        $role = $this->where('id',$request->id)->first();
        return (isset($role) && $role->update(['description'=>$request->description]))? $role : $this->create(['name'=>$request->name,'display_name'=>$request->display_name,'description'=>$request->description]);
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }


}
