<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketReplay extends Model
{
    use SoftDeletes;
    protected $fillable = ['ticket_id', 'admin_id', 'text',];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id')->withDefault();
    }

    public function hasAdmin()
    {
        return isset($this->admin_id);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }

    public function name()
    {
        return $this->hasAdmin() ? $this->admin->name : $this->ticket->userName();
    }

    public function avatar($size = null)
    {
        return $this->hasAdmin() ? image_url($this->admin->photo, $size) : $this->ticket->userAvatar($size);
    }


}
