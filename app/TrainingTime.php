<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingTime extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'day_id',  'start_time', 'end_time',
    ];


    public function day()
    {
        return $this->belongsTo('App\Day', 'day_id', 'id');
    }


}
