<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{

    public $timestamps = false;
    
    protected $table = 'user_information';
    
    protected $fillable = [
        'user_id', 'qualified_title', 'experience' ,'video_link'
    ];
}
