<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherTrainingType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'is_online', 'is_interview', 'online_price', 'interview_price'
    ];
}
