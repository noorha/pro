<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Speciality extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'name_en', 'icon'
    ];

    protected $hidden = [
        'pivot',  'name', 'name_en', 'created_at', 'updated_at', 'deleted_at',
    ];
    protected $appends = ['text', 'iconUrl'];

    /**
     * @return string
     */

    public function getIconUrlAttribute()
    {
        return image_url($this->icon);
    }

    /**
     * @return string
     */
    public function getTextAttribute()
    {
        return get_text_locale($this, 'name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subSpecialties()
    {
        return $this->hasMany('App\SubSpecialty', 'specialty_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userSubSpecialties()
    {
        return $this->belongsToMany('App\SubSpecialty', 'user_sub_specialties', 'specialty_id', 'sub_id');
    }


}
