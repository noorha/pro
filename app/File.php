<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
//    use SoftDeletes;

    protected $fillable = [
        'display_name', 'file_name', 'mime_type', 'size'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'file_name', 'pivot',
    ];

    protected $appends = ['fileUrl'];
    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp,svg,pdf,doc,docx,rar,zip,xls'
    ];

    public static $messages = [
        'file.mimes' => 'الملف الذي تحاول رفعه له صيغة غير مدعومة',
        'file.required' => 'الملف مطلوب'
    ];


    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getFileUrlAttribute()
    {
        return file_url($this->file_name);
    }

}
