<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfoFile extends Model
{

    public $timestamps = false;
    
    protected $table = 'user_information_file';
    
    protected $fillable = [
        'user_id', 'file'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
