<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'user_id', 'available', 'pending', 'withdrawn',
    ];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function total()
    {
        return ($this->available + $this->pending);
    }

    public function available()
    {
        return ($this->available);
    }
}
