<?php

namespace App;

use App\Notifications\UserNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithdrawRequest extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id', 'process_id', 'status', 'email', 'value'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    public function getUserName()
    {
        return (isset($this->user)) ? $this->user->name : no_data();
    }


    public function scopeStatus($query, $status)
    {
        return isset($status) ? $query->where('status', $status) : $query;
    }

    public function availableCash()
    {
        return (isset($this->user)) ? $this->user->availableCash() : no_data();
    }


    public function hasWithdrawProcess()
    {
        return isset($this->withdrawProcess);
    }

    public function canWithdraw()
    {
        return ($this->availableCash() >= $this->value);
    }

    public function accept()
    {
        if (isset($this->user) && $this->hasWithdrawProcess()) {
            $this->user->notify(new UserNotification($this->user->id, 'user', 'تمت عملية سحب رصيد بقيمة (' . $this->value . ' دولار)  بنجاح',
                __('تمت عملية سحب رصيد بقيمة').' '. $this->value . ' $ .', lang_route('profile.balance')));
            $this->update(['status' => 'accepted']);
            $this->user->decrementWalletAvailable($this->value);
            return $this->withdrawProcess->update(['is_confirmed' => 1]);
        }
        return false;
    }

    public function reject()
    {
        if (isset($this->user)) {
            $this->user->notify(new UserNotification($this->user->id, 'user', 'تم رفض طلب السحب الخاص بك وتم إعادة المبلغ المسحوب لك بقيمة (' . $this->value . ' دولار)',
                __('تم رفض طلب السحب الخاص بك وتم إعادة المبلغ المسحوب لك بقيمة').' '. $this->value . ' $.', lang_route('profile.balance')));
            return ($this->withdrawProcess()->delete() && $this->user->updateWalletFromLog() && $this->update(['status' => 'rejected']));
        }
        return false;
    }

    public function withdrawProcess()
    {
        return $this->belongsTo('App\FinancialProcess', 'process_id', 'id');

    }

    public function createWithdraw()
    {
        return FinancialProcess::create([
            'user_id' => $this->user_id,
            'type' => 'withdraw',
            'credit_type' => 'request',
            'value' => $this->value,
        ]);
    }


}
