<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseSection extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'course_id', 'title', 'text', 'json'
    ];
    public function courses()
    {
        return $this->hasMany('App\Course', 'category_id', 'id');
    }

    public function orderByMostCount()
    {
        return  $this->orderBy('count', 'DESC');
    }

}
