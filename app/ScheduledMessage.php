<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class ScheduledMessage extends Model
{
    use SoftDeletes;
    const FILLABLE = ['title', 'text', 'gender', 'type', 'period', 'is_enabled', 'last_update'];
    protected $fillable = self::FILLABLE;


    public function scopeEnabled($q)
    {
        return $q->where('is_enabled', 1);
    }

    public function isActive()
    {
        $daysNo = 0;
        switch ($this->period) {
            case 'daily' :
                $daysNo = 1;
                break;
            case 'weekly' :
                $daysNo = 7;
                break;
            case 'monthly' :
                $daysNo = 30;
                break;
            case 'yearly' :
                $daysNo = 365;
                break;
        }
        return $this->is_enabled == 1 && isset($this->last_update) && (get_date_from_timestamp(Carbon::now()->subDay($daysNo)) == get_date_from_timestamp($this->last_update));
    }

    public function getUsers()
    {
        $request = new Request($this->toArray());
        return  User::query()->scheduledMessages($request)->get();
    }
}
