<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;


    protected $fillable = [
        'name', 'name_en', 'icon', 'is_arab', 'code', 'is_enabled'
    ];

    protected $hidden = [
        'name', 'name_en', 'created_at', 'updated_at', 'deleted_at',
    ];

    protected $appends = ['text', 'iconUrl'];

    /**
     * @return string
     */
    public function getTextAttribute()
    {
        return get_text_locale($this, 'name');
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getIconUrlAttribute()
    {
        return image_url($this->icon);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('App\City', 'country_id', 'id');
    }

    public function scopeArab($q)
    {
        return $q->where('is_arab', 1);
    }

    public function scopeEnabled($q)
    {
        return $q->where('is_enabled',1);
    }

    public function scopeNonArab($q)
    {
        return $q->where('is_arab', 0);
    }

}
