<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'text_ar', 'text_en'
    ];

    protected $appends=['text'];

    public function getTextAttribute()
    {
        return locale_value($this->text_ar, $this->text_en);
    }

    public function blogs()
    {
        return $this->hasMany(Blog::class, 'category_id', 'id');
    }


}
