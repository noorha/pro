<?php

namespace App;

use App\Mail\ReplayMail;
use App\Notifications\UserNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DeleteAccountRequest extends Model
{
    use SoftDeletes;

    protected $fillable = ['status', 'user_id', 'admin_comment'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }


    public function isRejected()
    {
        return $this->status == 'rejected';
    }

    public function getUserName()
    {
        return (isset($this->user)) ? $this->user->name : no_data();
    }

    public function deleteUser()
    {
        if (isset($this->user)){
            Mail::to($this->user->email, 'HalaPro')->send(new  ReplayMail(' حذف الحساب الخاص بك ','تم حذف الحساب الخاص بك بناء على طلبك داخل موقع هلا برو', $this->user->email));
        }
        return isset($this->user) ? ($this->user->delete() && $this->update(['status' => 'accepted'])) : false;
    }

    public function reject(Request $request)
    {
        $this->update(['status'=>'rejected','admin_comment' =>$request->comment]);
        $this->user->notify(new UserNotification(0, 'manager', 'تم رفض طلب الحذف - '.$request->comment, 'Your Delete Request is Rejected - '.$request->comment, '#'));
        $this->delete();
        return true;
    }


    public function scopeStatus($query, $status)
    {
        return isset($status) ? $query->where('status', $status) : $query;
    }


}
