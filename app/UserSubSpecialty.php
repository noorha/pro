<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubSpecialty extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'specialty_id', 'user_id', 'sub_id'
    ];
}
