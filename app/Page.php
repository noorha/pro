<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $fillable = [
        'type', 'title', 'text', 'title_en', 'text_en', 'phone', 'email', 'address'
    ];

    protected $hidden = ['title', 'text', 'title_en', 'text_en', 'created_at', 'updated_at', 'deleted_at', 'phone', 'email', 'address', 'type',];

    protected $appends = ['titleText', 'content'];

    /**
     * @return string
     */
    public function getTitleTextAttribute()
    {
        return get_text_locale($this, 'title');
    }

    /**
     * @return string
     */
    public function getContentAttribute()
    {
        return (get_text_locale($this, 'text'));
    }

    /**
     * @param $type
     * @return mixed
     */
    public function type($type)
    {
        return $this->where('type', $type)->first();
    }


}
