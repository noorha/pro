<?php

namespace App\Traits;
use Exception;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

trait FireBase
{
    private function setup(){
        // This assumes that you have placed the Firebase credentials in the same directory
        // as this PHP file.
        $serviceAccount = ServiceAccount::fromJsonFile(app_path('Firebase/halapronew-firebase-adminsdk-quqwv-b8a076f72f.json'));
        return (new Factory)
            ->withServiceAccount($serviceAccount)
            // The following line is optional if the project id in your credentials file
            // is identical to the subdomain of your Firebase project. If you need it,
            // make sure to replace the URL with the URL of your project.
            ->withDatabaseUri('https://halapronew.firebaseio.com')
            ->create();
    }
    /**
     * @param $chat_id
     * @param null $last
     * @return bool|\Kreait\Firebase\Database\Snapshot
     */
    public function getMessagesChats($last = null, $paginate = 1000){
        try{
            $fireBase = $this->setup();
            if ($last)
                return $fireBase->getDatabase()->getReference("/messagingChats")->orderByKey()->getSnapshot();
            return $fireBase->getDatabase()->getReference("/messagingChats")->orderByKey()->getSnapshot();
        }catch (Exception $e){
            return false; }
    }

    /**
     * @param $chat_id
     * @return bool|\Kreait\Firebase\Database\Snapshot
     */
    public function getMessagesChatByID($chat_id){
        try{
            $fireBase = $this->setup();
            return $fireBase->getDatabase()->getReference("/messagingChats/".$chat_id)->getSnapshot();
        }catch (Exception $e){
            return false; }
    }

    /**
     * @param $chat_id
     * @return bool|\Kreait\Firebase\Database\Snapshot
     */
    public function getTrainingChatByID($chat_id){
        try{
            $fireBase = $this->setup();
            return $fireBase->getDatabase()->getReference("/trainingChats/".$chat_id)->getSnapshot();
        }catch (Exception $e){
            return false; }
    }

    /**
     * @param $chat_id
     * @return bool|\Kreait\Firebase\Database\Snapshot
     */
    public function getMessagesChatMessages($chat_id){
        try{
            $fireBase = $this->setup();
            return $fireBase->getDatabase()->getReference("/messages/".$chat_id)->getSnapshot();
        }catch (Exception $e){
            return false; }
    }

}