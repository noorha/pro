<?php

namespace App\Traits;

use Twilio\Exceptions\RestException as exception;
use Twilio\Rest\Api\V2010\Account\TwilioException as TwilioException;
use Twilio\Rest\Client;

trait Sms
{

    public function send(array $data)
    {
        $data['status'] = false;
        $data['error'] = null;
        try {
            $sid = env('TWILIO_SID'); // Your Account SID from www.twilio.com/console
            $token = env('TWILIO_TOKEN'); // Your Auth Token from www.twilio.com/console
            $client = new Client($sid, $token);
            $message = $client->messages->create(
                $data['to'],
                [
                    'from' => env('TWILIO_FROM'),
                    'body' => $data['message']
                ]
            );
            if ($message->errorCode) {
                $data['error'] = $message->errorMessage;
                $data['message'] = trans('messages.wrong_mobile');
            }
            $data['status'] = true;
        } catch (exception $ex) {
//            $data['error'] = $ex->getMessage();
            $data['message'] = trans('messages.wrong_mobile');
        } catch (TwilioException $e) {
            $data['message'] = trans('messages.wrong_mobile');
        }
        return $data;
    }


}