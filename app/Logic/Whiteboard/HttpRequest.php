<?php

namespace App\Logic\Whiteboard;

class HttpRequest
{

    /**
     * @param $url
     * @param $data
     * @param null $optional_headers
     * @return bool|string
     * @throws \Exception
     */

    function wiziq_do_post_request($url, $data, $optional_headers = null)
    {

        $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
        ));


        if ($optional_headers !== null) {
            $params['http']['header'] = $optional_headers;
        }
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', true, $ctx);
        if (!$fp) {
            throw new \Exception("Problem with " . $url);
        }
        $response = @stream_get_contents($fp);


        if ($response === false) {
            throw new \Exception("Problem reading data from $url, $php_errormsg");
        }
        return $response;
    }

}