<?php
/**
 * Created by PhpStorm.
 * User: OMEN
 * Date: 9/30/2018
 * Time: 10:04 AM
 */

namespace App\Logic\Whiteboard;
use App\User;

class ScheduleClass
{

    function create($data)
    {
        $authBase = new AuthBase();
        $method = "create";
        $requestParameters["signature"] = $authBase->GenerateSignature($method, $requestParameters);
        #for teacher account pass parameter 'presenter_email'
        //This is the unique email of the presenter that will identify the presenter in WizIQ. Make sure to add
        //this presenter email to your organization�s teacher account. � For more information visit at: (http://developer.wiziq.com/faqs)


        $requestParameters["presenter_email"] = $data['email'];
        #for room based account pass parameters 'presenter_id', 'presenter_name'
        //$requestParameters["presenter_id"] = "40";
        //$requestParameters["presenter_name"] = "vinugeorge";
        $requestParameters["start_time"] = $data['start_time'];
        $requestParameters["title"] = $data['title']; //Required
        $requestParameters["duration"] = "60"; //optional
//        $requestParameters["time_zone"]="Asia/Kolkata"; //optional
//        $requestParameters["attendee_limit"]=""; //optional
//        $requestParameters["control_category_id"]=""; //optional
//        $requestParameters["create_recording"]=""; //optional
//        $requestParameters["return_url"]=""; //optional
//        $requestParameters["status_ping_url"]=""; //optional
        $requestParameters["language_culture_name"] = "en-us";
        $httpRequest = new HttpRequest();
        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request("https://classapi.wiziqxt.com/apimanager.ashx?method=create", http_build_query($requestParameters, '', '&'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if (!empty($XMLReturn)) {
            try {
                $objDOM = new \DOMDocument();
                $objDOM->loadXML($XMLReturn);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");
            if ($attribNode == "ok") {
                $response['status'] = true;
                $response['method'] = $objDOM->getElementsByTagName("method")->item(0)->nodeValue;
                $response['board_id'] = $objDOM->getElementsByTagName("class_id")->item(0)->nodeValue;
                $response['recording_link'] = $objDOM->getElementsByTagName("recording_url")->item(0)->nodeValue;
                $response['presenter_email'] = $objDOM->getElementsByTagName("presenter_email")->item(0)->nodeValue;
                $response['teacher_link'] = $objDOM->getElementsByTagName("presenter_url")->item(0)->nodeValue;
                return $response;
            } else if ($attribNode == "fail") {
                $error = $objDOM->getElementsByTagName("error")->item(0);
                $response['status'] = false;
                $response['code'] = $error->getAttribute("code");
                $response['msg'] = $error->getAttribute("msg");
                return $response;
            }
        }//end if
        return null;
    }//end function

    function addTeacher(User $user)
    {
        $authBase = new AuthBase();
        $method = "add_teacher";
        $requestParameters["signature"] = $authBase->GenerateSignature($method, $requestParameters);
        #for teacher account pass parameter 'presenter_email'
        //This is the unique email of the presenter that will identify the presenter in WizIQ. Make sure to add
        //this presenter email to your organization�s teacher account. � For more information visit at: (http://developer.wiziq.com/faqs)
        $requestParameters["name"] = $user->name;
        $requestParameters["email"] = $user->email;
        $requestParameters["password"] =  'W@pqaz123';
        $requestParameters["about_the_teacher"] = $user->description;
        $requestParameters["can_schedule_class"] = true;
        $requestParameters["is_active"] = true;
        $requestParameters["language_culture_name"] = "en-us";
        $httpRequest = new HttpRequest();
        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request("https://classapi.wiziqxt.com/apimanager.ashx?method=add_teacher", http_build_query($requestParameters, '', '&'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if (!empty($XMLReturn)) {
            try {
                $objDOM = new \DOMDocument();
                $objDOM->loadXML($XMLReturn);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");
            if ($attribNode == "ok") {
                $response['status'] = true;
                $response['method'] = $objDOM->getElementsByTagName("method")->item(0)->nodeValue;
                $response['teacher_id'] = $objDOM->getElementsByTagName("teacher_id")->item(0)->nodeValue;
                $response['teacher_email'] = $objDOM->getElementsByTagName("teacher_email")->item(0)->nodeValue;
                return $response;
            } else if ($attribNode == "fail") {
                $error = $objDOM->getElementsByTagName("error")->item(0);
                $response['status'] = false;
                $response['code'] = $error->getAttribute("code");
                $response['msg'] = $error->getAttribute("msg");
                return $response;
            }
        }//end if
        return null;
    }

    function AddAttendee($data)
    {
        $authBase = new AuthBase();

        $XMLAttendee =
            "<attendee_list>
               <attendee>
                 <attendee_id><![CDATA[".rand(10,100)."]]></attendee_id>
                 <screen_name><![CDATA[".$data['name']."]]></screen_name>
                 <language_culture_name><![CDATA[en-us]]></language_culture_name>
               </attendee>
            </attendee_list>";
        $method = "add_attendees";
        $requestParameters["signature"] = $authBase->GenerateSignature($method, $requestParameters);
        $requestParameters["class_id"] = $data['class_id'];//required
        $requestParameters["attendee_list"] = $XMLAttendee;
        $httpRequest = new HttpRequest();
        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request('https://classapi.wiziqxt.com/apimanager.ashx?method=add_attendees', http_build_query($requestParameters, '', '&'));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        if (!empty($XMLReturn)) {
            try {
                $objDOM = new \DOMDocument();
                $objDOM->loadXML($XMLReturn);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");
            if ($attribNode == "ok") {
                $data['status'] = true;
                $data['method'] = $objDOM->getElementsByTagName("method")->item(0)->nodeValue;
                $data['class_id'] = $objDOM->getElementsByTagName("class_id")->item(0)->nodeValue;
//                $data['add_attendeesStatus'] = $objDOM->getElementsByTagName("add_attendeesStatus")->item(0)->getAttribute("status");
                $data['add_attendees'] = $objDOM->getElementsByTagName("add_attendees")->item(0)->nodeValue;
                $attendeeTag = $objDOM->getElementsByTagName("attendee");
                $length = $attendeeTag->length;
                for ($i = 0; $i < $length; $i++) {
                    $data['attendee']['attendee_id'] = $objDOM->getElementsByTagName("attendee_id")->item($i)->nodeValue;
                    $data['attendee']['attendee_url'] = $objDOM->getElementsByTagName("attendee_url")->item($i)->nodeValue;
                }
                return $data;
            } else if ($attribNode == "fail") {
                $error = $objDOM->getElementsByTagName("error")->item(0);
                $data['code'] = $error->getAttribute("code");
                $data['msg'] = $error->getAttribute("msg");
                return $data;
            }
        }
        return null;//end if
    }//end function

}