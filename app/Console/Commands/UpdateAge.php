<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UpdateAge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'update:age';


    protected $description = 'update Age for all users';



    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $users = new User();
        $users = $users->get();
        foreach ($users as $user){
            $user->updateAge();
        }
    }
}
