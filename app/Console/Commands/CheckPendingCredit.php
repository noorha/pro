<?php

namespace App\Console\Commands;

use App\OnHoldCash;
use Illuminate\Console\Command;

class CheckPendingCredit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:credit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pending = new OnHoldCash();
        $items = $pending->status('pending')->whereDate('created_at', '<=', Carbon::now()->addWeek(2));
        foreach ($items as $item){
            $item->update(['status'=>'done']);
            if (isset($item->user)){
                $item->user->decrementWalletOnHold($item->value);
                $item->user->incrementWalletAvailable($item->value);
            }
        }
    }
}
