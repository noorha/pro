<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectTrainingReason extends Model
{
    protected $table = 'reject_reasons';

    public $timestamps = false;

    protected $fillable = [
        'request_id', 'note'
    ];
}
