<?php

namespace App;

use App\Notifications\UserNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ReportReason extends Model
{
    protected $table = 'report_reasons';


    protected $fillable = [
        'type', 'status', 'user_id', 'request_id', 'note'
    ];

    public static function create(array $data)
    {
        $instance = new self;
        $instance->fill($data);

        $instance->user_id = auth()->check() ? auth()->user()->id : (auth('api')->check() ? auth('api')->user()->id : 0);
        $instance->save();
        return $instance;
    }

    public function request()
    {
        return $this->belongsTo(TrainingRequest::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function hasRequest()
    {
        return isset($this->request);
    }

    public function notifyForAccept()
    {
        if ($this->hasUser()) {
            $this->user->notify(new UserNotification($this->id, 'training', __('تم إلغاء طلب التدريب بناءً على الشكوى المرسلة من طرفك'),__('تم إلغاء طلب التدريب بناءً على الشكوى المرسلة من طرفك'), lang_route('student.training.requests',[$this->request_id])));
        }
        return true;
    }

    public function notifyForReject(Request $request)
    {
        if ($this->hasUser()) {
            $this->user->notify(new UserNotification($this->id, 'training', __('تم رفض الشكوى المرسلة من طرفك بخصوص طلب التدريب بسبب').' '.$request->comment,__('تم رفض الشكوى المرسلة من طرفك بخصوص طلب التدريب بسبب').' '.$request->comment, lang_route('student.training.requests',[$this->request_id])));
        }
        return true;
    }

    public function hasUser()
    {
        return isset($this->user);
    }

    public function getUserName()
    {
        return $this->hasUser() ? $this->user->getUserName() : '';
    }

}
