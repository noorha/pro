<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    public function item(){
        return $this->hasOne('App\FrontSocialMedia','social_id','id');
    }

    public function updateItem($link){
        if (!isset($this->item)){
            $item = FrontSocialMedia::create([
                'social_id'=> $this->id,
                'link'=> isset($link)? $link : '#',
            ]);
            return ( isset($item)) ? true : false;
        }
        return $this->item->update(['link'=>isset($link)? $link : '#']);
    }

}
