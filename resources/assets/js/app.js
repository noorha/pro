
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
window.Vue = require('vue');
// window.mime = require('mime-to-extensions');

import VueResource from "vue-resource"
// import VueScrollTo from "vue-scrollto"
import axios from 'axios'
import VueContentPlaceholders from 'vue-content-placeholders'

Vue.use(VueContentPlaceholders);

Vue.use(VueResource);
Vue.use(axios);

// Vue.use(VueScrollTo, {
//         container: ".all_fstnotif messages",
//         duration: 500,
//         easing: "ease",
//         offset: 0,
//         cancelable: true,
//         onStart: false,
//         onDone: false,
//         onCancel: false,
//         x: false,
//         y: true
//     });



Vue.component('messaging', require('./components/MessgingComponent.vue'));
// Vue.component('chat', require('./components/ChatComponent.vue'));

const app = new Vue({
    el: '#app'
});
