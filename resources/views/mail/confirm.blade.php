<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style media="all" type="text/css">
        @media only screen and (max-width: 620px) {
            .span-2,
            .span-3 {
                max-width: none !important;
                width: 100% !important;
            }

            .span-2 > table,
            .span-3 > table {
                max-width: 100% !important;
                width: 100% !important;
            }
        }

        @media all {
            .btn-primary table td:hover {
                background-color: #34495e !important;
            }

            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }

        @media all {
            .btn-secondary a:hover {
                border-color: #34495e !important;
                color: #34495e !important;
            }
        }

        @media only screen and (max-width: 620px) {
            h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }

            h2 {
                font-size: 22px !important;
                margin-bottom: 10px !important;
            }

            h3 {
                font-size: 16px !important;
                margin-bottom: 10px !important;
            }

            p,
            ul,
            ol,
            td,
            span,
            a {
                font-size: 16px !important;
            }

            .wrapper,
            .article {
                padding: 10px !important;
            }

            .content {
                padding: 0 !important;
            }

            .container {
                padding: 0 !important;
                width: 100% !important;
            }

            .header {
                margin-bottom: 10px !important;
            }

            .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            .btn table {
                width: 100% !important;
            }

            .btn a {
                width: 100% !important;
            }

            .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }

            .alert td {
                border-radius: 0 !important;
                padding: 10px !important;
            }

            .receipt {
                width: 100% !important;
            }
        }

        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>

<body class=""
      style="font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f3f3f3; margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" class="body"
       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f3f3f3;"
       width="100%" bgcolor=" #f3f3f3">
    <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container"
            style="font-family: sans-serif; font-size: 14px; vertical-align: top; Margin: 0 auto !important; max-width: 580px; padding: 10px; width: 580px;"
            width="580" valign="top">
            <div class="content"
                 style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                <table border="0" cellpadding="0" cellspacing="0" class="main"
                       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;"
                       width="100%">

                    <tr style="background-color: #f3f3f3">
                        <td style="padding-left: 40%;padding-top: 3%;padding-bottom: 3%;">
                            <img src="{{url('/front/images/logo-astazk-co.png')}}" alt="HalaPro">
                        </td>
                    </tr>
                    <!-- START NOTIFICATION BANNER -->

                <!-- END NOTIFICATION BANNER -->

                    @php
                        $name =   $user->getUserName();
                    @endphp
                    <tr>
                        <td class="wrapper"
                            style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;"
                            valign="top">
                            @if($user->isLocaleAr())
                                <table border="0" cellpadding="0" cellspacing="0"
                                       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                       width="100%">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"
                                            valign="top">
                                            <p style="font-family: sans-serif;text-align: right;  font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                مرحباً بك </p>
                                            <p style="font-family: sans-serif;font-size: 20px; text-align: center;  font-weight: 550; margin: 0;Margin-bottom: 15px;">
                                                لتفعيل الحساب الخاص بك الرجاء النقر على الزر بالأسفل </p>
                                            <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary"
                                                   style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;"
                                                   width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center"
                                                        style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;"
                                                        valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                               style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="font-family: sans-serif; text-align: right; font-size: 14px; vertical-align: top; background-color: #de5698; border-radius: 5px; text-align: center;"
                                                                    valign="top" bgcolor="#de5698" align="center">
      كود التفعيل الخاص بك هو    {{$link}}
                                                                 </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <p style="font-family: sans-serif; text-align: right;font-size: 13px;font-weight: normal;margin: 0;Margin-bottom: 15px; margin-top: 20px;color: #888888;">
                                                تم إرسال هذه الرسالة بناءً على طلبك لتسجيل مستخدم جديد في هلا برو.
                                            </p>

                                            <p style="font-family: sans-serif; text-align: right;font-size: 13px;font-weight: normal;margin: 0;Margin-bottom: 15px; margin-top: 20px;color: #888888;">
                                                شكرأ جزيلاً 😊
                                                فريق هلا برو
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            @else
                                <table border="0" cellpadding="0" cellspacing="0"
                                       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                       width="100%">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"
                                            valign="top">
                                            <p style="font-family: sans-serif;text-align: left;  font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                                                Hello, </p>
                                            <p style="font-family: sans-serif;font-size: 20px; text-align: left;  font-weight: 550; margin: 0;Margin-bottom: 15px;">
                                                To activate your account, please click on the below button:
                                            </p>
                                            <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary"
                                                   style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box; min-width: 100% !important;"
                                                   width="100%">
                                                <tbody>
                                                <tr>
                                                    <td align="center"
                                                        style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;"
                                                        valign="top">
                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                               style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="font-family: sans-serif; text-align:center; font-size: 14px; vertical-align: top; background-color: #de5698; border-radius: 5px; text-align: center;"
                                                                    valign="top" bgcolor="#de5698" align="center">
                                                           Your activation code is    {{$link}}

                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <p style="font-family: sans-serif; text-align: left;font-size: 13px;font-weight: normal;margin: 0;Margin-bottom: 15px; margin-top: 20px;color: #888888;">
                                                This message was sent as per your request to register a new user in
                                                HalaPro.
                                            </p>

                                            <p style="font-family: sans-serif; text-align: left;font-size: 13px;font-weight: normal;margin: 0;Margin-bottom: 15px; margin-top: 20px;color: #888888;">
                                                Thank You 😊
                                                HalaPro Team
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            @endif
                        </td>
                    </tr>
                    <tr style="background-color: #f3f3f3">
                        <td>
                            @if($user->isLocaleAr())
                                <p style="text-align: center">جميع الحقوق محفوظة © لموقع هلا بر 2018</p>
                            @else
                                <p style="text-align: center">HalaPro Copyrights © 2018</p>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
    </tr>
</table>
</body>
</html>