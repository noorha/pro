@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        <style>
            .ckbox-inline {
                display: inline-block !important;
            }

            .ckbox label {
                font-size: 15px;
            }

            .label {
                margin-top: 5px !important;
                display: inline-block !important;
            }

            fieldset {
                border: 1px solid #ccc !important;
                margin: 10px 0px 65px !important;
                padding: 25px 10px 10px 10px !important;
                border-radius: 10px;
                position: relative;
            }

            fieldset .ckbox-fs {
                display: inline-block;
                position: absolute;
                top: -10px;
                right: -5px;
            }
        </style>
    @endpush
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> الصلاحيات /  إضافة مجموعة صلاحيات </span></h2>
    </div>
    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','url'=>url()->current(),'to'=>admin_url('role/all')]) !!}
            <input type="hidden" name="id" value="{{@$role->id}}">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="control-label" style="text-align: center">إسم المجموعة :</label>
                                <input type="text" name="name" class="form-control" value="{{@$role->name}}" required/>
                            </div>

                            <div class="form-group">
                                <label class=" control-label" style="text-align: center">وصف المجموعة :</label>
                                <textarea class="form-control" rows="5" name="description" required>{{@$role->description}}</textarea>
                            </div>

                            <div style="margin-top: 40px" class="roles">

                                <h4>الدورات</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input id="courses" type="checkbox" class="parent"/>
                                        <label for="courses"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" name="permissions[]" id="add_course" value="add_course" @if(@$role->hasPermission('add_course')) checked @endif />
                                            <label for="add_course">إضافة / تعديل</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_course" name="permissions[]"
                                                   @if(@$role->hasPermission('view_course')) checked @endif         value="view_course"/>
                                            <label for="view_course">عرض</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_course"  name="permissions[]"
                                                   @if(@$role->hasPermission('delete_course')) checked @endif      value="delete_course"/>
                                            <label for="delete_course">حذف</label>
                                        </div>
                                    </div>

                                </fieldset>


                                <h4>المدربين</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input id="trainers" class="parent" type="checkbox"/>
                                        <label for="trainers"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="add_teacher" class="panel_course"
                                                   @if(@$role->hasPermission('add_teacher')) checked @endif   name="permissions[]" value="add_teacher"/>
                                            <label for="add_teacher">إضافة / تعديل </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_teacher" name="permissions[]"
                                                   @if(@$role->hasPermission('view_teacher')) checked @endif     value="view_teacher"/>
                                            <label for="view_teacher">عرض </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_teacher" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_teacher')) checked @endif   value="delete_teacher"/>
                                            <label for="delete_teacher">حذف </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="notify_teacher" name="permissions[]"
                                                   @if(@$role->hasPermission('notify_teacher')) checked @endif    value="notify_teacher"/>
                                            <label for="notify_teacher">إرسال الإشعارات </label>
                                        </div>
                                    </div>


                                </fieldset>


                                <h4>الطلاب</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input id="students" class="parent" type="checkbox"/>
                                        <label for="students"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="add_student" name="permissions[]"
                                                   @if(@$role->hasPermission('add_student')) checked @endif    value="add_student"/>
                                            <label for="add_student">إضافة / تعديل </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_student" name="permissions[]"
                                                   @if(@$role->hasPermission('view_student')) checked @endif  value="view_student"/>
                                            <label for="view_student">عرض </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_student" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_student')) checked @endif    value="delete_student"/>
                                            <label for="delete_student">حذف </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="notify_student" name="permissions[]"
                                                   @if(@$role->hasPermission('notify_student')) checked @endif    value="notify_student"/>
                                            <label for="notify_student">إرسال الإشعارات </label>
                                        </div>
                                    </div>


                                </fieldset>

                                <h4>العمليات المالية</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="financial_process"/>
                                        <label for="financial_process"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_credit_financial_process"
                                                   @if(@$role->hasPermission('view_credit_financial_process')) checked @endif     name="permissions[]" value="view_credit_financial_process"/>
                                            <label for="view_credit_financial_process">عرض عمليات الإيداع</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_withdraw_financial_process"
                                                   @if(@$role->hasPermission('view_withdraw_financial_process')) checked @endif     name="permissions[]" value="view_withdraw_financial_process"/>
                                            <label for="view_withdraw_financial_process">عرض عمليات السحب</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="accept_withdraw_financial_process"
                                                   @if(@$role->hasPermission('accept_withdraw_financial_process')) checked @endif    name="permissions[]" value="accept_withdraw_financial_process"/>
                                            <label for="accept_withdraw_financial_process">قبول / رفض عمليات
                                                السحب</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h4>طلبات التدريب</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="training_requests"/>
                                        <label for="training_requests"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_training_request" name="permissions[]"
                                                   @if(@$role->hasPermission('view_training_request')) checked @endif   value="view_training_request"/>
                                            <label for="view_training_request">عرض الطلبات</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_conversation_training_request"
                                                   @if(@$role->hasPermission('view_conversation_training_request')) checked @endif   name="permissions[]" value="view_conversation_training_request"/>
                                            <label for="view_conversation_training_request">عرض محادثات الطلبات</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h4>المحادثات</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="conversations"/>
                                        <label for="conversations"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_conversation" name="permissions[]"
                                                   @if(@$role->hasPermission('view_conversation')) checked @endif  value="view_conversation"/>
                                            <label for="view_conversation">عرض </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h4>الصلاحيات</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="roles"/>
                                        <label for="roles"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="add_role" name="permissions[]"
                                                   @if(@$role->hasPermission('add_role')) checked @endif  value="add_role"/>
                                            <label for="add_role">إضافة / تعديل </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_role" name="permissions[]"
                                                   @if(@$role->hasPermission('view_role')) checked @endif value="view_role"/>
                                            <label for="view_role">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_role" class="front_course"
                                                   @if(@$role->hasPermission('delete_role')) checked @endif   name="permissions[]" value="delete_role"/>
                                            <label for="delete_role">حذف</label>
                                        </div>
                                    </div>
                                </fieldset>


                                <h4>رسائل الزوار</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="inbox"/>
                                        <label for="inbox"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_inbox" name="permissions[]"
                                                   @if(@$role->hasPermission('view_inbox')) checked @endif  value="view_inbox"/>
                                            <label for="view_inbox">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="replay_inbox" name="permissions[]"
                                                   @if(@$role->hasPermission('replay_inbox')) checked @endif   value="replay_inbox"/>
                                            <label for="replay_inbox"> الرد على الرسائل </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_inbox" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_inbox')) checked @endif       value="delete_inbox"/>
                                            <label for="delete_inbox">حذف</label>
                                        </div>
                                    </div>
                                </fieldset>


                                <h4>التذاكر</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="ticket"/>
                                        <label for="ticket"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_ticket" name="permissions[]"
                                                   @if(@$role->hasPermission('view_ticket')) checked @endif  value="view_ticket"/>
                                            <label for="view_ticket">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="replay_ticket" name="permissions[]"
                                                   @if(@$role->hasPermission('replay_ticket')) checked @endif   value="replay_ticket"/>
                                            <label for="replay_ticket"> الرد على التذاكر </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_ticket" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_ticket')) checked @endif       value="delete_ticket"/>
                                            <label for="delete_ticket">حذف</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h4>طلبات الحذف</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="delete_request"/>
                                        <label for="delete_request"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_delete_request" name="permissions[]"
                                                   @if(@$role->hasPermission('view_delete_request')) checked @endif    value="view_delete_request"/>
                                            <label for="view_delete_request">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="accept_delete_request" name="permissions[]"
                                                   @if(@$role->hasPermission('accept_delete_request')) checked @endif  value="accept_delete_request"/>
                                            <label for="accept_delete_request"> قبول / رفض </label>
                                        </div>
                                    </div>

                                </fieldset>


                                <h4>الإبلاغات</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="report"/>
                                        <label for="report"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_report" name="permissions[]"
                                                   @if(@$role->hasPermission('view_report')) checked @endif  value="view_report"/>
                                            <label for="view_report">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_report" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_report')) checked @endif    value="delete_report"/>
                                            <label for="delete_report">حذف </label>
                                        </div>
                                    </div>

                                </fieldset>


                                <h4>شركاء الموقع</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="partners"/>
                                        <label for="partners"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="add_partner" class="front_course"
                                                   @if(@$role->hasPermission('add_partner')) checked @endif name="permissions[]" value="add_partner"/>
                                            <label for="add_partner">إضافة / تعديل </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_partner" name="permissions[]"
                                                   @if(@$role->hasPermission('view_partner')) checked @endif  value="view_partner"/>
                                            <label for="view_partner">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_partner" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_partner')) checked @endif       value="delete_partner"/>
                                            <label for="delete_partner">حذف</label>
                                        </div>
                                    </div>
                                </fieldset>


                                <h4>القائمة البريدية</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="mailing_list"/>
                                        <label for="mailing_list"></label>
                                    </div>


                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_mailing_list" name="permissions[]"
                                                   @if(@$role->hasPermission('view_mailing_list')) checked @endif     value="view_mailing_list"/>
                                            <label for="view_mailing_list">عرض </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="send_mailing_list" name="permissions[]"
                                                   @if(@$role->hasPermission('send_mailing_list')) checked @endif        value="send_mailing_list"/>
                                            <label for="send_mailing_list">إرسال بريد </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="active_mailing_list" name="permissions[]"
                                                   @if(@$role->hasPermission('active_mailing_list')) checked @endif   value="active_mailing_list"/>
                                            <label for="active_mailing_list">تفعيل / إلغاء تفعيل </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_mailing_list" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_mailing_list')) checked @endif     value="delete_mailing_list"/>
                                            <label for="delete_mailing_list">حذف</label>
                                        </div>
                                    </div>
                                </fieldset>


                                <h4>حسابات الإدارة</h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="admin"/>
                                        <label for="admin"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="add_admin" name="permissions[]"
                                                   @if(@$role->hasPermission('add_admin')) checked @endif  value="add_admin"/>
                                            <label for="add_admin">إضافة / تعديل الحساب </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="delete_admin" name="permissions[]"
                                                   @if(@$role->hasPermission('delete_admin')) checked @endif    value="delete_admin"/>
                                            <label for="delete_admin">حذف الحساب</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="view_admin" name="permissions[]"
                                                   @if(@$role->hasPermission('view_admin')) checked @endif  value="view_admin"/>
                                            <label for="view_admin">عرض جميع الحسابات</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h4> إعدادات الموقع </h4>
                                <fieldset>
                                    <div class="ckbox ckbox-warning ckbox-fs">
                                        <input type="checkbox" class="parent" id="settings"/>
                                        <label for="settings"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="page_settings" name="permissions[]"
                                                   @if(@$role->hasPermission('page_settings')) checked @endif  value="page_settings"/>
                                            <label for="page_settings"> صفحات الموقع </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="template_settings" name="permissions[]"
                                                   @if(@$role->hasPermission('template_settings')) checked @endif   value="template_settings"/>
                                            <label for="template_settings">إعدادات القوالب </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                        <div class="ckbox ckbox-primary">
                                            <input type="checkbox" id="constant_settings" name="permissions[]"
                                                   @if(@$role->hasPermission('constant_settings')) checked @endif     value="constant_settings"/>
                                            <label for="constant_settings">ثوابت الموقع</label>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary">حفظ&nbsp;
                                &nbsp; <i style="top: inherit;left: AUTO;"
                                          class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>


    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('/panel/js/check-all.js') !!}
        {!! HTML::script('panel/js/simple_form.js') !!}
    @endpush
@stop