@extends('panel.layout.index')
@section('main')
    @push('panel_css')

        <style>
            .home-label {
                font-size: 15px!important;
            }
        </style>
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i>الصفحة الرئيسية </h2>
    </div>

    <div class="contentpanel">

        @php
            $students = new \App\User();
            $students = $students->students()->orderBy('created_at','DESC');

            $std_array =get_students_data();
            $teachers_array =get_teachers_data();

            $teachers = new \App\User();
            $teachers = $teachers->teachers()->orderBy('created_at','DESC');
        @endphp
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="panel panel-success panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="/panel/images/is-user.png" alt="" />
                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label home-label">عدد المدرسين</small>
                                    <h1>{{$teachers_array['all']}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">آخر يوم</small>
                                    <h4>{{$teachers_array['day']}}</h4>
                                </div>

                                <div class="col-xs-6">
                                    <small class="stat-label">آخر شهر</small>
                                    <h4>{{$teachers_array['month']}}</h4>
                                </div>
                            </div><!-- row -->
                        </div><!-- stat -->

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-danger panel-stat">
                    <div class="panel-heading">

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="/panel/images/is-document.png" alt="" />
                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label home-label">عدد الطلاب </small>
                                    <h1>{{$std_array['all']}}</h1>
                                </div>
                            </div><!-- row -->

                            <div class="mb15"></div>


                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">آخر يوم</small>
                                    <h4>{{$std_array['day']}}</h4>
                                </div>

                                <div class="col-xs-6">
                                    <small class="stat-label">آخر شهر</small>
                                    <h4>{{$std_array['month']}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-primary panel-stat">
                    <div class="panel-heading">

                        @php
                        $training = get_training_request_data();
                        @endphp

                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="/panel/images/is-document.png" alt="" />
                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label home-label">طلبات التدريب</small>
                                    <h1>{{$training['all']}}</h1>
                                </div>
                            </div>
                            <div class="mb15"></div>

                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stat-label">قيد الإنتظار</small>
                                    <h4>{{$training['pending']}}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stat-label">قيد التنفيذ</small>
                                    <h4>{{$training['progress']}}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stat-label">المكتملة</small>
                                    <h4>{{$training['completed']}}</h4>
                                </div>
                            </div>
                        </div>

                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->

            <div class="col-sm-6 col-md-3">
                <div class="panel panel-dark panel-stat">
                    <div class="panel-heading">
                        @php
                        $msg = get_visitor_data()

                        @endphp
                        <div class="stat">
                            <div class="row">
                                <div class="col-xs-4">
                                    <img src="/panel/images/is-money.png" alt="" />
                                </div>
                                <div class="col-xs-8">
                                    <small class="stat-label home-label">رسائل إتصل بنا</small>
                                    <h1>{{$msg['all']}}</h1>
                                </div>
                            </div><!-- row -->
                            <div class="mb15"></div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">آخر يوم</small>
                                    <h4>{{$msg['day']}}</h4>
                                </div>
                                <div class="col-xs-6">
                                    <small class="stat-label">آخر شهر</small>
                                    <h4>{{$msg['month']}}</h4>
                                </div>
                            </div><!-- row -->
                        </div><!-- stat -->
                    </div><!-- panel-heading -->
                </div><!-- panel -->
            </div><!-- col-sm-6 -->
        </div>

        <div class="row">


            <div class="col-sm-6 col-md-6">
                <div class="panel panel-default panel-alt widget-messaging">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="{{lang_route('panel.teacher.all')}}" class="panel-edit"><i class="fa fa-sign-in"></i></a>
                        </div><!-- panel-btns -->
                        <h3 class="panel-title text-cario home-label">المدرسين المضافين حديثاً</h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            @foreach($teachers->take(10)->get() as $teacher)
                            <li>
                                <small class="pull-right">{{get_date_from_timestamp($teacher->created_at)}}</small>
                                <h4 class="sender">{{$teacher->getUserName()}}</h4>
                                <small>{{$teacher->getFullLocation()}}</small>
                            </li>
                            @endforeach
                        </ul>
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div>

            <div class="col-sm-6 col-md-6">
                <div class="panel panel-default panel-alt widget-messaging">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="{{lang_route('panel.student.all')}}" class="panel-edit"><i class="fa fa-sign-in"></i></a>
                        </div><!-- panel-btns -->
                        <h3 class="panel-title text-cario home-label">الطلاب المضافين حديثاً</h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            @foreach($students->take(10)->get() as $student)
                                <li>
                                    <small class="pull-right">{{get_date_from_timestamp($student->created_at)}}</small>
                                    <h4 class="sender">{{$student->getUserName()}}</h4>
                                    <small>{{$student->getFullLocation()}}</small>
                                </li>
                            @endforeach
                        </ul>
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div>
        </div>
    </div>


    @push('panel_js')

    @endpush
@stop