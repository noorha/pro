@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}

    @endpush
    <div class="pageheader" xmlns="http://www.w3.org/1999/html">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>طلبات التدريب / عرض   </span>
        </h2>
    </div>
    <div class="contentpanel">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="padding-bottom: 56px;padding-top: 56px" class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> حالة الطلب </label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{get_request_status($item)}}" class="form-control"
                                       required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> الموضوع </label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{$item->getSubSpecialtyName()}}"
                                       class="form-control" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> عدد الساعات </label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{$item->hour_no}}" class="form-control" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> السعر الإجمالي </label>
                            <div class="col-sm-8">
                                <input type="text" disabled
                                       value="{{get_currency_value($item->total).' '.get_currency_text()}}"
                                       class="form-control" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">مبلغ الخصم </label>
                            <div class="col-sm-8">
                                <input type="text" disabled
                                       value="{{get_currency_value($item->discount_value).' '.get_currency_text()}}"
                                       class="form-control" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">مبلغ بعد الخصم </label>
                            <div class="col-sm-8">
                                <input type="text" disabled
                                       value="{{get_currency_value($item->total_with_discount).' '.get_currency_text()}}"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">نوع التدريب </label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{$item->getType()}}" class="form-control"
                                       required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">طريقة الدفع</label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{$item->getPaymentMethodText()}}"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> ملاحظات </label>
                            <div class="col-sm-8">
                                <textarea name="mobile" disabled class="form-control" rows="5"
                                          required/>{{$item->note}}</textarea>
                            </div>
                        </div>

                        @if(isset($item->files) && $item->files()->count() >0)
                            <h3>@lang('المرفقات')</h3>
                            @foreach($item->files as $file)
                                <a target="_blank" href="{{file_url($file->file_name)}}">{{$file->display_name}}</a>
                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
            <div class="col-md-4">
                @if(isset($item->teacher))
                    @php
                        $teacher =  $item->teacher;
                    @endphp
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4 class="text-center"> المدرب</h4>
                            <h3 class="text-center">
                                <a target="_blank"
                                   href="{{lang_route('panel.teacher.show', [$item->teacher->id])}}">
                                    {{$item->getTeacherName()}}
                                </a>
                            </h3>
                            <div class="col-md-4">
                                <div class="monyf">
                                    <div class="text_monyf">
                                        <p>الرصيد المعلق</p>
                                        <h4>{{($teacher->pending_cash)}}<span>$</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="monyf">

                                    <div class="text_monyf">
                                        <p> المبلغ المستحق</p>
                                        <h4>{{$teacher->totalNonPaidProfit() > 0 ? $teacher->totalNonPaidProfit() : 0}}<span>$</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="monuy2">
                                    <div class="monyf hvlsw">
                                        <div class="text_monyf">
                                            <p>المبلغ القابل للسحب</p>
                                            <h4>{{($teacher->walletAvailableCash())}}<span>$</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 jasny-padding">


                                <form id="single">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail"
                                             style="width: 200px; height: 200px;max-width:200px; max-height:200px">
                                            <img src="{{image_url($item->getTeacherPhoto(),'300x300')}}"
                                                 data-src="holder.js/100%x100%"
                                                 alt="">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="text-center"> الطالب</h4>
                        <h3 class="text-center">
                                {{$item->getStudentName()}}
                        </h3>

                        <div class="col-sm-12 jasny-padding">
                            <form id="single">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 200px;max-width:200px; max-height:200px">
                                        <img src="{{image_url($item->getStudentPhoto(),'300x300')}}"
                                             data-src="holder.js/100%x100%"
                                             alt="">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/user_form.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        <script>
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
        </script>
    @endpush
@stop