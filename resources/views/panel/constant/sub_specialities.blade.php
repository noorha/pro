@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> إعدادات الموقع / ثوابت الموقع /  التخصصات الفرعية    </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <div class="panel-btns">
                        <button style="width: 13%" class="btn btn-success" data-toggle="modal"
                                data-target=".bs-example-modal-photo"><i class="glyphicon glyphicon-plus"></i>&nbsp;إضافة
                            جديد
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" id="table1">
                            <thead>
                            <tr class="text-center">
                                <th class="text-center text-cario" width="5%">#</th>
                                <th class="text-center text-cario" width="10%"> الظهور </th>
                                <th class="text-center text-cario" width="10%"> التخصص الأساسي </th>
                                <th class="text-center text-cario" width="10%"> الموضوع  </th>
                                <th class="text-center text-cario" width="10%"> الموضوع (باللغة إنجليزية) </th>
                                <th class="text-center text-cario" width="20%">خيارات</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div id="country_modal" class="modal fade bs-example-modal-photo " tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-photo-viewer">
            {!! Form::open(['id'=>'form','url'=>admin_url('constant/countries/create')]) !!}
            <div style="height:auto" class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"> ثوابت الموقع </h4>
                </div>
                <div class="modal-body">
                    <div id="loader" class="col-md-offset-6 hidden">
                        <img src="/panel/images/loaders/loader10.gif" alt="">
                    </div>

                    <div id="modal_body" class="row">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="icon" id="photo">
                        <div class="col-md-9 col-md-offset-2">

                            @php
                                $specialities = get_all_specialities();
                            @endphp

                            <div class="form-group">
                                <label class="control-label" style="text-align: center">التخصص الأساسي :</label>
                                <select id="specialty_id" class="select2" name="specialty_id" data-placeholder="الرجاء تحديد التخصص"
                                        required>
                                    <option></option>
                                    @if(isset($specialities) && $specialities->count() > 0)
                                        @foreach($specialities as $speciality)
                                            <option value="{{$speciality->id}}">{{$speciality->name .'  ('.$speciality->name_en.')'}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="text-align: center">الموضوع  :</label>
                                <input  type="text" id="name" name="name" placeholder="" class="form-control" required/>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="text-align: center">الموضوع (باللغة الإنجليزية) :</label>
                                <input  style="text-align: left" type="text" id="name_en" name="name_en" placeholder="" class="form-control" required/>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row btn-padding">
                        <button style="width: 15%" class="btn btn-success pull-right"> حفظ &nbsp; &nbsp; <i
                                    style="top: inherit;left: AUTO;"
                                    class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        <script>
            var url = '{{lang_route('panel.constant.sub-specialities.data')}}';
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
        </script>
        {!! HTML::script('panel/js/sub-specialities-datatable.js') !!}

    @endpush
@stop