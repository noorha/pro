<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/panel/images/favicon.png" type="image/png">

    <title>HalaPro CPanel</title>
    @include('panel.layout.css')

    {!! HTML::script('panel/js/html5shiv.js') !!}
    {!! HTML::script('panel/js/respond.min.js') !!}

</head>


<body class="signin">
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<section>
    <div class="signinpanel">
        <div class="row">
            <div class="col-md-7">
                <div class="signin-info">
                    <div class="logopanel">
                        <img src="/front/images/logo-astazk-co.png">
                    </div>

                    <div class="mb20"></div>
                    <h5><strong style="font-size: 18px">مرحباً بكم في لوحة تحكم موقع HalaPro </strong></h5>
                    <ul>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i>  التحكم في طلبات التدريب</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> التحكم في مستخدمين الموقع </li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> التحكم في قوالب وثوابت الموقع </li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> التحكم في العمليات المالية للمستخدمين</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> بالإضافة لميّزات عديدة ...</li>
                    </ul>
                    <div class="mb20"></div>
                </div><!-- signin0-info -->
            </div><!-- col-sm-7 -->
            <div class="col-md-5">
                {!! Form::open(['method'=>'POST','url'=>lang_route('panel.login')]) !!}
                @if(session()->has('response'))
                    <div class="alert alert-danger">
                        {{session()->get('response')}}
                    </div>
                @endif
                <h4 class="nomargin">تسجيل الدخول</h4>
                <input type="email" class="form-control uname" name="email" placeholder="البريد الإلكتروني" required/>
                <input type="password" class="form-control pword" name="password" placeholder="كلمة المرور" required/>
                <button class="btn btn-success btn-block default-back-color">تسجيل الدخول</button>
                {!! Form::close() !!}
            </div>
        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                جميع الحقوق محفوظة لموقع Ustazk&nbsp;
                <a href="https://arapeak.com/ar/index" target="_blank">AraPeak Team</a>
            </div>
        </div>
    </div><!-- signin -->
</section>

{!! HTML::script('panel/js/jquery-1.11.1.min.js') !!}
{!! HTML::script('panel/js/jquery-migrate-1.2.1.min.js') !!}
{!! HTML::script('panel/js/bootstrap.min.js') !!}
{!! HTML::script('panel/js/modernizr.min.js') !!}
{!! HTML::script('panel/js/toggles.min.js') !!}
{!! HTML::script('panel/js/jquery.cookies.js') !!}
{!! HTML::script('panel/js/jquery.sparkline.min.js') !!}
{!! HTML::script('panel/js/retina.min.js') !!}
{!! HTML::script('panel/js/custom.js') !!}



</body>
</html>
