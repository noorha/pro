@php
    $sections = $section->sectionType()->get();
@endphp
@if(isset($sections) )
    @foreach($sections as $item)
        <div style="margin-bottom: 30px" class="panel panel-info">
            <div class="panel-heading"
                 style="background-color: #eeeeee!important;border-radius: 5px;color: red!important;padding: 30px;">
                <div class="panel-btns pull-right">
                    <button title="تعديل" style="margin-right: 10px" data-type="edit" data-id="{{$item->id}}"
                            class="{{get_edit_class($item->type)}} btn btn-sm btn-primary ">
                        <i style="margin-left: 5px" class="fa fa-check-square-o"></i>تعديل
                    </button>
                    <button data-toggle="reject" title="حذف"
                            data-url="{{admin_url('landing/section/delete/'.$item->id)}}"
                            style="margin-right: 10px;background-color: #FA2A00"
                            class="btn btn-sm btn-danger delete"><i
                                class="glyphicon glyphicon-remove"></i> حذف
                    </button>
                </div>

                @if(isset($item->photo))
                    <p>
                        <img style="margin-bottom: 30px" src="{{image_url($item->photo,'150x150')}}">
                    </p>
                @endif
                <h3 style="color: #179be8;font-family: Cairo;font-size: 18px"
                    class="panel-title">{{$item->title}}</h3>
                @if($item->type != '1')
                    <p style="color: #2a2323!important;">{!! strip_tags($item->text) !!}</p>
                @else
                    @php
                        $lines = json_decode($item->text);
                    @endphp
                    @if(isset($lines))
                        @foreach($lines as $i=>$line)
                            <p style="color: #2a2323!important;">{!! strip_tags($line) !!}</p>
                        @endforeach
                    @endif
                @endif

            </div>
        </div>
    @endforeach
@endif