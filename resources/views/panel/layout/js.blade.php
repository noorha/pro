{!! HTML::script('panel/js/jquery-1.11.1.min.js') !!}
{!! HTML::script('panel/js/jquery-migrate-1.2.1.min.js') !!}
{!! HTML::script('panel/js/jquery-ui-1.10.3.min.js') !!}
{!! HTML::script('panel/js/bootstrap.min.js') !!}
{!! HTML::script('panel/js/modernizr.min.js') !!}
{!! HTML::script('panel/js/jquery.sparkline.min.js') !!}
{!! HTML::script('panel/js/toggles.min.js') !!}
{!! HTML::script('panel/js/jquery.cookies.js') !!}
{!! HTML::script('panel/js/bootstrap-timepicker.min.js') !!}
{!! HTML::script('panel/js/flot/jquery.flot.min.js') !!}
{!! HTML::script('panel/js/flot/jquery.flot.resize.min.js') !!}
{!! HTML::script('panel/js/flot/jquery.flot.spline.min.js') !!}
{!! HTML::script('panel/js/custom.js') !!}
@stack('panel_js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click','.btn-href', function (event) {
        event.preventDefault();
        location.href = $(this).data('url');
    });
    $(document).ready(function () {
        $('#logout').on('click', function () {
            var url = $(this).data('link');
            $.ajax({
                url: url,
                type: 'POST',
                success: function (response) {
                    window.location = '/admin';
                }
            });
        });
    });
</script>
