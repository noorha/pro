<div class="headerbar">
    <a class="menutoggle"><i class="fa fa-bars"></i></a>
    <div class="header-right">
        <ul class="headermenu">
            <li>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        @if(isset(auth()->user()->name))
                            <img src="{{image_url(auth()->user()->photo , '100x100')}}" alt="" />
                            {{auth()->user()->name}}
                        @endif
                        <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-usermenu">
                        {{--<li><a href="{{admin_url('profile')}}"><i class="glyphicon glyphicon-user"></i> الملف الشخصي</a></li>--}}
                        <li><a id="logout"  href="javascript:;" data-link="{{admin_url('logout')}}"><i class="glyphicon glyphicon-log-out"></i> تسجيل الخروج </a></li>
                    </ul>

                </div>
            </li>
        </ul>
    </div>
</div>
