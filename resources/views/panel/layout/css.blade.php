{!! HTML::style('panel/css/style.default.css') !!}
{!! HTML::style('panel/css/style.default-rtl.css') !!}

@stack('panel_css')
<style>
    .swal2-title {
        font-family: Cairo;
        line-height: 40px !important;
        font-size: 24px !important;
    }

    .swal2-content {
        font-family: Cairo;
        line-height: 40px;
        font-size: 24px;
    }

    .swal2-modal .swal2-styled {
        font-family: Cairo;
    }

    .panel-title a {
        font-family: Cairo;
        text-align: center!important;
    }

    video {
        background-color: #0b0b0b;
    }

</style>

