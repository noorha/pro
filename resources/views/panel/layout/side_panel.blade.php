<div class="leftpanel">


    <div class="logopanel panel-logo-img" style="padding: 4px">
        <img src="/front/images/logo-astazk-co.png">
    </div>
    <div class="leftpanelinner">
        <h5 class="sidebartitle"></h5>

        <ul class="nav nav-pills nav-stacked nav-bracket">
            <li class="{{is_active('dashboard')}}">
                <a href="{{lang_route('panel.dashboard')}}"><i class="fa fa-home"></i>
                    <span>الصفحة الرئيسية</span>
                </a>
            </li>
        </ul>

        <h5 class="sidebartitle">أقسام الموقع</h5>

        <ul class="nav nav-pills nav-stacked nav-bracket">
            @permission(['add_course','view_course'])
            <li class="nav-parent {{is_element_active('/course/i')}}"><a href=""><i class="fa fa-desktop"></i> <span> الدورات </span></a>
                <ul class="children"
                    style="{{is_nav_active(['course/create','course/all','course/edit','course/requests'])}}">
                    @permission(['add_course'])

                    <li class="{{is_active('course/create')}}">
                        <a href="{{lang_route('panel.course.create')}}">
                            <i class="fa fa-caret-right"></i>إضافة دورة
                        </a>
                    </li>
                    @endpermission

                    @permission(['view_course'])

                    <li class="{{is_active('course/all')}}">
                        <a href="{{lang_route('panel.course.all')}}"><i class="fa fa-caret-right"></i> عرض الدورات </a>
                    </li>
                    <li class="{{is_active('course/requests')}}">
                        <a href="{{lang_route('panel.course.requests')}}"><i class="fa fa-caret-right"></i> عرض طلبات
                            الإنضمام </a>
                    </li>
                    @endpermission

                </ul>
            </li>
            @endpermission

            @permission(['add_teacher','view_teacher','notify_teacher'])

            <li class="nav-parent {{is_element_active('/teacher/i')}}"><a href=""><i class="fa fa-male"></i> <span>المدرسين</span></a>
                <ul class="children"
                    style="{{is_nav_active(['teacher/create','teacher/all','teacher/edit','teacher/notifications'])}}">
                    @permission(['add_teacher'])

                    <li class="{{is_active('teacher/create')}}"><a href="{{lang_route('panel.teacher.create')}}"><i
                                    class="fa fa-caret-right"></i> إنشاء مدرس </a></li>
                    @endpermission

                    @permission(['notify_teacher'])

                    <li class="{{is_active('teacher/notifications')}}"><a
                                href="{{lang_route('panel.teacher.notifications')}}"><i class="fa fa-caret-right"></i>
                            إرسال
                            إشعارات </a></li>
                    @endpermission

                    @permission(['view_teacher'])

                    <li class="{{is_active('teacher/all')}}"><a href="{{lang_route('panel.teacher.all')}}"><i
                                    class="fa fa-caret-right"></i> عرض الكل </a></li>
                    @endpermission

                </ul>
            </li>
            @endpermission
            @permission(['add_student','view_student','notify_student'])

            <li class="nav-parent {{is_element_active('/student/i')}}"><a href=""><i class="fa fa-user"></i> <span>الطلاب</span></a>
                <ul class="children"
                    style="{{is_nav_active(['student/create','student/all','student/edit','student/notifications'])}}">
                    @permission(['add_student'])

                    <li class="{{is_active('student/create')}}"><a href="{{lang_route('panel.student.create')}}"><i
                                    class="fa fa-caret-right"></i> إنشاء طالب </a></li>
                    @endpermission

                    @permission(['notify_student'])

                    <li class="{{is_active('student/notifications')}}"><a
                                href="{{lang_route('panel.student.notifications')}}"><i class="fa fa-caret-right"></i>
                            إرسال
                            إشعارات </a></li>
                    @endpermission

                    @permission(['view_student'])

                    <li class="{{is_active('student/all')}}"><a href="{{lang_route('panel.student.all')}}"><i
                                    class="fa fa-caret-right"></i> عرض الكل </a></li>
                    @endpermission

                </ul>
            </li>
            @endpermission

            @permission(['view_credit_financial_process','view_withdraw_financial_process'])

            @php
                $pendingCount = \App\WithdrawRequest::status('pending')->count();
            @endphp

            <li class="nav-parent {{is_element_active('/payment/i')}}"><a href=""><i class="fa fa-money"></i> <span> العمليات المالية </span></a>
                <ul class="children" style="{{is_nav_active(['payment/credit','payment/withdraw'])}}">
                    @permission(['view_credit_financial_process'])

                    <li class="{{is_active('payment/credit')}}"><a href="{{lang_route('panel.payment.credit')}}"><i
                                    class="fa fa-caret-right"></i> عمليات الإيداع </a></li>
                    @endpermission

                    @permission(['view_withdraw_financial_process'])

                    <li class="{{is_active('payment/withdraw')}}">
                        <a href="{{lang_route('panel.payment.withdraw')}}">
                            @if(isset($pendingCount) && $pendingCount > 0 )
                                <span class="pull-right badge "> {{$pendingCount}}  </span>
                            @endif
                            <i class="fa fa-caret-right"></i> عمليات السحب </a>
                    </li>
                    @endpermission

                </ul>
            </li>
            @endpermission

            @permission(['view_training_request'])
            <li class="{{(preg_match('/training/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('training/all')}}">
                    <i class="fa fa-bars"></i>
                    <span>طلبات التدريب</span>
                </a>
            </li>
            @endpermission

            @permission(['view_conversation'])

            <li class="{{(preg_match('/conversation/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('conversation/all')}}">
                    <i class="fa fa-comment"></i>
                    <span>المحادثات</span>
                </a>
            </li>
            @endpermission

            @permission(['add_admin','view_admin'])
            <li class="nav-parent {{is_element_active('/account/i')}}"><a href=""><i class="fa fa-suitcase"></i> <span> حسابات الإدارة </span></a>
                <ul class="children" style="{{is_nav_active(['/account/create','/account/all'])}}">
                    @permission(['add_admin'])
                    <li class="{{is_active('account/create')}}">
                        <a href="{{admin_url('account/create')}}">
                            <i class="fa fa-caret-right"></i>إنشاء حساب جديد
                        </a>
                    </li>
                    @endpermission

                    @permission(['view_admin'])
                    <li class="{{is_active('account/all')}}">
                        <a href="{{admin_url('account/all')}}">
                            <i class="fa fa-caret-right"></i>عرض الكل
                        </a>
                    </li>
                    @endpermission
                </ul>
            </li>
            @endpermission

            @permission(['add_role','view_role'])
            <li class="nav-parent {{is_element_active('/role/i')}}">
                <a href=""><i class="fa fa-suitcase"></i> <span> الصلاحيات </span></a>
                <ul class="children" style="{{is_nav_active(['/role/create','/role/all','/role'])}}">
                    @permission(['add_role'])
                    <li class="{{is_active('role/create')}}">
                        <a href="{{admin_url('role/create')}}">
                            <i class="fa fa-caret-right"></i> إنشاء مجموعة صلاحيات
                        </a>
                    </li>
                    @endpermission

                    @permission(['view_role'])
                    <li class="{{is_active('role/all')}}">
                        <a href="{{admin_url('role/all')}}">
                            <i class="fa fa-caret-right"></i> عرض الكل
                        </a>
                    </li>
                    @endpermission
                </ul>
            </li>
            @endpermission

            @permission(['view_inbox'])

            @php
                $un_read = get_unread_message_count();
            @endphp

            <li class="{{(preg_match('/inbox/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('inbox/all')}}">
                    @if(isset($un_read) && $un_read > 0 )
                        <span class="pull-right badge "> {{$un_read}}  </span>
                    @endif
                    <i class="fa fa-envelope-o"></i>
                    <span>رسائل الزوار</span>
                </a>
            </li>
            @endpermission

            @permission(['view_ticket'])
            <li class="{{(preg_match('/ticket/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('ticket/all')}}">
                    @if(isset($pendingTickets) && $pendingTickets > 0 )
                        <span class="pull-right badge "> {{$pendingTickets}}  </span>
                    @endif
                    <i class="fa fa-ticket"></i>
                    <span>تذاكر المستخدمين</span>
                </a>
            </li>
            @endpermission

            <li class="nav-parent {{is_element_active('/blog/i')}}"><a href=""><i class="fa fa-bug"></i>
                    <span> المدونة </span></a>
                <ul class="children" style="{{is_nav_active(['/blog/all','/blog/create','/blog/categories',])}}">
                    <li class="{{is_active('blog/categories')}}"><a href="{{admin_url('blog/categories')}}"><i
                                    class="fa fa-caret-right"></i>تصنيفات المدونة</a></li>
                    <li class="{{is_active('blog/create')}}"><a href="{{admin_url('blog/create')}}"><i
                                    class="fa fa-caret-right"></i>إضافة تدوينة</a></li>
                    <li class="{{is_active('blog/all')}}"><a href="{{admin_url('blog/all')}}"><i
                                    class="fa fa-caret-right"></i>عرض جميع التدوينات</a></li>

                </ul>
            </li>

            @permission(['view_delete_request'])

            @php
                $pendingDeleteRequests = get_pending_delete_count();
            @endphp
            <li class="{{(preg_match('/delete-request/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('delete-request/all')}}">
                    @if(isset($pendingDeleteRequests) && $pendingDeleteRequests > 0 )
                        <span class="pull-right badge "> {{$pendingDeleteRequests}}  </span>
                    @endif
                    <i class="glyphicon glyphicon-remove-sign"></i>
                    <span>طلبات الحذف</span>
                </a>
            </li>
            @endpermission

            @permission(['view_report'])

            <li class="{{(preg_match('/report/i', url()->current())) ? 'active' : ''}}">
                <a href="{{admin_url('report/all')}}">
                    {{--@if(isset($un_read) && $un_read > 0 )--}}
                    {{--<span class="pull-right badge "> {{$un_read}}  </span>--}}
                    {{--@endif--}}
                    <i class="glyphicon glyphicon-warning-sign"></i>
                    <span>الإبلاغات</span>
                </a>
            </li>

            @endpermission

            <li class="nav-parent {{is_element_active('/promo-code/i')}}"><a href=""><i class="fa fa-user"></i> <span> الخصومات </span></a>
                <ul class="children"
                    style="{{is_nav_active(['promo-code/create','promo-code/all','promo-code/edit'])}}">

                    <li class="{{is_active('promo-code/create')}}">
                        <a href="{{lang_route('panel.promo-code.create.index')}}">
                            <i class="fa fa-caret-right"></i>إضافة بروموكود
                        </a>
                    </li>

                    <li class="{{is_active('promo-code/all')}}">
                        <a href="{{lang_route('panel.promo-code.all.index')}}">
                            <i class="fa fa-caret-right"></i>عرض الكل
                        </a>
                    </li>

                </ul>
            </li>

            @permission(['view_partner'])

            <li class="{{(preg_match('/partners/i', url()->current())) ? 'active' : ''}}">
                <a href="{{lang_route('panel.partners.index')}}">
                    <i class="fa fa-suitcase"></i>
                    <span>شركاء الموقع</span>
                </a>
            </li>
            @endpermission

            @permission(['view_mailing_list','send_mailing_list'])

            <li class="nav-parent {{is_element_active('/mail-list/i')}}"><a href=""><i
                            class="glyphicon  glyphicon-folder-open"></i> <span> القائمة البريدية </span></a>
                <ul class="children" style="{{is_nav_active(['mail-list/create','mail-list'])}}">
                    @permission(['send_mailing_list'])
                    <li class="{{is_active('mail-list/create')}}"><a href="{{lang_route('panel.mail.create')}}"><i
                                    class="fa fa-caret-right"></i> إرسال بريد إلكتروني </a>
                    </li>
                    @endpermission

                    @permission(['view_mailing_list'])
                    <li class="{{is_active('mail-list/')}}">
                        <a href="{{lang_route('panel.mail.index')}}"><i class="fa fa-caret-right"></i> عرض القائمة
                            البريدية</a>
                    </li>
                    @endpermission

                </ul>
            </li>

            @endpermission

            <li class="nav-parent {{is_element_active('/scheduled-messages/i')}}">
                <a href="">
                    <i class="fa fa-tasks"></i>
                    <span> الرسائل المجدولة </span></a>
                <ul class="children"
                    style="{{is_nav_active(['scheduled-messages/send','scheduled-messages/all','scheduled-messages/create'])}}">

                    {{--@permission(['send_mailing_list'])--}}
                    <li class="{{is_active('scheduled-messages/send')}}">
                        <a href="{{lang_route('panel.scheduled-messages.send.index')}}"><i
                                    class="fa fa-caret-right"></i> رسالة مباشرة </a>
                    </li>
                    {{--@endpermission--}}
                    <li class="{{is_active('scheduled-messages/create')}}">
                        <a href="{{lang_route('panel.scheduled-messages.create.index')}}"><i
                                    class="fa fa-caret-right"></i> إضافة رسالة مجدولة </a>
                    </li>
                    {{--@permission(['view_mailing_list'])--}}
                    <li class="{{is_active('scheduled-messages/all')}}">
                        <a href="{{lang_route('panel.scheduled-messages.all.index')}}"><i class="fa fa-caret-right"></i>
                            الرسائل المجدولة </a>
                    </li>
                    {{--@endpermission--}}

                </ul>
            </li>


        </ul>

        @permission(['page_settings','template_settings','constant_settings'])

        <h5 class="sidebartitle"> إعدادات الموقع </h5>
        <ul class="nav nav-pills nav-stacked nav-bracket">
            @permission(['page_settings'])
            <li class="nav-parent {{is_element_active('/page/i')}}"><a href=""><i class="fa fa-file-text"></i> <span> صفحات الموقع </span></a>
                <ul class="children"
                    style="{{is_nav_active(['page/edit/about','page/edit/privacy','page/edit/policy','page/edit/contact','page/edit/work','page/edit/how'])}}">

                    <li class="{{is_active('page/faq')}}">
                        <a href="{{lang_route('panel.page.faq.index')}}"><i class="fa fa-caret-right"></i>الأسئلة
                            الشائعة </a>
                    </li>

                    <li class="{{is_active('page/edit/how')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'how'])}}"><i class="fa fa-caret-right"></i>
                            كيف
                            نعمل </a>
                    </li>
                    <li class="{{is_active('page/edit/work')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'work'])}}"><i class="fa fa-caret-right"></i>
                            إعمل
                            معنا </a>
                    </li>
                    <li class="{{is_active('page/edit/about')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'about'])}}"><i class="fa fa-caret-right"></i>
                            من
                            نحن </a>
                    </li>
                    <li class="{{is_active('page/edit/contact')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'contact'])}}"><i
                                    class="fa fa-caret-right"></i>إتصل
                            بنا </a>
                    </li>

                    <li class="{{is_active('page/edit/privacy')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'privacy'])}}"><i
                                    class="fa fa-caret-right"></i>سياسة
                            الخصوصية </a>
                    </li>
                    <li class="{{is_active('page/edit/policy')}}">
                        <a href="{{lang_route('panel.page.edit',['type'=>'policy'])}}"><i class="fa fa-caret-right"></i>سياسة
                            الإستخدام </a>
                    </li>
                </ul>
            </li>
            @endpermission

            @permission(['template_settings'])
            <li class="nav-parent {{is_element_active('/template/i')}}"><a href=""><i class="fa fa-cogs"></i> <span> إعدادات القوالب </span></a>
                <ul class="children"
                    style="{{is_nav_active(['template/settings','template/main','template/socials'])}}">

                    <li class="{{is_active('template/settings/')}}">
                        <a href="{{lang_route('panel.template.settings')}}"><i class="fa fa-caret-right"></i> إعدادات
                            الموقع
                        </a>
                    </li>


                    <li class="{{is_active('template/socials')}}">
                        <a href="{{ lang_route('panel.template.socials')}}"><i class="fa fa-caret-right"></i> مواقع
                            التواصل الإجتماعي</a>
                    </li>


                    <li class="{{is_active('template/main/')}}"><a href="{{lang_route('panel.template.main')}}">
                            <i class="fa fa-caret-right"></i> إعدادات الصفحة الرئيسية </a>
                    </li>

                </ul>
            </li>
            @endpermission

            @permission(['constant_settings'])
            <li class="nav-parent {{is_element_active('/constant/i')}}">
                <a href=""><i class="fa fa-file-text"></i>
                    <span> ثوابت الموقع </span></a>
                <ul class="children"
                    style="{{is_nav_active(['/constant/countries','/constant/categories','constant/cities','constant/specialities','constant/sub-specialities'])}}">

                    <li class="{{is_active('constant/categories')}}"><a
                                href="{{lang_route('panel.constant.categories.index')}}"><i
                                    class="fa fa-caret-right"></i>
                            تصنيفات الدورات </a></li>

                    <li class="{{is_active('constant/countries')}}"><a
                                href="{{lang_route('panel.constant.countries.index')}}"><i
                                    class="fa fa-caret-right"></i>
                            الدول </a></li>
                    <li class="{{is_active('constant/cities')}}"><a
                                href=" {{lang_route('panel.constant.cities.index')}}"><i
                                    class="fa fa-caret-right"></i> المدن </a></li>
                    <li class="{{is_active('constant/specialities')}}"><a
                                href=" {{lang_route('panel.constant.specialities.index')}}"><i
                                    class="fa fa-caret-right"></i>
                            التخصصات </a></li>
                    <li class="{{is_active('constant/sub-specialities')}}"><a
                                href=" {{lang_route('panel.constant.sub-specialities.index')}}"><i
                                    class="fa fa-caret-right"></i> التخصصات الفرعية </a></li>
                </ul>
            </li>
            @endpermission

        </ul>
        @endpermission


    </div>
</div>
