<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="\front\images\favicon.ico" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{isset($sub_title) ? $sub_title.' | لوحة التحكم' : 'لوحة تحكم موقع Ustazk'}} </title>

    @include('panel.layout.css')
    {!! HTML::script('panel/js/html5shiv.js') !!}
    {!! HTML::script('panel/js/respond.min.js') !!}
    {!! HTML::style('/front/css/sweetalert2.min.css') !!}

</head>

<body>

<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
    @include('panel.layout.side_panel')
    <div class="mainpanel">
        @include('panel.layout.header')
        @yield('main')
    </div>
</section>

@include('panel.layout.js')

{!! HTML::script('/front/js/sweetalert2.all.min.js') !!}
{!! HTML::script('/front/js/custom.sweet.js') !!}
@if(session()->has('alert'))
    <script>
        @php
            $alert = session()->get('alert');
            $errors_html = (isset($alert['errors_object'])) ?  $alert['errors_object'] : '';
        @endphp
        customSweetAlert('{{$alert['type']}}', '{{$alert['message']}}', "{!! trim($errors_html) !!}");
    </script>
@endif

</body>
</html>
