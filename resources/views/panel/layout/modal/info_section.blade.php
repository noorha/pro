<div id="{{$id}}_modal" class="modal fade bs-example-modal-photo {{$id}}" tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-photo-viewer">
        {!! Form::open(['id'=> $id.'_form','url'=>url('/admin/landing/create')]) !!}
        <div style="height: auto" class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">{{$title}} </h4>
            </div>
            <div class="modal-body">
                <div class="col-md-offset-6 loader hidden">
                    <img src="/panel/images/loaders/loader10.gif" alt="">
                </div>

                <div class="row modal_content" style="margin: auto;">
                    <input type="hidden" name="photo" id="{{$id}}_photo">
                    <input type="hidden" name="type" id="type" value="{{$type}}">
                    <div class="col-md-9 col-md-offset-2">
                        <div class="form-group">
                            <label class=" control-label" style="text-align: center">عنوان :</label>
                            <input type="text" id="{{$id}}_title" name="title" placeholder="الرجاء إدخال العنوان " class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label class="control-label" style="text-align: center">النص :</label>
                            <textarea class="form-control " id="{{$id}}_text" name="text" rows="5"
                                      placeholder=" أكتب النص هنا " required></textarea>
                        </div>
                        <div class="form-group ">
                            <label class="control-label" style="text-align: center">صورة القسم :</label>
                            <div class="row">
                                <form id="single" action="{{csrf_token()}}">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="sr-only">Loading...</span>
                                        <div class="progress jasny_progress hidden">
                                            <div class="progress-bar progress-bar-striped active jasny_percent"
                                                 role="progressbar"
                                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                 style="width:40%;background-color: #11a724">0%
                                            </div>
                                        </div>
                                        <div class="fileinput-new thumbnail {{$id}}-thumbnail"
                                             style="width: 150px; height: 150px;max-width:150px; max-height:150px">
                                            <img id="preview_{{$id}}" src="{{ image_url('default.png','200x150/')}}"
                                                 data-src="holder.js/100%x100%" alt="">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail  {{$id}}-thumbnail"
                                             style="max-width: 150px; max-height: 150px;"></div>
                                        <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>
                                                    <span class="fileinput-exists">تغيير الصورة</span>
                                                    <input type="file" id="{{$id}}_photo_fileupload" class="fileupload">
                                                </span>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row btn-padding">
                    <button style="width: 15%" class="btn btn-success pull-right"> حفظ &nbsp; &nbsp; <i
                                style="top: inherit;left: AUTO;"
                                class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
