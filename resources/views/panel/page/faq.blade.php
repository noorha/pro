@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية
            <span> إعدادات الموقع / صفحات  الموقع /  الأسئلة الشائعة    </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <div class="panel-btns">
                        <button style="width: 13%" class="btn btn-success btn-href"
                                data-url="{{lang_route('panel.page.faq.create.index')}}"><i
                                    class="glyphicon glyphicon-plus"></i>&nbsp;إضافة
                            جديد
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" id="table1">
                            <thead>
                            <tr class="text-center">
                                <th class="text-center text-cario" width="5%">#</th>
                                <th class="text-center text-cario" width="10%"> السؤال</th>
                                {{--<th class="text-center text-cario" width="10%"> الجواب</th>--}}
                                <th class="text-center text-cario" width="10%">خيارات</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>
    {{--<div id="country_modal" class="modal fade bs-example-modal-photo " tabindex="-1" role="dialog"--}}
    {{--aria-labelledby="myLargeModalLabel"--}}
    {{--aria-hidden="true">--}}
    {{--<div class="modal-dialog modal-photo-viewer">--}}
    {{--{!! Form::open(['id'=>'form','url'=>admin_url('page/faq/create')]) !!}--}}
    {{--<div style="height:auto" class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>--}}
    {{--<h4 class="modal-title"> ثوابت الموقع </h4>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<div id="loader" class="col-md-offset-6 hidden">--}}
    {{--<img src="/panel/images/loaders/loader10.gif" alt="">--}}
    {{--</div>--}}

    {{--<div id="modal_body" class="row">--}}
    {{--<input type="hidden" name="id" id="id">--}}
    {{--<input type="hidden" name="icon" id="photo">--}}
    {{--<div class="col-md-9 col-md-offset-2">--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label" style="text-align: center">السؤال ( اللغة العربية ) :</label>--}}
    {{--<input type="text" id="question_ar" name="question_ar" placeholder="الرجاء إدخال السؤال" class="form-control" required/>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label" style="text-align: center">الرد  (اللغة العربية ) :</label>--}}
    {{--<textarea  id="replay_ar"  rows="4" name="replay_ar" placeholder="" class="form-control" required></textarea>--}}
    {{--</div>--}}


    {{--<div class="form-group">--}}
    {{--<label class="control-label" style="text-align: center">السؤال  (اللغة الإنجليزية ) :</label>--}}
    {{--<input  style="text-align: left" type="text" id="question_en" name="question_en" placeholder="" class="form-control" required/>--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<label class="control-label" style="text-align: center">الرد  (اللغة الإنجليزية ) :</label>--}}
    {{--<textarea  style="text-align: left" id="replay_en" rows="4" name="replay_en" placeholder="" class="form-control" required></textarea>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<div class="row btn-padding">--}}
    {{--<button style="width: 15%" class="btn btn-success pull-right"> حفظ &nbsp; &nbsp; <i--}}
    {{--style="top: inherit;left: AUTO;"--}}
    {{--class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--{!! Form::close() !!}--}}

    {{--</div>--}}
    {{--</div>--}}


    @push('panel_js')
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        <script>
            var url = '{{lang_route('panel.page.faq.data')}}';

        </script>
        {!! HTML::script('panel/js/faq-datatable.js') !!}

    @endpush
@stop