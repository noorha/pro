@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> إضافة مدونة / التدوينات </span></h2>
    </div>


    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','url'=> admin_url('blog/create') , 'to'=>admin_url('blog/all')]) !!}
            <input type="hidden" id="photo" name="image">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">اللغة :</label>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <select class="select2" name="locale" data-placeholder="الرجاء تحديد اللغة" required>
                                            <option></option>
                                            <option value="ar"> العربية</option>
                                            <option value="en"> الإنجليزية</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">تصنيف المدونة :</label>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <select class="select2" name="categories[]" multiple data-placeholder="الرجاء تحديد تصنيف"
                                                required>
                                            <option></option>
                                            @if(isset($categories) && $categories->count() > 0)
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->text_ar}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="form-group">
                                <label class="col-sm-3 control-label">عنوان المدونة :</label>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <input type="text" name="title" class="form-control" value="" required/>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label class="col-sm-3 control-label">رابط اليوتيوب :</label>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12" style="margin-top: 10px;">--}}
                                        {{--<input type="url" name="link" class="form-control" value="" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label class=" col-sm-3 control-label">محتوى المدونة :</label>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 10px;">
                                    <textarea class="form-control ckeditor" rows="15" name="text" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary">إضافة&nbsp; &nbsp; <i
                                        style="top: inherit;left: AUTO;"
                                        class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-sm-12 jasny-padding">
                            <form id="single" action="{{csrf_token()}}">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="sr-only">Loading...</span>
                                    <div id="jasny_progress" class="progress hidden">
                                        <div id="jasny_percent" class="progress-bar progress-bar-striped active"
                                             role="progressbar"
                                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                             style="width:40%;background-color: #11a724">0%
                                        </div>
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 200px;max-width:200px; max-height:200px">
                                        <img src="{{url('/image/300x300/default.png')}}"
                                             data-src="holder.js/100%x100%"
                                             alt="">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 200px;"></div>
                                    <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>
                                                    <span class="fileinput-exists">تغيير الصورة</span>
                                                    <input type="file" class="fileupload">
                                                </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>



    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/ckeditor/ckeditor.js') !!}
        {!! HTML::script('panel/js/ckeditor/adapters/jquery.js') !!}
        {!! HTML::script('panel/js/ckeditor.js') !!}
        {!! HTML::script('panel/js/page_form.js') !!}


        <script>
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
        </script>
    @endpush
@stop