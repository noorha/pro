@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> سحوبات المستخدمين /  طلبات قيد الإنتظار    </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">

            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr class="text-center">
                        <th class="text-center text-cario" width="5%">#</th>
                        <th class="text-center text-cario" width="10%">إسم المستخدم</th>
                        <th class="text-center text-cario" width="10%">البريد الإلكتروني</th>
                        <th class="text-center text-cario" width="10%">الرصيد القابل للسحب</th>
                        <th class="text-center text-cario" width="10%">مبلغ السحب</th>
                        <th class="text-center text-cario" width="10%">تاريخ الطلب</th>
                        <th class="text-center text-cario" width="10%">خيارات</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">


                    </tbody>
                </table>
            </div>

        </div>

    </div>


    @push('panel_js')
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        <script>
            var url = '/ar/admin/withdraw/data/pending';
        </script>
        {!! HTML::script('panel/js/withdraw-datatable.js') !!}
    @endpush
@stop