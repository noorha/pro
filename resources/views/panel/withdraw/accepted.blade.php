@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
        {{--{!! HTML::style('panel/css/view-items.css') !!}--}}

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> سحوبات المستخدمين /  الطلبات المقبولة    </span></h2>
    </div>


    <div class="contentpanel">
        <div class="row">
            <div style="width: 16%;" class="form-group pull-right">
                <div class="col-sm-12">
                    <input type="text" name="datetimes" class="form-control data_range" placeholder="بحث حسب التاريخ"/>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr class="text-center">
                        <th class="text-center text-cario" width="5%">#</th>
                        <th class="text-center text-cario" width="10%">إسم المستخدم</th>
                        <th class="text-center text-cario" width="10%">البريد الإلكتروني</th>
                        <th class="text-center text-cario" width="10%">الرصيد القابل للسحب</th>
                        <th class="text-center text-cario" width="10%">مبلغ السحب</th>
                        <th class="text-center text-cario" width="10%">تاريخ الطلب</th>
                        <th class="text-center text-cario" width="10%">خيارات</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">


                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="4" class="text-cario" style="text-align:right">الإجمالي : 1000</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>


    @push('panel_js')

        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

        <script>
            var url = '/ar/admin/withdraw/data/accepted';
        </script>


        {!! HTML::script('panel/js/withdraw-datatable.js') !!}

    @endpush
@stop