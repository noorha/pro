@php
    $title = isset($item) ?  'تعديل' : 'إضافة' ;
@endphp

@extends('panel.layout.index',['sub_title'=>$title])
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}
        {!! HTML::style('/panel/css/bootstrap-timepicker.min.css') !!}

    @endpush
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>  الرموز الترويجية  /  {{$title}} </span></h2>
    </div>

    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','method'=>'POST','url'=>url()->current(),'to'=>lang_route('panel.promo-code.all.index')]) !!}
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="padding-bottom: 56px;padding-top: 56px" class="panel-body">
                        <input type="hidden" name="id" value="{{@$item->id}}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> الكود </label>
                            <div class="col-sm-8">
                                <input type="text" name="code" id="coupon_code" value="{{ @$item->code }}" class="form-control"  required/>
                            </div>
                            {{--<a href="javascript:;" class="btn btn-success" id="change_code_btn"> تغيير </a>--}}
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> قيمة الخصم </label>
                            <div class="col-sm-8">
                                <input type="number" name="discount" value="{{@$item->discount}}" class="form-control"
                                       required/>
                            </div>
                            <label style="margin-top: 10px;font-size: 15px">%</label>
                        </div>

                        <div class="form-group">
                            <label for="datepicker_1" class="col-sm-3 control-label" style="text-align: center"> تاريخ
                                البداية</label>
                            <div class="col-sm-8">
                                <div class="input-group mb15">
                                    <input id="datepicker_1" type='text' class="form-control datepicker" value="{{get_date_from_timestamp(@$item->start_time)}}" name="start_time" required/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="datepicker_2" class="col-sm-3 control-label" style="text-align: center"> تاريخ
                                النهاية</label>
                            <div class="col-sm-8">
                                <div class="input-group mb15">
                                    <input id="datepicker_2" type='text' class="form-control datepicker" name="end_time"
                                           value="{{get_date_from_timestamp(@$item->end_time)}}" required/>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> الحد الأقصى لعدد الطلاب </label>
                            <div class="col-sm-8">
                                <input type="number" name="max_student_no" value="{{@$item->max_student_no}}" class="form-control" required/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary"> حفظ &nbsp; &nbsp;
                                <i style="top: inherit;left: AUTO;" class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {{--        {!! HTML::script('/panel/js/bootstrap-datetimepicker.min.js') !!}--}}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/user_form.js') !!}
        <script>
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
            // jQuery('.datepicker').datepicker();

            $('#datepicker_1').datepicker({dateFormat: 'yy-mm-dd'});
            $('#datepicker_2').datepicker({dateFormat: 'yy-mm-dd'});

            $('#change_code_btn').on('click', function (event) {
                event.preventDefault();
                $('#coupon_code').val('{{generateCouponCode()}}');
            });
        </script>
    @endpush
@stop
