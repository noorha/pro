@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.tagsinput.css') !!}
    @endpush

    @php
        $text = ( $type == 'student') ? ' الطلاب' : 'المدرسين';
    @endphp
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>  {{$text}} /  إرسال إشعارات  </span>
        </h2>
    </div>
    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','url'=>lang_route('panel.'.$type.'.notifications.send'),'to'=>lang_route('panel.'.$type.'.notifications.send')]) !!}
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="padding-bottom: 56px;padding-top: 56px" class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">تحديد الكل </label>
                            <div class="col-sm-8">
                                <input type="checkbox" id="select_all" name="select_all" value="1">
                                <label for="select_all" style="text-align: center"> تحديد كافة {{$text}}  </label>
                            </div>
                        </div>

                        <input type="hidden" name="type" value="{{$type}}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> تحديد {{$text}} </label>
                            <div class="col-sm-8">
                                <select class="select2" id="select_user" data-placeholder=" الرجاء تحديد {{$text}}">
                                    <option></option>
                                    @if(isset($items) && $items->count() > 0)
                                        @foreach($items as $item)
                                            <option value="{{$item->first_name}}">{{$item->getUserName()}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">المستخدمين المحددين
                                :</label>
                            <div class="col-sm-8">
                                <input name="users" id="users" placeholder="إضافة مستخدم" data-placement="إضافة مستخدم"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">نص الإشعار :</label>
                            <div class="col-sm-8">
                                <textarea name="text" rows="7" class="form-control" required></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary"> إرسال&nbsp; &nbsp; <i
                                        style="top: inherit;left: AUTO;"
                                        class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/user_form.js') !!}
        {!! HTML::script('panel/js/jquery.tagsinput.min.js') !!}

        <script>
            var tags = $('#users').tagsInput({
                width: 'auto',
                allowDuplicates: false
            });
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });

            $('#select_user').on('change', function (event) {
                var select = $(this);
                tags.addTag(select.val());
            })
        </script>
    @endpush
@stop