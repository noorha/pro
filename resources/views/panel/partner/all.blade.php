@extends('panel.layout.index',['sub_title'=>'شركاء الموقع'])
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i>  الصفحة الرئيسية<span> شركاء الموقع   </span></h2>
    </div>
    <div class="contentpanel">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <button style="width: 13%" class="btn btn-success" data-toggle="modal"
                                data-target=".bs-example-modal-photo"><i class="glyphicon glyphicon-plus"></i>&nbsp;إضافة جديد
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" id="table1">
                            <thead>
                            <tr class="text-center">
                                <th class="text-center text-cario" width="5%">#</th>
                                <th class="text-center text-cario" width="20%">الشعار</th>
                                <th class="text-center text-cario" width="20%">الرابط</th>
                                <th class="text-center text-cario" width="20%">خيارات</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div id="partners_modal" class="modal fade bs-example-modal-photo " tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-photo-viewer">
            {!! Form::open(['id'=>'form','url'=>admin_url('constant/categories/create')]) !!}
            <div style="height: auto" class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"> شركاء الموقع </h4>
                </div>
                <div class="modal-body">
                    <div id="loader" class="col-md-offset-6 hidden">
                        <img src="/panel/images/loaders/loader10.gif" alt="">
                    </div>

                    <div id="modal_body" class="row">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="photo" id="photo">
                        <div class="col-md-9 col-md-offset-2">
                            <div class="form-group">
                                <label class="control-label" style="text-align: center">الرابط :</label>
                                <input type="text" id="link" name="link" placeholder="الرجاء إدخال الرابط" class="form-control" required/>
                            </div>

                            <div class="form-group">
                                <label class="control-label" style="text-align: center">الشعار :</label>
                                <div class="row">
                                    <form id="single" action="{{csrf_token()}}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="sr-only">Loading...</span>
                                            <div id="jasny_progress" class="progress hidden">
                                                <div id="jasny_percent" class="progress-bar progress-bar-striped active"
                                                     role="progressbar"
                                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                     style="width:40%;background-color: #11a724">0%
                                                </div>
                                            </div>
                                            <div class="fileinput-new thumbnail jasny_photo"
                                                 style="max-width: 200px;max-height: 200px;">
                                                <img id="jasny_photo" src="{{ image_url('default.png','200x150')}}" data-src="holder.js/100%x100%" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 100px; max-height: 100px;"></div>
                                            <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>
                                                    <span class="fileinput-exists">تغيير الصورة</span>
                                                    <input type="file" class="fileupload">
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row btn-padding">
                        <button style="width: 15%" class="btn btn-success pull-right"> حفظ &nbsp; &nbsp; <i
                                    style="top: inherit;left: AUTO;"
                                    class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


    @push('panel_js')
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        <script>
            var url = '{{lang_route('panel.partners.data')}}';
        </script>
        {!! HTML::script('/panel/js/partners.js') !!}

    @endpush
@stop