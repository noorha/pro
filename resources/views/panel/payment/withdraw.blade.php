@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> العمليات المالية /  عمليات السحب    </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="form-group pull-right">
                <select id="status" class="select2 col-md-8" data-placeholder="التصنيف حسب الحالة">
                    <option></option>
                    <option value="">الكل</option>
                    <option value="pending">طلبات قيد الإنتظار</option>
                    <option value="accepted">طلبات مكتملة</option>
                    <option value="rejected">طلبات مرفوضة</option>
                </select>
            </div>
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th class=" text-cario" width="5%">#</th>
                        <th class=" text-cario" width="20%%">إسم المستخدم</th>
                        <th class=" text-cario" width="20%">البريد الإلكتروني</th>
                        <th class=" text-cario" width="10%">مبلغ السحب</th>
                        <th class=" text-cario" width="15%">حالة الطلب</th>
                        <th class=" text-cario" width="15%">تاريخ الطلب</th>
                        <th class=" text-cario" width="20%">خيارات</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        <script>
            var url = '{{lang_route('panel.payment.withdraw.data')}}';
        </script>
        {!! HTML::script('panel/js/withdraw-requests.js') !!}
    @endpush
@stop