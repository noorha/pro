@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> المحادثات </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th style="width: 5%;font-family: Cairo">#</th>
                        <th style="width: 15%;font-family: Cairo">إسم المدرب</th>
                        <th style="width: 15%;font-family: Cairo">إسم الطالب</th>
                        <th style="width: 10%;font-family: Cairo">وقت فتح المحادثة</th>
                        <th style="width: 10%;font-family: Cairo">العمليات</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
        <script>
            var config = {
                apiKey: "AIzaSyDLPEV3tQGZ8A7LGigAUwN3J0fMzKqrBp0",
                authDomain: "newhalapro.firebaseapp.com",
                databaseURL: "https://newhalapro.firebaseio.com",
                projectId: "newhalapro",
                storageBucket: "newhalapro.appspot.com",
                messagingSenderId: "54458046918",
            };
            firebase.initializeApp(config);
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                firebase.database().ref('messagingChats').orderByChild('createdAt').once('value', (snapshot) => {
                    return snapshot;
                }).then(function (snapshot) {
                    console.log(snapshot.val());
                });

            });
        </script>
    @endpush
@stop