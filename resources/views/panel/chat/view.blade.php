@extends('panel.layout.index')
@section('main')
    @push('panel_css')

        <style>
            .activity-list .act-thumb {
                width: 60px;
            }
        </style>
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> عرض المحادثة </span></h2>
    </div>
    <div class="contentpanel panel-email">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{--<div class="col-md-8 col-md-offset-2">--}}
{{--                            @if(isset($messages) && $messages->count() >0)--}}
                                <div class="activity-list" id="message_container">
{{--                                    @foreach($messages as $message)--}}

                                    {{--@endforeach--}}
{{--                                    {{$messages->links()}}--}}
                                </div>
                                {{--@else--}}
                                <button class="btn btn-primary text-center" id="load_more">المزيد</button>
                            {{--@endif--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('panel_js')
        <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
        <script>
            var config = {
                apiKey: "AIzaSyDLPEV3tQGZ8A7LGigAUwN3J0fMzKqrBp0",
                authDomain: "newhalapro.firebaseapp.com",
                databaseURL: "https://newhalapro.firebaseio.com",
                projectId: "newhalapro",
                storageBucket: "newhalapro.appspot.com",
                messagingSenderId: "54458046918",
            };
            firebase.initializeApp(config);
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                var teacherID = {{ $teacher->id }};
                var studentID = {{ $student->id }};
                var last_message = null;
                var limit = 10;
                function addTeacherMessage($message, $time) {
                    var _html = `<div class="media act-media">
                        <a class="pull-left" href="#">
                            <img class="media-object act-thumb" style="border-radius: 50px" src="{{ $teacher->photoUrl }}" alt="">
                        </a>
                        <div class="media-body act-media-body">
                            <strong> <a target="_blank" href="{{lang_route('profile',[$teacher->id])}}"> {{$teacher->getUserName()}} </a></strong> <br>
                            <small class="text-muted" style="direction: ltr">`+$time+`</small>
                            <h4 class="media-title"> (مدرب)</h4>
                            <p>`+$message+`</p>
                        </div>
                    </div>`;
                   $("#message_container").prepend(_html);
                }
                function addStudentMessage($message, $time) {
                    var _html = `<div class="media act-media">
                        <a class="pull-left" href="#">
                            <img class="media-object act-thumb" style="border-radius: 50px" src="{{ $student->photoUrl }}" alt="">
                        </a>
                        <div class="media-body act-media-body">
                            <strong> <a target="_blank" href="{{lang_route('profile',[$student->id])}}"> {{$student->getUserName()}} </a></strong> <br>
                            <small class="text-muted" style="direction: ltr">`+$time+`</small>
                            <h4 class="media-title"> (طالب)</h4>
                            <p>`+$message+`</p>
                        </div>
                    </div>`;
                   $("#message_container").prepend(_html);
                }

                function loadMessage($last) {
                    firebase.database().ref('messages/'+'{{ $id }}').limitToLast(limit).once('value', (snapshot) => {
                        return snapshot;
                    }).then(function (snapshot) {
                        console.log(snapshot);
                        var arr = snapshot.val();

                        $('#load_more').prop('disabled', false);
                        if (arr) {
                            $("#message_container").html('');
                            var _length = 0;
                            $.each(arr, function($index, $val){
                                _length++;
                                if ($val.senderID == teacherID) {
                                    var date = new Date($val.createdAt);
                                    var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                    addTeacherMessage($val.content, str);
                                }else{
                                    var date = new Date($val.createdAt);
                                    var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                    addStudentMessage($val.content, str);
                                }
                                last_message = $index;
                            });
                            if (_length < limit) {
                                $('#load_more').prop('disabled', true);
                            }
                        }else{

                            $('#load_more').prop('disabled', true);
                        }
                        // addTeacherMessage($message, $time);
                    });
                }
                loadMessage(last_message);
                $('#load_more').click(function (e) {
                    $('#load_more').prop('disabled', true);
                    limit += 10;
                    loadMessage(last_message);
                });
            });
        </script>
    @endpush
@stop