@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}

    @endpush
    <div class="pageheader" xmlns="http://www.w3.org/1999/html">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>  {{( $type == 'student') ? ' الطلاب' : 'المدرسين'}}
                / عرض   </span>
        </h2>
    </div>
    <div class="contentpanel">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="padding-bottom: 56px;padding-top: 56px" class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">الإسم </label>
                            <div class="col-sm-8">
                                <input type="text" disabled value="{{$user->name}}" class="form-control" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">البريد الإلكتروني </label>
                            <div class="col-sm-8">
                                <input type="email" name="email" disabled value="{{$user->email}}" class="form-control"
                                       required/>
                            </div>
                        </div>

                        @php
                            $countries = get_all_countries();
                        @endphp


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">العنوان </label>
                            <div class="col-sm-8">
                                <textarea name="mobile" disabled class="form-control" rows="3"
                                          required/>{{$user->getFullLocation()}}</textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">الموبايل </label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" disabled class="form-control"
                                       value="{{$user->getMobileNo()}}" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">سعر ساعة الأونلاين </label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" disabled class="form-control"
                                       value="{{$user->online_price}}" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">سعر ساعة المقابلة </label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" disabled class="form-control"
                                       value="{{$user->interview_price}}" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">عن المدرب </label>
                            <div class="col-sm-8">
                                <textarea name="mobile" disabled class="form-control" rows="5"
                                          required/>{{$user->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">التقييم </label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" disabled class="form-control" value="{{$user->rating}}"
                                       required/>
                            </div>
                        </div>

                        @php
                            $specialties = $user->specialties->unique();
                        @endphp
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">التخصصات </label>
                            <div class="col-sm-8">
                                <div class="media">
                                    @if(isset($specialties) && $specialties->count() >0)
                                        @foreach($specialties as $specialty)

                                            <div class="media-body">
                                                <h4 class="filename text-primary">{{$specialty->name}}</h4>
                                                @foreach($specialty->subSpecialties as $subSpecialty)
                                                    @if($user->hasSubSpeciality($subSpecialty->id))
                                                        <small class="text-muted">{{$subSpecialty->name}}</small>
                                                        <br>
                                                    @endif
                                                @endforeach
                                            </div>

                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-4">
                            <div class="monyf">
                                <div class="text_monyf">
                                    <p>الرصيد المعلق</p>
                                    <h4>{{($user->pending_cash)}}<span>$</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="monyf">

                                <div class="text_monyf">
                                    <p> المبلغ المستحق</p>
                                    <h4>{{$user->totalNonPaidProfit() > 0 ? $user->totalNonPaidProfit() : 0}}<span>$</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="monuy2">
                                <div class="monyf hvlsw">
                                    <div class="text_monyf">
                                        <p>المبلغ القابل للسحب</p>
                                        <h4>{{($user->walletAvailableCash())}}<span>$</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 jasny-padding">

                            <form id="single">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 200px;max-width:200px; max-height:200px">
                                        <img src="{{image_url($user->photo,'300x300')}}"
                                             data-src="holder.js/100%x100%"
                                             alt="">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/user_form.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        <script>
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
        </script>
    @endpush
@stop