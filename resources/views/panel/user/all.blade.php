@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush
    @php
        $text_title = ( $type == 'student') ? ' الطلاب' : 'المدرسين';
    @endphp
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>  {{$text_title}} /  عرض الكل  </span>
        </h2>
    </div>
    <div class="contentpanel">
        <div class="row">

            {!! Form::open(['id'=>'search_form','method'=>'GET','url'=> url()->current() ])!!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">فلترة</h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-3">
                        <label class="control-label">بحث بالإسم</label>
                        <input type="text" name="text" placeholder="أكتب نص البحث" class="form-control  on-keyup">
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label"> حسب الجنس </label>
                        <select class="form-control on-change" name="gender" data-placement="التصنيف حسب الجنس">
                            <option selected hidden disabled>التصنيف حسب الجنس</option>
                            <option value="">الكل</option>
                            <option value="1">ذكر</option>
                            <option value="2">أنثى</option>
                        </select>
                    </div>
                    @php
                        $countries = get_all_countries();
                    @endphp
                    <div class="col-sm-3">
                        <label class="control-label"> حسب الدولة </label>
                        <select class="form-control on-change" name="country_id" data-placement="التصنيف حسب الدولة">
                            <option selected hidden disabled>التصنيف حسب الدولة</option>
                            <option value="">الكل</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->text}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="col-sm-3">--}}
                    {{--<label class="control-label"> حسب الجهة المعلنة</label>--}}
                    {{--<select class="form-control on-change" name="advertiser" data-placement="التصنيف حسب الجهة المعلنة">--}}
                    {{--<option selected hidden disabled>التصنيف حسب الجهة المعلنة</option>--}}
                    {{--<option value="">الكل</option>--}}
                    {{--@foreach($advertisers as $advertiser)--}}
                    {{--<option value="{{$advertiser->id}}">{{string_limit($advertiser->title,25)}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-3" style="padding-top:5px ">--}}
                    {{--<label class="control-label"> حسب تاريخ الإضافة</label>--}}
                    {{--<input type="text" class="form-control datepicker on-change" name="created_at"--}}
                    {{--placeholder="mm/dd/yyyy">--}}
                    {{--</div>--}}

                    <div class="col-md-3" style="padding-top:20px ">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> بحث
                        </button>
                    </div>
                    <div class="col-md-12">
                        <h5> عدد {{$text_title}} : <span id="count_number"> 0 </span></h5>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}


            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr class="text-center">
                        <th class="text-center text-cario" width="5%">#</th>
                        <th class="text-center text-cario" width="10%">إسم المستخدم</th>
                        <th class="text-center text-cario" width="10%">البريد الإلكتروني</th>
                        <th class="text-center text-cario" width="10%">حالة الحساب</th>
                        <th class="text-center text-cario" width="10%">تاريخ التسجيل</th>
                        <th class="text-center text-cario" width="20%">خيارات</th>
                    </tr>
                    </thead>
                    <tbody class="text-center"></tbody>
                </table>
            </div>
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        <script>
            var tbl, info, count;
            var url = '{{lang_route('panel.'.$type.'.all.data')}}';
            jQuery(document).ready(function () {
                tbl = $('#table1').DataTable({
                    "columnDefs": [
                        {"orderable": false, targets: '_all'}
                    ],
                    "rowCallback": function (nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                        return nRow;
                    },
                    "fnDrawCallback": function (oSettings) {
                        info = tbl.page.info();
                        count = info.recordsTotal;
                        $('#count_number').text(count);
                    },
                    "bSort": false,
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "info": false,
                    "direction": 'ltr',
                    "ajax": {
                        "url": url
                    },
                    "columns": [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'status', name: 'status'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action'}
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            text: '',
                            className: 'hidden'
                        }
                    ],
                    "bLengthChange": true,
                    "bFilter": true,
                    "pageLength": 10,
                    language: {
                        "sSearch": " ",
                        "searchPlaceholder": "إبحث ",
                        "sProcessing": " جارٍ التحميل ... ",
                        "sLengthMenu": "أظهر _MENU_ مدخلات",
                        "sZeroRecords": "لم يعثر على أية سجلات",
                        "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                        "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                        "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                        "sInfoPostFix": "",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "الأول",
                            "sPrevious": "السابق",
                            "sNext": "التالي",
                            "sLast": "الأخير"
                        }
                    }
                });

                var form = $(document).find('#search_form');
                $(document).on('keyup', '.on-keyup', function (e) {
                    form.submit();
                });
                $(document).on('change', '.on-change', function (e) {
                    form.submit();
                });
                $(document).on('submit', '#search_form', function (e) {

                    e.preventDefault();
                    tbl.ajax.url(url + '?' + form.serialize()).load();
                });

                $(document).on('click', '.activate-account', function (event) {
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        method: 'PATCH',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                tbl.ajax.reload();
                            }
                        },
                        error: function (response) {
                            errorCustomSweet();
                        }
                    });

                });
                $(document).on('click', '.delete', function (event) {
                    var delete_url = $(this).data('url');
                    event.preventDefault();
                    swal({
                        title: '<span class="info">  هل أنت متأكد من الحذف  ؟</span>',
                        type: 'info',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'موافق',
                        cancelButtonText: 'إغلاق',
                        confirmButtonColor: '#56ace0',
                        width: '500px'
                    }).then(function (value) {
                        $.ajax({
                            url: delete_url,
                            method: 'delete',
                            type: 'json',
                            success: function (response) {
                                if (response.status) {
                                    customSweetAlert(
                                        'success',
                                        response.message,
                                        response.item,
                                        function (event) {
                                            tbl.ajax.reload();
                                        }
                                    );
                                } else {
                                    customSweetAlert(
                                        'error',
                                        response.message,
                                        response.errors_object
                                    );
                                }
                            },
                            error: function (response) {
                                $('.upload-spinn').addClass('hidden');
                                errorCustomSweet();
                            }
                        });
                    });
                });

                $(document).on('click', '.status', function (event) {
                    var status_url = $(this).data('url');
                    event.preventDefault();
                    $.ajax({
                        url: status_url,
                        method: 'GET',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                tbl.ajax.reload();
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });

                });
                $(document).on('click', '.type', function (event) {
                    var url = $(this).data('url');
                    event.preventDefault();
                    $.ajax({
                        url: url,
                        method: 'GET',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                tbl.ajax.reload();
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });

                });
            });
        </script>
    @endpush
@stop