@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> طلبات إنضمام مدربين /  عرض الكل    </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="form-group pull-right">
                <select id="status" class="select2 col-md-8" data-placeholder="التصنيف حسب الحالة">
                    <option></option>
                    <option value="">الكل</option>
                    <option value="pending">طلبات قيد الإنتظار</option>
                    <option value="accepted">طلبات مكتملة</option>
                    <option value="rejected">طلبات مرفوضة</option>
                </select>
            </div>
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th style="width: 5%;font-family: Cairo">#</th>
                        <th style="width: 15%;font-family: Cairo">مقدّم الطلب</th>
                        <th style="width: 15%;font-family: Cairo">حالة الطلب</th>
                        <th style="width: 15%;font-family: Cairo">التخصص</th>
                        <th style="width: 20%;font-family: Cairo">عن مقدّم الطلب</th>
                        <th style="width: 10%;font-family: Cairo">تاريخ الطلب</th>
                        <th width="30%" style="font-family: Cairo">خيارات</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        {!! HTML::script('withdraw-withdraw-requests.js') !!}
    @endpush
@stop