@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية
            <span> طلبات إنضمام مدربين /  عرض تفاصيل طلب الإنضمام كمدرب    </span></h2>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default panel-alt widget-messaging">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <h4 class="sender trainer-row">حالة الطلب</h4>
                                <small style="font-size: 17px">
                                    @switch($request->status)
                                        @case('pending')
                                        قيد الإنتظار
                                        @break
                                        @case('accepted')
                                        مقبول
                                        @break
                                        @case('rejected')
                                        مرفوض
                                        @break
                                    @endswitch
                                </small>
                            </li>
                            <li>
                                <h4 class="sender trainer-row">مقدّم الطلب</h4>
                                <small><a href="#" style="font-size: 17px">{{$request->getUserName()}}</a></small>
                            </li>
                            <li>
                                <h4 class="sender trainer-row">التخصص</h4>
                                <small style="font-size: 17px">{{$request->getCategory()}}</small>
                            </li>
                            <li>
                                <h4 class="sender trainer-row">نبذة عن مقدّم الطلب</h4>
                                <small style="font-size: 17px">{{$request->about}}</small>
                            </li>
                            <li>
                                <h4 class="sender trainer-row">أسباب الإنضمام كمدرب في منصة دراية</h4>
                                <small style="font-size: 17px"><p>{!! nl2br($request->description) !!}</p></small>
                            </li>
                        </ul>
                    </div><!-- panel-body -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row" style="padding: 40px 25px 40px 25px">
                            @if($request->status == 'pending')
                                <button title="قبول" style="margin-bottom: 20px;background-color: #02a502;border-color: #038803;" href="{{admin_url('trainer/accept/'.$request->id)}}"
                                        class="btn btn-sm btn-success col-md-12 accept"><i style="margin-left: 5px"
                                                                                           class="fa fa-check-square-o"></i>قبول
                                </button>
                                <button title="رفض" style="background-color: #FA2A00"
                                        href="{{admin_url('trainer/reject/'.$request->id)}}"
                                        class="btn btn-sm btn-danger  col-md-12 reject"><i
                                            class="glyphicon glyphicon-remove"></i> رفض
                                </button>
                            @else
                                <button title="حذف" style="margin-bottom: 10px;background-color: #FA2A00"
                                        data-url="{{ admin_url('trainer/delete/'.$request->id)}}"
                                        class="btn btn-sm btn-danger col-md-12 delete"><i
                                            class="glyphicon glyphicon-trash"></i> حذف
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('panel_js')
        <script>
            $(document).on('click', '.accept', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من قبول طلب الإنضمام ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    var formData = new FormData();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.reload();
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
            $(document).on('click', '.reject', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من رفض   طلب الإنضمام ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    var formData = new FormData();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.reload();
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
            $(document).on('click', '.delete', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من حذف طلب الإنضمام ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    $.ajax({
                        url: url,
                        method: 'delete',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.reload();
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
        </script>
    @endpush
@stop