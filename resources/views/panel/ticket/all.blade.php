@extends('panel.layout.index')
@section('main')
    @push('panel_css')

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> التذاكر /  عرض الكل </span></h2>
    </div>
    <div class="contentpanel panel-email">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                                <button class="btn btn-sm btn-white tooltips end" type="button" data-toggle="tooltip"
                                     data-url="{{admin_url('ticket/end')}}"   title="إنهاء">
                                    <i class="fa fa-check-circle"></i>
                                </button>
                                <button class="btn btn-sm btn-white tooltips delete" type="button"
                                        data-url="{{admin_url('ticket/delete')}}"    data-toggle="tooltip"
                                        title="حذف"><i class="glyphicon glyphicon-trash"></i></button>
                            </div>
                        </div><!-- pull-right -->

                        <h5 class="subtitle mb5"></h5>
                        <p class="text-muted">عرض التذاكر </p>
                        {!! Form::open(['id'=>'form','method'=>'post','url'=>admin_url('ticket/delete'),'to'=> admin_url('ticket/all')]) !!}
                        <div class="table-responsive">
                            <table class="table table-email">
                                <tbody>
                                @foreach($tickets as $i=>$ticket)
                                    <tr @if(isset($ticket->read_at)) class="unread" @endif>
                                        <td>
                                            <div class="ckbox ckbox-success">
                                                <input type="checkbox" name="tickets[{{$i}}]" value="{{$ticket->id}}"
                                                       id="checkbox{{$i}}">
                                                <label for="checkbox{{$i}}"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="star"><i class="glyphicon glyphicon-star"></i></a>
                                        </td>
                                        <td>
                                            <div class="media" data-url="{{admin_url('ticket/view/'.$ticket->id)}}">
                                                <a href="#" class="pull-left">
                                                    <img alt="" src="{{$ticket->userAvatar('100x100')}}" style="margin-top: 50px;border-radius: 25px" class="media-object">
                                                </a>
                                                <div class="media-body">
                                                    <span class="media-meta pull-right">{{' تاريخ الإضافة : '.get_date_from_timestamp($ticket->created_at)}} </span>
                                                    <br>
                                                    <span class="media-meta pull-right">{{'آخر تحديث  : '.diff_for_humans($ticket->updated_at)}}  </span>
                                                    <br>

                                                    @if(!$ticket->canReplay())
                                                        <p class="media-meta span-replay pull-right">محلولة</p>
                                                    @endif
                                                    <h4 class="text-primary">{{$ticket->userName()}}</h4>
                                                    <small class="text-muted">{{$ticket->subject}}</small>
                                                    {{--<p class="email-summary" style="color: #d17d7d">{{$ticket->email}}</p>--}}
                                                    <p class="email-summary">{!! string_limit($ticket->text,200) !!}</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-offset-5">
                                {{ $tickets->links() }}
                            </div>

                        </div><!-- table-responsive -->
                        {!! Form::close() !!}

                    </div><!-- panel-body -->
                </div><!-- panel -->

            </div><!-- col-sm-9 -->

        </div><!-- row -->
    </div>


    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('/panel/js/simple_form.js') !!}

        <script>
            jQuery(document).ready(function () {

                var form = $('#form');
                "use strict";

                jQuery('.delete').click(function () {
                    var _this = $(this);
                    swal({
                        html: '<span class="info">  هل أنت متأكد من حذف التذاكر المحددة  ؟</span>',
                        type: 'info',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'موافق',
                        cancelButtonText: 'إغلاق',
                        confirmButtonColor: '#56ace0',
                        width: '500px'
                    }).then(function (value) {
                        form.attr('action',_this.data('url'));
                        form.submit();
                    });
                });
                jQuery('.end').click(function () {
                    var _this = $(this);
                    swal({
                        html: '<span class="info">  هل أنت متأكد من إنهاء التذاكر المحددة  ؟</span>',
                        type: 'info',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'موافق',
                        cancelButtonText: 'إغلاق',
                        confirmButtonColor: '#56ace0',
                        width: '500px'
                    }).then(function (value) {
                        form.attr('action',_this.data('url'));
                        form.submit();
                    });
                });

                jQuery('.ckbox input').click(function () {
                    var t = jQuery(this);
                    if (t.is(':checked')) {
                        t.closest('tr').addClass('selected');
                    } else {
                        t.closest('tr').removeClass('selected');
                    }
                });

                // Star
                jQuery('.star').click(function () {
                    if (!jQuery(this).hasClass('star-checked')) {
                        jQuery(this).addClass('star-checked');
                    }
                    else
                        jQuery(this).removeClass('star-checked');
                    return false;
                });

                jQuery('.table-email .media').click(function () {
                    location.href = $(this).data('url');
                });

            });
        </script>
    @endpush
@stop