@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        <style>
            .replay-container {
                margin-bottom: 20px;
                background-color: #f5f5f5;
                padding: 20px;
                border-radius: 30px;
            }
        </style>

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> التذاكر /  عرض التذكرة </span></h2>
    </div>
    <div class="contentpanel panel-email">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                                <button class="btn btn-sm btn-white tooltips end" type="button" data-toggle="tooltip"
                                        data-url="{{admin_url('ticket/end/'.$ticket->id)}}" title="إنهاء">
                                    <i class="fa fa-check-circle"></i>
                                </button>
                                <button class="btn btn-sm btn-white tooltips delete" type="button" data-toggle="tooltip"
                                        data-url="{{admin_url('ticket/delete/'.$ticket->id)}}" title="حذف"><i
                                            class="glyphicon glyphicon-trash"></i></button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 25px;">
                            <div class="read-panel">
                                <div class="media">
                                    <a href="#" class="pull-left">
                                        <img alt="" src="{{$ticket->userAvatar('150x150')}}"
                                             style="margin-top: 50px;border-radius: 25px" class="media-object">
                                    </a>
                                    <div class="media-body">
                                        <span class="media-meta pull-right">{{' تاريخ الإضافة : '.get_date_from_timestamp($ticket->created_at)}} </span>
                                        <br>
                                        <span class="media-meta pull-right">{{'آخر تحديث  : '.diff_for_humans($ticket->updated_at)}}  </span>
                                        <br>
                                        <h4 class="text-primary">{{$ticket->userName()}}</h4>
                                        <p class="text-muted">{{$ticket->subject}}</p>
                                    </div>
                                </div>
                                <p>{!! nl2br($ticket->text) !!}</p>

                                @if(isset($ticket->file)&& !empty($ticket->file))
                                    <div class="col-md-12">
                                        <a class="text-center" href="{{file_url($ticket->file)}}">
                                            تحميل الملف المرفق
                                        </a>
                                    </div>
                                @endif
                                <div id="replays" style="padding: 20px 60px;">
                                    @foreach($ticket->replays as $replay)
                                        <div class="replay-container">
                                            <div class="media">
                                                <a href="#" class="pull-left">
                                                    <img alt="" src="{{$replay->avatar('150x150')}}"
                                                         style="border-radius: 25px" class="media-object">
                                                </a>
                                                <h4 class="text-primary">{{$replay->name() . '  ' . ($replay->hasAdmin() ? '( الإدارة )':'')}}</h4>
                                                <div class="media-body">
                                                    <span class="media-meta pull-right">{{diff_for_humans($replay->created_at)}}</span>
                                                </div>
                                            </div>
                                            <p>{!! $replay->text !!}</p>
                                        </div>

                                    @endforeach
                                </div>

                                @if($ticket->canReplay())
                                    {!! Form::open(['id'=>'form','method'=>'post','url'=>admin_url('ticket/replay/'.$ticket->id),'to'=> url()->current()]) !!}
                                    <div style="margin-top: 50px" class="media">
                                        <a href="#" class="pull-left">
                                            <img alt="" src="{{image_url(auth()->user('admin')->photo,'150x150')}}"
                                                 style="margin-top: 50px;border-radius: 25px" class="media-object">
                                        </a>
                                        <div class="media-body">
                                            <div class="form-group">
                                                <div>
                                                    <textarea class="form-control " name="text" rows="5"
                                                              placeholder=" أكتب رد هنا " required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right" style="margin-top: 10px">
                                            <button class="btn btn-info">
                                                &nbsp; <i style="top: inherit;left: AUTO;"
                                                          class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                                                إرسال
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('panel_js')
        <script>
            $('.delete').on('click', function (event) {
                var delete_url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من حذف  الرسالة ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    $.ajax({
                        url: delete_url,
                        method: 'delete',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.href = '/admin/inbox/all';
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
            $('.end').on('click', function (event) {
                var delete_url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من إنهاء التذكرة ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    $.ajax({
                        url: delete_url,
                        method: 'POST',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.href = '/admin/ticket/all';
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
        </script>
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('/panel/js/simple_form.js') !!}

    @endpush
@stop