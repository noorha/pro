@extends('panel.layout.index',['sub_title'=>'تعديل دورة'])
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}

    @endpush
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> الدورات  /  تعديل   </span></h2>
    </div>

    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','method'=>'PUT','url'=>lang_route('panel.course.edit',[$course->id]),'to'=>lang_route('panel.course.all')]) !!}
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="padding-bottom: 56px;padding-top: 56px" class="panel-body">
                        @php
                            $countries = get_all_countries();
                        @endphp
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> الدولة : </label>
                            <div class="col-sm-8">
                                <select class="select2" name="country_id" data-placeholder="الرجاء تحديد دولة"
                                        required>
                                    <option></option>
                                    @if(isset($countries) && $countries->count() > 0)
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}"
                                                    @if($course->country_id == $country->id)  selected @endif>{{$country->name .'  ('.$country->name_en.')'}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        @php
                            $items = get_all_course_categories();
                        @endphp
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> التخصص :</label>
                            <div class="col-sm-8">
                                <select class="select2" name="category_id" data-placeholder="الرجاء تحديد تخصص"
                                        required>
                                    <option></option>
                                    @if(isset($items) && $items->count() > 0)
                                        @foreach($items as $item)
                                            <option value="{{$item->id}}"
                                                    @if($course->category_id == $item->id )  selected @endif>{{$item->text .'  ('.$item->text_en.')'}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center">عنوان الدورة : </label>
                            <div class="col-sm-8">
                                <input type="text" name="title" class="form-control" value="{{$course->title}}"
                                       required/>
                            </div>
                        </div>
                        <input type="hidden" id="photo" name="photo" value="{{$course->photo}}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> وصف الدورة :</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows="15" name="description" required>{!! $course->description !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> سعر الدورة </label>
                            <div class="col-sm-8">
                                <input type="number" name="price" value="{{$course->price}}" class="form-control"
                                       required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: center"> إسم المركز </label>
                            <div class="col-sm-8">
                                <input type="text" name="center_name"  value="{{$course->center_name}}"  class="form-control" required/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label  for="datepicker" class="col-sm-3 control-label" style="text-align: center"> تاريخ بداية
                                الدورة </label>
                            <div class="col-sm-8">
                                <div class="input-group mb15">
                                    <input  id="datepicker" type='text'  value="{{$course->start_date}}"  class="form-control datepicker" name="start_date" required/>
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary">تعديل&nbsp; &nbsp; <i
                                        style="top: inherit;left: AUTO;"
                                        class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    {{--<div class="panel-heading">--}}
                    {{--<h4 class="panel-title">تحميل شعار الموقع</h4>--}}
                    {{--</div>--}}
                    <div class="panel-body">
                        <div class="col-sm-12 jasny-padding">
                            <form id="single" action="{{csrf_token()}}">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="sr-only">Loading...</span>
                                    <div id="jasny_progress" class="progress hidden">
                                        <div id="jasny_percent" class="progress-bar progress-bar-striped active"
                                             role="progressbar"
                                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                             style="width:40%;background-color: #11a724">0%
                                        </div>
                                    </div>
                                    <div class="fileinput-new thumbnail"
                                         style="width: 200px; height: 200px">
                                        <img src="{{ image_url($course->photo)}}"
                                             data-src="holder.js/100%x100%"
                                             alt="">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 300px; max-height: 300px;"></div>
                                    <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>
                                                    <span class="fileinput-exists">تغيير الصورة</span>
                                                    <input type="file" class="fileupload">
                                                </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @push('panel_js')
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/user_form.js') !!}
        <script>
            jQuery(".select2").select2({
                width: '100%',
                direction: 'ltr'
            });
        </script>
    @endpush
@stop
