@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jquery.datatables.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> الإبلاغات </span></h2>
    </div>


    <div class="contentpanel">

        <div class="row">
            <div class="table-responsive">
                <table class="table" id="table1">
                    <thead>
                    <tr>
                        <th style="width: 5%;font-family: Cairo">#</th>
                        <th style="width: 10%;font-family: Cairo">صاحب الشكوى</th>
                        <th style="width: 10%;font-family: Cairo">إسم المدرب</th>
                        <th style="width: 10%;font-family: Cairo">إسم الطالب</th>
                        <th style="width: 15%;font-family: Cairo">نص الإبلاغ</th>
                        <th style="width: 10%;font-family: Cairo">وقت الإبلاغ</th>
                        <th style="width: 20%;font-family: Cairo">خيارات</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @push('panel_js')
        {!! HTML::script('panel/js/select2.min.js') !!}
        {!! HTML::script('panel/js/jquery.datatables.min.js') !!}
        <script>
            var url = '{{lang_route('panel.report.data')}}';
            tbl = $('#table1').DataTable({
                "columnDefs": [
                    {"orderable": false, targets: '_all'}
                ],
                "bSort": false,
                "processing": true,
                "serverSide": true,
                "info": false,
                "ajax": {
                    "url": url
                },
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'user_id', name: 'user_id'},
                    {data: 'teacher', name: 'teacher'},
                    {data: 'student', name: 'student'},
                    {data: 'note', name: 'note'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action'}
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'hidden'
                    }
                ],
                "bLengthChange": true,
                "bFilter": true,
                "pageLength": 10
                , language: {
                    "sSearch": " ",
                    "searchPlaceholder": "إبحث ",
                    "sProcessing": " جارٍ التحميل ... ",
                    "sLengthMenu": "أظهر _MENU_ مدخلات",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
            });
            $(document).on('click', '.delete', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من الحذف ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    $.ajax({
                        url: url,
                        method: 'delete',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        tbl.ajax.reload();
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
            $(document).on('click', '.accept', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info"> هل أنت متأكد من إلغاء طلب التدريب  ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    var formData = new FormData();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        tbl.ajax.reload();
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
            $(document).on('click', '.reject', function (event) {
                var url = $(this).data('url');
                event.preventDefault();
                sweetAlertReject(url) ;
            });
            function sweetAlertReject(url) {
                (async function getText () {
                    const {value: text} = await swal({
                        input: 'textarea',
                        inputPlaceholder: 'أكتب سبب الرفض هنا',
                        showCancelButton: true,
                        confirmButtonText: 'رفض',
                        cancelButtonText: 'إلغاء'
                    }).then(function (text) {
                        if(text !== undefined && text !== ''){
                            var formData = new FormData();
                            formData.append('comment',text);
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: formData,
                                dataType: 'json',
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    swal({
                                        title: '',
                                        text:  response.message ,
                                        type: 'success',
                                        confirmButtonText: 'موافق'
                                    }).then(function () {
                                        location.reload();
                                    });
                                },
                                error: function (response) {
                                    swal({
                                        title:' فشلت عملية الرفض',
                                        text: 'ربما لا تملك صلاحيات للقيام بهذه العملية',
                                        type: 'error',
                                        confirmButtonText: 'موافق'
                                    })
                                }
                            });
                        }else {
                            swal({
                                title:'',
                                text: 'الرجاء إدخال سبب الرفض',
                                type: 'warning',
                                confirmButtonText: 'موافق',
                            }).then(function () {
                                sweetAlertReject(url,user_id) ;
                            });
                        }
                    });
                })();
            }
        </script>
    @endpush
@stop