@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}
        {!! HTML::style('panel/css/jquery.tagsinput.css') !!}
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية
            <span>  إعدادات الموقع / إعدادات القوالب / إعدادات الموقع  </span></h2>
    </div>


    @php
        $constant = new \App\Constant();
    @endphp


    <div class="contentpanel">
        <div class="row">
            {!! Form::open(['id'=>'form','method' =>'PUT','url'=> lang_route('panel.template.settings') , 'to'=> lang_route('panel.template.settings')]) !!}
            <input type="hidden" id="photo" name="logo" value="{{$constant->valueOf('logo')}}">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#home3" data-toggle="tab"><strong>اللغة العربية</strong></a>
                            </li>
                            <li><a href="#profile3" data-toggle="tab"><strong>اللغة الإنجليزية</strong></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="home3">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">عنوان الموقع
                                            :</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="title"
                                                   value="{{$constant->valueOf('title')}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">وصف الموقع
                                            :</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" rows="6" name="description"
                                                      required>{!! $constant->valueOf('description') !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">كلمات دلالية
                                            :</label>
                                        <div class="col-sm-8">
                                            <input name="tags" id="tags" class="form-control" required
                                                   value="{{$constant->valueOf('tags')}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">نص حقوق الملكية
                                            :</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="copyright" class="form-control"
                                                   value="{{$constant->valueOf('copyright')}}" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile3">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center"> عنوان الموقع
                                            :</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="title_en"
                                                   value="{{$constant->valueOf('title_en')}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">وصف الموقع
                                            :</label>
                                        <div class="col-sm-8">
                        <textarea class="form-control" rows="6" name="description_en"
                                  required>{!! $constant->valueOf('description_en') !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center"> كلمات دلالية
                                            :</label>
                                        <div class="col-sm-8">
                                            <input name="tags_en" id="tags_en" class="form-control" required
                                                   value="{{$constant->valueOf('tags_en')}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-align: center">نص حقوق الملكية
                                            :</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="copyright_en" class="form-control"
                                                   value="{{$constant->valueOf('copyright_en')}}" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top:30px">
                                <label class="col-sm-3 control-label" style="text-align: center"> رابط جوجل بلاي
                                    :</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="google_play"
                                           value="{{$constant->valueOf('google_play')}}" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="text-align: center"> رابط أبل ستور
                                    :</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="apple_store"
                                           value="{{$constant->valueOf('apple_store')}}" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row btn-padding">
                            <button style="width: 80%" class=" btn btn-primary">تعديل&nbsp; &nbsp; <i
                                        style="top: inherit;left: AUTO;"
                                        class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                        </div>
                    </div>
                </div>
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h4 class="panel-title">تحميل شعار الموقع</h4>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="col-sm-12 jasny-padding">--}}
                            {{--<form id="single" action="{{csrf_token()}}">--}}
                                {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                                    {{--<span class="sr-only">Loading...</span>--}}
                                    {{--<div id="jasny_progress" class="progress hidden">--}}
                                        {{--<div id="jasny_percent" class="progress-bar progress-bar-striped active"--}}
                                             {{--role="progressbar"--}}
                                             {{--aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"--}}
                                             {{--style="width:40%;background-color: #11a724">0%--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="fileinput-new thumbnail"--}}
                                         {{--style="width: 200px; height: 150px;max-width:200px; max-height:150px">--}}
                                        {{--<img src="{{ image_url($constant->valueOf('logo'),'500x120')}}"--}}
                                             {{--data-src="holder.js/100%x100%"--}}
                                             {{--alt="">--}}
                                    {{--</div>--}}
                                    {{--<div class="fileinput-preview fileinput-exists thumbnail"--}}
                                         {{--style="max-width: 200px; max-height: 200px;"></div>--}}
                                    {{--<div>--}}
                                                {{--<span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>--}}
                                                    {{--<span class="fileinput-exists">تغيير الصورة</span>--}}
                                                    {{--<input type="file" class="fileupload">--}}
                                                {{--</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            {!! Form::close() !!}

        </div>
    </div>



    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/ckeditor/ckeditor.js') !!}
        {!! HTML::script('panel/js/ckeditor/adapters/jquery.js') !!}
        {!! HTML::script('panel/js/ckeditor.js') !!}
        {!! HTML::script('panel/js/page_form.js') !!}
        {!! HTML::script('panel/js/jquery.tagsinput.min.js') !!}
        <script>
            jQuery('#tags').tagsInput({width: 'auto'});
            jQuery('#tags_en').tagsInput({width: 'auto'});
        </script>
    @endpush
@stop