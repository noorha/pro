@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/jasny-bootstrap.min.css') !!}

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span>  إعدادات الموقع / إعدادات القوالب / إعدادات الصفحة الرئيسية  </span>
        </h2>
    </div>


    <div class="contentpanel">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"> إعدادات الصفحة الرئيسية </h4>
                    </div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#profile3" data-toggle="tab"><strong>قسم السلايدر</strong></a>
                            </li>
                            <li><a href="#home3" data-toggle="tab"><strong>قسم الفيديو</strong></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile3">
                                <div style="margin-top: 60px" class="row">
                                    <button style="width: 13%;margin-top: -60px"
                                            class="btn btn-success pull-left section" data-toggle="modal"
                                            data-type="2" data-target=".bs-example-modal-photo"><i
                                                class="glyphicon glyphicon-plus"></i>&nbsp;إضافة جديد
                                    </button>
                                    @php
                                        $sliders = get_main(2);
                                    @endphp
                                    @foreach($sliders as $item)
                                        <div class="panel panel-info">
                                            <div class="panel-heading"
                                                 style="background-color: #eeeeee!important;border-radius: 5px;color: red!important;">
                                                <div class="panel-btns pull-right">
                                                    <button title="تعديل" style="margin-right: 10px" data-toggle="modal"
                                                            data-id="{{$item->id}}" data-type="2"
                                                            data-target=".bs-example-modal-photo"
                                                            class="btn btn-sm btn-success section"><i
                                                                style="margin-left: 5px"
                                                                class="fa fa-check-square-o"></i>تعديل
                                                    </button>
                                                    <button data-toggle="reject" title="حذف"
                                                            data-url="{{lang_route('panel.template.main.delete',[$item->id])}}"
                                                            style="margin-right: 10px;background-color: #FA2A00"
                                                            class="btn btn-sm btn-danger delete"><i
                                                                class="glyphicon glyphicon-remove"></i> حذف
                                                    </button>
                                                </div><!-- panel-btns -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <img style="max-height: 250px;max-width: 250px"
                                                             src="{{image_url($item->photo)}}">

                                                    </div>
                                                    <div class="col-md-4">
                                                        <h3 style="color: #179be8"
                                                            class="panel-title">{{$item->title}}</h3>
                                                        <p style="color: #2a2323!important;">{!! strip_tags($item->text) !!}</p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h3 style="color: #179be8;text-align: left"
                                                            class="panel-title">{{$item->title_en}}</h3>
                                                        <p style="color: #2a2323!important;text-align: left;">{!! strip_tags($item->text_en) !!}</p>
                                                    </div>
                                                </div>
                                                <div class="row" style="padding: 30px;">
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @php
                                $q = get_main(1);
                                $item = isset($q->first()->id) ? $q->first() : null;
                            @endphp
                            <div class="tab-pane " id="home3">
                                <div style="margin-top: 60px" class="row">
                                    {!! Form::open(['id'=>'form','url'=>lang_route('panel.template.main.edit',[$item->id]),'to'=>lang_route('panel.template.main')]) !!}
                                    @if(isset($item))
                                        <div class="col-md-8 col-md-offset-2">
                                            {{--<div class="form-group">--}}
                                                {{--<label class="control-label" style="text-align: center">رابط اليوتيوب :</label>--}}
                                                {{--<input type="text" name="link" placeholder="الرجاء إدخال الرابط "--}}
                                                       {{--class="form-control" value="{{$item->link}}" required/>--}}
                                            {{--</div>--}}
                                            <input type="hidden" name="type" value="1">
                                            <div class="form-group">
                                                <label class="control-label" style="text-align: center">عنوان :</label>
                                                <input type="text" name="title"
                                                       placeholder="الرجاء إدخال العنوان "
                                                       class="form-control" value="{{$item->title}}" required/>
                                            </div>
                                            <div class="form-group">
                                                <label class=" control-label" style="text-align: center">النص :</label>
                                                <textarea name="text" placeholder="أكتب نص هنا " rows="5"
                                                          class="form-control"
                                                          required>{{$item->text}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" style="text-align: center">عنوان (اللغة
                                                    الإنجليزية)
                                                    :</label>
                                                <input style="text-align: left" type="text" name="title_en"
                                                       placeholder="الرجاء إدخال العنوان باللغة الإنجليزية "
                                                       class="form-control" value="{{$item->title_en}}" required/>
                                            </div>
                                            <div class="form-group">
                                                <label class=" control-label" style="text-align: center">النص (اللغة
                                                    الإنجليزية)
                                                    :</label>
                                                <textarea style="text-align: left" name="text_en"
                                                          placeholder="أكتب نص هنا " rows="5" class="form-control"
                                                          required>{{$item->text_en}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-success pull-left "><i class="fa fa-edit"></i>&nbsp;
                                                    تعديل
                                                    <i style="top: inherit;left: AUTO;"
                                                       class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                                                </button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label class="control-label" style="text-align: center">الرابط :</label>
                                                <input type="text" name="link" placeholder="الرجاء إدخال الرابط "
                                                       class="form-control" required/>
                                            </div>
                                            <input type="hidden" name="type" value="1">
                                            <div class="form-group">
                                                <label class="control-label" style="text-align: center">عنوان :</label>
                                                <input type="text" name="title"
                                                       placeholder="الرجاء إدخال العنوان "
                                                       class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                <label class=" control-label" style="text-align: center">النص :</label>
                                                <textarea name="text" placeholder="أكتب نص هنا " rows="5"
                                                          class="form-control"
                                                          required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" style="text-align: center">عنوان (اللغة
                                                    الإنجليزية)
                                                    :</label>
                                                <input style="text-align: left" type="text" name="title_en"
                                                       placeholder="الرجاء إدخال العنوان باللغة الإنجليزية "
                                                       class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                <label class=" control-label" style="text-align: center">النص (اللغة
                                                    الإنجليزية)
                                                    :</label>
                                                <textarea style="text-align: left" name="text_en"
                                                          placeholder="أكتب نص هنا " rows="5" class="form-control"
                                                          required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-success pull-left "><i
                                                            class="fa fa-edit"></i>&nbsp;تعديل
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="template_modal" class="modal fade bs-example-modal-photo " tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-photo-viewer">
            {!! Form::open(['id'=>'template_form','url'=>admin_url('template/create')]) !!}
            <div style="height: auto" class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title"> تعديل ثوابت الصفحة الرئيسية </h4>
                </div>
                <div class="modal-body">
                    <div id="loader" class="col-md-offset-6 hidden">
                        <img src="/panel/images/loaders/loader10.gif" alt="">
                    </div>

                    <div id="modal_body" class="row">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="photo" id="photo">
                        <input type="hidden" name="type"  value="2">
                        <div class="col-md-9 col-md-offset-2">
                            <div class="form-group">
                                <label class="control-label" style="text-align: center">الرابط :</label>
                                <input type="text" id="link" name="link" placeholder="الرجاء إدخال الرابط "
                                       class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" style="text-align: center">عنوان :</label>
                                <input type="text" id="title" name="title" placeholder="الرجاء إدخال العنوان "
                                       class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class=" control-label" style="text-align: center">النص :</label>
                                <textarea id="text" name="text" placeholder="أكتب نص هنا " rows="5" class="form-control"
                                          required></textarea>
                            </div>
                            <div class="form-group" style="margin-top: 20px">
                                <label class="control-label" style="text-align: center">عنوان (اللغة الإنجليزية)
                                    :</label>
                                <input style="text-align: left" type="text" id="title_en" name="title_en"
                                       placeholder="الرجاء إدخال العنوان باللغة الإنجليزية "
                                       class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class=" control-label" style="text-align: center">النص (اللغة الإنجليزية)
                                    :</label>
                                <textarea style="text-align: left" id="text_en" name="text_en"
                                          placeholder="أكتب نص هنا " rows="5" class="form-control" required></textarea>
                            </div>


                            <div id="photo_form" class="form-group">
                                <label class="control-label" style="text-align: center">الأيقونة :</label>
                                <div class="row">
                                    <form id="single" action="{{csrf_token()}}">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="sr-only">Loading...</span>
                                            <div id="jasny_progress" class="progress hidden">
                                                <div id="jasny_percent" class="progress-bar progress-bar-striped active"
                                                     role="progressbar"
                                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                                     style="width:40%;background-color: #11a724">0%
                                                </div>
                                            </div>
                                            <div class="fileinput-new thumbnail"
                                                 style="width: 300px; height: 300px">
                                                <img id="jasny_photo" src="{{ image_url('default.png')}}"
                                                     data-src="holder.js/100%x100%"
                                                     alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 300px; max-height: 300px;"></div>
                                            <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">إختيار صورة</span>
                                                    <span class="fileinput-exists">تغيير الصورة</span>
                                                    <input type="file" class="fileupload">
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row btn-padding">
                        <button style="width: 15%" class="btn btn-success pull-right"> حفظ &nbsp; &nbsp; <i
                                    style="top: inherit;left: AUTO;"
                                    class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i></button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/main.js') !!}
        {!! HTML::script('panel/js/simple_form.js') !!}
    @endpush
@stop