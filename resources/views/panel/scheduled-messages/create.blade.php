@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/select2.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> الرسائل المجدولة  /   إرسال بريد مباشر   </span></h2>
    </div>


    <div class="contentpanel">
        <div class="row">

            {!! Form::open(['id'=>'form','url'=>url()->current(),'to'=>lang_route('panel.scheduled-messages.all.index')]) !!}
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="margin-top:30px;width: 70%;display: none"
                         class="alert alert-danger errors col-md-offset-2">
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label" style="text-align: center">عنوان الرسالة :</label>
                            <input type="text" name="title" class="form-control" placeholder="عنوان الرسالة" value="{{@$item->title}}" required/>
                        </div>

                        <div class="form-group">
                            <label class=" col-sm-3 control-label">الموضوع :</label>
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <textarea class="form-control ckeditor" rows="15" name="text" style="text-align: left!important;"required>{{@$item->text}}</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <button style="width: 100%" class=" btn btn-primary">إرسال &nbsp; &nbsp;
                                <i style="top: inherit;left: AUTO;" class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">نوع الحساب
                                <span class="asterisk">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="all_account_type" value="0" name="type" @if(@$item->type == 0) checked @endif>
                                    <label for="all_account_type">الكل</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" value="2" id="teacher" name="type" @if(@$item->type == 2) checked @endif>
                                    <label for="teacher">مدربين</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" value="1" id="student" name="type" @if(@$item->type == 1) checked @endif>
                                    <label for="student">طلاب</label>
                                </div>

                                <label class="error" for="type"></label>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label class="col-sm-4 control-label">حسب الجنس <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="all_gender" value="0" name="gender" @if(@$item->gender == 0) checked @endif >
                                    <label for="all_gender">الكل</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="male" value="1" name="gender" required="" @if(@$item->gender == 1) checked @endif>
                                    <label for="male">ذكر</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio"id="female" value="2"  name="gender" @if(@$item->gender == 2) checked @endif >
                                    <label for="female">أنثى</label>
                                </div>
                                <label class="error" for="gender"></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">توقيت الإرسال <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select class="selectpicker" name="period" style="width: 80%" required>
                                    <option selected disabled="" hidden> </option>
                                    <option @if(@$item->period == 'daily') selected @endif  value="daily">يومياً</option>
                                    <option @if(@$item->period == 'weekly') selected @endif  value="weekly">أسبوعياً</option>
                                    <option @if(@$item->period == 'monthly') selected @endif  value="monthly">شهرياً</option>
                                    <option @if(@$item->period == 'yearly') selected @endif  value="yearly">سنوياً</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>



    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/ckeditor/ckeditor.js') !!}
        {!! HTML::script('panel/js/ckeditor/adapters/jquery.js') !!}
        {!! HTML::script('panel/js/ckeditor.js') !!}
        {!! HTML::script('panel/js/page_form.js') !!}}
        {!! HTML::script('panel/js/select2.min.js') !!}

        <script>
            jQuery(".select2").select2({
                placeholder: 'توقيت الإرسال',
            });

        </script>
    @endpush
@stop