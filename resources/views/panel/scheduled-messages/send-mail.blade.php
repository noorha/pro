@extends('panel.layout.index')
@section('main')
    @push('panel_css')
        {!! HTML::style('panel/css/select2.css') !!}
    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية <span> الرسائل المجدولة  /   إرسال بريد مباشر   </span></h2>
    </div>


    <div class="contentpanel">
        <div class="row">

            {!! Form::open(['id'=>'form','url'=>lang_route('panel.scheduled-messages.send.post'),'to'=>lang_route('panel.scheduled-messages.send.index')]) !!}
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div style="margin-top:30px;width: 70%;display: none" class="alert alert-danger errors col-md-offset-2"></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label" style="text-align: center">عنوان الرسالة :</label>
                            <input type="text" name="title" class="form-control" placeholder="عنوان الرسالة" required/>
                        </div>
                        <div class="form-group">
                            <label class=" col-sm-3 control-label">الموضوع :</label>
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <textarea class="form-control ckeditor" rows="15" name="text" style="text-align: left!important;" required></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <button style="width: 100%" class=" btn btn-primary">إرسال &nbsp; &nbsp;
                                <i style="top: inherit;left: AUTO;" class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">نوع الحساب
                                <span class="asterisk">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="all_account_type" value="0" name="account_type" checked>
                                    <label for="all_account_type">الكل</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" value="2" id="teacher" name="account_type">
                                    <label for="teacher">مدربين</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" value="1" id="student" name="account_type">
                                    <label for="student">طلاب</label>
                                </div>

                                <label class="error" for="account_type"></label>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label class="col-sm-4 control-label">حسب الجنس <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="all_gender" value="0" name="gender" checked>
                                    <label for="all_gender">الكل</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" id="male" value="1" name="gender" required="">
                                    <label for="male">ذكر</label>
                                </div>
                                <div class="rdio rdio-primary">
                                    <input type="radio" value="2" id="female" name="gender">
                                    <label for="female">أنثى</label>
                                </div>
                                <label class="error" for="gender"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="select2" style="width: 100%;" name="users[]" multiple  data-placeholder="البحث بالإسم أو بالبريد الإلكتروني" required>
                                    <option></option>
                                    @if(isset($users) && $users->count() > 0)
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name .' | '.$user->email}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>



    @push('panel_js')
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('panel/js/jasny-bootstrap.min.js') !!}
        {!! HTML::script('panel/js/jasny.js') !!}
        {!! HTML::script('panel/js/ckeditor/ckeditor.js') !!}
        {!! HTML::script('panel/js/ckeditor/adapters/jquery.js') !!}
        {!! HTML::script('panel/js/ckeditor.js') !!}
        {!! HTML::script('panel/js/page_form.js') !!}}
        {!! HTML::script('panel/js/select2.min.js') !!}

        <script>
            jQuery(".select2").select2({
                placeholder: 'إبحث بالإسم أو بالبريد الإلكتروني',
            });

        </script>
    @endpush
@stop