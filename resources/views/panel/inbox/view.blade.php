@extends('panel.layout.index')
@section('main')
    @push('panel_css')

    @endpush

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> الصفحة الرئيسية<span> رسائل الزوار /  عرض الرسالة </span></h2>
    </div>
    <div class="contentpanel panel-email">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                                {{--<button class="btn btn-sm btn-white tooltips" type="button" data-toggle="tooltip"--}}
                                {{--title="إبلاغ"><i class="glyphicon glyphicon-exclamation-sign"></i>--}}
                                {{--</button>--}}
                                <button class="btn btn-sm btn-white tooltips delete" type="button" data-toggle="tooltip"
                                        data-url="{{admin_url('inbox/delete/'.$msg->id)}}" title="حذف"><i
                                            class="glyphicon glyphicon-trash"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="read-panel">
                                <div class="media">
                                    {{--<a href="#" class="pull-left">--}}
                                    {{--<img alt="" src="../images/photos/user2.png" class="media-object">--}}
                                    {{--</a>--}}
                                    <div class="media-body">
                                        <span class="media-meta pull-right">{{diff_for_humans($msg->created_at)}}</span>
                                        <h4 class="text-primary">{{$msg->name}}</h4>
                                        <small class="text-muted">{{'  من : '.$msg->email}}</small>
                                        <p class="text-muted">{{$msg->subject}}</p>
                                    </div>
                                </div>
                                <p>{!! nl2br($msg->text) !!}</p>

                                @foreach($msg->replays as $replay)
                                    <div class="media">
                                        <div class="media-body">
                                            <span class="media-meta pull-right">{{diff_for_humans($replay->created_at)}}</span>
                                        </div>
                                    </div>
                                    <p>{!! $replay->text !!}</p>
                                @endforeach

                                {!! Form::open(['id'=>'form','method'=>'post','url'=>admin_url('inbox/replay-msg/'.$msg->id),'to'=> admin_url('inbox/all')]) !!}
                                <div style="margin-top: 50px" class="media">
                                    {{--<a href="#" class="pull-left">--}}
                                    {{--<img alt="" src="{{url('/image/80x80/default-msg.png')}}" class="media-object">--}}
                                    {{--</a>--}}
                                    <div class="media-body">
                                        <div class="form-group">
                                            <div>
                                                <textarea class="form-control " name="text" rows="5"
                                                          placeholder=" أكتب رد هنا " required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="name" value="{{$msg->id}}">
                                    <input type="hidden" name="email" value="{{$msg->email}}">

                                    <div class="pull-right" style="margin-top: 10px">
                                        <button class="btn btn-info">
                                            &nbsp; <i style="top: inherit;left: AUTO;"
                                                      class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw  hidden"></i>
                                            إرسال
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('panel_js')
        <script>
            $('.delete').on('click', function (event) {
                var delete_url = $(this).data('url');
                event.preventDefault();
                swal({
                    title: '<span class="info">  هل أنت متأكد من حذف  الرسالة ؟</span>',
                    type: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'موافق',
                    cancelButtonText: 'إغلاق',
                    confirmButtonColor: '#56ace0',
                    width: '500px'
                }).then(function (value) {
                    $.ajax({
                        url: delete_url,
                        method: 'delete',
                        type: 'json',
                        success: function (response) {
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        location.href = '/admin/inbox/all';
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            errorCustomSweet();
                        }
                    });
                });
            });
        </script>
        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        {!! HTML::script('/panel/js/simple_form.js') !!}

    @endpush
@stop