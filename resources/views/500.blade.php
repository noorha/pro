@extends('front.layout.index',['sub_title' => __('صفحة غير موجودة')])
@section('main')
    @push('front_css')


    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <h1 class="text-center"><strong> 404 </strong></h1>
                <h2 class="text-center">
                    @lang('صفحة غير موجودة')
                </h2>
            </div>
        </div>
    </section>

    @push('front_js')

    @endpush
@stop