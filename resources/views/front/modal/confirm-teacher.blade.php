{!! Form::open(['id'=>'confirm_form','method'=>'POST' , 'url'=>lang_route('confirm.request.'.$type,[$id])]) !!}

<div class="modal fade" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                        class="icon-cross-symbol-1"></i></button>
            <div class="div_mosw">
                <h3>@lang('تأكيد العملية')</h3>
                <p>@lang('هل ترغب بتاكيد عملية التسليم ؟')</p>
                <ul>
                    <li>
                        <div class="inputsw2w btsw">
                            <button>@lang('نعم')
                                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                            </button>
                        </div>

                    </li>
                    <li>
                        <div class="inputsw2w btsw nobtnsw">
                            <button data-dismiss="modal">@lang('لا')</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

