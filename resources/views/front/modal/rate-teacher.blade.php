
{!! Form::open(['id'=>'rate_form','method'=>'POST' , 'url'=>lang_route('rating',[$training_request->id])]) !!}

<div class="modal fade" id="rate_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                        class="icon-cross-symbol-1"></i></button>
            <div class="div_mosw rateswo">
                <input type="hidden" name="request_id" value="{{$training_request->id}}">
                <div class="image_mposdw">
                    <img src="{{image_url($training_request->getTeacherPhoto(),'100x100')}}">
                </div>
                <h3>{{$training_request->getTeacherName()}}</h3>
                @php
                    $specialties = $training_request->teacher->specialties->unique();
                @endphp
                <div class="text_profilesec2">
                    @if(isset($specialties) && $specialties->count() >0)
                        <span>@lang('التخصص') :
                            @foreach($specialties as $i=>$specialty)
                                @if($i!=0) | @endif
                                {{get_text_locale($specialty,'name')}}
                            @endforeach
                        </span>
                    @endif
                </div>
                <div class="ratinasdl">
                    <select class="example-fontawesome" name="value" autocomplete="off" required>
                        <option value="1" selected>1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div class="inputsw2w">
                    <textarea rows="5" name="text" placeholder="@lang('أضف تعليقك')"></textarea>
                </div>
                <div class="inputsw2w btsw">
                    <button>@lang('إرسال')
                        <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
