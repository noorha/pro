{!! Form::open(['id'=>'form','method'=>'POST' , 'url'=>lang_route('profile.withdraw.send')]) !!}

<div class="modal fade" id="withdraw_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                        class="icon-cross-symbol-1"></i></button>
            <div class="div_mosw contact-us">
                <h3>@lang('سحب رصيد من حسابك')</h3>
                <div class="col-md-7">
                    <div class="inputsws bgw ">
                        <input type="email" name="email" placeholder="@lang('البريد الإلكتروني') " required>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="inputsws bgw">
                        <input type="number" name="value" placeholder="@lang('المبلغ')" required>
                        <i class="fa fa-dollar dollar-icon" ></i>
                    </div>
                </div>
                <ul>
                    {{--<li>الحد الأدنى للسحب <span>100$</span></li>--}}
                    {{--<li>الحد الأعلى للسحب <span>5350$</span></li>--}}
                </ul>
                <div class="inputsw2w btsw">
                    <button>سحب
                        <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
