{!! Form::open(['id'=>'form','method'=>'post']) !!}

<div class="modal fade" id="reject_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                        class="icon-cross-symbol-1"></i></button>
            <div class="div_mosw">
                <h3>@lang('تبليغ عن مشكلة')</h3>
                <div class="inputsw2w martops">
                    <textarea rows="8" name="note" placeholder="@lang('ملاحظاتك')"></textarea>
                </div>
                <div class="inputsw2w btsw">
                    <button id="reject_btn" >@lang('إرسال')
                        <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}
