@extends('front.layout.index',['sub_title' =>__('إعادة تعيين كلمة المرور')])
@section('main')
    @push('front_css')


    @endpush
    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('password.reset',['id'=>$user->id])]) !!}
                    <div class="logdata">
                        <h3>@lang('إعادة تعيين كلمة المرور')</h3>
                        <div class="inputsws pass">
                            <input id="password" type="password" name="password" placeholder="@lang('كلمة المرور')"
                                   required>
                        </div>
                        <div class="inputsws pass">
                            <input type="password" name="password_confirmation" placeholder="@lang('تأكيد كلمة المرور')"
                                   required>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="inputsws btnward">
                                <button>@lang('إعادة تعيين كلمة المرور')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    @push('front_js')

        <script>
            function afterSuccess() {
                location.href = '/' + window.lang + '/login'
            }
        </script>
        {!! HTML::script('/front/js/form.js') !!}
    @endpush
@stop