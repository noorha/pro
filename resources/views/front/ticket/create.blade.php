@extends('front.layout.index',['sub_title' => __('lang.create_ticket')])
@section('main')
    @push('front_css')

    @endpush
    <div class="main_tickets">
        <div class="container">
            <div class="content_tickets">
                <ul class="list-nav_tickets">

                    <li>
                        <a href="{{lang_route('ticket.all')}}">@lang('lang.my_tickets')</a>
                    </li>
                    <li>
                        <a href="{{lang_route('ticket.create.index')}}"  class="active">@lang('lang.open_ticket')</a>
                    </li>
                </ul>

                <div class="innerContent_tickets">
<!--                    <h3 class="title_tickets">@lang('lang.open_ticket')</h3>-->
                    {{--<p class="info_tickets">نرجوا تزويدنا بتفاصيل وافية وكافية عن المشكلة حتى يتسنى لنا حلها في أسرع--}}
                        {{--وقت</p>--}}
                    <div class="row">
                        <div class="col-lg-5">
                            {!! Form::open(['id'=>'form','method'=>'post','enctype' => 'multipart/form-data','url'=> lang_route('ticket.create.post')]) !!}
                            <div class="form-group">
                                <label for="" class="lable">@lang('lang.title')</label>
                                <input name="subject" type="text" required>
                            </div>
                            <div class="form-group">
                                <label for="text" class="lable">@lang('lang.ticket_details')</label>
                                <textarea name="text" id="text" cols="30" rows="10" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="" class="lable"> @lang('lang.attachments')</label>
                                <div class="inputfile-box">
                                    <input type="file" id="file" name="file" class="inputfile" onchange='uploadFile(this)'
                                           style="display: none;">
                                    <label for="file">
                                        <span id="file-name" class="file-box"></span>
                                        <span class="file-button">
                                            @lang('lang.choose_file')
                                            </span>
                                    </label>
                                </div>
                                <div class="info-name hidden">
                                    <label class="name-upload">.png</label>
                                    <button id="removeIimg"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn_tickets">@lang('إرسال')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function uploadFile(target) {
            document.getElementById("file-name").innerHTML = target.files[0].name;
        }

        $("#file").change(function () {
            $(".name-upload").text(this.files[0].name);
            $(this).closest('.form-group').find('.info-name').removeClass('hidden')
        });
        $('#removeIimg').click(function (e) {
            e.preventDefault();

            $('.file-box').html('');
            $(this).closest('.form-group').find('.info-name').addClass('hidden')

        });
    </script>
    @push('front_js')

        {!! HTML::script('/front/js/form.js') !!}
        <script>
            function afterSuccess() {
                location.href='{{lang_route('ticket.all')}}';
            }
        </script>

    @endpush
@stop