@extends('front.layout.index',['sub_title' => __('lang.create_ticket')])
@section('main')
    @push('front_css')

    @endpush
    <div class="main_tickets">
        <div class="container">
            <div class="content_tickets">
                <ul class="list-nav_tickets">
                    <li>
                        <a href="{{lang_route('ticket.all')}}" class="active">تذاكري</a>
                    </li>
                    <li>
                        <a href="{{lang_route('ticket.create.index')}}">فتح تذكرة</a>
                    </li>

                </ul>
                <div class="innerContent_tickets">
                    {{--<div class="form-group">--}}
                    {{--<input type="text" placeholder="ابحث عن تذكرة..." class="search_tickets">--}}
                    {{--</div>--}}
                    <h3 class="title_tickets">تفاصيل التذكرة
                        <button class="hidden_tickets">اخفاء المحلولة</button>
                    </h3>
                    <div class="table-responsive">
                        <table class="table_tickets" width="100%">
                            <thead>
                            <tr class="ticket-list-header color-white background">
                                <th width="100" class="border-bottomTop-right-radius-10">رقم التذكرة</th>
                                <th width="100">اخر تحديث</th>
                                <th width="100">أخر من رد</th>
                                <th width="100">القسم</th>
                                <th class="stats_tickets  border-bottomTop-left-radius-10" width="150">الحالة</th>
                            </tr>

                            </thead>
                            <tbody>

                            @foreach($tickets as $ticket)
                                <tr class="ticket-list-height"></tr>
                                <tr>
                                    <td class="ticket-list-title" width="100%" colspan="5">
                                        <a href="{{lang_route('$ticket.view',[$ticket->id])}}">{{$ticket->subject}}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100" class="ticket-list-info border-bottom-right-radius-10">
                                        {{$ticket->id}}
                                    </td>
                                    <td width="100" class="ticket-list-info">
                                        {{get_date_from_timestamp($ticket->updated_at).' '.format_24_to_12($ticket->updated_at)}}
                                    </td>
                                    <td width="100" class="ticket-list-info">
                                        محمد أحمد
                                    </td>
                                    <td width="100" class="ticket-list-info">
                                        طلبات التدريب
                                    </td>
                                    <td width="150" class="ticket-list-info stats_tickets border-bottom-left-radius-10">
                                        محلولة
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('front_js')
    @endpush
@stop