@extends('front.layout.index',['sub_title' => __('lang.create_ticket')])
@section('main')
    @push('front_css')
        <style>
            .ticket_post_avatar {
                border-radius: 25px;
            }
        </style>
    @endpush
    <div class="main_tickets">
        <div class="container">
            <div class="content_tickets">
                <ul class="list-nav_tickets">
                    <li>
                        <a href="{{lang_route('ticket.all')}}" class="active">@lang('lang.my_tickets')</a>
                    </li>
                    <li>
                        <a href="{{lang_route('ticket.create.index')}}">@lang('lang.open_ticket')</a>
                    </li>

                </ul>
                <div class="innerContent_tickets">
                    <h3 class="title_tickets">@lang('lang.view_ticket') </h3>
                    <p class="date_tickets"> @lang('lang.published_at') {{get_date_from_timestamp($ticket->created_at)}}  </p>
                    <h3 class="title_tickets">{{$ticket->subject}}</h3>
                    @if(isset($ticket->file) && !empty($ticket->file))
                        <a href="{{file_url($ticket->file)}}">@lang('lang.download_attachment')</a>
                    @endif
                    <table class="table_tickets" width="100%">
                        <thead>
                        <tr class="ticket-list-header color-black">
                            <th width="100">@lang('lang.ticket_no')</th>
                            <th width="100">@lang('lang.last_update')</th>
                            <th width="100">@lang('lang.last_reply')</th>
                            <th class="stats_tickets" width="150">@lang('lang.status')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="100" class="ticket-list-info">
                                {{$ticket->id}}
                            </td>
                            <td width="100" class="ticket-list-info">
                                {{diff_for_humans($ticket->updated_at)}}
                            </td>
                            <td width="100" class="ticket-list-info">
                                {{$ticket->lastReplayName()}}
                            </td>
                            <td width="150" class="ticket-list-info stats_tickets">
                                {{$ticket->statusText()}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @foreach($ticket->replays as $replay)
                        <div class="ticket_post_container">
                            <div class="row no-gutters d-lg-flex align-items-center">
                                <div class="col-lg-9">
                                    <div class="ticket_post_content">
                                        <p class="ticket_post_date"> {{diff_for_humans($replay->created_at)}} </p>
                                        <h3 class="ticket_post_title">{{$replay->text}}</h3>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="ticket_cell">
                                        <div class="table-cell vertical-middle">
                                            <img src="{{$replay->avatar('45x45')}}" alt="" class="ticket_post_avatar">
                                        </div>
                                        <div class="table-cell vertical-middle pr-10">
                                            <p class="ticket_post_name">{{$replay->name()}}</p>
                                            @if($replay->hasAdmin())
                                                <p class="ticket_post_badgered">@lang('lang.admin')</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @if($ticket->canReplay())
                        <button class="add_replay_post"><i class="fa fa-reply-all" aria-hidden="true"></i>@lang('lang.add_replay')
                        </button>
                        <div class="ticket_replay_post_container">
                            <p>@lang('lang.replay')</p>
                            {!! Form::open(['id'=>'form','method'=>'post','url'=> lang_route('ticket.replay.post',[$ticket->id])]) !!}
                            <textarea rows="10" name="text" placeholder="@lang('lang.your_message')" required></textarea>
                            <button type="submit" class="ticket_sent-message">@lang('إرسال')
                                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                            </button>
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.ticket_replay_post_container').hide();
        $('.add_replay_post').click(function () {
            $('.ticket_replay_post_container').slideToggle();
        })
    </script>
    @push('front_js')

        {!! HTML::script('/front/js/form.js') !!}
        <script>
            function afterSuccess() {
                location.reload();
            }
        </script>
    @endpush
@stop