@extends('front.layout.index',['sub_title' => __('lang.create_ticket')])
@section('main')
    @push('front_css')

    @endpush
    <div class="main_tickets">
        <div class="container">
            <div class="content_tickets">
                <ul class="list-nav_tickets">
                    <li>
                        <a href="#">تذاكري</a>
                    </li>
                    <li>
                        <a href="#" class="active">فتح تذكرة</a>
                    </li>
                    <li class="pull-left">
                        <a href="" >تسجيل الخروج</a>
                    </li>
                </ul>
                <div class="innerContent_tickets">
                    <div class="form-group">
                       <input type="text" placeholder="ابحث عن تذكرة..." class="search_tickets">
                    </div>
                    <h3 class="title_tickets">تفاصيل التذكرة</h3>
                    <div class="row">
                        <div class="col-lg-5">
                            <form action="">
                                <div class="form-group">
                                    <label class="container-radio">المدرب
                                        <input type="radio" checked="checked" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="container-radio">المتدرب / الطالب
                                        <input type="radio" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <a hraf=""  class="btn_tickets mt-20">التالي<i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('front_js')
    @endpush
@stop