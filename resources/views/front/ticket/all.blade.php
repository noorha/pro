@extends('front.layout.index',['sub_title' => __('lang.all_tickets')])
@section('main')
    @push('front_css')

    @endpush
    <div class="main_tickets">
        <div class="container">
            <div class="content_tickets">
                <ul class="list-nav_tickets">
                    <li>
                        <a href="{{lang_route('ticket.all')}}" class="active">@lang('lang.my_tickets')</a>
                    </li>
                    <li>
                        <a href="{{lang_route('ticket.create.index')}}">@lang('lang.open_ticket')</a>
                    </li>

                </ul>
                <div class="innerContent_tickets">
                    {{--<div class="form-group">--}}
                    {{--<input type="text" placeholder="ابحث عن تذكرة..." class="search_tickets">--}}
                    {{--</div>--}}
                    {{--<h3 class="title_tickets">تفاصيل التذكرة--}}
                    {{--<button class="hidden_tickets">اخفاء المحلولة</button>--}}
                    {{--</h3>--}}
                    <div class="table-responsive">
                        <table class="table_tickets" width="100%">
                            <thead>
                            <tr class="ticket-list-header color-white background">
                                <th width="100" class="border-bottomTop-right-radius-10">@lang('lang.ticket_no')</th>
                                <th width="100">@lang('lang.last_update')</th>
                                <th width="100">@lang('lang.last_reply')</th>
                                <th class="stats_tickets  border-bottomTop-left-radius-10" width="150">@lang('lang.status')</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($tickets as $ticket)
                                <tr class="ticket-list-height"></tr>
                                <tr>
                                    <td class="ticket-list-title" width="100%" colspan="5">
                                        <a href="{{lang_route('ticket.view',[$ticket->id])}}">{{$ticket->subject}}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100" class="ticket-list-info border-bottom-right-radius-10">
                                        {{$ticket->id}}
                                    </td>
                                    <td width="100" class="ticket-list-info">
                                        {{diff_for_humans($ticket->updated_at)}}
                                    </td>
                                    <td width="100" class="ticket-list-info">
                                        {{$ticket->lastReplayName()}}
                                    </td>

                                    <td width="150" class="ticket-list-info stats_tickets border-bottom-left-radius-10">
                                        {{$ticket->statusText()}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('front_js')


    @endpush
@stop