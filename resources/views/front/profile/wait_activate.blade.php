@extends('front.layout.index',['sub_title' => __('تفعيل الحساب')])
@section('main')
    @push('front_css')
    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="logdata">
                        <h3>@lang('تفعيل الحساب')</h3>

                       <div class="alert alert-danger">
                       برجاء الانتظار ...حتى يطلع الادمن ع الملف الخاص بك وسيتم تفعيل الحساب خلال 24 ساعة
                       </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
{{--        {!! HTML::script('/front/js/form.js') !!}--}}
    @endpush
@stop