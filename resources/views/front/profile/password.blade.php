<div class="row">
    <div class="col-md-9">
        <div class="fiserig4">
            <div class="header_fiserig4">
                <h3>@lang('كلمة المرور')</h3>
            </div>
            <div class="inputsws bgw pass">
                <input id="password" type="password" name="password" placeholder="@lang('كلمة المرور')" >
            </div>
            <div class="inputsws bgw pass">
                <input type="password" name="password_confirmation" placeholder="@lang('تأكيد كلمة المرور')" >
            </div>
        </div>
    </div>
</div>

