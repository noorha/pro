<div class="row">
    <div class="col-md-9">
        <div class="fiserig4">
            <div class="header_fiserig4">
                <h3>@lang('إعدادات الحساب')</h3>
            </div>
            <div class="inputsws bgw email">
                <input type="text" name="email"  value="{{$user->email}}" placeholder="@lang('البريد الالكتروني')">
            </div>

            @php
                $countries = get_all_countries();
            @endphp
            <div class="input2div sings">
                <div class="inputsw mobile">
                    <input type="text" name="mobile" value="{{$user->mobile}}" placeholder="@lang('رقم الجوال')" required>
                </div>
                <div class="inputsw">
                    <select class="selectpicker" name="nationality_id">
                        @foreach($countries as $country)
                            <option value="{{$country->id}}" @if($user->nationality_id ==$country->id ) selected @endif>{{$country->code}}<img src=""></option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="header_fiserig4">
                <h3>@lang('كلمة المرور')</h3>
            </div>
            <div class="inputsws bgw pass">
                <input id="password" type="password" name="password" placeholder="@lang('كلمة المرور')" >
            </div>
            <div class="inputsws bgw pass">
                <input type="password" name="password_confirmation" placeholder="@lang('تأكيد كلمة المرور')" >
            </div>
        </div>
    </div>
</div>

