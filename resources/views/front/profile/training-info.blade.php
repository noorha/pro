<div class="sumonsws">
    <div class="chexboxs hw20">
        <h3>@lang('نوع التدريب')</h3>
        <div class="checbosx">
            <input type="checkbox" class="chx training-type-cls" name="training_type[]" id="chs1" value="1"
                   @if(auth()->user()->isWorkOnline()) checked @endif required>
            <label for="chs1">@lang('أونلاين')</label>
        </div>
        <div class="checbosx">
            <input type="checkbox" class="chx training-type-cls" name="training_type[]" id="chs2" value="2"
                   @if(auth()->user()->isWorkInterview()) checked @endif required>
            <label for="chs2">@lang('مقابلة')</label>
        </div>
    </div>
    <div class="asdiow @if(auth()->user()->isWorkInterview()) active @endif" id="asdiow_2">
        <label>@lang('سعر ساعة المقابلة')</label>
        <div class="inputsws nopadright bgw tiomso">
            <input type="number" min="1" id="interview_price" name="interview_price" placeholder="@lang('سعر الساعة')"
                   value="{{(auth()->user()->interviewPrice())}}" required>
            <i aria-hidden="true">$</i>
        </div>
    </div>
    <div class="asdiow  @if(auth()->user()->isWorkOnline()) active @endif" id="asdiow_1">
        <label>@lang('سعر ساعة الأونلاين')</label>
        <div class="inputsws nopadright bgw tiomso ">
            <input type="number" id="online_price" min="1" name="online_price" placeholder="@lang('سعر الساعة')"
                   value="{{(auth()->user()->onlinePrice())}}" required>
            <i aria-hidden="true">$</i>
        </div>
        @php
            $contacts = get_all_contacts();
        @endphp
        @foreach($contacts as $contact)
            <div class="asdiow active">
                <label>{{$contact->name}}</label>
                <div class="inputsws nopadright bgw tiomso">
                    <input type="text"  name="contacts[{{$contact->id}}]" placeholder="{{$contact->name}}" value="{{auth()->user()->getContact($contact->id)}}" >
                </div>
            </div>
        @endforeach
    </div>


</div>
