@extends('front.layout.index',['sub_title' => __('صفحة الأرصدة للمدرب')])
@section('main')
    @push('front_css')
    @endpush
    <section class="meniasdmw">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="monyf">
                        <div class="img_monyf">
                            <img src="/front/images/change.png">
                        </div>
                        <div class="text_monyf">
                            <h3>{{get_currency_value(auth()->user()->pending_cash)}}
                                <span>{{get_currency_text()}}</span></h3>
                            <p>@lang('الرصيد المعلق')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="monyf">
                        <div class="img_monyf">
                            <img src="/front/images/rich.png">
                        </div>
                        <div class="text_monyf">
                            <h3>{{auth()->user()->totalNonPaidProfit() > 0 ? get_currency_value(auth()->user()->totalNonPaidProfit()) : 0}}
                                <span>{{get_currency_text()}}</span></h3>
                            <p>@lang('عليك') <span>
                                    <a id="training_payment_btn" href="javascript:;"> @lang('تسديد المبلغ')</a> </span>
                            </p>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="monuy2">
                        <div class="monyf hvlsw">
                            <div class="img_monyf">
                                <img src="/front/images/change.png">
                            </div>
                            <div class="text_monyf">
                                <h3>{{get_currency_value(auth()->user()->walletAvailableCash())}}
                                    <span>{{get_currency_text()}}</span></h3>
                                <p>@lang('المبلغ القابل للسحب')</p>
                            </div>
                        </div>
                        <div class="monyf hvlsw">
                            <a @if(auth()->user()->walletAvailableCash() >= 30) data-toggle="modal"
                               data-target="#withdraw_modal" @else disabled class="balance-pop-up" @endif><i
                                        class="icon-paypal2"></i>@lang('سحب رصيد')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sec">
        <div class="container">
            <div class="logdata colsw notall">
                @include(view_front_profile().'processes-table')
            </div>
        </div>
    </section>

    {!! Form::open(['id'=>'paypal_form' ,'method'=>'post','url'=>lang_route('addmoney.paypal')]) !!}
    <input type="hidden" name="email" value="{{auth()->user()->email}}">
    <input id="total_un_paid" type="hidden" name="value" value="{{auth()->user()->totalNonPaidProfit()}}">
    <input type="hidden" name="type" value="cash">
    {!! Form::close() !!}

    @include(view_front_modal().'withdraw')


    @push('front_js')
        {!! HTML::script('/front/js/form.js') !!}
        <script>
            function afterSuccess() {

            }

            $(document).on('click', '.balance-pop-up', function (event) {
                customSweetAlert('info', '', window.lang === 'ar' ? 'يجب أن يكون الحد الأدنى لرصيدك أكبر من 30 دولار' : ' Your balance must be greater than 30$ ');
            });
            $('#training_payment_btn').on('click', function (event) {
                event.preventDefault();
                if(parseInt($('#total_un_paid').val()) >0 ){
                    swal({
                        title: window.lang === 'ar' ? 'إختر وسيلة الدفع' : 'Select a payment method',
                        input: 'select',
                        inputOptions: {
                            'website': window.lang === 'ar' ? 'خصم من رصيد الموقع' : 'Withdraw from your site balance',
                            'paypal': window.lang === 'ar' ? 'دفع عن طريق paypal' : 'PayPal'
                        },
                        confirmButtonText:  window.lang === 'ar' ? 'دفع' : 'Submit',
                        cancelButtonText: window.lang === 'ar' ? 'إلغاء' : 'Cancel',
                        showCancelButton: true,
                        inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value === 'website') {
                        $.ajax({
                            url: '/' + window.lang + '/profile/cash',
                            method: 'POST',
                            processData: false,
                            contentType: false,
                            type: 'json',
                            success: function (response) {
                                $('.upload-spinn').addClass('hidden');
                                if (response.status) {
                                    customSweetAlert(
                                        'success',
                                        response.message,
                                        response.item,
                                        function (event) {
                                            location.reload();
                                        }
                                    );
                                } else {
                                    customSweetAlert(
                                        'error',
                                        response.message,
                                        response.errors_object
                                    );
                                }
                            },
                            error: function (jqXhr) {
                                $('.upload-spinn').addClass('hidden');
                                getErrors(jqXhr, '/' + window.lang + '/login');
                            }
                        });
                    } else {
                        $('#paypal_form').submit();
                    }
                })
                }
                });
                }else {
                    customSweetAlert('info',window.lang ==='ar' ? 'لا يوجد عليك مستحقات' : 'You dont have un paid requests');
                }


                // if (country) {
                //     swal('You selected: ' + country)
                // }
            });
        </script>
    @endpush
@stop