@extends('front.layout.index',['sub_title' => __('تعديل الملف الشخصي')])
@section('main')
    @push('front_css')
    @endpush

    @php
        $user = auth()->user();
    @endphp
<style>
.inputswss.calenders:before {
    content: '';
}
.inputsw2w.email:before, .inputsws.email:before {
    content: '';
}
.inputsw2w.mobile:before, .inputsw.mobile:before {
    content: '';
}
.inputsws textarea, .inputsws.date.select button, .inputsws input {
    padding: 20px;
}
.inputsws.pass:before {
    content: '';
}
</style>
    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="availablil">
                            <h3>@lang('تعديل بياناتي')</h3>
                            <ul>
                                <li><a data-id="1" class="active">@lang('البيانات الشخصية')</a></li>
                                <li><a data-id="2">@lang('ظهور الملف الشخصي')</a></li>
                                 <li><a data-id="3">@lang('عن المدرب')</a></li>
                                <!-- <li><a data-id="3">المؤهلات العلمية</a></li> -->
                                <!-- <li><a data-id="4">الخبرات العلمية</a></li> -->
                                <li><a data-id="4">@lang('إعدادات التخصص')</a></li>
                                <li><a data-id="5">@lang('المواعيد المتاحة')</a></li>
                                <li><a data-id="6">@lang('إعدادات التدريب')</a></li>
                                <li><a data-id="7">@lang('إعدادات المكان')</a></li>
                                <li><a data-id="8">@lang('تخصيص عنوان URL')</a></li>
                                <li><a data-id="9">@lang('إعدادات الحساب')</a></li>
                                <li><a data-id="10">@lang('lang.delete_account')</a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8" id="laskd">
                        {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('profile.complete.post')]) !!}

                        @php
                            $constant = new \App\Constant();
                        @endphp
                        <div class="tabosw_ds active" id="tab_dsh_1">
                            @include(view_front_profile().'personal', ['user'=> auth()->user()])
                        </div>

                        <div class="tabosw_ds" id="tab_dsh_2">
                            <div class="fiserig4">
                                <div class="header_fiserig4">
                                    <h3>@lang('عرض الملف الشخصي')</h3>
                                </div>
                                <div class="items_sdw">
                                    <p>{{$constant->valueOf( get_current_locale() == 'ar' ? 'allow_profile_txt' : 'allow_profile_txt_en')}}</p>
                                    <div class="chexboxs hw20">
                                        <div class="chexbox">
                                            <input type="radio" name="is_public" value="1" id="ch1" @if(auth()->user()->isPublic()) checked @endif>
                                            <label for="ch1">@lang('نعم')</label>
                                        </div>
                                        <div class="chexbox">
                                            <input type="radio" name="is_public" value="0"  id="ch2" @if(!auth()->user()->isPublic()) checked @endif>
                                            <label for="ch2"> @lang('لا')
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="inputswts pull-left">
                                        <button class="pull-right">@lang('حفظ')
                                            <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabosw_ds" id="tab_dsh_3">
                            <div class="fiserig4">
                                <div class="header_fiserig4">
                                    <h3>@lang('عن المدرب')</h3>
                                </div>
                              
                                <div class="list_tow_righst nopadw">
                                   @include(view_front_profile().'training-about' , ['user'=> auth()->user()])
                                </div>
                            </div>
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>

                        <div class="tabosw_ds" id="tab_dsh_10">
                            @include(view_front_profile().'delete-section' , ['user'=> auth()->user()])
                        </div>

                        <div class="tabosw_ds" id="tab_dsh_4">
                            <div class="fiserig4">
                                <div class="header_fiserig4">
                                    <h3>@lang('إعدادات التخصص')</h3>
                                </div>
                                @php
                                    $specialities = get_all_specialities();
                                @endphp
                                <div class="list_tow_righst nopadw">
                                    @include(view_front_profile().'specialty-info' , ['specialities'=>$specialities])
                                </div>
                            </div>
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        <div class="tabosw_ds" id="tab_dsh_5">
                            <div class="fiserig4">
                                <div class="header_fiserig4">
                                    <h3>@lang('المواعيد المتاحة')</h3>
                                </div>
                                @include(view_front_profile().'times-info')
                            </div>
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        <div class="tabosw_ds" id="tab_dsh_6">
                            <div class="col-md-6">
                                <div class="fiserig4">
                                    <div class="header_fiserig4">
                                        <h3>@lang('إعدادات التدريب')</h3>
                                    </div>
                                    @include(view_front_profile().'training-info')
                                </div>
                            </div>
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        <div class="tabosw_ds" id="tab_dsh_7">
                            @include(view_front_profile().'location-info' , ['user'=> auth()->user()])
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        <div class="tabosw_ds" id="tab_dsh_8">
                            <div class="fiserig4">
                                <div class="header_fiserig4">
                                    <h3>@lang('تخصيص عنوان URL')</h3>
                                </div>
                                <div class="text_urlsw">
                                    <p>@lang('من هنا يمكن تخصيص الرابط الخاص بالبروفايل الشخصي الخاص بك وهذا يسهل من عملية مشاركة رابطك بروفايل')</p>
                                    <div class="row">
                                        <div class="col-md-8 col-sm-7">
                                            <div class="inputswss spansurl">
                                                <input type="text" name="uri_link" placeholder="@lang('الرابط')" value="{{auth()->user()->uri_link}}">
                                                <span>http://halapro.com/{{get_current_locale()}}/profile/</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-5">
                                            <div class="inputswss">
                                                <button class="pull-right">@lang('حفظ')
                                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabosw_ds" id="tab_dsh_9">
                            @include(view_front_profile().'settings' , ['user'=> auth()->user()])
                            <div class="inputswts pull-left">
                                <button class="pull-right">@lang('حفظ')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </section>


    @push('front_js')
        {!! HTML::script('/front/js/form.js') !!}
        <script type="text/javascript">
            $(document).on('click','.delete',function (event) {
                event.preventDefault();
                var delete_url = $(this).data('url');
                swal({
                    title: '<span class="warning">'+(window.lang === 'ar' ? 'هل أنت متأكد من حذف الحساب نهائياً ؟' : 'Are you sure you want to permanently delete your account?') +'</span>',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: window.lang === 'ar'? 'حذف' : 'Delete',
                    cancelButtonText: window.lang === 'ar'? 'إلغاء' : 'Cancel',
                    confirmButtonColor: '#56ace0'
                }).then(function (value) {
                        if (!(value.dismiss === 'cancel')) {
                            $.ajax({
                                url: delete_url,
                                method: 'delete',
                                type: 'json',
                                success: function (response) {
                                    if (response.status) {
                                        customSweetAlert(
                                            'success',
                                            response.message,
                                            response.item,
                                            function (event) {
                                                location.reload();

                                            }
                                        );
                                    } else {
                                        customSweetAlert(
                                            'error',
                                            response.message,
                                            response.errors_object
                                        );
                                    }
                                },
                                error: function (response) {
                                    errorCustomSweet();
                                }
                            });
                        }
                    });
            });
            $(document).on('click','.cancel-delete',function (event) {
                event.preventDefault();
                var delete_url = $(this).data('url');
                swal({
                    title: '<span class="warning">'+(window.lang === 'ar' ? 'هل أنت متأكد من  إلغاء طلب حذف الحساب ؟' : 'Are you sure to cancel the account deletion request?') +'</span>',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: window.lang === 'ar'? 'نعم' : 'Yes',
                    cancelButtonText: window.lang === 'ar'? 'لا' : 'No',
                    confirmButtonColor: '#56ace0'
                }).then(function (value) {
                        if (!(value.dismiss === 'cancel')) {
                            $.ajax({
                                url: delete_url,
                                method: 'post',
                                type: 'json',
                                success: function (response) {
                                    if (response.status) {
                                        customSweetAlert(
                                            'success',
                                            response.message,
                                            response.item,
                                            function (event) {
                                                location.reload();
                                            }
                                        );
                                    } else {
                                        customSweetAlert(
                                            'error',
                                            response.message,
                                            response.errors_object
                                        );
                                    }
                                },
                                error: function (response) {
                                    errorCustomSweet();
                                }
                            });
                        }
                    });
            });

            $('.specialties').on('change', function (event) {
                var parent_id = $(this).data('parent');
                var id = $(this).data('id');
                if (!$('#speciality_' + parent_id).is(":checked")) {
                    $('#speciality_' + parent_id).prop('checked', true);
                }
            });

            $('.side-spec').on('click', function (event) {
                var specialty_id = $(this).data('id');
                var collection = $('.sub-' + specialty_id);
                collection.each(function (object) {
                    $(this).prop('checked', $('#speciality_' + specialty_id).is(":checked"));
                });
            });

            function afterSuccess() {

            }

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#balh').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                    var formData = new FormData();
                    formData.append('image', input.files[0]);
                    var progress = $('.progress');
                    var percent = $('#progress_percent');
                    $.ajax({
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    percent.css("width", percentComplete + '%');
                                    percent.attr('aria-valuenow', percentComplete);
                                    percent.html(percentComplete + '%');
                                    if (percentComplete === 100) {
                                        setTimeout(function () {
                                            progress.addClass('hidden');
                                        }, 500);
                                    }
                                }
                            }, false);
                            return xhr;
                        },
                        url: '/'+window.lang+'/image/upload',
                        type: 'POST',
                        data: formData,
                        beforeSend: function (xhr) {
                            progress.removeClass('hidden');
                            percent.css("width", '0%');
                            percent.attr('aria-valuenow', '0');
                            percent.html('0%');

                        },
                        success: function (response) {
                            $('#photo').val(response.file_name);
                        },
                        error: function (jqXhr) {
                            progress.addClass('hidden');
                            if (jqXhr.status === 400) {
                                customSweetAlert(
                                    'error',
                                    jqXhr.responseJSON.message,
                                    ''
                                );
                                return false;
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }

            }

            $("#upfild").change(function () {
                readURL(this);
            });
        </script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.datetimepickers').datetimepicker({
                    // debug:true
                    format: 'LT'
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var index = 0;
                $('.item_avail p').click(function () {
                    var day = $(this).data('id');
                    var item = "<div class='item_avail1f'><div class='inputsws bgw nopadrights'><input type='text' name='start_"+day+"[]' class='datetimepickers start' data-id='"+index+"' placeholder='الساعة' required></div><div class='inputsws bgw nopadrights'><input type='text'  name='end_"+day+"[]' class='datetimepickers end' data-id='"+index+"' placeholder='الساعة' required></div></div>";
                    $(this).parent().find('.item_avail1').append(item);
                    // addTimeToObject((day-1),index);
                    index++;
                    $('.datetimepickers').datetimepicker({
                        // debug:true
                        format: 'LT'
                    });
                });
            });

        </script>


        
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyD4saBT04W6hCXtK5uwxBDsupssHpPhyFk'></script>
{!! HTML::script('/front/js/map.js') !!}


    <script>

        $('#map').locationpicker({
            location: {
                latitude: "{{isset($user) ? $user->latitude : 24.7191607}}",
                longitude: "{{isset($user) ? $user->longitude : 46.712501}}"
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us_restaurant-lat'),
                longitudeInput: $('#us_restaurant-lon'),
                locationNameInput: $('#us_restaurant-address')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });
    </script>

    @endpush


@stop