@php
    $data['share_title'] = $user->getUserName();
    $data['share_image'] = image_url($user->photo);
    $data['share_description'] = $user->description;
@endphp
@extends('front.layout.index',array_merge(['sub_title' => __('عرض الملف الشخصي  : ').$user->getUserName()],$data))
@section('main')
    @push('front_css')

        {!! HTML::style('front/css/jssocials-theme-flat.css') !!}
        {!! HTML::style('front/css/jssocials.css') !!}
        <style>
            .text-white {
                color: white;
            }

            .jssocials-share-link {
                padding: 0.6em .6em;
            }
            button:disabled {
                background: #999 !important;
            }
        </style>
    @endpush

    <section class="meniasdmw">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="img_profilesec">
                        <div class="span_online new-span-on">
                            <span class="person_active circle-active" style="display: none"></span>
                            <span class="person_inactive circle-inactive" style="display: none"></span>
                            <img src="{{image_url($user->photo,'300x300')}}">
                        </div>
                        <h3 class="title-profilesec">{{$user->getUserName()}}</h3>
                        <div class="rating">
                            <ul>
                                @for($i = 1 ; $i <= 5 ; $i++)
                                    <li class="rate @if(!((int)$user->rating < $i)) active @endif">
                                        <i class="icon-star3"></i>
                                    </li>
                                @endfor
                                <li><span>{{$user->rating>0?$user->rating:''}}</span></li>
                            <!-- ({{$user->TeacherRatingsCount()}})-->
                            </ul>
                            @if($user->isMyProfile())
                                <span class="btn_edit"><a href="{{lang_route('profile.dashboard')}}"><i
                                                class="icon-edit2"></i></a></span>
                            @endif
                        </div>
                        <div id="last_seen_div" class="span_online-1" style="display: none">
                            @lang('lang.last_seen') <span id="last_seen" class="text-white "> </span>
                        </div>
                        <div class="rat-teacher">
                            @if($user->rating > 4.5)
                                @lang('lang.distinguished_tutor')
                                <img class="img-rated" src="{{url('/front/img/distinguished.svg')}}">
                            @elseif($user->rating > 3.5 && $user->rating <= 4.5)
                                @lang('lang.certified_tutor')
                                <img class="img-rated" src="{{url('/front/img/new.svg')}}">
                            @elseif( $user->rating <= 3.5)
                                @lang('lang.new_tutor')
                                <img class="img-rated" src="{{url('/front/img/certified.svg')}}">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="text_profilesec">
                        <div class="text_profilesecs">
                            @if($user->isWorkOnline())
                                <div class="text_profilesec1">
                                    <p>
                                        <span style="font-size: 15px">@lang('أونلاين')</span>
                                        {{get_currency_value($user->onlinePrice())}} <span>{{get_currency_text()}}
                                            / @lang('ساعة')</span>
                                    </p>
                                </div>
                            @endif
                            @if($user->isWorkInterview())
                                <div class="text_profilesec1">
                                    <p>
                                        <span style="font-size: 15px">@lang('مقابلة')</span>
                                        {{get_currency_value($user->interviewPrice())}} <span>{{get_currency_text()}}
                                            / @lang('ساعة')</span>
                                    </p>
                                </div>
                            @endif
                            @php
                                $specialties = $user->specialties->unique();
                            @endphp
                            <div class="text_profilesec2 w-100">

                                @if(isset($specialties) && $specialties->count() >0)
                                    <p>@lang('التخصص') :
                                        @foreach($specialties as $i=>$specialty)
                                            @if($i!=0) | @endif
                                            {{get_text_locale($specialty,'name')}}
                                        @endforeach
                                    </p>
                                @endif
                            </div>

                        </div>

                        <p>{!! $user->description !!}</p>
                        <ul>
                            <li><i class="icon-placeholder2"></i>{{$user->getFullLocation()}}</li>
                            {{--<li><i class="icon-phone-call2"></i> {{ $user->getMobileNo() }}</li>--}}
                            {{--<li><i class="icon-envelope"></i> {{$user->email}} </li>--}}
                        </ul>
                        <div class="img_profilesec d-felx">
                            @php
                                $isStudent = auth()->check() ?  auth()->user()->isStudent() : true;
                            @endphp
                            @if($isStudent)
                                @if(!$user->isMyProfile())
                                    <a  href="#" class="contact-modal" data-id="{{ $user->id }}">@lang('تواصل مع المدرب')</a>
                                    <a href="{{lang_route('student.request.send',[$user->id])  }}">@lang('إختيار المدرب')</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    <h4 class="text-white">@lang('lang.share_on')</h4>
                    <div id="social_share" class="styled-icons icon-sm icon-gray icon-circled"></div>
                </div>
        @if(auth()->check())
                @if(auth()->user()->user_favourite($user->id))
                <a class="add-to-fav added remove-from-favorite"  id="remove-from-favorite" href="javascript:;" data-id="{{$user->id}}"><i class="fa fa-star"></i>&nbsp;&nbsp;<span>@lang("المفضلة")</span>
                @else
                <a  class="add-to-fav add-to-favorite" id="add-to-favorite"  href="javascript:;" data-id="{{$user->id}}" ><i class="fa fa-star-o"></i>&nbsp;&nbsp;<span>@lang("اضف الى المفضله")</span></a>
                @endif
       @endif
            </div>
        </div>
    </section>
    <section class="profisc2">
        <div class="container">
            <div class="header_profsc2">
                <h3>@lang('مجالات التدريب')</h3>
            </div>
            <div class="item_profsc2">
                <div class="row">
                    @if(isset($specialties) && $specialties->count() >0)
                        @foreach($specialties as $specialty)
                            <div class="col-md-4 col-sm-3">
                                <div class="f_itemprfolesc2">
                                    <div class="img_fpisec2">
                                        <img src="{{image_url($specialty->icon)}}">
                                    </div>
                                    <div class="text_fpisec2">
                                        <h3>{{$specialty->text}}</h3>
                                        <ul>
                                            @foreach($specialty->subSpecialties as $subSpecialty)
                                                @if($user->hasSubSpeciality($subSpecialty->id))
                                                    <li>
                                                        <a href="{{lang_route('student.request',[$user->id]).'?sub_specialty='.$subSpecialty->id}}">{{$subSpecialty->text}}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </section>


    <div class="modal fade" id="sendMessageModal" tabindex="-1" role="dialog" aria-labelledby="sendMessageModal"
         aria-hidden="true">
        {!! Form::open(['id' => 'sendMessageModalForm', 'method'=>'POST', 'url'=> lang_route('profile.message.create')]) !!}
        <input type="hidden" name="contact_id" value="0" id="contact_id_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                            class="icon-cross-symbol-1"></i></button>
                <div class="div_mosw">
                    <h3>@lang('اكتب رسالتك')</h3>
                    <div class="inputsw2w martops">
                        <textarea rows="8" name="message" id="messageModal" placeholder="@lang('أكتب نص الرسالة')" required></textarea>
                    </div>
                    <div class="inputsw2w btsw">
                        <button type="submit">@lang('إرسال')</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


    @push('front_js')
        {!! HTML::script('/front/js/jssocials.js') !!}
        {!! HTML::script('/front/js/favourite.js') !!}

        <script>
            var user;
            var userRef = firebase.database().ref('users/{{$user->id}}');

            userRef.once('value', (snapshot) => {
                user = snapshot.val();
                if (isOffline(user)) {
                    $('.person_active').fadeOut();
                    $(' #last_seen').text(getDateFormat(user.lastSeen));
                    $('.person_inactive , #last_seen_div ,  #last_seen').fadeIn();
                } else {
                    $('.person_active').fadeIn();
                    $('.person_inactive , #last_seen_div ,  #last_seen').fadeOut();
                }
            });
            userRef.on('child_changed', (snapshot) => {
                user[snapshot.key] = snapshot.val();
                if (isOffline(user)) {
                    $('.person_active').fadeOut();
                    $('#last_seen').text(getDateFormat(user.lastSeen));
                    $('.person_inactive , #last_seen_div , #last_seen').fadeIn();
                } else {
                    $('.person_active').fadeIn();
                    $('.person_inactive , #last_seen_div ,  #last_seen').fadeOut();
                }
                // user[snapshot.key] = snapshot.val();
                // _this.firebase_user[snapshot.key] = snapshot.val();
                //
                // console.log(snapshot);
                // alert('changed');
            });

            // function isOnline(user) {
            //     var minutes = parseInt((new Date().getTime() - new Date(user.lastSeen).getTime()) / 60000);
            //     return !isNaN(minutes) && minutes !== undefined && minutes <= 5;
            // }
            function getDateFormat(timestamp) {
                var date = new Date(timestamp);
                return date.toLocaleDateString("en-US") + ' '+date.toLocaleTimeString("en-US");
            }
            function isOffline(user) {
                var minutes = parseInt((new Date().getTime() - new Date(user.lastSeen).getTime()) / 60000);
                return !isNaN(minutes) && minutes !== undefined && minutes > 5;
            }
        </script>
        <script>
            $("#social_share").jsSocials({
                showLabel: false,
                shareIn: "popup",
                shares: ["twitter", "facebook", "linkedin", "email"]
            });
            $('.rate').click(function (event) {
                event.preventDefault();
                location.href = '{{lang_route('teacher.ratings',[$user->id])}}'
            })
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(document).on('click', '.contact-modal', function (e) {
                    e.preventDefault();
                    $('#contact_id_modal').val($(this).data('id'));
                    $('#sendMessageModal').modal('show');
                    // console.log($(this).data('id'));
                });
                $('#sendMessageModalForm').submit(function (s) {
                    s.preventDefault();

                    var form = $(this);
                    var formData = new FormData(this);
                    var subbtn = form.find(":submit");
                    subbtn.prop('disabled', true);
                    $.ajax({
                        type: 'POST',
                        url: form.prop('action'),
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data){
                            if (data.status) {
                                $('#sendMessageModal').modal('hide');
                                $('#messageModal').val('');
                                window.location.href = '{{ lang_route('profile.messages') }}';
                            }
                            subbtn.prop('disabled', false);
                        }
                    }).fail(function (jqXhr) {
                        subbtn.prop('disabled', false);
                    }).always(function () {
                        subbtn.prop('disabled', false);
                    });
                });
            });
        </script>
    @endpush
@stop