<div class="row">
    <div class="col-md-4 col-sm-4">
        <div class="flistrigs">
            <ul>
                @foreach($specialities as $i=>$speciality)
                    <li>
                        <a data-id="{{$speciality->id}}" @if($i==0 ) class="active" @endif>
                            <div class="checbosx ">
                                <input type="checkbox" class="side-spec"
                                       data-id="{{$speciality->id}}"
                                       id="speciality_{{$speciality->id}}"
                                       @if(auth()->user()->hasSpeciality($speciality->id)) checked @endif>
                                <label for="speciality_{{$speciality->id}}"></label>
                                <span>{{get_text_locale($speciality,'name')}}</span>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>

        </div>
    </div>
    <div class="col-md-8 col-sm-8">

        @foreach($specialities as $i=>$speciality)
            <div class="flistrigs2  @if($i==0 ) active @endif" id="register2_{{$speciality->id}}">

                @if(isset($speciality->subSpecialties))
                    @foreach($speciality->subSpecialties as $subSpecialty)
                        @php
                            $json = json_encode(['specialty_id'=>$speciality->id,'sub_id'=>$subSpecialty->id]);
                            $input_id = 'ch'.$speciality->id.$subSpecialty->id;
                        @endphp
                        <div class="checbosx sub-group{{$speciality->id}}">
                            <input type="checkbox" data-parent="{{$speciality->id}}"
                                   data-id="{{$subSpecialty->id}}"
                                   class="sub-{{$speciality->id}} specialties"
                                   name="sub_specialties[]" value="{{$json}}" id="{{$input_id}}"
                                   @if(auth()->user()->hasSubSpeciality($subSpecialty->id)) checked @endif>
                            <label for="{{$input_id}}">{{get_text_locale($subSpecialty,'name')}}</label>
                        </div>
                    @endforeach
                @endif
            </div>
        @endforeach

    </div>
</div>