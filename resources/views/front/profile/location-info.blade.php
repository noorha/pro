<div class="header_fiserig4">
    <h3>@lang('إعدادات المكان')</h3>
</div>
@php
    $countries = get_all_countries();
@endphp

<div class="row">
    <div class="col-md-9">
        <div class="items_sdw">
            <h3 class="titl-loca">@lang('الدولة')</h3>
            <div class="inputsws bgw">
                <select id="country_id" name="country_id" class="selectpicker" required>
                    @foreach($countries as $country)
                        <option value="{{$country->id}}"
                                @if($user->country_id == $country->id) selected @endif >{{get_text_locale($country,'name')}}
                            <img src=""></option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="items_sdw">
            <h3 class="titl-loca">@lang('المدينة')</h3>
            <div id="cities_select" class="inputsws bgw" required>
                @include(view_front_layout().'cities-select',['items' => $user->getCountryCities(), 'user' => $user])
            </div>
        </div>
        <div class="items_sdw">
            <h3 class="titl-loca">@lang('تحديد العنوان')</h3>
            <div class="inputsws bgw nopadright">
                <input type="text" name="address" value="{{$user->address}}" id="us_restaurant-address" placeholder="@lang('العنوان')">
            </div>
        </div>

        <div class="items_sdw">
            <div id="map" style="width: 100%; height: 300px;"></div>

            <input type="hidden" name="latitude" id="us_restaurant-lat"/>
            <input type="hidden" name="longitude" id="us_restaurant-lon"/>


        </div>

    </div>
</div>