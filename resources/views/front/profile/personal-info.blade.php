<div class="header_fiserig4">
    <h3>@lang('البيانات الشخصية')</h3>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="items_sdw">
            <h3>@lang('الإسم الأول')</h3>
            <div class="inputsws nopadright bgw">
                <input type="text" name="first_name" value="{{$user->first_name}}" placeholder="@lang('الإسم الأول')" required>
            </div>
        </div>
        <div class="items_sdw">
            <h3>@lang('الإسم الأخير')</h3>
            <div class="inputsws nopadright bgw">
                <input type="text" name="last_name" value="{{$user->last_name}}" placeholder="@lang('الإسم الأخير')" required>
            </div>
        </div>
        <div class="items_sdw">
            <h3>@lang('الجنس')</h3>
            <div  class="chexboxs hw20">
                <div class="chexbox">
                    <input type="radio" name="gender" value="1" id="ch1" @if($user->gender == 1) checked @endif>
                    <label for="ch1">@lang('ذكر')</label>
                </div>
                <div class="chexbox">
                    <input type="radio" name="gender"  value="2"  @if($user->gender != 1) checked @endif  id="ch2">
                    <label for="ch2">@lang('أنثى')</label>
                </div>
            </div>
        </div>
        <div class="items_sdw">
            <h3>@lang('تاريخ الميلاد')</h3>
            <div class="inputsws bgw date">
                <input type="text" class="datetimepicker" value="{{$user->birth_date}}" name="birth_date" placeholder="@lang('تاريخ الميلاد')" required>
            </div>
        </div>
        <div class="items_sdw">
            <h3>@lang('نبذة عني')</h3>
            <div class="inputsws nopadright bgw">
                <textarea rows="8" placeholder="@lang('نبذة عني')"  name="description">{{$user->description}}</textarea>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <input type="hidden" id="photo" name="photo" value="{{$user->photo}}">
        <div class="items_sdw">
            <h3 class="text-center">@lang('الصورة الشخصية')</h3>
            <div class="upload_img">
                <div class="progress hidden">
                    <div id="progress_percent" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%
                    </div>
                </div>
                <div class="img_imgupldad">
                    <img id="balh" src="{{image_url($user->photo,'200x200')}}">
                </div>
            </div>
            <div class="inputsws bgw file dis-content">
                <input id="upfild" type="file" name="">
                <label class="new-rt-y" for="upfild">@lang('تحديد صورة')</label>
            </div>
        </div>

    </div>
</div>