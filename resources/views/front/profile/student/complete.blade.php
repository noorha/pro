@extends('front.layout.index',['sub_title' => __('إكمال الملف الشخصي للطالب')])
@section('main')
    @push('front_css')
    @endpush
    <style>
        .inputsws textarea, .inputsws.date.select button, .inputsws input {
            padding: 20px;
        }

        .inputsw2w.date::before, .inputsws.date::before {
            content: '';
        }

        .tiomso label {
            background: transparent !important;
            border: 0 !important;
        }

        .list_all_tabs_register.v3item li {
            width: 25% !important;
        }
    </style>
    <section class="meniasdmw">
        <div class="container">
            <div class="register">
                <div class="header_register">
                    <h3>@lang('إكمال الملف الشخصي')</h3>
                </div>
                <div class="list_all_tabs_register v3item" data-id="3">
                    <ul>
                        <li><a class="active" data-id="1" id="reis_menu_1"><span>1</span> @lang('البيانات الشخصية')</a></li>
                        <li><a class="" data-id="2" id="reis_menu_2"><span>2</span> @lang('إعدادات المكان')</a></li>
                        <li><a class="" data-id="3" id="reis_menu_3"><span>3</span> @lang('lang.specialities_that_you_are_interested')</a></li>
                        <li><a class="" data-id="4" id="reis_menu_4"><span>4</span> @lang('الشروط والأحكام')</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="se0">
        <div class="container">

            {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('profile.complete.post')]) !!}

            <input type="hidden" name="is_profile_completed" value="1">
            <div class="risgetser_sce studkap hobsts active" id="sregis_1">
                @include(view_front_profile().'personal-info' , ['user'=>auth()->user()])

                <div class="col-md-12">
                    <div class="inputswts rito">
                        <button data-id="2">@lang('التالي')</button>
                    </div>
                    {{--<div class="inputswts lito">--}}
                    {{--<button  data-id="2">تخطي</button>--}}
                    {{--</div>--}}
                </div>
            </div>

            <div class="risgetser_sce studkap hobsts" id="sregis_2">

                @include(view_front_profile().'location-info' , ['user'=>auth()->user()])

                <div class="col-md-12">
                    <div class="inputswts rito">
                        <button data-id="3">@lang('التالي')</button>
                    </div>
                    {{--<div class="inputswts lito">--}}
                    {{--<button  data-id="3">تخطي</button>--}}
                    {{--</div>--}}
                </div>
            </div>
            .
            <div class="risgetser_sce studkap hobsts" id="sregis_3">
                <div class="header_fiserig4">
                    <h3>@lang('التخصص')</h3>
                </div>

                @php
                    $specialities = get_all_specialities();
                @endphp
                <div class="list_tow_righst nopadw">
                    @include(view_front_profile().'specialty-info' , ['user'=> auth()->user() ,'specialities'=>$specialities])
                </div>
                <div class="col-md-12">
                    <div class="inputswts rito">
                        <button data-id="4" data-bl="4" class="btn-prevent">@lang('التالي')</button>
                    </div>
                    {{--<div class="inputswts lito">--}}
                    {{--<button data-id="4">تخطي</button>--}}
                    {{--</div>--}}
                </div>
            </div>

            <div class="risgetser_sce studkap hobsts" id="sregis_4">
                <h3>@lang('الشروط والأحكام')</h3>
                <div class="regis6">
                    <div class="regis6s">
                        <p>{!! get_constant_value(get_current_locale() == 'ar' ? 'student_policy' : 'student_policy_en') !!}</p>
                    </div>
                </div>
                <div class="item_chex">
                    <div class="checbosx">
                        <input type="checkbox" name="accept_privacy" id="accept_privacy">
                        <label for="accept_privacy">@lang('الموافقة على الشروط والأحكام')</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="inputswts rito">
                        <button data-id="last">@lang('حفظ')
                            <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                        </button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </section>

    @push('front_js')
        {!! HTML::script('/front/js/complete_form.js') !!}
        <script type="text/javascript">

            $('.specialties').on('change', function (event) {
                var parent_id = $(this).data('parent');
                var id = $(this).data('id');
                if (!$('#speciality_' + parent_id).is(":checked")) {
                    $('#speciality_' + parent_id).prop('checked', true);
                }
            });

            $('.side-spec').on('click', function (event) {
                var specialty_id = $(this).data('id');
                var collection = $('.sub-' + specialty_id);
                collection.each(function (object) {
                    $(this).prop('checked', $('#speciality_' + specialty_id).is(":checked"));
                });
            });

            function afterSuccess() {
                location.href = '{{lang_route('profile',[auth()->user()->id])}}'
            }

            // $('.btn-prevent').on('click',function (event) {
            //    event.preventDefault();
            // });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#balh').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                    var formData = new FormData();
                    formData.append('image', input.files[0]);
                    var progress = $('.progress');
                    var percent = $('#progress_percent');
                    $.ajax({
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    percent.css("width", percentComplete + '%');
                                    percent.attr('aria-valuenow', percentComplete);
                                    percent.html(percentComplete + '%');
                                    if (percentComplete === 100) {
                                        setTimeout(function () {
                                            progress.addClass('hidden');
                                        }, 500);
                                    }
                                }
                            }, false);
                            return xhr;
                        },
                        url: '/' + window.lang + '/image/upload',
                        type: 'POST',
                        data: formData,
                        beforeSend: function (xhr) {
                            progress.removeClass('hidden');
                            percent.css("width", '0%');
                            percent.attr('aria-valuenow', '0');
                            percent.html('0%');

                        },
                        success: function (response) {
                            $('#photo').val(response.file_name);
                        },
                        error: function (jqXhr) {
                            progress.addClass('hidden');
                            if (jqXhr.status === 400) {
                                customSweetAlert(
                                    'error',
                                    jqXhr.responseJSON.message,
                                    ''
                                );
                                return false;
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }

            }

            $("#upfild").change(function () {
                readURL(this);
            });
        </script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
    @endpush
@stop