@php
    $data['share_title'] = $user->getUserName();
    $data['share_image'] = image_url($user->photo);
    $data['share_description'] = $user->description;
@endphp
@extends('front.layout.index',array_merge(['sub_title' => __('عرض الملف الشخصي للطالب : ').$user->getUserName()],$data))
@section('main')
    @push('front_css')
        {!! HTML::style('front/css/jssocials-theme-flat.css') !!}
        {!! HTML::style('front/css/jssocials.css') !!}
    @endpush
    <section class="meniasdmw">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="img_profilesec">
                        <img src="{{image_url($user->photo,'300x300')}}">
                        {{--@if(!$user->isMyProfile())--}}
                        {{--<a href="#"><i class="icon-mail"></i>أرسل رسالة</a>--}}
                        {{--@endif--}}

                    </div>
                </div>
                <div class="col-md-9 col-sm-8">

                    <div class="text_profilesec">
<!--
                        <div class="text_profilesecs">
                            <div class="text_profilesec2">
                                
                            </div>
                        </div>
-->
                        <h3>{{$user->getUserName()}}</h3>
                        <p>{!! $user->description !!}</p>
                        <ul>
                            <li><i class="icon-placeholder2"></i>{{$user->getFullLocation()}}</li>
<!--
                            <li><i class="icon-phone-call2"></i> {{ $user->getMobileNo() }}</li>
                            <li><i class="icon-envelope"></i> {{$user->email}} </li>
-->
                        </ul>


                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="profisc2">
        <div class="container">
            <div class="header_profsc2">
                <h3>@lang('المجالات التي تم التدريب عليها')</h3>
            </div>

            @php
                $specialties = $user->specialties->unique();
            @endphp
            <div class="item_profsc2">
                <div class="row">
                    @if(isset($specialties) && $specialties->count() >0)
                        @foreach($specialties as $specialty)
                            <div class="col-md-4 col-sm-3">
                                <div class="f_itemprfolesc2">
                                    <div class="img_fpisec2">
                                        <img src="{{image_url($specialty->icon)}}">
                                    </div>
                                    <div class="text_fpisec2">
                                        <h3>{{$specialty->text}}</h3>
                                        <ul>
                                            @foreach($specialty->subSpecialties as $subSpecialty)
                                                @if($user->hasSubSpeciality($subSpecialty->id))
                                                    <li>
                                                        <a href="{{lang_route('student.request',[$user->id]).'?sub_specialty='.$subSpecialty->id}}">{{$subSpecialty->text}}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </section>
    @push('front_js')
        {!! HTML::script('/front/js/jssocials.js') !!}

        <script>
            $("#social_share").jsSocials({
                showLabel: false,
                shareIn: "popup",
                shares: ["twitter", "facebook", "linkedin", "email"]
            });
        </script>
    @endpush
@stop