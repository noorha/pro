@extends('front.layout.index',['sub_title' => __('صفحة الأرصدة للطالب')])
@section('main')
    @push('front_css')
    @endpush

    <section class="meniasdmw">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="monyf">
                        <div class="img_monyf">
                            <img src="/front/images/change.png">
                        </div>
                        <div class="text_monyf">
                            <h3>{{get_currency_value(auth()->user()->pending_cash)}}
                                <span>{{get_currency_text()}}</span></h3>
                            <p>@lang('الرصيد المعلق')</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="monuy2">
                        <div class="monyf hvlsw">
                            <div class="img_monyf">
                                <img src="/front/images/rich.png">
                            </div>
                            <div class="text_monyf">
                                <h3>{{get_currency_value(auth()->user()->walletAvailableCash())}}
                                    <span>{{get_currency_text()}}</span></h3>
                                <p>@lang('المبلغ القابل للسحب')</p>
                            </div>
                        </div>
                        <div class="monyf hvlsw smpad">
                            <a data-toggle="modal" data-target="#exampleModal3"><i
                                        class="icon-black-and-white-credit-cards"></i>@lang('إيداع رصيد')</a>
                            <a @if(auth()->user()->walletAvailableCash() >= 30) data-toggle="modal" data-target="#withdraw_modal" @else disabled  class="balance-pop-up" @endif><i class="icon-paypal2"></i>@lang('سحب رصيد')</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <h3>@lang('السجل المالي')</h3>
                @include(view_front_profile().'processes-table')

            </div>
        </div>
    </section>

    @include(view_front_modal().'withdraw')

    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                            class="icon-cross-symbol-1"></i></button>
                {!! Form::open(['method'=>'post','url'=>lang_route('addmoney.paypal')]) !!}
                <div class="div_mosw">
                    <h3>@lang('إيداع رصيد إلى حسابك')</h3>
                    <div class="tabos_modals">
                        <ul>
                            <li><a data-id="2" class="active"><i class="icon-paypal2"></i>@lang('بايبال')</a></li>
                            <li><a data-id="1"><i class="icon-black-and-white-credit-cards"></i>@lang('بطاقة إئتمان')
                                </a></li>

                        </ul>
                    </div>
                    <div class="itemspw " id="itemswaw_01">
                        <h2 class="text-center">@lang('قريباً')</h2>
                        {{--<div class="col-md-12">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<input type="number" name="" placeholder="المبلغ">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<input type="text" name="" placeholder="اسم حامل البطاقة">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<input type="text" name="" placeholder="رقم البطاقة">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<input type="text" name="" placeholder="رقم CVV">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<select class="selectpicker">--}}
                        {{--<option>YYYY</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3">--}}
                        {{--<div class="inputsw2w">--}}
                        {{--<select class="selectpicker">--}}
                        {{--<option>MM</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="itemspw active" id="itemswaw_02">
                        <div class="col-md-8">
                            <div class="inputsw2w">
                                <input type="email" name="email" placeholder="@lang('البريد الإلكتروني')" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="inputsw2w">
                                <input id="amount" type="number" name="value" placeholder="@lang('المبلغ')" required>
                            </div>
                        </div>
                    </div>
                    <ul>
{{--                        @if(get_current_currency() !== 'USD')--}}
                            <li>$ <span id="amount_in_dollar">0</span> @lang(' المبلغ بالدولار') </li>
                        {{--@endif--}}
                        {{--<li>الحد الأعلى للسحب <span>99999$</span></li>--}}
                    </ul>
                    <div class="inputsw2w btsw">
                        <button>@lang('إيداع')</button>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                            class="icon-cross-symbol-1"></i></button>
                <div class="div_mosw">
                    <h3>تمت العملية بنجاح!</h3>
                    <span class="succses">
                  <i class="icon-success"></i>
              </span>
                    <ul>
                        <li>تم شحن رصيدك بقيمة <span>9999$</span></li>
                    </ul>
                    <div class="inputsw2w btsw">
                        <button data-dismiss="modal">الملف المالي</button>
                    </div>
                </div>
            </div>
        </div>
    </div>




    @push('front_js')

        {!! HTML::script('/front/js/form.js') !!}
        <script>
            $(document).on('click','.balance-pop-up',function (event) {
                customSweetAlert('info','',window.lang === 'ar' ? 'يجب أن يكون الحد الأدنى لرصيدك أكبر من 30 دولار' :' Your balance must be greater than 30$ ');
            });

            function afterSuccess() {

            }

            var toDollar = parseFloat('{{convert_to_USD(1)}}');
            $(document).on('keyup', '#amount', function (event) {
                var am = parseInt($(this).val());
                if (!am) am = 0;
                $('#amount_in_dollar').text(parseInt((am * 100 * toDollar ) / 100));
            });
        </script>
    @endpush
@stop