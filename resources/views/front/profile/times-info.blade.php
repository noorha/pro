@php
    $days = get_all_days();
@endphp
<div class="all_favaila">
    @foreach($days as $i=>$day)
        <div class="f_avila  @if($i==0 || auth()->user()->hasDay($day->id)) active @endif">
            <div class="checbosx">
                <input type="checkbox" class="days-select" name="days[]" value="{{$day->id}}" id="day{{$day->id}}"
                       @if($i==0 || auth()->user()->hasDay($day->id)) checked @endif >
                <label for="day{{$day->id}}">{{$day->text}}</label>
            </div>
            {{--<div class="item_avail">--}}
                {{--@php--}}
                    {{--$trainingTimesByDay = auth()->user()->trainingTimesByDay($day->id);--}}
                {{--@endphp--}}
                {{--<div class="item_avail1">--}}
                    {{--@if(isset($trainingTimesByDay) && $trainingTimesByDay->count()>0)--}}
                        {{--@foreach($trainingTimesByDay->get() as $item)--}}
                            {{--<div class="item_avail1f">--}}
                                {{--<div class="inputsws bgw nopadrights active">--}}
                                    {{--<input type="text" name="start_{{$day->id}}[]"--}}
                                           {{--value="{{format_24_to_12($item->start_time)}}" class="datetimepickers"--}}
                                           {{--placeholder="@lang('التوقيت')" required>--}}
                                {{--</div>--}}
                                {{--<div class="inputsws bgw nopadrights active">--}}
                                    {{--<input type="text" name="end_{{$day->id}}[]"--}}
                                           {{--value="{{format_24_to_12($item->end_time)}}" class="datetimepickers"--}}
                                           {{--placeholder="@lang('التوقيت')" required>--}}
                                {{--</div>--}}
                                {{--<div class="remo_thoisw">--}}
                                    {{--<a><i class="icon-cross-symbol"></i></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--@else--}}
                        {{--<div class="item_avail1f">--}}
                            {{--<div class="inputsws bgw nopadrights active">--}}
                                {{--<input type="text" name="start_{{$day->id}}[]" class="datetimepickers"--}}
                                       {{--placeholder="@lang('التوقيت')" required>--}}
                            {{--</div>--}}
                            {{--<div class="inputsws bgw nopadrights active">--}}
                                {{--<input type="text" name="end_{{$day->id}}[]" class="datetimepickers"--}}
                                       {{--placeholder="@lang('التوقيت')" required>--}}
                            {{--</div>--}}
                            {{--<div class="remo_thoisw">--}}
                                {{--<a><i class="icon-cross-symbol"></i></a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<p data-id="{{$day->id}}">@lang('إضافة توقيت جديد')</p>--}}
            {{--</div>--}}
        </div>
    @endforeach

</div>