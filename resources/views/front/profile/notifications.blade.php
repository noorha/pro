@extends('front.layout.index',['sub_title' => __('صفحة الإشعارات')])
@section('main')
    @push('front_css')

    @endpush

    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <div class="col-md-4 col-md-offset-4">
                    <!-- <p>المزيد من الخبراء المؤهلين والمهنيين من أي مكان آخر، وعلى استعداد للمساعدة.</p> -->
                </div>
                <div class="lik_tabo">

                    @if(isset($notifications) && $notifications->count() >0)
                        <div class="head_tabo">
                            <h3>@lang('صفحة الإشعارات')</h3>
                        </div>
                        <div class="all_fstnotif">
                            @foreach($notifications as $notification)

                                @php
                                    $data = json_decode($notification->data)
                                @endphp

                                <div class="stnotif">
                                    <a href="#">
                                        {{--<div class="img_stnotif">--}}
                                        {{--<img src="/front/images/Layer 8 copy 4.png">--}}
                                        {{--</div>--}}
                                        <div class="text_stnotif">
                                            <h3>
                                                <span>
                                            <i class="icon-clock-circular-outline"></i>
                                                    {{get_date_from_timestamp($notification->created_at)}}
                                        </span>
                                            </h3>
                                            @if(get_current_locale() == 'ar')
                                                <p>{{$data->text}}</p>
                                            @else
                                                <p>{{$data->text_en}}</p>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <ul>
                            <li>
                               <h3 style="font-size: 20px" class="text-center"> @lang('لا يوجد إشعارات') </h3>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </section>


    @push('front_js')

    @endpush
@stop