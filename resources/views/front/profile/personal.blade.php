<div class="row">
    <div class="col-md-7">
        <div class="fiserig4">
            <div class="header_fiserig4">
                <h3>@lang('البيانات الشخصية')</h3>
            </div>
            <div class="items_dahslw">
                <div class="inputswss colortextword">
                    <input type="text" name="first_name" value="{{$user->first_name}}"
                           required placeholder="@lang('الإسم الأول')">
                </div>

                <div class="inputswss colortextword">
                    <input type="text" name="last_name" value="{{$user->last_name}}"
                           required placeholder="@lang('الإسم الأخير')">
                </div>
                <div class="inputswss colortextword">
                    <select name="gender" class="selectpicker" required>
                        <option>@lang('الجنس')</option>
                        <option @if($user->isMale())selected @endif value="1">@lang('ذكر')</option>
                        <option @if($user->isFemale())selected @endif value="2">@lang('أنثى')
                        </option>
                    </select>
                </div>
                <div class="inputswss calenders colortextword">
                    <input type="text" class="datetimepicker" name="birth_date"
                           value="{{$user->birth_date}}"
                           placeholder="@lang('تاريخ الميلاد')">
                </div>

                <div class="inputswss">
                    <textarea rows="8" name="description" placeholder="@lang('نبذة عني')" >{{$user->description}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="fiserig4">
            <input type="hidden" id="photo" name="photo" value="{{$user->photo}}">
            <div class="items_sdw">
                <div class="header_fiserig4">
                    <h3>@lang('الصورة الشخصية')</h3>
                </div>
                <div class="upload_img">
                    <div class="progress hidden">
                        <div id="progress_percent"
                             class="progress-bar progress-bar-success progress-bar-striped active"
                             role="progressbar"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             style="width:0%">0%
                        </div>
                    </div>
                    <div class="img_imgupldad">
                        <img id="balh" src="{{image_url($user->photo,'200x200')}}">
                    </div>
                </div>
                <div class="inputsws bgw file dis-content">
                    <input id="upfild" type="file" name="">
                    <label class="new-rt-y" for="upfild">@lang('تحديد صورة')</label> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="inputswts ">
            <button class="pull-right">@lang('حفظ')
                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
            </button>
        </div>
    </div>
</div>
