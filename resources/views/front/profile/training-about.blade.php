<div class="sumonsws">
  
  
    <div class="items_sdw">
        <label>@lang('عنوان المؤهل الدراسى')</label>
        <div class="inputsws nopadright bgw tiomso ">
            <input type="text" name="qualified_title" placeholder="@lang('عنوان المؤهل الدراسى')"
                   value="{{($user->userinfo) ? $user->userinfo->qualified_title : ''}}" required>
        </div>
   </div>


   <div class="items_sdw">
        <label>@lang('الخبرات')</label>
        <div class="inputsws nopadright bgw tiomso ">
            <input type="text" name="experience" id="training_experience" 
            value="{{($user->userinfo) ? $user->userinfo->experience : ''}}" required>
        </div>
   </div>


   <div class="items_sdw">
        @php
          $userinfo_file = \App\UserInfoFile::where('user_id',$user->id)->get();
        @endphp
        <label>@lang('المرفقات')</label>
        <div class="inputsws nopadright bgw tiomso ">
            <input type="file" name="training_file[]" id="training_file" multiple  
                  @if(count($userinfo_file) == 0) required @endif>
        </div>
<br/>
       
        @if(count($userinfo_file) > 0)
          @foreach($userinfo_file as $itr=>$item_file)
               <a href="{{image_url($item_file->file)}}" target="_blank">@lang('المرفق')_{{++$itr}}</a> 
               &nbsp;&nbsp;&nbsp;&nbsp;
          @endforeach     
        @endif
   </div>

<div class="clearfix"></div>
<br/>

   <div class="items_sdw">
        <label>@lang('رابط الفيديو')</label>
        <div class="inputsws nopadright bgw tiomso ">
            <input type="url" name="video_link" placeholder="@lang('رابط الفيديو')"
            value="{{($user->userinfo) ? $user->userinfo->video_link : ''}}" required>
        </div>
   </div>


</div>
