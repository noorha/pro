@if($user->hasDeleteRequest())
    <div class="inputswts pull-left">
        <a href="javascript:;" class="pull-right cancel-delete" data-url="{{lang_route('front.profile.cancel-delete')}}">@lang('lang.cancel_delete_account')</a>
    </div>
@else
    <div class="header_fiserig4">
        <h3>@lang('lang.are_you_sure_to_delete')</h3>
    </div>
    <div class="text_urlsw">
        <p>@lang('lang.delete_account_text')</p>
    </div>

    <div class="inputswts pull-left">
        <a class="pull-right delete"  href="javascript:;" data-url="{{lang_route('front.profile.delete')}}">@lang('lang.delete_confirmation')</a>
    </div>
@endif