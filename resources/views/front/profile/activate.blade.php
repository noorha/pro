@extends('front.layout.index',['sub_title' => __('تفعيل الحساب')])
@section('main')
    @push('front_css')
    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    {!! Form::open(['id'=>'form','method'=>'post','url'=> lang_route('profile.activate')]) !!}
                    <div class="logdata">
                        <h3>@lang('تفعيل الحساب')</h3>

                        <div class="inputsws ">
                            <input type="text" name="code" placeholder="@lang('كود التفعيل')" required>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="inputsws btnward">
                                <button>@lang('إرسال')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
{{--        {!! HTML::script('/front/js/form.js') !!}--}}
    @endpush
@stop