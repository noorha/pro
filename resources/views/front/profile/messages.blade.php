@extends('front.layout.index',['sub_title' => __('عرض جميع الرسائل')])
@section('main')
    @push('front_css')
        <style>
            .chat_avatar {
                border-radius: 25px;
            }
            .vue-content-placeholders-heading__img{
                margin-left: 15px !important;
            }
            .un-read-count{
                float: left;
                background-color: #de5698;
                color: white;
                width: 20px;
                height: 20px;
                text-align: center;
                border-radius: 25px;
                padding-top: 2px;
            }
        </style>
    @endpush
    <div id="app">
        <messaging :init_data="{{json_encode(['id'=>auth()->user()->id,'type'=>auth()->user()->type,'hash'=>@$chat['hash'],'locale'=>get_current_locale()])}}"></messaging>
    </div>
    {!! HTML::script(asset('/js/messaging-chat.js')) !!}
{{--    {!! HTML::script(asset('/js/app.js')) !!}--}}
    @push('front_js')

    @endpush
@stop