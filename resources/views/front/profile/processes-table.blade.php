<div class="tablse_mny">
    @php
        $financialProcesses = auth()->user()->financialProcesses()->paginate(7);
    @endphp
    @if(isset($financialProcesses) && $financialProcesses->count() >0)

        <div class="table-responsive">
            <table>
                <thead>
                <tr>
                    <td>@lang('رقم العملية')</td>
                    <td>@lang('تفاصيل العملية')</td>
                    <td>@lang('المبلغ')</td>
                    <td>@lang('التاريخ')</td>
                </tr>
                </thead>
                <tbody>
                @foreach($financialProcesses as $financialProcess)
                    <tr>
                        <td>{{$financialProcess->id}}</td>
                        <td><span>{{get_financial_process_status($financialProcess)}}</span></td>
                        <td>{{($financialProcess->isCredit() ? '+ ' : '- ') .get_currency_value($financialProcess->value).' '.get_currency_text()}}</td>
                        <td>{{get_date_from_timestamp($financialProcess->created_at)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagsw">
            <nav aria-label="Page navigation example">
                {{$financialProcesses->links()}}
            </nav>
        </div>
    @else
        <p style="font-size: 20px;color: #a9a5a5;">@lang('لا يوجد معاملات مالية')</p>
    @endif
</div>