@extends('front.layout.index',['sub_title' => __('عرض الطلاب')])
@section('main')
    @push('front_css')
        {!! HTML::style('/front/css/jquery-ui.css') !!}
        {!! HTML::style('/front/css/bootstrap-slider.min.css') !!}
        {!! HTML::style('/front/css/fontawesome-stars.css') !!}
        <style>
            .br-theme-fontawesome-stars .br-widget a{
                font-size: 25px !important; ;
            }
            .br-theme-fontawesome-stars .br-widget a:after {
                color: #a5a5a5 ;
            }
            .se20 {
                padding: 30px 0;
                background: #f1f1f1;
            }

            button:disabled {
                background: #999 !important;
            }
        </style>
    @endpush
    <section class="se20">
        <div class="container">
            <div class="request1">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        {!! Form::open(['id'=>'form','method'=>'GET','url'=>lang_route('student.search')]) !!}
                        <div class="rig_requests1 rig_requests_nu">
                            <div class="f_rig_request1">
                                <h3>@lang('فلترة')</h3>
                                <input placeholder="@lang('lang.search_by_std_name')" id="teacher" name="teacher">
                                @php
                                    $countries = get_all_countries();
                                @endphp
                                <div class="inputswss">
                                    <h4>@lang('الدولة')</h4>
                                    <select name="country" id="country" class="selectpicker">
                                        <option value="">@lang('الكل')</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}"
                                                    @if(isset($inputs['country']) && $inputs['country'] == $country->id) selected @endif>{{get_text_locale($country,'name')}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="inputswss">
                                    <h4>@lang('المدينة')</h4>
                                    <div id="city_div">
                                        <select name="city_id" id="city_id" class="selectpicker">
                                            <option value="">@lang('الكل')</option>
                                        </select>
                                    </div>
                                </div>

                                @php
                                    $specialities = get_all_specialities();
                                @endphp

                                <div class="inputswss">
                                    <h4>@lang('التخصص')</h4>
                                    <select name="specialities" id="specialities" class="selectpicker">
                                        <option value="">@lang('الكل')</option>
                                        @foreach($specialities as $speciality)
                                            <option value="{{$speciality->id}}"  {{is_speciality_selected($inputs,$speciality)}} >{{get_text_locale($speciality,'name')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{----}}
                                {{--<div class="inputswss">--}}
                                    {{--<h4>@lang('التخصص')</h4>--}}
                                    {{--<select class="selectpicker" id="specialities" name="specialities"--}}
                                            {{--title="@lang('إختيار تخصص')">--}}
                                        {{--<option value="">@lang('الكل')</option>--}}
                                        {{--@foreach($specialities as $speciality)--}}
                                            {{--<option  {{is_speciality_selected($inputs,$speciality)}} value="{{$speciality->id}}" selected>{{get_text_locale($speciality,'name')}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}


                                @php
                                    $subSpecialities = get_all_sub_specialities();
                                @endphp
                                <div class="inputswsss">
                                    <h4>@lang('الموضوع')</h4>
                                    <select name="subSpecialty" id="sub_specialty" autofocus="autofocus"
                                            autocorrect="off" autocomplete="off" placeholder="@lang('إختر الموضوع')">
                                        <option value=""></option>
                                        @foreach($subSpecialities as $subSpeciality)
                                            <option value="{{$subSpeciality->id}}" @if(isset($inputs['subSpecialty']) && $inputs['subSpecialty'] == $subSpeciality->id) selected @endif
                                                data-specialty="{{$subSpeciality->specialty_id}}"    data-alternative-spellings="{{$subSpeciality->name_en}} |{{$subSpeciality->name}}">{{get_text_locale($subSpeciality,'name')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="f_rig_request1 chskw">
                                    <h4>@lang('الجنس')</h4>
                                    <div class="checbosx">
                                        <input type="checkbox" class="gender" name="gender[]" id="gender1" value="1"
                                               checked>
                                        <label for="gender1">@lang('ذكر')</label>
                                    </div>
                                    <div class="checbosx">
                                        <input type="checkbox" class="gender" name="gender[]" id="gender2" value="2"
                                               checked>
                                        <label for="gender2">@lang('أنثى')</label>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="f_rig_request1">--}}
                                {{--<p>@lang('عمر المدرس :') 10 - 90 </p>--}}
                                {{--<input id="age_range" name="age_range" class="ex12c0" type="text"/>--}}
                            {{--</div>--}}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div id="items">
                            @include(view_front().'student.paginate',['students'=>$students])
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
        {!! HTML::script('front/js/jquery.barrating.js') !!}

        <script>
            window.specialities = '{{(isset($inputs['specialities'])) ?  $inputs['specialities'] : ''}}';
            window.max_range = parseInt('{{get_currency_value(500)}}');

            $(function () {
                function ratingEnable() {
                    $('.example-fontawesome').barrating({
                        theme: 'fontawesome-stars',
                        showSelectedRating: false
                    });
                    var currentRating = $('.example-fontawesome-o').data('current-rating');
                    $('.stars-example-fontawesome-o .current-rating')
                        .find('span')
                        .html(currentRating);

                    $('.stars-example-fontawesome-o .clear-rating').on('click', function (event) {
                        event.preventDefault();

                        $('.example-fontawesome-o')
                            .barrating('clear');
                    });

                    $('.example-fontawesome-o').barrating({
                        theme: 'fontawesome-stars-o',
                        showSelectedRating: false,
                        initialRating: currentRating,
                        onSelect: function (value, text) {
                            if (!value) {
                                $('.example-fontawesome-o')
                                    .barrating('clear');
                            } else {
                                $('.stars-example-fontawesome-o .current-rating')
                                    .addClass('hidden');

                                $('.stars-example-fontawesome-o .your-rating')
                                    .removeClass('hidden')
                                    .find('span')
                                    .html(value);
                            }
                        },
                        onClear: function (value, text) {
                            $('.stars-example-fontawesome-o')
                                .find('.current-rating')
                                .removeClass('hidden')
                                .end()
                                .find('.your-rating')
                                .addClass('hidden');
                        }
                    });
                }

                function ratingDisable() {
                    $('select').barrating('destroy');
                }

                $('.rating-enable').click(function (event) {
                    event.preventDefault();
                    ratingEnable();
                    $(this).addClass('deactivated');
                    $('.rating-disable').removeClass('deactivated');
                });
                $('.rating-disable').click(function (event) {
                    event.preventDefault();
                    ratingDisable();
                    $(this).addClass('deactivated');
                    $('.rating-enable').removeClass('deactivated');
                });
                ratingEnable();
            });



        </script>
        {!! HTML::script('/front/js/jquery-ui.min.js') !!}
        {!! HTML::script('/front/js/bootstrap-slider.min.js') !!}
        {!! HTML::script('/front/js/jquery.select-to-autocomplete.js') !!}
        {!! HTML::script('/front/js/teacher-search.js') !!}
    @endpush
@stop