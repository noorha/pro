@extends('front.layout.index',['sub_title' => __('إنشاء طلب تدريب')])
@section('main')
    @push('front_css')
        {!! HTML::style('/front/css/jquery-ui.css') !!}
        {!! HTML::style('/front/css/bootstrap-slider.min.css') !!}
        <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

        <style>
            .dropzone {
                min-height: 150px;
                border: 1px solid rgba(114, 66, 89, 0.3);
                background: white;
                padding: 20px 20px;
                border-radius: 25px;
            }

            .dropzone.dz-clickable .dz-message, .dropzone.dz-clickable .dz-message * {
                cursor: pointer;
                text-align: center;
                color: #de5698;
            }

        </style>
    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="logdata colsw bgwith">
                        <h3>@lang('إنشاء طلب تدريب')</h3>
                        <div class="item_taps">
                            <div class="items_taps">
                                <div class="tabs_itemsw active" id="tsp_1">
                                    {!! Form::open(['id'=>'form' ,'method'=>'post','url'=>lang_route('student.request.send',['id'=>$teacher->id])]) !!}
                                    <input type="hidden" name="student_id" value="{{auth()->user()->id}}">
                                    @php
                                        $sub_specialities = $teacher->subSpecialties()->get();
                                        if ( session()->has('sub_specialty')){
                                            $sub_speciality = session()->get('sub_specialty');
                                            session()->forget('sub_specialty');
                                        }
                                    @endphp
                                    <div class="inputsws bgw pres seeclt">
                                        <select name="sub_id" id="sub_specialty" class="selectpicker" required>
                                            <option value="" disabled selected hidden>@lang('إختر الموضوع')</option>
                                            @foreach($sub_specialities as $i=>$subSpeciality)
                                                <option value="{{$subSpeciality->id}}"
                                                        @if(isset($sub_speciality) && $sub_speciality == $subSpeciality->id || ((isset($sub_specialty_input)) && $subSpeciality->id == (int)$sub_specialty_input))  selected @endif>{{get_text_locale($subSpeciality,'name')}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="inputsws bgw pres seeclt">
                                        <select name="payment_method" id="payment_method" class="selectpicker itemmasp"
                                                required>
                                            <option value="" selected disabled
                                                    hidden>@lang('إختر طريقة الدفع') </option>
                                            <option value="website"> @lang('باي بال') </option>
                                            <option value="cash"> @lang('كاش') </option>
                                        </select>
                                    </div>
                                    <div class="inputsws bgw pres seeclt">
                                        <select id="type" name="type" class="selectpicker itemmasp" required>
                                            @if($teacher->isWorkOnline())
                                                <option value="1">@lang('أونلاين')</option>
                                            @endif
                                            @if($teacher->isWorkInterview())
                                                <option value="2">@lang('مقابلة')</option>
                                            @endif
                                        </select>
                                    </div>

                                    {{--<div class="inputsws bgw pres seeclt">--}}
                                    {{--<select id="type" name="type" class="selectpicker itemmasp" required>--}}
                                    {{--@if($teacher->isWorkOnline())--}}
                                    {{--<option value="1">@lang('أونلاين')</option>--}}
                                    {{--@endif--}}
                                    {{--@if($teacher->isWorkInterview())--}}
                                    {{--<option value="2">@lang('مقابلة')</option>--}}
                                    {{--@endif--}}
                                    {{--</select>--}}
                                    {{--</div>--}}

                                    <div class="inputsws bgw tiomso question">
                                        <input type="number" id="hour_no" name="hour_no"
                                               placeholder="@lang('عدد الساعات')" required>
                                        <label id="hour_no_total"> @lang('السعر الإجمالي')
                                            0 {{get_currency_text()}} </label>
                                    </div>


                                    <div id="coupon_main_cont" class="inputsws">
                                        <span>
                                            <a id="coupon_btn"
                                               href="javascript:;"> @lang('lang.do_you_have_promo_code') </a></span>
                                        <div style="display: none;margin-top: 10px;"
                                             class="inputsws bgw textarea btnward">
                                            <input type="text" id="promo_code"
                                                   placeholder="@lang('lang.enter_promo_code')" style="width: 70%">
                                            <button id="promo_code_btn" style="width: 25%;margin-top: 0 !important;"
                                                    data-url="{{lang_route('front.coupon.check')}}"
                                                    type="button">@lang('lang.check')
                                                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                            </button>
                                            <span id="coupon_status"></span>
                                        </div>
                                    </div>
                                    <div id="discount_container" class="inputsws hidden">
                                        <h4>  @lang('lang.total_after_discount') <span
                                                    id="value_after_discount">0</span> {{get_current_currency()}} </h4>
                                    </div>
                                    <input type="hidden" id="coupon_code" name="coupon_code">

                                    <input type="hidden" name="total" value="0" id="total">
                                    @php
                                        $availableDays = $teacher->availableDays()
                                    @endphp

                                    {{--@if($teacher->contacts()->count() > 0)--}}
                                    {{--<h4>@lang('lang.contacts_available')</h4>--}}
                                    {{--@foreach($teacher->contacts as $contact)--}}
                                    {{--<div class="itmasksw_rest contact-div">--}}
                                    {{--<img class="img-contact" src="{{url('/front/img/'.$contact->key.'.svg')}}">--}}
                                    {{--{{$contact->name}}--}}
                                    {{--</div>--}}
                                    {{--@endforeach--}}
                                    {{--@endif--}}
                                    <div class="inputsws bgw">
                                        <h3>@lang('lang.contacts_available')</h3>
                                        <div class="chexboxs hw20">
                                            @foreach($teacher->contacts as $contact)
                                                <div class="checbosx radio-btn">
                                                    <input type="radio" name="contact" value="{{$contact->id}}" required checked id="contact_ch{{$contact->id}}">
                                                    <label for="contact_ch{{$contact->id}}">{{$contact->name}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="inputsws bgw">
                                        <h3>@lang('أيام التدريب المتاحة')</h3>
                                        <div class="chexboxs hw20">
                                            @foreach($availableDays as $day)
                                                <div class="checbosx">
                                                    <input type="checkbox" name="days[]" value="{{$day->id}}" required
                                                           checked id="ch{{$day->id}}">
                                                    <label for="ch{{$day->id}}">{{get_text_locale($day,'name')}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <inpu>
                                        <div class="inputsws bgw textarea file">
                                            <span>@lang('إضافة ملاحظات')</span>
                                            <textarea class="new-select-cs" rows="8" name="note"
                                                      placeholder="@lang('إضافة ملاحظات')"></textarea>
                                        </div>
                                        <input type="hidden" name="files_array" id="files">


                                        {!! Form::close() !!}

                                        <div class="inputsws bgw file">
                                            <span>@lang('إرفاق ملفات')</span>
                                            {!! Form::open(['class'=>'dropzone','id'=>'image-upload' ,'method'=>'post','url'=>lang_route('file.upload')]) !!}
                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                                <div class="dz-default dz-message" data-dz-message data-dz-default>
                                                    <span>قم برفع ملفاتك هنا</span>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>


                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="inputsws btnward">
                                                <button id="submit_btn">@lang('تقديم طلب')
                                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="reqsits">
                        <div class="ite_profd_rest">
                            <div class="img_itprfr">
                                <img src="{{image_url($teacher->photo,'90x90')}}">
                            </div>
                            <div class="itmasksw_rest">
                                <h3>{{$teacher->getUserName()}} </h3>
                                @if($teacher->isWorkOnline())
                                    <span class="itemsokwdp">{{__('أونلاين').' : '. get_currency_value($teacher->onlinePrice()) .' '.get_currency_text().'/'.__('ساعة')}}</span>
                                @endif
                                @if($teacher->isWorkInterview())
                                    <span class="itemsokwdp"
                                          style="margin-top: 10px">{{__('مقابلة').' : '.get_currency_value($teacher->interviewPrice()).' '.get_currency_text().'/'.__('ساعة')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="contacts-available">
                            @if($teacher->contacts()->count() > 0)
                                <h4>@lang('lang.contacts_available')</h4>
                                @foreach($teacher->contacts as $contact)
                                    <div class="itmasksw_rest contact-div">
                                        <img class="img-contact" src="{{url('/front/img/'.$contact->key.'.svg')}}">
                                        {{$contact->name}}
                                    </div>
                                @endforeach
                            @endif
                        </div>


                        <div class="ntex">
                            <h3>@lang('عن المدرب')</h3>
                            <p>{{$teacher->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
        {!! HTML::script('/front/js/form.js') !!}
        {!! HTML::script('/front/js/jquery-ui.min.js') !!}
        {!! HTML::script('/front/js/bootstrap-slider.min.js') !!}
        {!! HTML::script('/front/js/dropzone.js') !!}
        {!! HTML::script('/front/js/jquery.select-to-autocomplete.js') !!}

        <script>
            function afterSuccess() {
                location.href = '{{lang_route('student.training.requests')}}'
            }

            var online_price = parseFloat('{{get_currency_value($teacher->onlinePrice())}}');
            var interview_price = parseFloat('{{get_currency_value($teacher->interviewPrice())}}');


        </script>
        {!! HTML::script('/front/js/create-training-request.js') !!}
    @endpush
@stop