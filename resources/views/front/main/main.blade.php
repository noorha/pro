@extends('front.layout.index',['sub_title' => __('الرئيسية')])
@section('main')
    @push('front_css')
        {{--{!! HTML::style('/front/css/jquery-ui.css') !!}--}}
        {{--{!! HTML::style('/front/css/plyr.css') !!}--}}
        {{--{!! HTML::script('/front/js/plyr.min.js') !!}--}}
        {{--{!! HTML::script('/front/js/plyr.polyfilled.min.js') !!}--}}
    @endpush
    @php
        $main_count = main_count()
    @endphp
    <section class="se2 new-secation-sta">

        <div id="scroll-to" class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="lisnts2_ner new-stast wow fadeIn">
                        <div class="img_lisnts3">
                            <img alt="@lang('عدد المدربين/ المدرسين')" src="/front/images/stude2.png">
                        </div>
                        <div class="new-stastices">
                            <h4 class="count">{{$main_count['teachers']}}</h4>
                            <span>@lang('عدد المدربين/ المدرسين')</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="lisnts2_ner new-stast wow fadeIn">
                        <div class="img_lisnts3">
                            <img alt="@lang('عدد الطلاب')" src="/front/images/teher1.png">
                        </div>
                        <div class="new-stastices">
                            <h4 class="count"> {{$main_count['students']}}</h4>
                            <span>@lang('عدد الطلاب')</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="lisnts2_ner new-stast wow fadeIn">
                        <div class="img_lisnts3">
                            <img alt="@lang('الدورات التعليمية')" src="/front/images/course3.png">
                        </div>
                        <div class="new-stastices">
                            <h4 class="count">{{$main_count['courses']}}</h4>
                            <span>@lang('الدورات التعليمية')</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="lisnts2_ner new-stast wow fadeIn">
                        <div class="img_lisnts3">
                            <img alt="@lang('طلبات تم تنفيذها')" src="/front/images/orde-leson.png">
                        </div>
                        <div class="new-stastices">
                            <h4 class="count">{{$main_count['requests']}}</h4>
                            <span>@lang('طلبات تم تنفيذها')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @php
        $specialities = get_all_specialities()
    @endphp

    @if(isset($specialities) && $specialities->count()>0)
        <section class="sect-main-cat" id="particles-jss">
            <div class="container">
                <h1 class="new-tit-cate">@lang('التخصصات الرئيسية')</h1>
                <div class="row">
                    @foreach($specialities as $i=>$speciality)
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <div class="lisnts2 wow fadeIn" data-wow-delay="{{0.2*($i+1)}}s">
                                <a href="{{lang_route('teacher.search',['specialities',$speciality->id])}}">
                                    <div class="img_lisnts2">
                                        <img alt="@lang('التخصصات الرئيسية')" src="{{image_url($speciality->icon)}}">
                                    </div>
                                    <p>{{get_text_locale($speciality,'name')}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @php
        $sliders = get_main(2);
    @endphp
    @if(isset($sliders) && $sliders->count()>0)
        <section class="se2 new-back-o">
            <div class="container">
                <div class="row">
                    <div class="owl-carousel2 owl-theme">
                        @foreach($sliders as $slider)
                            <div class="item">
                                <div class="col-md-6">
                                    <div class="lisnts25 wow fadeIn">
                                        <div class="img_lisnts25">
                                            <img alt="image-post" src="{{image_url($slider->photo)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="lisnts25 wow fadeIn">
                                        <h1>{{get_text_locale($slider,'title')}}</h1>
                                        <p>{!! get_text_locale($slider,'text') !!}</p>
                                        <a href="{{$slider->link}}">@lang('المزيد')</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="sec3 bagpsw" id="particles-js">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                    <div class="header_sc3  wow fadeInUp" data-wow-delay="0.2s">
                        <p>@lang('التخصصات الأكثر طلباً')</p>
                        <div class="inputs_halv">
                            <div class="inputsw">
                                <select id="speciality" class="selectpicker">
                                    <option value="all">@lang('الكل')</option>
                                    @foreach($specialities as $speciality)
                                        <option value="{{$speciality->id}}">{{ get_text_locale($speciality,'name') }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list_s3cs" id="items">
                @include(view_front().'layout.sub-speciality',['subSpecialties'=> get_selected_sub_specs(12)])
            </div>
        </div>
    </section>

    @php
        $q = get_main(1);
        $item = isset($q->first()->id) ? $q->first() : null;
    @endphp

    @if(isset($item))
        <section class="se2">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="lisnts25 wow fadeIn">
                            <div class="img_lisnts25">
                            <div class="vid-sec">
                                <video poster="@if(get_current_locale() == 'ar')  /front/images/Cakkpture.PNG @else{{image_url('hala_video_thump.jpeg')}}  @endif " id="player" playsinline controls>
                                    <source src="{{ (get_current_locale() == 'ar') ? lang_url('file/hala_pro_video.mp4') :  lang_url('file/hala_pro_video_en.mp4')}}" type="video/mp4">
                                    <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default>
                                </video>
                                {{--<script>const player = new Plyr('#player');</script>--}}

                                {{--<video  autoplay="" muted="" controls="false" style="width: 100%;">--}}
                                        {{--<source src="{{ (get_current_locale() == 'ar') ? lang_url('file/hala_pro_video.mp4') :  lang_url('file/hala_pro_video_en.mp4')}}" type="video/mp4">--}}
                                    {{--</video>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="lisnts25 wow fadeIn">
                            <h3>{{get_text_locale($item,'title')}}</h3>
                            <p>{!! get_text_locale($item,'text')!!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="sec2">
        <div class="container">
            @php
                $partners = get_all_partners();
            @endphp
            @if(isset($partners) && $partners->count() >0)
                <div class="list_comp">
                    <h1 class="new-tit-media">@lang('هلا برو في الميديا')</h1>
                    <div class="row">
                        <div class="owl-carousel owl-theme">
                            @foreach($partners as $partner)
                                <div class="item">
                                    <div class="flist_comp wow fadeInUp" data-wow-delay="0.2s">
                                    <span>
                                      <a href="{{$partner->link}}" target="_blank"><img alt="@lang('هلا برو في الميديا')" src="{{image_url($partner->photo)}}"></a>
                                    </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>


    @if(isset($most_teachers) && $most_teachers->count()>0)
        <section class="sec4 sec-op">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                        <div class="header_sc3">
                            <p>@lang('lang.trending')</p>
                            <h3>@lang('lang.tutors_trainers')</h3>
                        </div>
                    </div>
                </div>
                <div class="list_mor">
                    <div class="row">
                        <div class="owl-carousel3 owl-theme">
                            @foreach($most_teachers as $teacher)
                                    <div class="item first_lmor">
                                        <a href="{{lang_route('profile',['id'=>$teacher->id])}}">
                                            <div class="img_flmor">
                                                <img src="{{image_url($teacher->photo,'100x100')}}">
                                            </div>
                                            <h3>{{$teacher->getUserName()}}</h3>
                                            @include(view_front().'layout.rating',$teacher)
                                            <span class="new-cuntey-tech"><img src="{{$teacher->getCountryFlag()}}"> {{$teacher->getCountryName()}}</span>
                                        </a>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @push('front_js')
        {!! HTML::script(asset('/front/js/home.min.js')) !!}

        {{--{!! HTML::script('front/js/particles.min.js') !!}--}}
        {{--{!! HTML::script('front/js/app.js') !!}--}}
        {{--{!! HTML::script('/front/js/jquery-ui.min.js') !!}--}}
        {{--{!! HTML::script('/front/js/bootstrap-slider.min.js') !!}--}}
        {{--{!! HTML::script('/front/js/jquery.select-to-autocomplete.js') !!}--}}
        {{--{!! HTML::script('/front/js/main.js') !!}--}}

    @endpush
@stop
