@extends('front.layout.index',['sub_title' => __('التسجيل')])
@section('main')
    @push('front_css')


    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('login')]) !!}
                    <div class="logdata">
                        <h3>@lang('تسجيل الدخول')</h3>
                        <p>@lang('نعمل في هلا برو على توفير تدريب مميز وإحترافي للجميع، نحن في هلا برو نقدم أفضل المدربين و المواهب و المدرسين المؤهلين في كافة المجالات!')</p>
                        <div class="inputsws email">
                            <input type="email" name="email" placeholder="@lang('البريد الإلكتروني')" required>
                        </div>
                        <div class="inputsws pass">
                            <input type="password" name="password" placeholder="@lang('كلمة المرور')" required>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="inputsws btnward">
                                <button >@lang('تسجيل الدخول')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        {{--<p>لا تمتلك حساب قم <a href="#">بالتسجيل الان</a></p>--}}
                        <p><a href="{{lang_route('forget')}}"> @lang('نسيت كلمة المرور ؟')</a></p>
                       <p>
                        <a href="{{ url('/auth/redirect/linkedin') }}" class="btn btn-linkedin" class="btn btn-linkedin"><i class="fa fa-linkedin"></i> Linked in</a>
                        <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-facebook" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                       </p> 
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>

    @push('front_js')
        <script>
            jQuery("#form").validate({
                success: function (element) {
                    $(element).each(function () {
                        $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).remove();
                    });
                },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    if (elem.hasClass("selectpicker")) {
                        error.appendTo(element.parent());
                        element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
                    } else {
                        error.insertAfter(element);
                        element.removeClass('has-success').addClass('has-error');
                    }
                    error.css('color', 'red');
                }
            });
        </script>
        {{--{!! HTML::script('/front/js/register.js') !!}--}}
    @endpush
@stop