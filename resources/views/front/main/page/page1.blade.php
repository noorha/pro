@extends('front.layout.index',['sub_title' =>'FAQ'])
@section('main')
    @push('front_css')
    <style>



    </style>
    @endpush

    <section class="blog_posts">
        <div class="container">
        <div id="myCarousel" class="carousel slide carousel_news" data-ride="carousel" data-interval="false">
    
    <div class="carousel-inner">


        <div class="item active">
            <img src="/front/images/d63Tz.jpg">
            <div class="carousel-caption">
                <p>نشر موقع "هاكرنون" الأمريكي تقريرا تحدث فيه عن موجة تطبيقات الويب التقدمية التي تغزو مجال تكنولوجيا الهاتف المحمول على حساب تطبيقي "أب ستور" و"غوغل بلاي".

وقال الموقع، في تقريره الذي ترجمته "عربي21"، إنه قد حان الوقت لموجة جديدة من تكنولوجيا الهاتف المحمول لتحتل الصدارة على حساب أب ستور وغوغل بلاي.</p>
            
                <a href="" class="btn-news-more">قراءة المزيد</a>
            </div>
        </div><!-- End Item -->

        <div class="item">
            <img src="/front/images/ERdpU.jpg">
            <div class="carousel-caption">
                <p>ما يزال خبراء الطيران يقدمون نظرياتهم وتفسيراتهم لحادث اختفاء الطائرة الماليزية (إم.إتش 370) وهي في طريقها من كوالامبور إلى بكين في الثامن من مارس عام 2014.

ومرت أكثر من خمس سنوات على اختفاء الطائرة التي كانت تقل 239 شخصا (227 من الركاب و12 من أفراد الطاقم)، إلا أن الحادث ما يزال يشكل أكبر لغز في عالم الطيران.</p>
                <a href="" class="btn-news-more">قراءة المزيد</a>
            </div>
        </div><!-- End Item -->

        <div class="item ">
            <img src="/front/images/AoDWr.jpg">
            <div class="carousel-caption">
                <p>يدخل الأسرى المضربين عن الطعام في سجون الاحتلال يومهم الثاني من الإضراب عن الطعام، بعد إعلان 150 أسيرا الشروع بالإضراب منذ مساء أمس الاثنين بعد فشل الحوار مع إدارة السجون وتعنتها في الاستجابة لمطالبهم.</p>
                <a href="" class="btn-news-more">قراءة المزيد</a>
            </div>
        </div><!-- End Item -->

        <div class="item">
            <img src="/front/images/KDhAZ.jpg">
            <div class="carousel-caption">
                <p>
                    أجبرت قوات الاحتلال، اليوم الثلاثاء، مواطنا على هدم منزله ذاتيا في منطقة بير عونة بمدينة بيت جالا غرب بيت لحم، بحجة عدم الترخيص.

                    وكانت سلطات الاحتلال سلمت المواطن أيمن زرينة إخطارا بهدم منزله المكون من طابقين في السابع من الشهر الجاري، وأمهلته حتى الغد، وإلا فإنها ستهدمه، وترغمه على دفع تكاليف الهدم.</p>
                <a href="" class="btn-news-more">قراءة المزيد</a>
            </div>
        </div><!-- End Item -->

        <div class="item">
            <img src="/front/images/gGKo1.jpg">
            <div class="carousel-caption">
                <p>أصدرت وزارة التربية والتعليم العالي اليوم الثلاثاء، توضيحات بخصوص المتقدمين لوظيفة معلم لدى وزارة التربية والتعليم العالي.

وكانت الوزارة قد أعلنت عن حاجتها للتعاقد لشغل الوظائف التدريسية للعام 2019- 2020، بالتعاون مع ديوان الموظفين العام، من كافة التخصصات التعليمية ولمختلف المراحل.</p>
                <a href="" class="btn-news-more">قراءة المزيد</a>
            </div>
        </div><!-- End Item -->

        

    </div><!-- End Carousel Inner -->


    <ul class="list-group col-sm-4">
        <li data-target=".carousel_news" data-slide-to="0" class="list-group-item active">
            <h4>تطبيقات الويب التقدمية.. هل حان وداع "آب ستور وغوغل بلاي"؟</h4>
            <div class="post_meta_top">
                <span class="post_meta_category">
                    <a href="">تعليم, رياضة</a>
                </span>
                <span class="post_meta_date">NOV 13, 2020</span>
            </div> 
        </li>
        <li data-target=".carousel_news" data-slide-to="1" class="list-group-item">
            <h4>فرضية جديدة لاختفاء الطائرة الماليزية.. هذه التفاصيل</h4>
            <div class="post_meta_top">
                <span class="post_meta_category">
                    <a href="">تعليم, رياضة</a>
                </span>
                <span class="post_meta_date">NOV 13, 2020</span>
            </div> 
        </li>

        <li data-target=".carousel_news" data-slide-to="2" class="list-group-item ">
            <h4>معركة الكرامة 2 تدخل يومها الثاني وانضمام المزيد من الأسرى</h4>
            <div class="post_meta_top">
                <span class="post_meta_category">
                    <a href="">تعليم, رياضة</a>
                </span>
                <span class="post_meta_date">NOV 13, 2020</span>
            </div>    
        </li>
        <li data-target=".carousel_news" data-slide-to="3" class="list-group-item">
            <h4>الاحتلال يجبر المواطن أيمن زرينة على هدم منزله في بيت جالا</h4>
            <div class="post_meta_top">
                <span class="post_meta_category">
                    <a href="">تعليم, رياضة</a>
                </span>
                <span class="post_meta_date">NOV 13, 2020</span>
            </div> 
        </li>
        <li data-target=".carousel_news" data-slide-to="4" class="list-group-item">
            <h4>توضيح هام من وزارة التربية والتعليم بشأن امتحان توظيف المعلمين</h4>
            <div class="post_meta_top">
                <span class="post_meta_category">
                    <a href="">تعليم, رياضة</a>
                </span>
                <span class="post_meta_date">NOV 13, 2020</span>
            </div> 
        </li>
        
    </ul>

    <div class="carousel-controls">
                          <a class="left carousel-control" href=".carousel_news" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                          </a>
                          <a class="right carousel-control" href=".carousel_news" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                          </a>
                      </div>

</div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row cardPost">
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                   <a href=""> <img src="/front/images/news.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                    <a href=""><img src="/front/images/news2.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى  التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                    <a href=""><img src="/front/images/news3.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى  التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                   <a href=""> <img src="/front/images/news4.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى  التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                   <a href=""> <img src="/front/images/news5.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى  التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <article>
                                <div class="post_img">
                                   <a href=""> <img src="/front/images/news6.jpg" alt=""></a>
                                </div>
                                <div class="post_text">
                                    <div class="post_meta_top">
                                        <span class="post_meta_category">
                                            <a href="">تعليم, رياضة</a>
                                        </span>
                                        <span class="post_meta_date">NOV 13, 2020</span>
                                    </div>
                                    <h5 class="post_title">
                                        <a href="blog-single-post.html">هذا النص هو مثال لنص يستبدل في نفس المساحة
                                        </a>
                                    </h5>
                                    <div class="post_content">
                                        <p> لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى  التطبيق.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                    <nav class="pagination_post">
                        <ul class="pagination">

                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span>
                                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                    </span>
                                    <span class="sr-only">التالي</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-4 mt-30px mt-lg-0">
                    <div class="widget">
                        <form class="search-post">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="بحث ..." name="q">
                                <div class="input-group-btn">
                                    <button class="btn" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="bg-color-grayflame widget pt-30px pb-30px px-30px">

                        <h5 class="widget-title">التصنيفات</h5>
                        <ul class="category-list list-unstyled mb-0">
                            <li>
                                <a href="#">
                                    علوم
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    تعليم
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    رياضة
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    ترفية
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    تكنولوجيا
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="widget bg-color-grayflame pt-30px pb-30px px-30px">
                        <h5 class="widget-title">الأكثر مشاهدة</h5>
                        <ul class="list-unstyled post-simple-list mb-0">
                            <li class="media">
                                <p class="number_post">1</p>
                                <div class="media-body">
                                    <a href="" class="media-title">هذا النص هو مثال لنص يستبدل في نفس المساحة</a>
                                    <div class="media_post">
                                        <span class="media_category">
                                            <a href="">علوم , تعليم</a>
                                        </span>
                                        <span class="media_date">NOV 13, 2019</span>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <p class="number_post">2</p>
                                <div class="media-body">
                                    <a href="" class="media-title">هذا النص هو مثال لنص يستبدل في نفس المساحة</a>
                                    <div class="media_post">
                                        <span class="media_category">
                                            <a href="">علوم , تعليم</a>
                                        </span>
                                        <span class="media_date">NOV 13, 2019</span>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <p class="number_post">3</p>
                                <div class="media-body">
                                    <a href="" class="media-title">هذا النص هو مثال لنص يستبدل في نفس المساحة</a>
                                    <div class="media_post">
                                        <span class="media_category">
                                            <a href="">علوم , تعليم</a>
                                        </span>
                                        <span class="media_date">NOV 13, 2019</span>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <p class="number_post">4</p>
                                <div class="media-body">
                                    <a href="" class="media-title">هذا النص هو مثال لنص يستبدل في نفس المساحة</a>
                                    <div class="media_post">
                                        <span class="media_category">
                                            <a href="">علوم , تعليم</a>
                                        </span>
                                        <span class="media_date">NOV 13, 2019</span>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <p class="number_post">5</p>
                                <div class="media-body">
                                    <a href="" class="media-title">هذا النص هو مثال لنص يستبدل في نفس المساحة</a>
                                    <div class="media_post">
                                        <span class="media_category">
                                            <a href="">علوم , تعليم</a>
                                        </span>
                                        <span class="media_date">NOV 13, 2019</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="widget_subscripe">
                        <h3>الاشتراك</h3>
                        <p>هذا النض هو مثال لنص يستبدل في نفس المساحة</p>
                        <div class="form-group ">
                            <form action="" class="search-post">
                                <div class="input-group mb-3 d-flex">
                                    <input type="email" name="email" class="form-control " placeholder="البريد الالكتروني">
                                    <div class="input-group-subsc">
                                        <button type="submit" class="btn rounded-50 form-custom-btn">اشترك</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @push('front_js')
        <script>
    
	
	var clickEvent = false;
	$('.carousel_news').carousel({
		interval:   4000	
	}).on('click', '.list-group li', function() {
			clickEvent = true;
			$('.list-group li').removeClass('active');
			$(this).addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.list-group').children().length -1;
			var current = $('.list-group li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.list-group li').first().addClass('active');	
			}
		}
		clickEvent = false;
    });
    
    jQuery(window).load(function() {
 
 /*
     Stop carousel
 */
 $('.carousel').carousel('pause');

});

$(document).ready(function(){
    //Event for pushed the video
    $('#your-carousel-id').carousel({
        pause: true,
        interval: false
    });
});
        </script>
    @endpush
@stop