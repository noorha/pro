@extends('front.layout.index',['sub_title' => get_text_locale($page,'title')])
@section('main')
    @push('front_css')
    @endpush

    <section class="se20">
        <div class="container">
            <div class="item_hedaw">
                <h3>{{ get_text_locale($page,'title') }}</h3>
            </div>
            <div class="abo">
                <div class="fiabo">

                    <p>{!! get_text_locale($page,'text') !!}</p>
                </div>
            </div>

        </div>
    </section>

    @push('front_js')
    @endpush
@stop