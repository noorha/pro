@extends('front.layout.index',['sub_title' => get_text_locale($page,'title')])
@section('main')
    @push('front_css')
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
                async defer>
        </script>
        <style>
            /*.capatcha-form {*/
            /*display: flex;*/
            /*justify-content: center;*/
            /*}*/

      
        </style>
    @endpush
    <section class="se40 new-stlye">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logdata colsw contact-us">
                        {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('contact.msg')]) !!}

                        <div class="item_taps">
                            <div class="items_taps">
                                <div class="tabs_itemsw active">
                                    <div class="inputsws bgw user ">
                                        <input type="text" name="name" placeholder="@lang('الإسم') " required>
                                    </div>
                                    <div class="inputsws bgw email">
                                        <input type="email" name="email" placeholder="@lang('البريد الإلكتروني')" required>
                                    </div>
                                    <div class="inputsws bgw help">
                                        <input type="text" name="subject" placeholder="@lang('عنوان الرسالة')" required>
                                    </div>
                                    <div class="inputsws bgw chat">
                                        <textarea rows="10" name="text" placeholder="@lang('نص الرسالة')" required ></textarea>
                                    </div>

                                    <div class="capatcha-form" id="html_element"></div>

                                    <div class="col-md-6 col-md-offset-3">
                                        <div style="margin-top: 35%;" class="inputsws btnward">
                                            <button>@lang('إرسال')
                                                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="registerS margen-0">
                        <h3>{{ get_text_locale($page,'title')}}</h3>
                        <p>{!! get_text_locale($page,'text')  !!}</p>
                        @php
                            $socials = get_socials();
                        @endphp
                        <ul class="soial ">
                            @foreach($socials as $social)
                                @if(isset($social->item )&& $social->item->link  != '#')
                                    <li><a target="_blank" href="{{$social->item->link}}" class="{{$social->key}}"><i
                                                    class="{{get_social_icon($social->key)}}"></i></a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
        {{--{!! HTML::script('/front/js/form.js') !!}--}}
        {{--<script>--}}
        {{--function afterSuccess() {--}}
        {{--location.reload();--}}
        {{--}--}}
        {{----}}
        {{----}}
        {{----}}
        {{--</script>--}}

        <script>
            var widgetId1;
            var is_checked = false;
            var onloadCallback = function () {
                widgetId1 = grecaptcha.render('html_element', {
                    'sitekey': '6Lcx3oQUAAAAAMmk3KcPwNDUc0PvL3IHQve2RaVv',
                    'callback': verifyCallback,
                });
            };
            var verifyCallback = function () {
                is_checked = true;
            };
            var form = $('#form');
            form.validate({
                success: function (element) {
                    $(element).each(function () {
                        $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).remove();
                    });
                },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    if (elem.hasClass("selectpicker")) {
                        error.appendTo(element.parent());
                        element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
                    } else {
                        error.insertAfter(element);
                        element.removeClass('has-success').addClass('has-error');
                    }
                    error.css('color', 'red');
                },
                submitHandler: function (f, e) {
                    e.preventDefault();
                    if (is_checked) {
                        $('.lds-ellipsis').removeClass('hidden');
                        $('.span-txt').addClass('hidden');
                        var formData = new FormData(form[0]);
                        var url = form.attr('action');
                        var redirectUrl = form.attr('to');
                        var _method = form.attr('method');
                        $.ajax({
                            url: url,
                            method: _method,
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                $('.lds-ellipsis').addClass('hidden');
                                $('.span-txt').removeClass('hidden');
                                if (response.status) {
                                    customSweetAlert(
                                        'success',
                                        response.message,
                                        response.item,
                                        function (event) {
                                            location.reload();
                                        }
                                    );
                                } else {
                                    customSweetAlert(
                                        'error',
                                        response.message,
                                        response.errors_object
                                    );
                                }
                            },
                            error: function (jqXhr) {
                                $('.lds-ellipsis').addClass('hidden');
                                $('.span-txt').removeClass('hidden');
                                getErrors(jqXhr, '/' + window.lang);
                            }
                        });
                    }
                    else {
                        customSweetAlert('warning','',window.lang ==='ar' ? 'الرجاء التحقق من  Captcha' : 'Please Check Captcha');
                    }
                }

            });

        </script>
    @endpush
@stop