@extends('front.layout.index',['sub_title' =>__('lang.how_it_works')])
@section('main')
    @push('front_css')
       <style>
 
       </style>
    @endpush

    <section class="howWorks">
        <div class="section-title">
            <h3>@lang('lang.how_it_works')</h3>
        </div>
        <div class="howWorksContent">
            <div class="container">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="content">
                                <h3 class="title">@lang('lang.teacher_how')</h3>
                                <p class="text">@lang('lang.how_teacher_text')</p>
                                <img src="/front/images/Teacher.png" alt="">
                                <button class="more tetcher">@lang('lang.more_how')</button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="content">
                                <h3 class="title">@lang('lang.student_how')</h3>
                                <p class="text">@lang('lang.how_student_text')</p>
                                <img src="/front/images/Student.png" alt="">
                                <button class="more student">@lang('lang.more_how')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="moreContentWorks teacher">
            <div class="container">
                <div class="text-right clearfix mb-20">
                    <button class="back pull-right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>@lang('lang.back')</button>
                </div>
                    <ul class="step_Works">
                        <li>
                            <div class="image">
                                <img src="/front/images/checklist.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="title">@lang('lang.stu_reg')</p>
                                    <p class="info">@lang('lang.stu_reg_txt')</p>
                                </div>
                            </div>
                          
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/file.png" alt="" >
                            </div>
                            
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="title">@lang('lang.stu_sea')</p>
                                    <p class="info">@lang('lang.stu_sea_txt')</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/search.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="title">@lang('lang.stu_sel')</p>
                                    <p class="info">@lang('lang.stu_sel_txt')</p>
                                </div>
                            </div>
                    
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/presentation2.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                <p class="title">@lang('lang.stu_boo')</p>
                                <p class="info">@lang('lang.stu_boo_txt')</p>
                                </div>
                            </div>
                       
                        </li>
                    </ul>
            </div>
        </div>
        <div class="moreContentWorks student">
            <div class="container">
                <div class="text-right clearfix mb-20">
                    <button class=" back pull-right"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>@lang('lang.back')</button>
                </div>
                <ul class="step_Works">
                        <li>
                            <div class="image">
                                <img src="/front/images/note.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="info">@lang('lang.teacher_reg')</p>
                                </div>
                            </div>
                          
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/smartphone.png" alt="" >
                            </div>
                            
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="info">@lang('lang.teacher_act')</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/job.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="info">@lang('lang.teacher_bro')</p>
                                </div>
                            </div>
                    
                        </li>
                        <li> 
                            <div class="image">
                                <img src="/front/images/alarm.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                <p class="info">@lang('lang.teacher_not')</p>
                                </div>
                            </div>
                       
                        </li>

                        <li>
                            <div class="image">
                                <img src="/front/images/online-class.png" alt="" >
                            </div>
                            
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="info">@lang('lang.teacher_don')</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/elearning.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                    <p class="info">@lang('lang.teacher_rat')</p>
                                </div>
                            </div>
                    
                        </li>
                        <li>
                            <div class="image">
                                <img src="/front/images/money.png" alt="" >
                            </div>
                            <div class="d-flex align-items-center justify-content-center h-70">
                                <div>
                                <p class="info">@lang('lang.teacher_ear')</p>
                                </div>
                            </div>
                       
                        </li>
                    </ul>
            </div>
        </div>
    </section>
    @push('front_js')
    <script>
        $('.more.tetcher').click(function(){
            

            $('.howWorksContent').slideUp(300)

            $('.moreContentWorks.student').slideDown(300)
        });

        $('.more.student').click(function(){
            $('.howWorksContent').slideUp(300)

            $('.moreContentWorks.teacher').slideDown(300)


        });


        $('.back').click(function(){
           
            $('.howWorksContent').slideDown(300)

            $('.moreContentWorks').slideUp(300)
        })
    </script>
    @endpush
@stop