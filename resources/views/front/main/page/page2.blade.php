@extends('front.layout.index',['sub_title' =>'FAQ'])
@section('main')
    @push('front_css')
    <style>
     
    </style>
    @endpush
    <section class="blog_posts">
        <div class="container">
            <div class="row single_news">
                <div class="col-md-12">
                    <div class="post_img mb-0">
                        <img src="/front/images/single-news.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-10 offset-lg-1">
                        <article>
                            <div class="post_text">
                                <div class="post_meta_top">
                                    <span class="post_meta_category">
                                        <a href="">تعليم, رياضة</a>
                                    </span>
                                    <span class="post_meta_date">NOV 13, 2020</span>
                                </div>
                                <h2 class="post_title mb-20px">
                                    <a href="">الطقس: الحرارة ترتفع وتصبح أعلى من معدلها بـ3 درجات</a>
                                </h2>
                                <div class="post_content">
                                    <p>توقعت دائرة الأرصاد الجوية أن يكون الجو اليوم الثلاثاء، صافياً بوجه عام، ويطرأ ارتفاع على درجات الحرارة لتصبح أعلى من معدلها السنوي العام بحدود 3 درجات مئوية، والرياح جنوبية غربية إلى جنوبية شرقية خفيفة إلى معتدلة السرعة، والبحر خفيف ارتفاع الموج.
                                    </p>

                                    <p>ويوم غد الأربعاء، يكون الجو غائماً جزئياً وبارداً نسبياً إلى بارد، ويطرأ انخفاض ملموس على درجات الحرارة لتصبح أدنى من معدلها السنوي العام بحدود 3 درجات مئوية، وتكون فرصة ضعيفة في ساعات الصباح لسقوط أمطار خفيفة متفرقة فوق بعض المناطق، والرياح غربية إلى شمالية غربية خفيفة إلى معتدلة السرعة، والبحر خفيف ارتفاع الموج.
                                    </p>

                                    <p>وبعد غد الخميس، يكون الجو غائما جزئيا إلى صاف ، ويطرأ ارتفاع على درجات الحرارة مع بقائها أقل من معدلها السنوي العام ، والرياح شمالية غربية خفيفة إلى معتدلة السرعة والبحر خفيف إلى متوسط ارتفاع الموج.
                                    </p>

                                    <p>
                                    ويوم الجمعة، يكون الجو صافيا بوجه عام، ويطرأ ارتفاع ملموس على درجات الحرارة لتصبح أعلى من معدلها السنوي العام، والرياح شمالية غربية إلى شمالية شرقية خفيفة إلى معتدلة السرعة، والبحر خفيف إلى متوسط ارتفاع الموج.</p>
                                </div>
                                <div class="post_meta_bottom mt-40px mb-30px">
                                    <div class="text-lg-left">
                                        <ul class="blog-post-tags mb-25px">
                                            <li>
                                                <a href="#" rel="tag">Creative</a>
                                            </li>
                                            <li>
                                                <a href="#" rel="tag">Design</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="separator-line mb-25px"></div>
                                    <div class="navigation post-navigation">
                                        <div class="row align-items-center nav-links">
                                            <div class="col-lg-6 ">
                                                <div class="nav-previous">
                                                    <div class="nav-subtitle"> الخبر السابق </div>
                                                    <div class="nav-title">
                                                        <a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> منظمة حقوقية: فلسطيني تعرض للخطف     </a></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-left">
                                                <div class="nav-next ">
                                                    <div class="nav-subtitle">الخبر التالي</div>
                                                    <div class="nav-title"><a href="#">اسعار العملات مقابل الشيكل

                                                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator-line mt-10px"></div>
                                </div>
                                <div class="blog-post-comments">
                                    <div class="comments-area mb-45px">
                                        <ol class="comment-list">
                                            <li class="comment" id="comment-1">
                                                <article id="div-comment-1" class="comment-body">
                                                    <header class="comment-meta">
                                                        <img class="avatar" src="/front/images/Layers8.png" alt="img" width="75" height="75">
                                                        
                                                        <a>
                                                            <span>December 21, 2017 at 2:34 am - </span>
                                                        </a>
                                                        <cite>
                                                            <a href="#">Belal Alayoubi</a>
                                                        </cite>
                                                    </header>
                                                    <section class="comment-content comment">
                                                        <p>الرئيسية  أخبار فلسطين الاسرى يواصلون اضرابهم لليوم الثاني في معركة الكرامة "2".</p>
                                                    </section>
                                                </article>
                                            </li>
                                        </ol>
                                    </div>
                                    <h5 class="comment-reply-title mb-4">اترك تعليقا</h5>

                                    <form class="form-fields-bg-gray">
                                        <div class="form-row">
                                            <div class="form-group ">
                                                <textarea rows="8" name="comment" class="form-control" placeholder="تعليق"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-xs-12">
                                                <button type="submit" class="btn btn-comment">ارسال</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </article>

                </div>
            </div>
        </div>
    </section>
    @push('front_js')

    @endpush
@stop