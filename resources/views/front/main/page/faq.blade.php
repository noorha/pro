@extends('front.layout.index',['sub_title' =>__('lang.faq')])
@section('main')
    @push('front_css')

    @endpush
    <section class="main_faq">
        <div class="container">
            <h3 class="title_section_faq">@lang('lang.faq')</h3>
            <div class="row">
                <div class="col-lg-10 m-auto float-none">
                    <div class="panel-group" id="accordion">
                        @foreach($items as $i=>$item)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle @if($i == 0 )  active  @endif " data-toggle="collapse"
                                           data-parent="#accordion" href="#collapse{{$i}}">{{$item->question}}</a>
                                    </h4>
                                </div>
                                <div id="collapse{{$i}}" class="panel-collapse collapse @if($i == 0 )  in  @endif">
                                    <div class="panel-body">
                                        {!! $item->replay !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>


    @push('front_js')
        <script>
            $('.accordion-toggle').click(function () {
                $(this).removeClass('active');
                $(this).parents('.panel').siblings().find('.panel-heading a').removeClass('active');
            })
        </script>
    @endpush

@stop
