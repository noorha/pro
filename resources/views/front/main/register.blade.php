@extends('front.layout.index',['sub_title' => __('التسجيل')])
@section('main')
    @push('front_css')

    @endpush
    <style>
        .input2div .inputsw input {
            padding: 14px 50px;
        }
    </style>
    <section class="se20 new-sec-30">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="registerS">
                        <!--                        <img src="/front/images/logo-astazk-co.png">-->
                        <h3>@lang('تدريب مميز وإحترافي للجميع')</h3>
                        <p> @lang('نعمل في هلا برو على توفير تدريب مميز وإحترافي للجميع، نحن في هلا برو نقدم أفضل المدربين و المواهب و المدرسين المؤهلين في كافة المجالات!')</p>
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="/front/images/discuss-issue.png">
                                    <span>@lang('الإجابة على الأسئلة السريعة')</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="/front/images/presentation.png">
                                    <span>@lang('إختيار المدرب الخاص بك')</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="/front/images/reading.png">
                                    <span>@lang('حجز الدرس الخاص بك')</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('post.register',['type'=>$type])]) !!}
                    <div class="logdata">
                        <h3>@lang('سجل الآن')</h3>

                        <input type="hidden" name="">
                        <input type="hidden" name="type" value="{{(isset($type) && $type == 'student') ? '1' : '2'}}">
                        <div class="inputsws user">
                            <input type="text" name="first_name" placeholder="@lang('الإسم الأول')" required>
                        </div>
                        <div class="inputsws user">
                            <input type="text" name="last_name" placeholder="@lang('الإسم الأخير')" required>
                        </div>
                        <div class="inputsws email">
                            <input type="email" name="email" placeholder="@lang('البريد الإلكتروني')" required>
                        </div>
                        <div class="inputsws pass">
                            <input id="password" type="password" name="password" placeholder="@lang('كلمة المرور')"
                                   required>
                        </div>
                        <div class="inputsws pass">
                            <input type="password" name="password_confirmation" placeholder="@lang('تأكيد كلمة المرور')"
                                   required>
                        </div>

                        @php
                            $countries = get_all_countries();
                        @endphp

                        <div class="input2div sings">
                            <div class="inputsw mobile new-mobile">
                                <input type="text" name="mobile" placeholder="@lang('رقم الجوال')" required>
                            </div>
                            <div class="inputsw">
                                <select name="nationality_id" class="selectpicker">
                                    @foreach($countries as $country)
                                        <option data-content="<img class='drop-down-img' src='{{image_url($country->icon,'20x20')}}'> {{$country->code}}"
                                                @if($country->id == 6) selected
                                                @endif  value="{{$country->id}}">{{$country->code}}  </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="inputsws btnward">
                                <button>@lang('تسجيل')
                                    <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                                </button>
                            </div>
                        </div>
                        <p>@lang('لدي حساب') -
                            <a href="{{lang_route('login')}}">@lang('تسجيل الدخول')</a>
                        </p>

                        @if(Request::is('en/register/student') || Request::is('ar/register/student'))
                        <p>
                          <a href="{{ url('/auth/redirect/linkedin') }}" class="btn btn-linkedin" class="btn btn-linkedin"><i class="fa fa-linkedin"></i> Linked in</a>
                           <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-facebook" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                        </p>
                        @endif
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
    @push('front_js')
        <script>
            jQuery("#form").validate({
                rules: {
                    password: {
                        minlength: 6
                    },
                    password_confirmation: {
                        minlength: 6,
                        equalTo: "#password"
                    }
                },
                success: function (element) {
                    $(element).each(function () {
                        $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
                        $(this).remove();
                    });
                },
                errorPlacement: function (error, element) {
                    var elem = $(element);
                    if (elem.hasClass("selectpicker")) {
                        error.appendTo(element.parent());
                        element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
                    } else {
                        error.insertAfter(element);
                        element.removeClass('has-success').addClass('has-error');
                    }
                    error.css('color', 'red');
                },
                submitHandler: function (f, e) {
                    e.preventDefault();
                    $('.upload-spinn').removeClass('hidden');
                    var formData = new FormData($('#form')[0]);
                    var url = $(this).attr('action');
                    var to = $(this).attr('to');
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'json',
                        success: function (response) {
                            $('.upload-spinn').addClass('hidden');
                            if (response.status) {
                                customSweetAlert(
                                    'success',
                                    response.message,
                                    response.item,
                                    function (event) {
                                        window.location = '/' + window.lang + '/'
                                    }
                                );
                            } else {
                                customSweetAlert(
                                    'error',
                                    response.message,
                                    response.errors_object
                                );
                            }
                        },
                        error: function (jqXhr) {
                            $('.upload-spinn').addClass('hidden');
                            getErrors(jqXhr, '/login');
                        }
                    });
                }

            });

            $('.selectpicker').selectpicker({
                showIcon: true,
                showImage: true
            });

        </script>
        {{--{!! HTML::script('/front/js/register.js') !!}--}}
    @endpush
@stop