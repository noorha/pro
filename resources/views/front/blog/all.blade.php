@extends('front.layout.index',['sub_title' =>__('lang.blog')])
@section('main')
    @push('front_css')

    @endpush

    <section class="blog_posts">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row cardPost">
                        @foreach($items as $item)
                            <div class="col-md-6 col-lg-6">
                                <article>
                                    <div class="post_img">
                                        <a href="{{lang_route('blog.view',[$item->id])}}"> <img
                                                    src="{{image_url($item->image,'360x266')}}" alt=""></a>
                                    </div>
                                    <div class="post_text">
                                        <div class="post_meta_top">
<!--
                                        <span class="post_meta_category">
                                            @foreach($item->categories as  $category)
                                                <a href="{{lang_route('blog.all',[$category->id])}}">{{$category->text}}</a>
                                            @endforeach
                                        </span>
-->
                                            <span class="post_meta_date">{{date_localized($item->created_at)}}</span>
                                        </div>
                                        <h5 class="post_title">
                                            <a href="{{lang_route('blog.view',[$item->id])}}">
                                                {{string_limit($item->title,150)}}
                                            </a>
                                        </h5>
                                        <div class="post_content">
                                            <p>{{string_limit(strip_tags($item->text),100)}}</p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                        @if($items->count() == 0)
                            <h3 class="text-center"> @lang('lang.no_posts')</h3>
                        @endif
                    </div>
                    <nav class="pagination_post">
                        {!! $items->links() !!}
                    </nav>
                </div>
                <div class="col-lg-4 mt-30px mt-lg-0">
                    <div class="widget">
                        <form class="search-post" action="{{url()->current()}}">
                            <div class="input-group">
                                <input type="text" class="form-control"  placeholder="@lang('lang.search')" name="q" value="{{@$q}}">
                                <div class="input-group-btn">
                                    <button class="btn" type="submit">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="bg-color-grayflame widget pt-30px pb-30px px-30px">

                        <h5 class="widget-title">@lang('lang.categories')</h5>
                        <ul class="category-list list-unstyled mb-0">
                            <li>
                                <a href="{{lang_route('blog.all')}}">
                                    @lang('lang.all')
                                </a>
                            </li>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{lang_route('blog.all',[$category->id])}}">
                                        {{$category->text}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="widget bg-color-grayflame pt-30px pb-30px px-30px">
                        <h5 class="widget-title">@lang('lang.most_views')</h5>
                        <ul class="list-unstyled post-simple-list mb-0">
                            @foreach($mostViews as $i=>$item)
                                <li class="media">
                                    <p class="number_post">{{$i+1}}</p>
                                    <div class="media-body">
                                        <a href="" class="media-title">{{$item->title}}</a>
                                        <div class="media_post">
                                        <span class="media_category">
                                            @foreach($item->categories as  $category)
                                                <a href="{{lang_route('blog.all',[$category->id])}}">{{$category->text}}</a>
                                            @endforeach
                                        </span>
                                            <span class="media_date">{{date_localized($item->created_at)}}</span>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
<!--
                    <div class="widget_subscripe">
                        <h3>@lang('lang.subscribe_title')</h3>
                        <p>@lang('lang.subscribe_text')</p>
                        <div class="form-group ">
                            <form action="" class="search-post">
                                <div class="input-group mb-3 d-flex">
                                    <input type="email" name="email" class="form-control " placeholder="@lang('lang.email')">
                                    <div class="input-group-subsc">
                                        <button type="submit" class="btn rounded-50 form-custom-btn">@lang('lang.subscribe')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
-->

                </div>
            </div>
        </div>
    </section>
    @push('front_js')

    @endpush
@stop