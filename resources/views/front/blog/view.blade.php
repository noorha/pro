@php
    $data['share_title'] = $blog->title;
    $data['share_image'] = image_url($blog->image);
    $data['share_description'] = string_limit(strip_tags($blog->text),200);
@endphp
@extends('front.layout.index',array_merge(['sub_title' =>__('lang.blog')],$data))
@section('main')
    @push('front_css')
        <link href='https://fonts.googleapis.com/earlyaccess/droidarabicnaskh.css' rel='stylesheet' type='text/css'/>
        {!! HTML::style('front/css/jssocials-theme-flat.css') !!}
        {!! HTML::style('front/css/jssocials.css') !!}
        <style>
            .share-post ul li
            {
                margin: 0px 5px;
            }

            .share-post ul li a {
                display: block;
                width: 30px;
                height: 30px;
                border-radius: 50%;
                color: #fff;
                font-size: 14px;
                text-align: center;
                line-height: 30px;
            }

            .share-post ul li a.face {
                background-color: #3b5998;
            }

            .share-post ul li a.tw {
                background-color: #55acee;
            }

            .share-post ul li a.gm {
                background-color: #E1584B;
            }
        </style>
    @endpush
    <section class="blog_posts">
        <div class="container">
            <div class="row single_news">
                <div class="col-md-12">
                    <div class="post_img mb-0">
                        <img src="{{image_url($blog->image,'1110x440')}}" alt="">
                    </div>
                </div>
                <div class="col-md-10 offset-lg-1">
                    <article>
                        <div class="post_text">
                            <div class="share-post text-left">
                                <ul class="d-flex align-items-center">
                                    <li>@lang('مشاركة')</li>
                                    <li><a href="https://www.facebook.com/sharer.php?u={{lang_route('blog.view',[$item->id])}}" class="face"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="https://twitter.com/intent/tweet?url={{lang_route('blog.view',[$item->id])}}&text={{$blog->title}}" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                                    <li><a href="mailto:?subject={{$blog->title}}&amp;body=@lang('اضغط على الرابط لروئية المقال') {{lang_route('blog.view',[$item->id])}}" class="gm"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="post_meta_top">
                                <span class="post_meta_date">{{date_localized($blog->created_at)}}</span>
                            </div>
                            <h2 class="post_title mb-20px">
                                <a href="">{{$blog->title}}</a>
                            </h2>
                            <div class="post_content">
                                <p>{!! $blog->text !!}</p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
@stop