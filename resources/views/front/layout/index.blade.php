<!doctype html>
<html lang="{{get_current_locale()}}" class="no-js">
<head>
    @php
        $title = $setting->valueOf(locale_value('title','title_en')).(isset($sub_title) ? ' | '.$sub_title : '');
        $description = isset($sub_description) ? $sub_description : $setting->valueOf(locale_value('description','description_en'));
        $image = isset($sub_image) ? $sub_image : url('/front/images/image-thu.jpeg');
    @endphp
    <title> {{$title}}</title>
    <meta property="og:site_name" content="{{$setting->valueOf(locale_value('title','title_en'))}}"/>
    <meta name="keywords" content="{{$setting->valueOf(locale_value('tags','tags_en'))}}"/>
    <meta name="description" content="{{$description}}"/>
    <meta property="og:type" content="website"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/x-icon" href="\front\images\favicon.ico"/>
    <meta property="og:title" content="{{isset($share_title) ? $share_title :  $title}}"/>
    <meta property="og:description" content=" {{isset($share_description) ? $share_description :  $description}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:locale" content="{{get_current_locale().'_'.strtoupper(get_current_locale())}}"/>
    <meta property="og:image" content="{{isset($share_image) ? $share_image :  $image}}"/>
    <meta name="copyright" content="{{$setting->valueOf(locale_value('copyright','copyright_en'))}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="{{isset($share_title) ? $share_title :  $title}}"/>
    <meta name="twitter:description" content="{{isset($share_description) ? $share_description :  $description}}"/>
    <meta name="twitter:site" content="{{url()->current()}}"/>
    <meta name="twitter:image" content="{{isset($share_image) ? $share_image :  $image}}"/>
    <meta name="google-site-verification" content="cfVkZtY8A13hNmjMERd3zCUapkXGXmr80tkomdSaR6M"/>
    @include(view_front_layout().'css')
    <script>
        window['lang'] = '{{get_current_locale()}}';
        window['currency'] = '{{get_current_currency()}}';
        window['currency_txt'] = '{{get_currency_text()}}';
        window.Laravel = {!! json_encode([
            'csrfToken'=> csrf_token(),
            ])
        !!};
    </script>
</head>
<body>
@php
    $isMobile = $mobileDetect->isMobile();
@endphp
<input type="hidden" id="hosturl" value="{{url('/')}}/{{get_current_locale()}}" />
@if($isMobile)
    <div class="notify-mobile">
        <img style="width: 20px;height: 20px;margin: 20px 15px;" class="closed-notify" src="/front/images/close-mob.png">
<!--        <i class="fa fa-times closed-notify" aria-hidden="true"></i>-->
        <div class="contect-mob">
            <img src="/front/images/halapro-mob.png">
            <p style="margin-top: 5px;">HalaPro - Find the best tutors and trainers</p>
            <p>INSTALLED</p>
        </div>
        <a class="new-link-mon" @if($mobileDetect->isAndroidOS()) href="{{$setting->valueOf('google_play')}}"
           @else  href="{{$setting->valueOf('apple_store')}}" @endif>@lang('lang.download_app')</a>
    </div>
@endif

@include(view_front_layout().'header',['isMobile'=>$isMobile])

@yield('main')

@include(view_front_layout().'footer')

@include(view_front_layout().'js')
{{--{!! HTML::script('/front/js/sweetalert2.all.min.js') !!}--}}
{{--{!! HTML::script('/front/js/custom.sweet.js') !!}--}}

@if(session()->has('alert'))
    <script>
        @php
            $alert = session()->get('alert');
            $errors_html = (isset($alert['errors_object'])) ?  $alert['errors_object'] : '';
        @endphp
        customSweetAlert('{{$alert['type']}}', '{{$alert['message']}}', "{!! trim($errors_html) !!}");
    </script>
@endif
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>