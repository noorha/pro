<style>
    .logo-en img{
        width: 80px;
    }

    .logo-ar img{
        width: 60px;
    }

</style>
<div id="new_sec_io" class="new-sec-io {{$class}} @if(isset($isMobile) && $isMobile) mt-80 @endif">
    <div class="container">
        <div id="menu_side" class="menu-side  @if(isset($isMobile) && $isMobile)  t-80 @endif">
            <div class="btn-menu-close"><i class="icon-cross-symbol" aria-hidden="true"></i></div>
            <ul class="menu-sidebar">
                <li><a href="{{lang_route('work.how')}}">@lang('كيف نعمل؟')</a></li>
                <li><a href="{{lang_route('about')}}">@lang('عن هلا برو')</a></li>
                <li><a href="{{lang_route('work.us')}}">@lang('انضم لفريقنا')</a></li>
                @if(!is_auth())
                    <li><a href="{{lang_route('register',['type'=>'teacher'])}}">@lang('تسجيل مدرب')</a></li>
                    <li><a href="{{lang_route('register',['type'=>'student'])}}">@lang('تسجيل طالب')</a></li>
                    <li><a href="{{lang_route('login')}}">@lang('الدخول')</a></li>
                @else
                    @if(auth()->user()->isTeacher())
                        <li><a href="{{lang_route('student.search')}}">@lang('lang.search_for_student')</a></li>
                    @else
                        <li><a href="{{lang_route('teacher.search')}}">@lang('lang.search_for_teacher')</a></li>
                    @endif
                @endif
            </ul>
            <div class="lang-ul-mo">
                <div class="dropdowni-menu">
                    <a class="dropdown-item"
                       href="{{ get_locale_changer_URL((get_current_locale() == 'en') ? 'ar' : 'en')}}">
                        {{(get_current_locale() == 'en') ? 'عربي' : 'English'}}
                    </a>
                </div>
                <ul class="lang-ul">
                    <li class="sty-dropdown dropdown show li-text">
                        <a class="btn-drop-cus dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{get_current_currency()}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item"
                               href="{{ get_currency_URL('USD')}}"> {{get_current_locale() == 'ar' ? 'دولار أمريكي' : 'USD'}} </a>
                            <a class="dropdown-item"
                               href="{{get_currency_URL('AED')}}">  {{get_current_locale() == 'ar' ? 'درهم إماراتي' : 'AED'}}  </a>
                            <a class="dropdown-item"
                               href="{{get_currency_URL('SAR')}}">   {{get_current_locale() == 'ar' ? 'ريال سعودي' : 'SAR'}} </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="back-menu"></div>
        <div id="main-menu">
            <div class="btn-menu"><i class="icon-menu-1" aria-hidden="true"></i></div>
            <div class="header-2 d-flex">
                <div class="logo">
                    <a class="new-logo-no-scroll log-s logo-{{get_current_locale()}}" href="{{lang_route('main')}}">
                        <img alt="logo" src="{{locale_value('/front/images/logo-ar-white.png','/front/images/logo-en-white.png')}}">

                    </a>
                    <a class="new-logo-scroll log-s logo-{{get_current_locale()}}" href="{{lang_route('main')}}">
                        <img alt="logo" src="{{locale_value('/front/images/logo-ar-colored.png','/front/images/logo-en-colored.png')}}">
                    </a>
                <!-- <a class="new-logo-no-scroll d-none" href="{{lang_route('main')}}">
                        <img alt="logo" src="/front/images/logo-sm1.png">
                    </a>
                    <a class="new-logo-scroll d-none" href="{{lang_route('main')}}">
                        <img alt="logo" src="/front/images/logo-sm2.png">
                    </a> -->
                </div>
                <div class="header-menu">
                    <ul class="head-menu hav_left">
                        <li><a href="{{lang_route('work.how')}}">@lang('كيف نعمل؟')</a></li>
                        <li><a href="{{lang_route('about')}}">@lang('عن هلا برو')</a></li>
                        <li><a href="{{lang_route('work.us')}}">@lang('انضم لفريقنا')</a></li>
                        @if(!is_auth())
                            <li><a href="{{lang_route('register',['type'=>'teacher'])}}">@lang('تسجيل مدرب')</a></li>
                        @else
                            @if(auth()->user()->isTeacher())
                                <li><a href="{{lang_route('student.search')}}">@lang('lang.search_for_student')</a></li>
                            @else
                                <li><a href="{{lang_route('teacher.search')}}">@lang('lang.search_for_teacher')</a></li>
                            @endif
                        @endif
                    </ul>
                </div>
                <div class="register-y">
                    @if(!is_auth())
                        <ul class="head-menu-sec">
                            <li><a href="{{lang_route('register',['type'=>'student'])}}">@lang('تسجيل طالب')</a>
                            </li>
                            <li><a href="{{lang_route('login')}}">@lang('الدخول')</a></li>
                        </ul>
                    @endif
                    @if(is_auth())
                        <div class="img-profile-r">

                            @php
                                $un_read = get_unread_notifications_count();
                            @endphp


                            @php
                                $un_read_messages = auth()->user()->unReadCount();
                            @endphp

                            <div class="user-avatar">
                                <a class="maiswpow profsiw">
                                    <img alt="user" src="{{image_url(auth()->user()->photo,'45x45')}}">
                                    <p>{{auth()->user()->getUserName()}}</p>
                                </a>
                                <div class="dropmenus profwsw">
                                    <ul>
                                        <li>
                                            <a href="{{lang_route('profile',['id'=>auth()->user()->id])}}">
                                                <i class="icon-man-user"></i>@lang('الملف الشخصي')
                                            </a>
                                        </li>
                                        <li><a href="{{lang_route('profile.dashboard')}}">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i>
                                                @lang('إعدادات الحساب')
                                            </a>
                                        </li>
                                        @php
                                            $pending_requests_count = auth()->user()->pendingRequestsCount();
                                        @endphp
                                        <li>
                                            <a href="{{lang_route((auth()->user()->isStudent() ?'student' : 'teacher') .'.training.requests')}}">
                                                <i class="fa fa-file-text-o"
                                                   aria-hidden="true"></i> @lang('طلبات التدريب')
                                                <span id="pending_requests_count" {!! (isset($pending_requests_count) && $pending_requests_count>0) ?'' : 'style="display: none"'  !!}>{{(isset($pending_requests_count) && $pending_requests_count>0) ? $pending_requests_count : ''}}</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{lang_route('profile.balance')}}">
                                                <i class="icon-black-and-white-credit-cards"></i> @lang('صفحة الأرصدة')
                                            </a>
                                        </li>


                                        <li><a href="{{lang_route('logout')}}" id="logout"><i
                                                        class="icon-exit"></i> @lang('تسجيل الخروج')</a>
                                        </li>
                                    </ul>
                                </div>

                            </div>

                            <div class="notif-ui">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" id="notify_btn" type="button"
                                            data-toggle="dropdown" data-url="{{lang_route('notifications.read')}}">
                                        @include(view_front_layout().'notifications-btn',['un_read'=>$un_read])
                                    </button>
                                    <ul class="dropdown-menu" id="notifications_body">
                                        @include(view_front_layout().'notifications')
                                    </ul>
                                </div>
                            </div>

                            <div class="notif-ui">
                                <a href="{{lang_route('profile.messages')}}" class="maiswpow">
                                    <i class="fa fa-envelope-o"></i>
                                    <span id="msg_counter" style="display: none"></span>
                                </a>
                            </div>
                        </div>

                    @endif
                </div>
                <div class="curancy">
                    <div class="dropdowni-menu">
                        <a class="dropdown-item"
                           href="{{ get_locale_changer_URL((get_current_locale() == 'en') ? 'ar' : 'en')}}">
                            {{(get_current_locale() == 'en') ? 'عربي' : 'English'}}
                        </a>
                    </div>
                    <ul class="lang-ul">
                        <li class="sty-dropdown dropdown show li-text">
                            <a class="btn-drop-cus dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{get_current_currency()}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item"
                                   href="{{ get_currency_URL('USD')}}"> {{get_current_locale() == 'ar' ? 'دولار أمريكي' : 'USD'}} </a>
                                <a class="dropdown-item"
                                   href="{{get_currency_URL('AED')}}">  {{get_current_locale() == 'ar' ? 'درهم إماراتي' : 'AED'}}  </a>
                                <a class="dropdown-item"
                                   href="{{get_currency_URL('SAR')}}">   {{get_current_locale() == 'ar' ? 'ريال سعودي' : 'SAR'}} </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>