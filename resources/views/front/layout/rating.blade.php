<ul>
    @for($i = 1 ; $i <= 5 ; $i++)
        <li data-id="{{$teacher->id}}" class=" rate @if((int)$teacher->rating >= $i) active @endif">
            <i class="icon-star3"></i>
        </li>
    @endfor
    <li><span>{{round($teacher->rating,1)>0?round($teacher->rating,1):''}}</span></li>
<!-- ({{$teacher->TeacherRatingsCount()}})-->
</ul>