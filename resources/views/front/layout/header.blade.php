
<section id="header_section_owl" class="header-section-owl logid  @if(isset($isMobile) && $isMobile) mt-80 @endif ">
    @include(view_front_layout().'header-menu',['class' => lang_route('main') != url()->current() ? 'all-page-header' : 'main-page-header'])
    @if(lang_route('main') == url()->current())
        <div class="main_sec_headers">
            <div class="video-background overlay-black">
                <video id="myvideo" loop="true" autoplay="true" muted="true" controls="false" playsinline="true">
                    <source src="/front/images/demo.mp4" type="video/mp4">
                    <source src="/front/images/demo.ogg" type="video/ogg">
                </video>
            </div>
            <div class="main_sec_header nivo-caption">
                @php
                    $countries = get_all_countries();
                                    $specialities = get_all_specialities();

                @endphp
                <div class="s-tb header_index wow fadeInUp" data-wow-delay="0.4s">
                    <div class="title-container s-tb-c">
                        @if(auth()->check() && auth()->user()->isTeacher())
                        <div class="trin_index">
                            <div class="list_in">
                                {!! Form::open(['method'=>'GET' , 'url'=>lang_route('student.search')]) !!}
                                <div class="list1 active" id="list_index_1">
                                    <div class="input2div">
                                        <div class="inputsw  bgw question">
                                            @php
                                                $sub_specialities = get_all_sub_specialities();
                                            @endphp
                                            <input type="hidden" name="type[]" value="1">
                                            {{--<select class="select2" name="sub_specialty" placeholder="@lang('إختر الموضوع')" title="sd" >--}}
                                                {{--@foreach($sub_specialities as $subSpeciality)--}}
                                                {{--<option></option>--}}
                                                {{--<option value="{{$subSpeciality->id}}" data-alternative-spellings="{{$subSpeciality->name_en}} |{{$subSpeciality->name}}">{{$subSpeciality->text}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}

                                            <select name="specialities" id="specialities" class="select2" placeholder="@lang('إختر التخصص')">
                                                <option value="">@lang('الكل')</option>
                                                @foreach($specialities as $speciality)
                                                    <option value="{{$speciality->id}}" data-alternative-spellings="{{$speciality->name_en}} |{{$speciality->name}}">{{get_text_locale($speciality,'name')}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="inputsw">
                                            <select name="country"  class="selectpicker">
                                                <option value="">@lang('الدولة')</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{ get_text_locale($country , 'name')}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="inputsw btnward">
                                        <button>@lang('ابحث عن طالب')</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>
                        </div>
                        @else
                            <div class="trin_index">
                                <ul>
                                    <li><a class="active" data-id="1"><img alt="@lang('شخصي')" src="/front/images/Shape1.png">@lang('شخصي')</a></li>
                                    <li><a data-id="2"><img alt="@lang('اونلاين')" src="/front/images/Shape2.png">@lang('اونلاين')</a></li>
                                    {{--<li><a data-id="3"><img alt="@lang('مراكز تدريبية')" src="/front/images/Shape3.png">@lang('مراكز تدريبية')</a></li>--}}
                                </ul>
                                <div class="list_in">
                                    {!! Form::open(['method'=>'GET' , 'url'=>lang_route('teacher.search')]) !!}
                                    <div class="list1 active" id="list_index_1">
                                        <div class="input2div">
                                            <div class="inputsw  bgw question">
                                                @php
                                                    $sub_specialities = get_all_sub_specialities();
                                                @endphp
                                                <input type="hidden" name="type[]" value="2">
                                            <!-- <select name="sub_specialty" class="sub_specialty_main" autofocus="autofocus" placeholder="@lang('إختر الموضوع')" autocorrect="off" autocomplete="off">

                                                @foreach($sub_specialities as $subSpeciality)
                                                <option></option>
                                                <option value="{{$subSpeciality->id}}" data-alternative-spellings="{{$subSpeciality->name_en}} |{{$subSpeciality->name}}">{{$subSpeciality->name.' | '.$subSpeciality->name_en}}</option>
                                                @endforeach
                                                    </select> -->

                                                <select class="select2" name="sub_specialty" placeholder="@lang('إختر الموضوع')" title="sd" >
                                                    @foreach($sub_specialities as $subSpeciality)
                                                        <option></option>
                                                        <option value="{{$subSpeciality->id}}" data-alternative-spellings="{{$subSpeciality->name_en}} |{{$subSpeciality->name}}">{{$subSpeciality->text}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="inputsw">
                                                <select name="country"  class="selectpicker">
                                                    <option value="">@lang('الدولة')</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{ get_text_locale($country , 'name')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="inputsw btnward">
                                            <button>@lang('ابحث عن مدرب/ مدرس')</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                    {!! Form::open(['method'=>'GET' , 'url'=>lang_route('teacher.search')]) !!}
                                    <div class="list1" id="list_index_2">
                                        <div class="input2div">
                                            <input type="hidden" name="type[]" value="1">

                                            <div class="inputsw  bgw question">
                                                <select name="sub_specialty" class="select2"
                                                        autofocus="autofocus"
                                                        placeholder="@lang('إختر الموضوع')" autocorrect="off"
                                                        autocomplete="off">
                                                    <option></option>
                                                    @foreach($sub_specialities as $subSpeciality)
                                                        <option value="{{$subSpeciality->id}}" data-alternative-spellings="{{$subSpeciality->name_en}} |{{$subSpeciality->name}}">{{$subSpeciality->text}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="inputsw">
                                                <select name="country" class="selectpicker">
                                                    <option value="">@lang('الدولة')</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{get_text_locale($country , 'name')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="inputsw btnward">
                                            <button>@lang('ابحث عن مدرب/ مدرس')</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}

                                    {!! Form::open(['method'=>'GET' , 'url'=>lang_route('course.all')]) !!}
                                    <div class="list1" id="list_index_3">
                                        <div class="input2div">
                                            <div class="inputsw input-cou brd-radus-new">
                                                <select class="select2" name="category"  autofocus="autofocus" placeholder="@lang('إختر التخصص')" autocorrect="off" autocomplete="off">
                                                    <option></option>
                                                    @foreach($courses as $course)
                                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="inputsw btnward">
                                            <button>@lang('تصفح الدورات')</button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endif
                        <div class="imt_lastind">
                            <div class="col-md-12 col-xs-12">
                                @auth
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="btns_oms awsw">
                                                <a href="{{lang_route('profile',['id'=>auth()->user()->id])}}"><i class="icon-teacher"></i>@lang('الملف الشخصي')</a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="btns_oms awsw">
                                                <a href="{{lang_route('register',['type'=>'teacher'])}}"><i class="icon-teacher"></i>@lang('تسجيل مدرب/ مدرس')</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="btns_oms awsw">
                                                <a href="{{lang_route('register',['type'=>'student'])}}"><i class="icon-graduate-student-avatar"></i> @lang('تسجيل طالب')
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endauth
                            </div>
                        </div>    
                        <div class="new-mobile-btn">
                            <h3>@lang('حمّل تطبيق هلا برو الآن')</h3>
                            @php
                                $constant = new \App\Constant();
                            @endphp
                            <div>
                                <a href="{{$constant->valueOf('apple_store')}}" target="_blank"><img alt="appstore" src="/front/images/appstore.png"></a>
                                <a href="{{$constant->valueOf('google_play')}}" target="_blank"><img alt="googleplay" src="/front/images/googleplay.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>
{{--<script src="{{ url('/front/js/select2.min.js') }}"></script>--}}
{{--<script>--}}
  {{--$('.select2').select2({--}}
        {{--placeholder: "اختر الموضوع",--}}
        {{--allowClear: true,--}}
        {{--dir: "rtl",--}}
        {{--dropdownPosition: 'below',--}}
        {{--language: {--}}
            {{--"noResults": function () {--}}
                {{--return "لا توجد نتائج";--}}
            {{--}--}}
        {{--}--}}
    {{--});--}}
    {{--$('.select2_2').select2({--}}
        {{--placeholder: "اختر التخصص",--}}
        {{--allowClear: true,--}}
        {{--dir: "rtl",--}}
        {{--dropdownPosition: 'below',--}}
        {{--language: {--}}
            {{--"noResults": function () {--}}
                {{--return "لا توجد نتائج";--}}
            {{--}--}}
        {{--}--}}
    {{--});--}}

{{--</script>--}}
