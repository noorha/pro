<select id="city_id" name="city_id" class="selectpicker" >
    <option value=""> @lang('الكل')</option>
    @if(isset($items))
        @foreach($items as $city)
            <option value="{{$city->id}}" @if((!isset($is_filter)) && $city->id == @$user->city_id) selected @endif >{{ get_text_locale($city,'name')}}<img src=""></option>
        @endforeach
    @endif
</select>