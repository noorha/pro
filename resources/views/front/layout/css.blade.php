{{--{!! HTML::style('/front/css/bootstrap.min.css') !!}--}}
{{--{!! HTML::style('/front/css/animate.css') !!}--}}
{{--{!! HTML::style('/front/css/bootstrap-select.css') !!}--}}
{{--{!! HTML::style('/front/css/bootstrap-datetimepicker.min.css') !!}--}}
{{--{!! HTML::style('/front/css/font-style.css') !!}--}}
{{--{!! HTML::style('/front/css/font-awesome.min.css') !!}--}}
{{--@if(get_current_locale() == 'en')--}}
{{--{!! HTML::style('/front/css/style.css') !!}--}}
{{--{!! HTML::style('/front/css/style_en.css') !!}--}}
{{--<link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">--}}
{{--@else--}}
{{--{!! HTML::style('/front/css/bootstrap-rtl.css') !!}--}}
{{--{!! HTML::style('/front/css/style.css') !!}--}}
{{--@endif--}}
{{--{!! HTML::style('/front/css/file-preview.css') !!}--}}
{{--{!! HTML::style('/front/css/owl.carousel.css') !!}--}}
{{--{!! HTML::style('/front/css/owl.theme.css') !!}--}}
@if(get_current_locale() == 'ar')
    {!! HTML::style('/front/css/ar.min.css') !!}
    {!! HTML::style('/select2/select2-rtl.css') !!}

@else
    {!! HTML::style('/front/css/en.min.css') !!}
    {!! HTML::style('/select2/select2.css') !!}

@endif

{!! HTML::script('front/js/jquery.min.js') !!}
{!! HTML::script('select2/select2.min.js') !!}

<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
<script>

    var config = {
    apiKey: "AIzaSyBDmv5agYH9UCZS99Eik6kIMgEsytk3KCY",
    authDomain: "halatest.firebaseapp.com",
    databaseURL: "https://halatest.firebaseio.com",
    projectId: "halatest",
    storageBucket: "",
    messagingSenderId: "596922933446",
    appId: "1:596922933446:web:0c18686f4b81abd2"
    };

    firebase.initializeApp(config);
</script>
@auth
    <script>

        var connectedRef = firebase.database().ref(".info/connected");
        connectedRef.on("value", function (snap) {
            if (snap.val() === true) {
                firebase.database().ref().child('users/{{auth()->user()->id}}').update({
                    isOnline: 1,
                    lastSeen:  firebase.database.ServerValue.TIMESTAMP,
                });
            } else {
                firebase.database().ref().child('users/{{auth()->user()->id}}').update({
                    isOnline: 0,
                });
            }
        });
        var ref = firebase.database().ref('users/{{auth()->user()->id}}' + '/isOnline');
        ref.onDisconnect().set(0);



        setInterval(function(){
            firebase.database().ref().child('users/{{auth()->user()->id}}').update({
                isOnline: 1,
                lastSeen:  firebase.database.ServerValue.TIMESTAMP,
            });
        }, 50000);

    </script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script>
        window.audio = new Audio('{{file_url('bing.MP3')}}');
        var pusher = new Pusher('d8e93d92ea33ee7289d4', {
            cluster: 'us2',
            encrypted: true
        });
        var channel = pusher.subscribe('notify_{{auth()->user()->id}}');
        channel.bind('new_message', function (data) {
            if (parseInt(window.conversation_id) !== parseInt(data.conversation_id)) {
                if (data.msg_counter > 0) {
                    $('#msg_counter').text(data.msg_counter);
                    $('#msg_counter').attr('style', 'display:block');
                }
                window.audio.play();
            }
        });
        channel.bind('new-notification', function (data) {
            window.audio.play();
            $.ajax({
                url: '{{lang_route('notifications.count')}}',
                type: 'GET',
                success: function (response) {
                    $('#notify_btn').html(response.item.count);
                    $('#notifications_body').html(response.item.body);
                    if (response.item.pending_requests_count > 0) {
                        $('#pending_requests_count').text(response.item.pending_requests_count);
                        $('#pending_requests_count').attr('style', 'display:block');
                    }
                    if (response.item.msg_counter > 0) {
                        $('#msg_counter').text(response.item.msg_counter);
                        $('#msg_counter').attr('style', 'display:block');
                    }
                }
            });
        });
    </script>
@endauth

@stack('front_css')