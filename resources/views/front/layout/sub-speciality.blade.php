<div id="loader" class="sub-loader hidden">
    <i  class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
</div>

<div id="container" class="row">
    @foreach($subSpecialties as $subSpecialty)
    <div class="col-md-2 col-sm-4 col-xs-6">
        <div class="item_list3  wow fadeInUp" data-wow-delay="0.2s">
            <a href="{{lang_route('teacher.search',['subSpecialty',$subSpecialty->id])}}">
               {{--
                <div class="img_item_list3">
                    <img src="{{image_url($subSpecialty->icon,'50x50')}}">
                </div>
                --}}
                <h3>{{get_text_locale($subSpecialty,'name')}}</h3>
            </a>
        </div>
    </div>
    @endforeach
</div>