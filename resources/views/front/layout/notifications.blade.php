@if(auth()->user()->notifications()->count() > 0)
    @foreach(auth()->user()->notifications->take(5) as $notification)
        <li>
            <a href="{{$notification->data['url']}}">
                @if(get_current_locale() == 'ar')
                    <p>{{$notification->data['text']}} </p>
                @else
                    <p>{{$notification->data['text_en']}} </p>
                @endif
                <span>{{diff_for_humans($notification->created_at)}}</span>
            </a>
        </li>
    @endforeach
    <li class="lastswo">
        <a href="{{lang_route('profile.notifications')}}">
            @lang('عرض جميع الإشعارات')
        </a>
    </li>
@else
    <li class="lastswo">
        <a href="{{lang_route('profile.notifications')}}">
            @lang('لا يوجد إشعارات')
        </a>
    </li>
@endif
