{{--{!! HTML::script('front/js/bootstrap.min.js') !!}--}}
{{--{!! HTML::script('front/js/bootstrap-select.min.js') !!}--}}
{{--{!! HTML::script('front/js/moment.js') !!}--}}
{{--{!! HTML::script('front/js/bootstrap-datetimepicker.min.js') !!}--}}
{{--{!! HTML::script('/front/js/jquery.validate.min.js') !!}--}}
{{--{!! HTML::script('/front/js/validate-ar.js') !!}--}}
{{--{!! HTML::script('/front/js/errors.js') !!}--}}
{{--{!! HTML::script('front/js/owl.carousel.min.js') !!}--}}
{!! HTML::script(asset('/front/js/script.min.js')) !!}

@stack('front_js')
{{--{!! HTML::script('front/js/function.js') !!}--}}
{{--{!! HTML::script('front/js/wow.min.js') !!}--}}
<script>
    new WOW().init();
</script>


@auth
    <script>
        var count = 0;
        var messagesCount = firebase.database().ref('users/{{auth()->user()->id}}/messagingChats');
        messagesCount.once('value', (snapshot) => {
            snapshot.forEach(function (object) {
                count = (object.val().unReadCount > 0) ? ++count : count;
            });
        }).then(function () {
            updateCount();
        });
        messagesCount.on('child_changed', (snapshot) => {
            count = 0;
            messagesCount.once('value', (snapshot) => {
                snapshot.forEach(function (object) {
                    count = (object.val().unReadCount > 0) ? ++count : count;
                });
            }).then(function () {
                updateCount();
            });
        });

        function updateCount() {
            var msg_span = $('#msg_counter');
            if (count > 0) {
                msg_span.text(count);
                msg_span.fadeIn();
            } else {
                msg_span.fadeOut();
            }
        }
    </script>
@endauth
