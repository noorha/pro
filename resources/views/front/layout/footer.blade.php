<section class="footer">
    <div class="container">
        <div class="item_footer">
            <div class="row">
                <div class="col-md-3">
                    <div class="item_footer_first">
                        <h3 class="hed_foter">@lang('صفحات هلا برو')</h3>
                        <ul class="page_footer">
                            <li><a href="{{lang_route('work.how')}}">@lang('كيف نعمل؟')</a></li>
                            <li><a href="{{lang_route('about')}}">@lang('عن هلا برو')</a></li>
                            <li><a href="{{lang_route('work.us')}}">@lang('انضم لفريقنا')</a></li>
                            <li><a href="{{lang_route('contact')}}">@lang('اتصل بنا')</a></li>
                            <li><a href="{{lang_route('faq')}}">@lang('lang.faq')</a></li>
                            <li><a href="{{lang_route('blog.all')}}">@lang('lang.blog')</a></li>
                            @auth
                                <li><a href="{{lang_route('ticket.all')}}">@lang('lang.my_tickets')</a></li>
                            @endauth
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="item_footer_first">
                        <h3 class="hed_foter">@lang('خدمات هلا برو')</h3>
                        <ul class="page_footer">
                            <li>
                                <a href="{{lang_route('teacher.search',['type','2','get'])}}">@lang('تدريب وتدريس شخصي')</a>
                            </li>
                            <li>
                                <a href="{{lang_route('teacher.search',['type','1','get'])}}">@lang('تدريب وتدريس اونلاين')</a>
                            </li>
                            {{--                            <li><a href="{{lang_route('course.all')}}">@lang('تدريب وتدريس في مراكز')</a></li>--}}
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item_footer_first w_50">
                        <h3 class="hed_foter">@lang('الإشتراك بالقائمة البريدية')</h3>
                        <p>@lang('قم بالإشتراك بالقائمة البريدية ليصلك جديد هلا برو')</p>
                        {!! Form::open(['id'=>'mailing_form','method'=>'post','url'=>lang_route('subscribe.mail')]) !!}
                        <div class="email_app">
                            <input type="email" name="email" placeholder="@lang('بريدك الإلكتروني')" required>
                            <button>@lang('إشترك الآن')
                                <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                            </button>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="item_footer">
                <div class="row">
                    <div class="col-md-3">
                        <div class="item_footer_first">
                            <h3 class="hed_foter">@lang('تابع هلابرو')</h3>


                            @php
                                $socials = get_socials();
                            @endphp

                            <ul class="page_footer-social">
                                @foreach($socials as $social)
                                    @if(isset($social->item )&& $social->item->link  != '#')
                                        <li><a target="_blank" href="{{$social->item->link}}"
                                               class="{{$social->key}}"><i
                                                        class="{{get_social_icon_footer($social->key)}}"></i></a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="item_footer_first">
                            <h3 class="hed_foter">@lang('بوابات الدفع')</h3>
                            <ul class="page_footer-pay">
                                <li><img alt="visa" src="/front/images/pay/visa.png"></li>
                                <li><img alt="paypal" src="/front/images/pay/paypal.png"></li>
                                <li><img alt="master" src="/front/images/pay/master.png"></li>
                                <li><img alt="armx" src="/front/images/pay/armx.png"></li>
                                <li><img alt="3bb" src="/front/images/pay/3bb.png"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item_footer_first">
                            <h3 class="hed_foter">@lang('حمل تطبيق هلا برو')</h3>
                            <ul class="page_footer-apps">
                                <li>
                                    <a target="_blank" href="{{$setting->valueOf('google_play')}}">
                                        <img alt="google-play" src="/front/images/pay/google-play.png">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="{{$setting->valueOf('apple_store')}}">
                                        <img alt="app-store" src="/front/images/pay/app-store.png">
                                    </a>
                                </li>
                                <!--                                <li><img alt="handby" class="handly-n" src="/front/images/pay/handby.png"></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer3">
        <div class="container">
            <div class="item_footer">
                <div class="row">
                    <div class="col-md-6">
                        <div class="item_footer_first">
                            <h3 class="hed_foter">@lang('المواضيع الأكثر طلبا')</h3>
                            <div class="row">
                                @php
                                    $subSpecialties = get_selected_sub_specs(50);
                                @endphp
                                <div class="col-md-12">
                                    @foreach($subSpecialties as $key => $subSpecialty)
                                        <a class="footer-a-new"
                                           href="{{lang_route('teacher.search',['subSpecialty',$subSpecialty->id])}}">{{$subSpecialty->text}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="item_footer_first">
                            <h3 class="hed_foter">@lang('المدربين حسب المكان')</h3>

                            @php
                                $country_items = get_footer_countries(true);
                                $arab_rest_countries = get_footer_countries(false);
                                $non_arab_rest_countries = get_footer_countries(false,false);
                            @endphp
                            @if(isset($country_items) && $country_items->count() > 0)

                                <ul class="page_footer-tow as350">
                                    @foreach($country_items as $country)
                                        <li>
                                            <a href="{{lang_route('teacher.search',['country',$country->id])}}">
                                                <img alt="{{get_text_locale($country,'name')}}"
                                                     src="{{image_url($country->icon, '20x20')}}"> {{get_text_locale($country,'name')}}
                                            </a>
                                        </li>
                                        @if( !($country->name == 'الكويت'||  $country->name == 'الأردن'))
                                            @foreach($country->cities()->take(4)->get() as $city)
                                                <li>
                                                    <a href="{{lang_route('teacher.search',['country',$country->id])}}">{{get_text_locale($city,'name')}}</a>
                                                </li>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </ul>
                            @endif

                            @if(isset($arab_rest_countries) && $arab_rest_countries->count() > 0)
                                <ul class="page_footer-tow as350">
                                    @foreach($arab_rest_countries as $country)
                                        <li>
                                            <a href="{{lang_route('teacher.search',['country',$country->id])}}">
                                                <img alt="{{get_text_locale($country,'name')}}"
                                                     src="{{image_url($country->icon, '20x20')}}"> {{get_text_locale($country,'name')}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            @if(isset($non_arab_rest_countries) && $non_arab_rest_countries->count() > 0)
                                <ul class="page_footer-tow as350">
                                    @foreach($non_arab_rest_countries as $country)
                                        <li>
                                            <a href="{{lang_route('teacher.search',['country',$country->id])}}">
                                                <img alt="{{get_text_locale($country,'name')}}" src="{{image_url($country->icon, '20x20')}}">
                                                {{get_text_locale($country,'name')}}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @php
        $constant = new \App\Constant();
    @endphp

    <div class="footer2 notopmr">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="new-copyright">{{$constant->valueOf((get_current_locale() == 'ar') ? 'copyright' : 'copyright_en')}}</h4>
                </div>
                <div class="col-md-6">
                    <ul class="list-menu-y">
                        <li><a href="{{lang_route('main')}}">@lang('الرئيسية')</a></li>
                    <!--                        <li><a href="#">@lang('خريطة الموقع')</a></li>-->
                        <li><a href="{{lang_route('policy')}}">@lang('سياسة الإستخدام')</a></li>
                        <li><a href="{{lang_route('privacy')}}">@lang('سياسة الخصوصية')</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "1541664222533988", // Facebook page ID
            whatsapp: "+966560022211", // WhatsApp number
            email: "info@HalaPro.com", // Email
            sms: "+971505940141", // Sms phone number
            call: "+971505940141", // Call phone number
            company_logo_url: "//storage.whatshelp.io/widget/a1/a1be/a1bec597073c8fa5df539aa55bf7bce9/logo.jpg", // URL of company logo (png, jpg, gif)
            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
            call_to_action: "Message us", // Call to action
            button_color: "#de5698", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp,sms,email,call", // Order of buttons
            ga: true, // Google Analytics enabled
            branding: false, // Show branding string
            mobile: true, // Mobile version enabled
            desktop: true, // Desktop version enabled
            greeting: true, // Greeting message enabled
            shift_vertical: 0, // Vertical position, px
            shift_horizontal: 0, // Horizontal position, px
            domain: "halapro.com", // site domain
            key: "-a_u4mauT_G1gwUOOAtlCA", // pro-widget key
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127443572-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-127443572-1');

    if ($("#training_experience").length > 0)
    {
        var res = "";
        $("#training_experience").select2({
            tags: res,
            width :"100%",
            dir: "rtl"

                    //  tags: ["fff","ffffffff"]
        });
    }
</script>
