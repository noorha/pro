@extends('front.layout.index',['sub_title' => __('عرض المحادثة') ])
@section('main')
    @push('front_css')

    @endpush

    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="towmessage">
                            <div class="image_towmessage">
                                @php
                                    $teacher = $vue['teacher'];
                                    $student = $vue['student'];
                                @endphp
                                @if(!auth()->user()->isStudent())
                                    <img src="{{isset($teacher ['photo']) ? $teacher ['photo'] : '#'}}">
                                    <img src="{{isset($student['photo']) ? $student ['photo'] : '#'}}">
                                @else
                                    <img src="{{isset($student['photo']) ? $student ['photo'] : '#'}}">
                                    <img src="{{isset($teacher ['photo']) ? $teacher ['photo'] : '#'}}">
                                @endif
                            </div>
                            <div class="text_towmessage">
                                <h3>{{(auth()->user()->isStudent()) ?$teacher['name'] : $student['name']}}</h3>
                                <span><i class="icon-clock-circular-outline"></i> {{get_date_from_timestamp($chat['created_at'])}}</span>
                                {{--<p>من هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن--}}
                                    {{--التركيز... على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها..</p>--}}
                                <!--
                                                                <a data-toggle="modal" data-target="#exampleModal"><i class="icon-checked"></i>تسليم الطلب</a>
                                                                <a data-toggle="modal" data-target="#exampleModal2" class="bn_nosw"><i class="icon-cross-symbol-1"></i>تبليغ</a>
                                -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8  col-md-pull-4">
                        <div class="onc2">
                            <div id="app">
                                <chat :init_data="{{json_encode($vue)}}"></chat>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('front_js')
        <script>
            window.conversation_id = '{{$chat['id']}}';
        </script>
        {!! HTML::script(asset('/js/app.js')) !!}
    @endpush
@stop