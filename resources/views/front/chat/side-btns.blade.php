<div class="towmessage">
    <div class="image_towmessage">
        @if(!auth()->user()->isStudent())
            <img src="{{image_url($training_request->getTeacherPhoto(),'90x90')}}">
            <img src="{{image_url($training_request->getStudentPhoto(),'90x90')}}">
        @else
            <img src="{{image_url($training_request->getStudentPhoto(),'90x90')}}">
            <img src="{{image_url($training_request->getTeacherPhoto(),'90x90')}}">
        @endif
    </div>
    <div class="text_towmessage">
        <h3>{{(auth()->user()->isStudent()) ? $training_request->getTeacherName(): $training_request->getStudentName()}}</h3>
        <span><i class="icon-clock-circular-outline"></i> {{get_date_from_timestamp(\Carbon\Carbon::createFromTimestamp($chat['createdAt']))}}</span>

        {{--@if(isset($training_request->note) && !empty($training_request->note))--}}
        {{--<p>{{$training_request->note}}</p>--}}
        {{--@endif--}}

        @if(auth()->user()->isStudent())
            @if($training_request->hasWhiteboard())
                {{--<a class="bn_nosw" style="background-color: #de5698"--}}
                   {{--href="{{lang_route('chat.whiteboard',['id'=>$training_request->id])}}">--}}
                    {{--<i class="fa fa-book-open"></i>@lang('فتح سبورة إلكترونية')--}}
                {{--</a>--}}
            @endif
        @else
            {{--<a class="bn_nosw" style="background-color: #de5698"--}}
               {{--href="{{lang_route('chat.whiteboard',['id'=>$training_request->id])}}">--}}
                {{--<i class="fa fa-book-open"></i>@lang('فتح سبورة إلكترونية')--}}
            {{--</a>--}}
        @endif
        @if(auth()->user()->isTeacher())
            @if(!$training_request->isConfirmByTeacher())
                <a data-toggle="modal" data-target="#confirm_modal"><i
                            class="icon-checked"></i>@lang('تسليم الطلب')</a>
            @endif
        @else
            @switch($training_request->status)
                @case('progress')
                @if($training_request->isConfirmByTeacher())
                    <a data-toggle="modal" data-target="#confirm_modal"><i
                                class="icon-checked"></i>@lang('تأكيد إستلام الطلب')</a>
                @endif
                @break
                @case('completed')
                @php
                    $teacher =  $training_request->teacher;
                @endphp
                @if(!$training_request->hasRating())
                    <a class="rateds" data-toggle="modal" data-target="#rate_modal"><i
                                class="icon-star3"></i>@lang('تقييم المدرب')</a>
                @endif
                @break

            @endswitch
        @endif
        @if($training_request->status != 'completed')
            <a data-toggle="modal" data-target="#report_modal" class="bn_nosw"><i
                        class="icon-cross-symbol-1"></i>@lang('تبليغ')</a>
        @endif
    </div>
</div>