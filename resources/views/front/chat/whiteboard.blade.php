@extends('front.layout.index',['sub_title' => __('عرض WhitBoard') ])
@section('main')
    @push('front_css')
        <style>
            body * {
                font-family: 'icomoon', DB, serif;
            }
        </style>
    @endpush

    <section class="se0">
        <div style="padding: 30px;height: 700px;width: 100%" class="row">
            <div class="whiteboard-container" id="aww-wrapper">

            </div>
        </div>
        <script src="https://awwapp.com/static/widget/js/aww.min.js">
        </script>
        <script type="text/javascript">
            var aww = new AwwBoard('#aww-wrapper', {
                apiKey: 'f6ca4dbe-8ad2-4fbd-86a6-eff5ee3510a8',
                boardLink: '{{$whiteboard->board_id}}',
                autoJoin: true,
                sizes: [3, 5, 8, 13, 20],
                fontSizes: [10, 12, 14, 16, 18, 22, 30, 40],
                menuOrder: ['colors', 'sizes', 'tools', 'admin'],
                tools: ['pencil', 'eraser', 'text', "line", "text", "lineStraight", "rectangle", "ellipse", "arc"],
                // tools: ['pencil', 'marker', 'lineStraight', 'eraser', 'eraserArea', 'rectangle', 'rectangleFilled', 'ellipse', 'image', 'ellipseFilled', 'text', 'image', 'postit'],
                colors: ["#000000", "#f44336", "#4caf50", "#2196f3", "#ffc107", "#9c27b0", "#e91e63", "#795548"],
                defaultColor: "#000000",
                defaultSize: 8,
                defaultTool: 'pencil',
            });
        </script>
    </section>

    @push('front_js')

    @endpush
@stop