@extends('front.layout.index',['sub_title' => __('عرض المحادثة')])
@section('main')
    @push('front_css')
        {!! HTML::style('/front/css/fontawesome-stars.css') !!}
        <style>
            .chat_avatar {
                border-radius: 25px;
            }
            .vue-content-placeholders-heading__img{
                margin-left: 15px !important;
            }



        </style>
    @endpush

    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div id="items">
                            @include('front.chat.side-btns')
                        </div>
                    </div>
                    <div class="col-md-8  col-md-pull-4">
                        <div id="app">
                            <chat :init_data="{{json_encode($vue)}}"></chat>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @include(view_front_modal().'confirm-teacher',['type'=>(auth()->user()->isStudent()) ? 'student' :'teacher','id'=>$training_request->id])
    @include(view_front_modal().'report-chat',['type'=>(auth()->user()->isStudent()) ? 'student' :'teacher','id'=>$training_request->id])
    @include(view_front_modal().'rate-teacher',$training_request)

    @push('front_js')
{{--        {!! HTML::script(asset('/js/app.js') )!!}--}}
        {!! HTML::script(asset('/front/js/training.js') )!!}
        {!! HTML::script('/front/js/jquery.barrating.js') !!}

        {!! HTML::script('/front/js/jquery.validate.min.js') !!}
        {!! HTML::script('/front/js/validate-ar.js') !!}
        {!! HTML::script('/front/js/errors.js') !!}
        <script>
            {{--window.conversation_id = '{{$chat['id']}}';--}}
        </script>
        <script type="text/javascript">
            $(function () {
                function ratingEnable() {
                    $('.example-fontawesome').barrating({
                        theme: 'fontawesome-stars',
                        showSelectedRating: false
                    });
                    var currentRating = $('.example-fontawesome-o').data('current-rating');
                    $('.stars-example-fontawesome-o .current-rating')
                        .find('span')
                        .html(currentRating);

                    $('.stars-example-fontawesome-o .clear-rating').on('click', function (event) {
                        event.preventDefault();

                        $('.example-fontawesome-o')
                            .barrating('clear');
                    });

                    $('.example-fontawesome-o').barrating({
                        theme: 'fontawesome-stars-o',
                        showSelectedRating: false,
                        initialRating: currentRating,
                        onSelect: function (value, text) {
                            if (!value) {
                                $('.example-fontawesome-o')
                                    .barrating('clear');
                            } else {
                                $('.stars-example-fontawesome-o .current-rating')
                                    .addClass('hidden');

                                $('.stars-example-fontawesome-o .your-rating')
                                    .removeClass('hidden')
                                    .find('span')
                                    .html(value);
                            }
                        },
                        onClear: function (value, text) {
                            $('.stars-example-fontawesome-o')
                                .find('.current-rating')
                                .removeClass('hidden')
                                .end()
                                .find('.your-rating')
                                .addClass('hidden');
                        }
                    });
                }

                function ratingDisable() {
                    $('select').barrating('destroy');
                }

                $('.rating-enable').click(function (event) {
                    event.preventDefault();
                    ratingEnable();
                    $(this).addClass('deactivated');
                    $('.rating-disable').removeClass('deactivated');
                });
                $('.rating-disable').click(function (event) {
                    event.preventDefault();
                    ratingDisable();
                    $(this).addClass('deactivated');
                    $('.rating-enable').removeClass('deactivated');
                });
                ratingEnable();
            });
            newForm('report_form', 'report_modal');
            newForm('rate_form', 'rate_modal');
            newForm('confirm_form', 'confirm_modal');

            function newForm(id, modal_id) {
                jQuery("#" + id).validate({
                    success: function (element) {
                        $(element).each(function () {
                            $(this).siblings('input').removeClass('has-error').removeClass('error').addClass('has-success');
                            $(this).siblings('textarea').removeClass('has-error').removeClass('error').addClass('has-success');
                            $(this).siblings('button').removeClass('has-error').removeClass('error').addClass('has-success');
                            $(this).remove();
                        });
                    },
                    errorPlacement: function (error, element) {
                        var elem = $(element);
                        if (elem.hasClass("selectpicker")) {
                            error.appendTo(element.parent());
                            element.siblings('.dropdown-toggle').removeClass('has-success').addClass('has-error');
                        } else {
                            error.insertAfter(element);
                            element.removeClass('has-success').addClass('has-error');
                        }
                        error.css('color', 'red');
                    },
                    submitHandler: function (f, e) {
                        e.preventDefault();
                        $('.upload-spinn').removeClass('hidden');
                        var formData = new FormData($('#' + id)[0]);
                        var url = $('#' + id).attr('action');
                        var to = $('#' + id).attr('to');
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,
                            type: 'json',
                            success: function (response) {
                                $('.upload-spinn').addClass('hidden');
                                if (response.status) {
                                    customSweetAlert(
                                        'success',
                                        response.message,
                                        '',
                                        function (event) {
                                            $('#' + modal_id).modal('hide');
                                            $('.modal-backdrop').hide();
                                            $('#items').html(response.item);
                                        }
                                    );
                                } else {
                                    customSweetAlert(
                                        'error',
                                        response.message,
                                        response.errors_object
                                    );
                                }
                            },
                            error: function (jqXhr) {
                                $('.upload-spinn').addClass('hidden');
                                getErrors(jqXhr, '/login');
                            }
                        });
                    }
                });
            }

        </script>

    @endpush
@stop