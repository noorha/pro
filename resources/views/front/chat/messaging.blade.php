@extends('front.layout.index',['sub_title' => __('عرض المحادثة') ])
@section('main')
    @push('front_css')
        <style>
             ::-webkit-scrollbar {
                width: 4px;
            }
        </style>
    @endpush
        <div class="main_chat_messgae">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="chat_right_content">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">الكل</a></li>
                                <li><a data-toggle="tab" href="#menu1">المقروءة</a></li>
                                <li><a data-toggle="tab" href="#menu2">الغير مقروءة </a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <ul class="chat_list_name">
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                    </ul>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                <ul class="chat_list_name">
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                    </ul>
                                </div>
                                <div id="menu2" class="tab-pane fade">
                                <ul class="chat_list_name">
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center active">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="active_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                        <li class="d-flex align-items-center">
                                            <a href="">
                                                <div class="table-cell vertical-middle position-relative">
                                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                                    <div class="inactive_person"></div>
                                                </div>
                                                <div class="table-cell vertical-middle pr-10 ">
                                                    <p class="chat_name">رعد الحلبي</p>
                                                </div>
                                            </a>
                                            <p class="date">من دقيقتين</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="chat_message_content">
                            <div class="header_chat_message_content">
                                <div class="table-cell vertical-middle position-relative">
                                    <img src="/front/images/Layer 12.png" alt="" class="chat_avatar">
                                    <div class="active_person"></div>
                                </div>
                                <div class="table-cell vertical-middle pr-10 ">
                                    <p class="chat_name">رعد الحلبي</p>
                                </div>
                            </div>
                            <div class="body_chat_message_content">
                                <ul class="list_chat_message_content">
                                    <li>
                                        <div class="replay d-flex align-items-center ">
                                            <div class="text col-lg-7">
                                                <p class="info">من هناك حقيقة مثبتة منذ زمن طويل لأنها تعطي توزيعاَ </p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="send d-flex align-items-center ">
                                            <img src="/front/images/Layer 12.png" alt="" class="person mb-30">
                                            <div class="text col-lg-7">
                                                <p class="info">من هناك حقيقة مثبتة منذ زمن طويل لأنها تعطي توزيعاَ.</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="replay d-flex align-items-center ">
                                            <div class="text col-lg-7">
                                                <p class="info">من هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="send d-flex align-items-center ">
                                            <img src="/front/images/Layer 12.png" alt="" class="person mb-30">
                                            <div class="text col-lg-7">
                                                <p class="info">شكراً لك</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="replay d-flex align-items-center ">
                                            <div class="text col-lg-7">
                                                <p class="info">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص هذا النص لا يعني شيء لك</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="send d-flex align-items-center ">
                                            <img src="/front/images/Layer 12.png" alt="" class="person mb-30">
                                            <div class="text col-lg-7">
                                                <p class="info">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص هذا النص لا يعني شيء لك</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="replay d-flex align-items-center ">
                                            <div class="text col-lg-7">
                                                <p class="info">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص هذا النص لا يعني شيء لك</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="send d-flex align-items-center ">
                                            <img src="/front/images/Layer 12.png" alt="" class="person mb-30">
                                            <div class="text col-lg-7">
                                                <p class="info">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص هذا النص لا يعني شيء لك</p>
                                                <p class="date">01/2/2017</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer_chat_message_content">
                                <form action="">
                                    <div class="form-group">
                                        <input type="text" placeholder="اضف نص الرسالة">
                                        <button type="submit"><img src="/front/images/send.png" alt=""></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @push('front_js')

    @endpush
@stop