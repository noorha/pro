@extends('front.layout.index',['sub_title' => __('عرض الدورات')])
@section('main')
    @push('front_css')

    @endpush

    @php
        $course = $item;
    @endphp
    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="viewCour">
                        <div class="singelsw">
                            <div class="img_scontent">
                                <div class="uptop ">
                                    <a href="#">{{$item->categoryName()}}</a>
                                </div>
                                <div class="uptop date-cors">
                                    <a href="#">{{__('المركز').' '.$item->center_name}}</a>
                                </div>
                                <div class="uptop date-cors-sec" >
                                    <a href="#">{{__('تاريخ إنعقاد الدورة').' '.$item->start_date}}</a>
                                </div>

                                <img src="{{image_url($item->photo,'750x470')}}">
                            </div>
                            <div class="conent_text_sing">
                                <div class="name_sing_content">
                                    <h3>{{$item->title}}</h3>

                                    <a data-toggle="modal" data-target="#exampleModal2">@lang('طلب الإنضمام')</a>
                                </div>


                                <div class="text_text_singcont">
                                    <p>{!! $item->description !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--
                <div class="col-md-4">
                    @if(isset($item->sections) && $item->sections()->count() >0)
                        <div class="left_item_itsw">
                            <h3>@lang('أقسام الدورة')</h3>
                            <ul class="list_of_list_left">
                                @foreach($item->sections as $i=>$section)
                                    <li>
                                        <a>
                                            <i class="icon-file"><span class="path1"></span><span
                                                        class="path2"></span><span class="path3"></span><span
                                                        class="path4"></span><span class="path5"></span>
                                                <span class="path6"></span><span class="path7"></span>
                                            </i>
                                            <p>{{$section->text}}</p>
                                        </a>
                                    </li>
                                    @if(isset($section->json))
                                        @php
                                            $array = json_decode($section->json);
                                        @endphp
                                        <ul>
                                            @foreach($array as $item)
                                                <li>
                                                    <a>
                                                        <p></p>
                                                        <span>{{$item}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
-->
            </div>
        </div>
    </section>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                            class="icon-cross-symbol-1"></i></button>
                <div class="div_mosw">
                    <h3>@lang('طلب الإنضمام للدورة')</h3>
                    {!! Form::open(['id'=>'form','method'=>'post','url'=>lang_route('course.subscribe',['id'=>$course->id])]) !!}
                    <div class="inputsw2w user">
                        <input type="text" name="name" placeholder="@lang('الإسم')" required>
                    </div>
                    <div class="inputsw2w mobile">
                        <input type="text" name="mobile" placeholder="@lang('رقم الجوال')" required>
                    </div>
                    <div class="inputsw2w email">
                        <input type="email" name="email" placeholder="@lang('البريد الإلكتروني')" required>
                    </div>
                    <div class="inputsw2w plac">
                        <input type="text" name="address" placeholder="@lang('العنوان الحالي')" required>
                    </div>
                    <input type="hidden" name="course_id" value="{{$course->id}}">
                    <div class="inputsw2w btsw">
                        <button>@lang('إرسال')
                            <i class="upload-spinn fa fa-cog fa-spin fa-1x fa-fw hidden"></i>
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @push('front_js')
        {!! HTML::script('/front/js/form.js') !!}

        <script>
            function afterSuccess() {
                $('#exampleModal2').modal('hide');
                $('#form').find("input[type=text], textarea").val("");
            }
        </script>
    @endpush

@stop