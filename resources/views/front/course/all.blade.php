@extends('front.layout.index',['sub_title' => 'عرض الدورات'])
@section('main')
    @push('front_css')
    @endpush

    <section class="se20">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list_ofs4 cusrsw">
                        <ul>
                            <li><a data-id="all" class="category active"><i class="icon-menu-grid"></i>@lang('الكل')</a></li>
                            @php
                                $categories = get_all_categories();
                            @endphp
                            @if(isset($categories) && $categories->count()>0)
                                @foreach($categories as $category)
                                    <li>
                                        <a class="category " data-id="{{$category->id}}">
                                            <img src="{{image_url($category->icon,'22x22')}}">
                                            {{--<i class=" {{$category->icon_class}}" </i>>--}}
                                           {{get_text_locale($category,'text')}}
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="list-pricesw">
                        <h3><i class="icon-tag"></i>@lang('السعر')</h3>
                        <div class="radiosw">
                            <div class="chexbox">
                                <input type="radio" name="price" value="" id="price1">
                                <label for="price1">@lang('الكل')</label>
                            </div>
                            <div class="chexbox">
                                <input type="radio" name="price" value="pay"  id="price2">
                                <label for="price2">@lang('مدفوع')</label>
                            </div>
                            <div class="chexbox">
                                <input type="radio" name="price" value="free"  id="price3">
                                <label for="price3">@lang('مجاني')</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="viewCour">
                        <div id="items">
                            @include(view_front().'course.paginate',$items)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @push('front_js')
        <script>
            var category = '';
            var price = '';
            $('.category').on('click', function (event) {
                event.preventDefault();
                $('.category').each(function(i, obj) {
                    $(obj).removeClass('active');
                });
                $(this).addClass('active');
                category = $(this).data('id');
                updateItems();
            });
            onChangeSelected('.chexbox');
            function onChangeSelected(selector) {
                $(selector).on('change', function (e) {
                    if (selector === '.chexbox') {
                        price = $(this).children('input').val();
                    }
                    updateItems();
                });
            }
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                updateItems(page);
            });
            function updateItems(page) {
                $('#loader').removeClass('hidden');
                $('#container').addClass('hidden');
                var url = searchURL(page);
                $.ajax({
                    type: 'GET',
                    url: url
                }).done(function (response) {
                    $('#container').removeClass('hidden');
                    $('#loader').addClass('hidden');
                    $('#items').html(response.item);
                });
            }

            function searchURL(page) {
                var page_number = page || 1;
                return document.URL + '?page=' + page_number + '&category=' + category +  '&price=' + price ;
            }

        </script>
    @endpush
@stop