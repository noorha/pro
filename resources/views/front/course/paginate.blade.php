<div id="loader" style="padding: 20% 47%;" class="hidden">
    <i style="color: #a6437e" class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
</div>

<div id="container" class="row">
    @if(isset($items) && $items->count()>0)
        @foreach($items as $item)
            <div class="col-md-4 col-sm-6">
                <div class="first_sec3_list">
                    <div class="uptop-cous-all">
                        <a href="{{lang_route('course.view',['id'=> $item->id])}}">{{is_free($item->price)}}</a>
                    </div>
                    <div class="img_se3s">
                        <a href="{{lang_route('course.view',['id'=> $item->id])}}"><img src="{{image_url($item->photo,'262x165')}}"> </a>
                    </div>
                    <div class="text_sec3s">
                        <div class="main_text_sec3s">
                            <a href="{{lang_route('course.view',['id'=> $item->id])}}"><h3>{{$item->title}}</h3> </a>
                        </div>
                        <p>
                            {{string_limit($item->description,90)}}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="pagsw">
            <nav aria-label="Page navigation example">
                {{$items->links()}}
            </nav>
        </div>
    @else
        <h4 class="text-center loader-txt"> @lang('لا يوجد دورات') </h4>
    @endif
</div>


