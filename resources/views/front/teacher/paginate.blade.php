<div id="loader" style="padding: 40% 47%;" class="hidden">
    <i style="color: #a6437e" class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
</div>

<div class="cont-oi">
    @if(isset($teachers) && $teachers->count()>0)
        @foreach($teachers as $teacher)
            <div class="item_Search min-c">
                <div class="min-content-left">
                    <div class="img_item_Search">
                        <a href="{{lang_route('profile',[$teacher->id])}}">
                            <img src="{{image_url($teacher->photo,'90x90')}}">
                        </a>
                    </div>
                    <div class="text-content-left">
                        <div class="main-title clearfix">
                            <a href="{{lang_route('profile',[$teacher->id])}}">
                                <h3>{{$teacher->getUserName()}}</h3>
                            </a>
                            <h4 class="teacher-loc">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                {{ $teacher->getCountryName().' - '.$teacher->getCityName() }}
                            </h4>
                            @include(view_front().'layout.rating',$teacher)
                        </div>
                        <div class="main-info">
                            @php
                                $teacher_specialties = $teacher->specialties->unique();
                            @endphp
                            <h4>
                                @foreach($teacher_specialties as $i=>$item)
                                    @if($i!=0) | @endif
                                    {{get_text_locale($item,'name')}}
                                @endforeach
                            </h4>
                            <p>{{limit_text($teacher->description,40)}}</p>
                        </div>
                    </div>
                </div>
                <div class="text_item_Searchs">
                    @if($teacher->isWorkInterview())
                        <h4>
                            <span class="new-tit-price">@lang('مقابلة')</span>
                            <span>{{get_currency_value($teacher->interviewPrice())}}</span> {{get_currency_text() .' / '.__('ساعة')}}
                        </h4>
                    @endif
                    @if($teacher->isWorkOnline())
                        <h4>
                            <span class="new-tit-price">@lang('أونلاين')</span>
                            <span>{{get_currency_value($teacher->onlinePrice())}}</span> {{get_currency_text().' / '.__('ساعة')}}
                        </h4>
                    @endif

                <!--                                <span><i class="icon-clock-circular-outline"></i> 125 ساعة تعليمية</span>-->
                    @php
                        $isStudent = auth()->check() ?  auth()->user()->isStudent() : true;
                    @endphp

                    @if($isStudent)
                        <a href="#" class="contact-modal" data-id="{{ $teacher->id }}">@lang('تواصل مع المدرب')</a>
                        <a href="{{lang_route('student.request.send',[$teacher->id])}}" class="btn_search2">@lang('إختيار المدرب')</a>
                    @endif
                </div>
            </div>
        @endforeach
        <div class="pagsw">
            <nav aria-label="Page navigation example">
                {{$teachers->links()}}
            </nav>
        </div>
    @else
        <h4 class="text-center loader-txt" style="margin-top: 25%;"> @lang('لا يوجد مدربين') </h4>
    @endif
</div>

<div class="modal fade" id="sendMessageModal" tabindex="-1" role="dialog" aria-labelledby="sendMessageModal"
     aria-hidden="true">
    {!! Form::open(['id' => 'sendMessageModalForm', 'method'=>'POST', 'url'=> lang_route('profile.message.create')]) !!}
    <input type="hidden" name="contact_id" value="0" id="contact_id_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="closes" data-dismiss="modal" aria-label="Close"><i
                        class="icon-cross-symbol-1"></i></button>
            <div class="div_mosw">
<!--                <h3>@lang('اكتب رسالتك')</h3>-->
                <div class="inputsw2w martops">
                    <textarea rows="8" name="message" id="messageModal" placeholder="@lang('أكتب نص الرسالة')" required></textarea>
                </div>
                <div class="inputsw2w btsw">
                    <button type="submit">@lang('إرسال')</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>


@push('front_js')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(document).on('click', '.contact-modal', function (e) {
                e.preventDefault();
                $('#contact_id_modal').val($(this).data('id'));
                $('#sendMessageModal').modal('show');
                // console.log($(this).data('id'));
            });
            $('#sendMessageModalForm').submit(function (s) {
                s.preventDefault();

                var form = $(this);
                var formData = new FormData(this);
                var subbtn = form.find(":submit");
                subbtn.prop('disabled', true);
                $.ajax({
                    type: 'POST',
                    url: form.prop('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data){
                        if (data.status) {
                            $('#sendMessageModal').modal('hide');
                            $('#messageModal').val('');
                            window.location.href = '{{ lang_route('profile.messages') }}';
                        }
                        subbtn.prop('disabled', false);
                    }
                }).fail(function (jqXhr) {
                    subbtn.prop('disabled', false);
                }).always(function () {
                    subbtn.prop('disabled', false);
                });
            });
        });
    </script>
@endpush