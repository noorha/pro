@extends('front.layout.index',['sub_title' => __('تقييمات المدرب : ').$teacher->getUserName()])
@section('main')
    @push('front_css')



    @endpush

    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <h3>@lang('تقييمات المدرب')</h3>
                <div class="col-md-4 col-md-offset-4">
                    {{--<p>المزيد من الخبراء المؤهلين والمهنيين من أي مكان آخر، وعلى استعداد للمساعدة.</p>--}}
                </div>
                @if(isset($ratings) && $ratings->count() >0 )
                    <div class="myrateall">
                        <div class="row">
                            @foreach($ratings as $rating)
                                <div class="col-md-12">
                                    <div class="fmytate">
                                            <div class="img_fmyrate">
                                                <img src="{{image_url($rating->getStudentPhoto(),'100x100')}}">
                                            </div>
                                            <div class="text_fmyrate">
                                                <h3>{{$rating->getStudentName()}}</h3>
                                                <ul>
                                                    @for($i = 1 ; $i <= 5 ; $i++)
                                                        <li class="@if((int)$rating->value >= $i) active @endif">
                                                            <i class="icon-star3"></i></li>
                                                    @endfor
                                                    <li><span>{{$rating->value}}</span></li>
                                                </ul>
                                                <p>{{$rating->text}}</p>
                                            </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="pagsw">
                                <nav aria-label="Page navigation example">
                                    {{$ratings->links()}}
                                </nav>
                            </div>
                        </div>
                    </div>
                @else
                    <p> @lang('لا يوجد تقييمات لهذا المدرب') </p>
                @endif
            </div>
        </div>
    </section>


    @push('front_js')

    @endpush
@stop