@extends('front.layout.index',['sub_title' => __('عرض طلبات التدريب')])
@section('main')
    @push('front_css')

    @endpush

    <section class="se0">
        <div class="container">
            <div class="logdata colsw notall">
                <div class="center_ourContact">
                    <h3>@lang('طلبات التدريب')</h3>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="inputsws bgw pres seeclt">
                        <select id="status" data-placement="@lang('بحث حسب الحالة')" class="selectpicker">
                            <option value="">@lang('الكل')</option>
                            <option value="pending_teacher">@lang('بإنتظار موافقة المدرس')</option>
                            <option value="pending_student">@lang('بإنتظار تأكيد الطالب')</option>
                            <option value="progress">@lang('قيد التنفيذ')</option>
                            <option value="completed">@lang('مكتملة')</option>
                            <option value="rejected">@lang('مرفوضة')</option>
                            <option value="canceled">@lang('ملغية')</option>
                        </select>
                    </div>
                </div>
                <div class="lik_tabo">
                    <div id="items">
                        @include(view_front_teacher().'request.paginate' , $requests)
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include(view_front_modal().'reject-training-request')
    @include(view_front_modal().'accept-training-request')
    @include(view_front_modal().'confirm-teacher',['type'=>(auth()->user()->isStudent()) ? 'student' :'teacher','id'=>0])

    @push('front_js')
        {!! HTML::script('/front/js/form.js') !!}
        {!! HTML::script('/front/js/view-training-teachers.js') !!}
    @endpush
@stop