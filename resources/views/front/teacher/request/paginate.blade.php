<div id="loader" style="padding:10% 50%;" class="hidden">
    <i style="color: #a6437e" class="fa fa-spinner fa-spin fa-2x fa-fw"></i>
</div>

@php
    $days = get_all_days();
@endphp
<div id="container" class="all_fstnotif">
    @if(isset($requests) && $requests->count()>0)
        <div class="head_tabo">
            <h3>@lang('عرض طلبات التدريب')</h3>
        </div>
        @foreach($requests as $request)
            <div class="stnotif">

                <div class="new-box-v">
                    <div class="img_stnotif">
                        <img src="{{image_url($request->getStudentPhoto(),'90x90')}}">
                    </div>
                    <div class="text_stnotif">
                        <h3>{{$request->getStudentName()}}
                            <span>
                               <i class="icon-clock-circular-outline"></i>
                                {{get_date_from_timestamp($request->created_at)}}
                            </span>
                        </h3>
                    </div>
                    <div class="op_item_swrequestss-new">
                        <span class="train-rqs-txt">{{get_request_status($request)}}</span>
                    </div>
                    <div class="new-yi">
                        @switch($request->status)
                            @case('pending_teacher')
                            <ul class="new-btns-top" data-id="{{$request->id}}">
                                <li><a class="accept_modal_btn"
                                       data-id="{{$request->total}}">@lang('قبول')</a></li>
                                <li><a class="reject_modal_btn">@lang('رفض')</a></li>
                            </ul>
                            @break

                            @case('progress')
                            <ul class="new-btns-top nm-u" data-id="{{$request->id}}">
                                @if(!$request->isConfirmByTeacher())
                                    @php
                                        $type = (auth()->user()->isStudent()) ? 'student' :'teacher'
                                    @endphp

                                    <li>
                                        <a class="confirm-btn-pg"
                                           data-url="{{lang_route('confirm.request.'.$type,[$request->id])}}"
                                           data-toggle="modal" data-target="#confirm_modal"><i
                                                    class="icon-checked"></i>@lang('تسليم الطلب')</a>
                                    </li>
                                @endif
                                <li>
                                    <a href="{{lang_route('teacher.chat.request',[$request->id])}}">@lang('محادثة الطلب')</a>
                                </li>
                            </ul>
                            @break

                            @case('completed')
                            <ul class="new-btns-top mt-y" data-id="{{$request->id}}">
                                <li>
                                    <a href="{{lang_route('teacher.chat.request',[$request->id])}}">@lang('محادثة الطلب')</a>
                                </li>
                            </ul>
                            @break
                        @endswitch
                    </div>
                </div>


                <div class="ite_skdw">
                    <div class="mor_ourcont">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="right_isow">
                                    {{--<h3>@lang('التخصص الأساسي') <p>{{$request->getSubSpecialtyName()}}</p></h3>--}}
                                    <h3>@lang('عدد الساعات المطلوبة')<p>{{$request->hour_no}}</p></h3>
                                    <h3>@lang('السعر الإجمالي')
                                        <p>{{get_currency_value($request->total).' '.get_currency_text()}}</p></h3>
                                    <h3>@lang('نوع التدريب') <p>{{$request->getType()}}</p></h3>
                                    {{--<h3>@lang('المؤهل العلمي') <p> {{$request->getQualificationName()}}</p></h3>--}}
                                    <h3>@lang('طريقة الدفع') <p> {{$request->getPaymentMethodText()}}</p></h3>
                                    @if(isset($request->note) && !empty($request->note))
                                        <h3>@lang('الملاحظات')</h3>
                                        <p>{{$request->note}}</p>
                                    @endif
                                    @if(isset($request->files) && $request->files()->count() >0)
                                        <h3>@lang('المرفقات')</h3>
                                        @foreach($request->files as $file)
                                            <a target="_blank"
                                               href="{{file_url($file->file_name)}}">{{$file->display_name}}</a>
                                        @endforeach
                                    @endif

                                    @if($request->hasCommunication())
                                        <div class="contacts-available">
                                            <div class="itmasksw_rest contact-div">
                                                <img class="img-contact"
                                                     src="{{url('/front/img/'.$request->communication->key.'.svg')}}">
                                                {{$request->communication->name . ' : '. $request->communicationLink()}}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5 col-md-offset-3">
                                <div class="tabso_right_isow">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td></td>
                                            <td>@lang('تحديد')</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($days as $day)
                                            <tr>
                                                <td>{{get_text_locale($day,'name')}}</td>
                                                @if($request->hasDay($day->id))
                                                    <td><i class="icon-checked"></i></td>
                                                @else
                                                    <td>--</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--@switch($request->status)--}}
                    {{--@case('pending_teacher')--}}
                    {{--<div class="mor_ourcont">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-4">--}}
                    {{--<div class="right_isow vufll">--}}
                    {{--<h3>@lang('هل تود التواصل مع هذا الطالب ؟')</h3>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-5 col-md-offset-3">--}}
                    {{--<div class="ulasda_jtesw">--}}
                    {{--<ul data-id="{{$request->id}}">--}}
                    {{--<li><a class="accept_modal_btn">@lang('قبول')</a></li>--}}
                    {{--<li><a class="reject_modal_btn">@lang('رفض')</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@break--}}


                    {{--@case('progress')--}}
                    {{--<div class="mor_ourcont">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="ulasda_jtesw">--}}
                    {{--<ul>--}}
                    {{--<li><a href="{{lang_route('teacher.chat.request',[$request->id])}}">@lang('محادثة الطلب')</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@break--}}


                    {{--@case('completed')--}}
                    {{--<div class="mor_ourcont">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<div class="ulasda_jtesw">--}}
                    {{--<ul>--}}
                    {{--<li><a href="{{lang_route('teacher.chat.request',[$request->id])}}">@lang('محادثة الطلب')</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@break--}}
                    {{--@endswitch--}}

                </div>
            </div>
        @endforeach
        <div class="pagsw">
            <nav aria-label="Page navigation example">
                {{$requests->links()}}
            </nav>
        </div>
    @else
        <h4 class="text-center loader-txt" style="margin-top: 10%;"> @lang('لا يوجد طلبات تدريب') </h4>
    @endif

</div>
