<?php
return [
    'success' => 'تمت العملية بنجاح',
    'value_not_found' => 'الرجاء التأكد من إدخال القيمة',
    'invalid_old_password' => 'كلمة المرور القديمة غير صحيحة',
    'inactive_account' => 'الحساب الخاص بك غير فعال',
    'suspended_account' => 'الحساب الخاص بك موقوف حالياً',
    'invalid_auth' => 'خطأ في البريد الإلكتروني أو كلمة المرور',
    'register_success' => 'تمت عملية التسجيل بنجاح',
    'user_not_found' => 'المستخدم غير موجود',
    'password_success' => 'تمت عملية تغيير كلمة المرور بنجاح',
    'invalid_code' => 'الكود المدخل غير صحيح',
    'server_error' => 'حدثت مشكلة أثناء المعالجة',
    'already_mobile_active' => 'تم تفعيل رقم الجوال مسبقاً',
    'activation_success' => 'تمت عملية التفعيل بنجاح',
    'message_sent_successfully' => 'تم إرسال رسالتك بنجاح',


];