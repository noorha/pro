<?php
return [
    'success' => 'Process Completed Successfully',
    'invalid_old_password' => 'Incorrect old password',
    'inactive_account' => 'your account is inactive',
    'suspended_account' => 'your account is suspended',
    'invalid_auth' => 'incorrect email or password',
    'register_success' => 'Registration completed successfully',
    'user_not_found' => 'User Not Found',
    'password_success' => 'Password changed successfully',
    'invalid_code' => 'Incorrect Code',
    'server_error' => 'Internal Server Error',
    'already_mobile_active' => 'Mobile No. is already active',
    'activation_success' => 'Activation completed successfully',
    'message_sent_successfully' => 'Message sent successfully',
    'value_not_found' => 'value is required',

];